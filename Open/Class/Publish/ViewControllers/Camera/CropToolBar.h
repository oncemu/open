//
//  CropToolBar.h
//  Open
//
//  Created by mfp on 17/8/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CropRadio) {
    CropImageAspectRatio9X16 = 0,
    CropImageAspectRatio3x4,
    CropImageAspectRatio1x1
};

@interface CropToolBar : UIView

@property(nonatomic, assign)CGSize radioSize;
@property(nonatomic, assign)NSInteger countDown;
@end
