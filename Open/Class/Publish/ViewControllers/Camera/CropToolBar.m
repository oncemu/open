//
//  CropToolBar.m
//  Open
//
//  Created by mfp on 17/8/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CropToolBar.h"

@interface CropToolBar ()

@property(nonatomic, strong) UIButton *ratioBtn1;// 9:16
@property(nonatomic, strong) UIButton *ratioBtn2;// 3:4
@property(nonatomic, strong) UIButton *ratioBtn3;// 1:1
@property(nonatomic, strong) UIView   *minLine;
@property(nonatomic, strong) UIButton *closeBtn; //关闭倒计时
@property(nonatomic, strong) UIButton *countDownBtn1; //3秒
@property(nonatomic, strong) UIButton *countDownBtn2; //6秒
@property(nonatomic, strong) UIButton *countDownBtn3; //10秒
@property(nonatomic, strong) NSArray  *titles;
@end

@implementation CropToolBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithHex:0x000000 alpha:0.4];
        self.layer.cornerRadius = 5.0f;
        self.layer.masksToBounds = YES;
        
        self.minLine = [[UIView alloc] initWithFrame:CM((SCREEN_WIDTH-38)/2.0f, 10, 0.5f, 78 - 20)];
        _minLine.backgroundColor = WhiteColor;
        [self addSubview:_minLine];
        self.radioSize = Size(1.0f, 1.0f);
        _titles = @[@"9:16",@"3:4",@"1:1",@"关闭",@"3秒",@"6秒",@"10秒"];
        //ColorWithHex(0xffd900)
        for (NSInteger i = 0; i<_titles.count; i++) {
            UIButton *btn = [ViewFactory getButton:CM(0, 0, 0, 0) font:Font(9) selectTitle:_titles[i] norTitle:_titles[i] sColor:RedColor norColor:WhiteColor target:self action:@selector(buttonClicked:)];
            if (i < 3) {
                [btn setBackgroundImage:getImage(@"publish_radio") forState:UIControlStateNormal];
                btn.frame = CM(i*(20+34)+19, 28, 20, 26);
            }else{
                btn.frame = CM((i-3)*((SCREEN_WIDTH-38)/8)+(SCREEN_WIDTH-38)/2, 0,(SCREEN_WIDTH-38)/8, 78);
            }
            if (i == 0 || i == 3) {
                btn.selected = YES;
            }
            btn.tag = i;
            [self addSubview:btn];
        }
    }
    return self;
}

- (void)buttonClicked:(UIButton *)button {
    button.selected = YES;
    for (UIButton *btn in self.subviews) {
        if ([btn isKindOfClass:[UIButton class]]) {
            if (button.tag<3 && btn.tag<3) {
                if (![btn isEqual:button]) {
                    btn.selected = NO;
                }else{
                    switch (btn.tag) {
                        case 0:
                            self.radioSize = Size(9.0f, 16.0f);
                            break;
                        case 1:
                            self.radioSize = Size(3.0f, 4.0f);
                            break;
                        case 2:
                            self.radioSize = Size(1.0f, 1.0f);
                            break;
                        default:
                            break;
                    }
                }
            }else if(btn.tag>=3 && button.tag>=3){
                if (![btn isEqual:button]) {
                    btn.selected = NO;
                }else{
                    NSString *title = [button.currentTitle substringWithRange:NSMakeRange(0, button.currentTitle.length-1)];
                    self.countDown = title.integerValue;
                }
            }
        }
    }
    
}

@end
