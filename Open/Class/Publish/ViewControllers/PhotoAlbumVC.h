//
//  PhotoAlbumVC.h
//  Open
//
//  Created by mfp on 17/8/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//  相册选图

#import "CollectionBaseVC.h"

@protocol PhotoAlubumVCDelegate <NSObject>

@optional
- (void)selectedPhotoImage:(UIImage *)image;

@end

@interface PhotoAlbumVC : CollectionBaseVC
@property(nonatomic, assign) NSInteger imageCount;//已选的数量
@property(nonatomic, assign) NSInteger maxCount;//已选的数量
@property(nonatomic, strong) void(^addPhotoBlock)(NSArray *images);//如果是发布时候再添加新图，则实现此block
@property(nonatomic, assign) PublishType type;
@property(nonatomic, weak) id<PhotoAlubumVCDelegate>delegate;

@end
