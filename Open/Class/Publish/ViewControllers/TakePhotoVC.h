//
//  TakePhotoVC.h
//  Open
//
//  Created by mfp on 17/8/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//  拍照

#import "BaseVC.h"

@interface TakePhotoVC : BaseVC

//如果是发布时候再添加新图，则实现此block
@property(nonatomic, strong) void(^addPhotoBlock)(UIImage *image);
@property(nonatomic, copy) void(^headerPhotoBlock)(UIImage *image);
@property(nonatomic, assign) PublishType type;

@end
