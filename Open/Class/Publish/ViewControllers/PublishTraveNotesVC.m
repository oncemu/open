//
//  PublishTraveNotesVC.m
//  Open
//
//  Created by mfp on 17/8/21.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PublishTraveNotesVC.h"
#import "PublishPictureView.h"
#import "SZTextView.h"
#import "PublishNetService.h"
#import "ImagePickView.h"
#import "UserLocalVC.h"

@interface PublishTraveNotesVC ()<UIScrollViewDelegate>
@property(nonatomic, strong) UIScrollView *contentView;
@property(nonatomic, strong) SZTextView *inputView;
@property(nonatomic, strong) ImagePickView *imagePickView;
@property(nonatomic, strong) PublishBar *bottomBar;
@property(nonatomic, strong) PublishNetService *publishService;
@property(nonatomic, strong) LocationInfo *localInfo;
@end

@implementation PublishTraveNotesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"发布游记";
    [self setupUI];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.contentView.contentSize = Size(0, CGRectGetMaxY(_inputView.frame));
}
- (void)setupUI {
    self.contentView = [[UIScrollView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 50)];
    self.contentView.backgroundColor = WhiteColor;
    _contentView.delegate = self;
    [self.view addSubview:self.contentView];
    
    self.inputView = [[SZTextView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, 150)];
    _inputView.placeholder = @"分享我的游记......";
    _inputView.textColor = ColorWithHex(0x333333);
    _inputView.font = Font(16);
    [self.contentView addSubview:_inputView];
    
    _imagePickView = [[ImagePickView alloc] initWithFrame:CM(19, CGRectGetMaxY(_inputView.frame), SCREEN_WIDTH - 38, (SCREEN_WIDTH - 38 - 4)/3) target:self];
    [_imagePickView.images addObjectsFromArray:self.photos];
    _imagePickView.selectBlock = ^(UIImage *img) {
        
    };
    [self.contentView addSubview:_imagePickView];
    
    self.bottomBar = [[PublishBar alloc] initWithFrame:CM(0, SCREEN_HEIGHT - 50, SCREEN_WIDTH, 50)];
    WS(weakSelf);
    _bottomBar.publisBlock = ^() {//发布
        [weakSelf checkLoginStatus];
    };
    _bottomBar.localBlock = ^() {//定位
        [weakSelf local];
    };
    [self.view addSubview:_bottomBar];
}

- (void)checkLoginStatus {
    if ([self checkLogin:YES]) {
        [self publish];
    }
}

- (void)loginSucceed {
    [self publish];
}

- (void)publish {
    [KVNProgress showProgress:0.0f status:@"正在发布..."];
    NSString *title = @"";
    NSString *description = self.inputView.text ? self.inputView.text : @"";
    NSString *country = self.localInfo.country ? self.localInfo.country : @"";
    NSString *city = self.localInfo.city ? self.localInfo.city : @"";
    NSString *detailedAddress = self.localInfo.name ? self.localInfo.name : @"";
    NSString *weather = self.localInfo.nowCond ? self.localInfo.nowCond : @"";
    CGFloat longitude = self.localInfo.location.coordinate.longitude;
    CGFloat latitude = self.localInfo.location.coordinate.latitude;
    WS(weakSelf);
    [self.publishService uploadImage:self.imagePickView.images params:@{@"category":@"JOURNEY",@"title":title,@"description":description,@"country":country,@"city":city,@"longitude":@(longitude),@"latitude":@(latitude),@"weather":weather,@"detailedAddress":detailedAddress} url:kPost progress:^(NSProgress *progress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *pro = [NSString stringWithFormat:@"%.1f",progress.fractionCompleted];
            [KVNProgress updateProgress:pro.floatValue animated:YES];
        });
    } success:^(NSString *requestInfo, id responseObject) {
        [KVNProgress showSuccessWithStatus:@"发布成功！"];
        [weakSelf goBack];
        [[NSNotificationCenter defaultCenter] postNotificationName:kPublishAlbumSuccess object:nil];
    } failure:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:responseObject[@"message"]];
    }];
}

- (void)local {
    UserLocalVC *vc = [[UserLocalVC alloc] initWithShowBackButton:YES];
    WS(weakSelf);
    vc.completeLocal = ^(LocationInfo *info) {
        weakSelf.localInfo = info;
        weakSelf.bottomBar.localName = info.city;
    };
    [self pushVC:vc animated:YES];
}

#pragma mark - scroll delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.inputView resignFirstResponder];
}

- (PublishNetService *)publishService {
    if (!_publishService) {
        _publishService = [[PublishNetService alloc] init];
    }
    return _publishService;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
