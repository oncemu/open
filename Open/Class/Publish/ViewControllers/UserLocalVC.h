//
//  UserLocalVC.h
//  Open
//
//  Created by mfp on 17/9/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//  选择位置

#import "TableBaseVC.h"
#import "LocationManager.h"


@interface UserLocalVC : TableBaseVC

@property(nonatomic, copy) void (^completeLocal)(LocationInfo *info);

@end
