//
//  PhotoAlbumVC.m
//  Open
//
//  Created by mfp on 17/8/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PhotoAlbumVC.h"
#import "AlbumEntity.h"
#import "PickImageViewModel.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "PickImageCell.h"
#import "SRAlertView.h"
#import "TakePhotoVC.h"
#import "PhotoAlbumView.h"
#import "PublishTraveNotesVC.h"
#import "PublishPictureVC.h"

@interface PhotoAlbumVC ()<CHTCollectionViewDelegateWaterfallLayout,SRAlertViewDelegate>
@property (nonatomic, assign) AlbumEntity         *albumEntity;
@property (nonatomic, strong) PickAlbumViewModel  *albumViewModel;
@property (nonatomic, strong) PickImageViewModel  *imageViewModel;
@property (nonatomic, strong) NSMutableArray        *selectImageList;
@property(nonatomic, strong) UIButton *nextBtn;

@end

static NSString *photocellid = @"photocellid";

@implementation PhotoAlbumVC

- (instancetype)initWithShowBackButton:(BOOL)isShowBackButton {
    self = [super initWithShowBackButton:isShowBackButton];
    if (self) {
        self.imageCount = 0;
        self.maxCount = 9;
    }
    return self;
}

- (void)setMode {
    self.selectImageList = [NSMutableArray array];
    self.albumViewModel = [[PickAlbumViewModel alloc] init];
    self.imageViewModel = [[PickImageViewModel alloc] init];
    self.imageViewModel.maxCount = self.maxCount - self.imageCount;
    WS(weakSelf);
    self.imageViewModel.exceedBoundsBlock = ^(){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"最多只能选%ld张图",(long)weakSelf.maxCount] message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
        [weakSelf presentViewController:alertController animated:YES completion:nil];
    };
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"相机胶卷";
    [self setMode];
    [self getAlbums];
    [self setUpCollectonView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goBack) name:kPublishAlbumSuccess object:nil];
}

- (void)setUpCollectonView {
    self.view.backgroundColor = WhiteColor;
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc]init];
    layout.sectionInset = UIEdgeInsetsZero;
    layout.columnCount = 3;
    layout.headerHeight = 0;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    
    self.collectionView.collectionViewLayout = layout;
    self.collectionView.backgroundColor = WhiteColor;
    self.collectionView.frameSizeHeight = SCREEN_HEIGHT - kTabBarHeight;
    [self.collectionView registerClass:[AlbumPhotoCell class] forCellWithReuseIdentifier:photocellid];
    
    _nextBtn = [ViewFactory getButton:CM(SCREEN_WIDTH - 90, SCREEN_HEIGHT - 26 - 12 , 60, 26) font:Font(15) selectTitle:nil norTitle:@"继续" sColor:ColorWithHex(0x000000) norColor:ColorWithHex(0x000000) target:self action:@selector(next)];
    [_nextBtn setBackgroundImage:getImage(@"public_lemonround") forState:UIControlStateNormal];
    [self.view addSubview:_nextBtn];
    _nextBtn.enabled = NO;
}

- (void)getAlbums {
    WS(weakSelf);
    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized) {
        self.albumEntity = [weakSelf.albumViewModel.albumArray firstObject];
    }else if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                weakSelf.albumEntity = [weakSelf.albumViewModel.albumArray firstObject];
            }else{

            }
        }];
    }else{
        [self showAccessDenied];
    }
}

#pragma mark - 提示允许访问相册功能
- (void)showAccessDenied {
    NSString *message = @"访问相册需要授权。请在系统-设置-相机中打开Open";
    SRAlertView *alert = [[SRAlertView alloc]initWithTitle:@"授权请求" message:message  leftActionTitle:@"取消" rightActionTitle:@"确定" animationStyle:AlertViewAnimationNone delegate:self];
    [alert show];
}

#pragma mark - SRAlertViewDelegate
- (void)alertViewDidSelectAction:(AlertViewActionType)actionType {
    
}

- (void)setNavBarbuttonState {
    BOOL buttonEnabled = (self.imageViewModel.selectMediaDataArray.count + self.imageCount)>0?YES:NO;
    self.nextBtn.enabled = buttonEnabled;
    if (buttonEnabled) {
        [self.nextBtn setTitle:[NSString stringWithFormat:@"继续 %ld",(long)self.imageViewModel.selectMediaDataArray.count+(long)self.imageCount] forState:UIControlStateNormal];
    }else{
        [self.nextBtn setTitle:@"继续" forState:UIControlStateNormal];
    }
}

- (void)getImageList {
    [self.selectImageList removeAllObjects];
    __block NSInteger count = 0;
    for (PHAsset *asset in self.imageViewModel.selectMediaDataArray) {
        @autoreleasepool {
            PHImageRequestOptions *opt = [[PHImageRequestOptions alloc]init];
            opt.synchronous = NO;
            opt.resizeMode = PHImageRequestOptionsResizeModeFast;
            opt.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
            
            PHImageManager *imageManager = [PHImageManager defaultManager];
            [imageManager requestImageForAsset:asset targetSize:CGSizeMake(MAXFLOAT, MAXFLOAT) contentMode:PHImageContentModeAspectFill options:opt resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                count ++;
                if (result) {
                    DLog(@"count = %ld",count);
                    [self.selectImageList addObject:result];
                }
            }];
        }
    }
    while (count<self.imageViewModel.selectMediaDataArray.count) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
}
- (void)next {
    [KVNProgress showProgress:0];
    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(globalQueue, ^{
        [self getImageList];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.addPhotoBlock) {
                self.addPhotoBlock(self.selectImageList);
                [self goBack];
            }else{
                switch (self.type) {
                    case PublishTypePicture:
                    {
                        PublishPictureVC *vc = [[PublishPictureVC alloc] initWithShowBackButton:YES];
                        vc.image = [self.selectImageList firstObject];
                        [self pushVC:vc animated:YES];
                    }
                        break;
                    case PublishTypeTravelNotes:
                    {
                        PublishTraveNotesVC *vc = [[PublishTraveNotesVC alloc] initWithShowBackButton:YES];
                        vc.photos = [NSMutableArray arrayWithArray:self.selectImageList];
                        [self pushVC:vc animated:YES];
                    }
                        break;
                    case PublishTypeNormal:
                    {
                        if (self.delegate && [self.delegate respondsToSelector:@selector(selectedPhotoImage:)]) {
                            [self.delegate selectedPhotoImage:[self.selectImageList firstObject]];
                        }
                        [self goBack];
                    }
                        break;
                    default:
                        break;
                }
            }
            [KVNProgress dismiss];
        });
    });
}

//打开相机
- (void)openCamera {
    TakePhotoVC *vc = [[TakePhotoVC alloc] initWithShowBackButton:YES];
    vc.type = self.type;
    CustomNavigationController *nav = [[CustomNavigationController alloc]initWithRootViewController:vc];
    if (self.addPhotoBlock) {
        WS(weakSelf);
        vc.addPhotoBlock = ^(UIImage *image) {
            if (self.type == PublishTypeNormal) {

            }else {
                [weakSelf.selectImageList addObject:image];
                weakSelf.addPhotoBlock(weakSelf.selectImageList);
                [weakSelf goBack];
            }
        };
    }
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    vc.headerPhotoBlock = ^(UIImage *image) {
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(selectedPhotoImage:)]) {
            [strongSelf.delegate selectedPhotoImage:image];
            [strongSelf goBack];
        }
    };
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - UICollectionViewDelegate && UICollectionViewDataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    [collectionView.collectionViewLayout invalidateLayout];
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.albumEntity.fetchResult.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AlbumPhotoCell *cell = (AlbumPhotoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:photocellid forIndexPath:indexPath];
    cell.imageViewModel = self.imageViewModel;
    cell.isCamera = (indexPath.item == 0);
    if (indexPath.item>0) {
        PHAsset *asset = self.albumEntity.fetchResult[indexPath.item - 1];
        cell.asset = asset;
        WS(weakSelf);
        cell.ClickSelectedButtonBlock = ^(PHAsset *asset){
            [weakSelf setNavBarbuttonState];
        };
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item == 0) { //拍照
        [self openCamera];
    }else {
        PickImageCell *cell = (PickImageCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cell.selectBtn.selected = !cell.selectBtn.selected;
    }
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return Size(SCREEN_WIDTH/3, SCREEN_WIDTH/3);
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *photo=nil;
    photo=[info objectForKey:UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
