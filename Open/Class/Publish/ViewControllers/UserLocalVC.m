//
//  UserLocalVC.m
//  Open
//
//  Created by mfp on 17/9/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserLocalVC.h"
#import <MapKit/MapKit.h>

static NSString *localcellid = @"localcellid";
static NSString *localheaderid = @"localheaderid";

@interface UserLocalVC ()<MKMapViewDelegate,UISearchBarDelegate>
{
    BOOL haveGetUserLocation;//是否获取到用户位置
    UIImageView *imgView;//大头钉
    BOOL spanBool;//是否是滑动
    BOOL pinchBool;//是否缩放
    CLLocation *selectLocation;//大头钉定位的location
    NSString *nowCond;//当前位置天气
}
@property(nonatomic, strong) MKMapView *mapView;
@property(nonatomic, strong) LocationManager *lcManager;
@property(nonatomic, strong) UISearchBar *searchBar;
@end

@implementation UserLocalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = @"我在这里";
    
    haveGetUserLocation = NO;
    [self.view addSubview:self.mapView];
    self.tableView.frame = CM(0, CGRectGetMaxY(self.mapView.frame), SCREEN_WIDTH, SCREEN_HEIGHT - CGRectGetMaxY(self.mapView.frame) - 64);
    self.tableView.rowHeight = 55;
    self.tableView.sectionHeaderHeight = 45;
    self.tableView.separatorColor = ColorWithHex(0xf2f2f2);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:localheaderid];
    [self.lcManager startLocation];
    WS(weakSelf);
    self.lcManager.localSuccessBlock = ^(LocationInfo *info) {
        [weakSelf parseWeather:info.city];
    };
}

- (void)parseLocalInfo:(CLLocation *)location searchKey:(NSString *)key {
    WS(weakSelf);
    [self.lcManager parseLocation:location searchKey:key success:^(NSArray *data) {
        [weakSelf.dataList removeAllObjects];
        [weakSelf.dataList addObjectsFromArray:data];
        [weakSelf.tableView reloadData];
    } failure:^(NSString *msg) {
        
    }];
}

#pragma mark - MapView Gesture
-(void)mapViewSpanGesture:(UIPanGestureRecognizer *)gesture {
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:{
        }
            break;
        case UIGestureRecognizerStateChanged:{
            spanBool = YES;
        }
            break;
        case UIGestureRecognizerStateCancelled:{
        }
            break;
        case UIGestureRecognizerStateEnded:{
        }
            break;
            
        default:
            break;
    }
}

-(void)mapViewPinchGesture:(UIGestureRecognizer*)gesture {
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:{
        }
            break;
        case UIGestureRecognizerStateChanged:{
            pinchBool = YES;
        }
            break;
        case UIGestureRecognizerStateCancelled:{
        }
            break;
        case UIGestureRecognizerStateEnded:{
            pinchBool = NO;
        }
            break;
            
        default:
            break;
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    spanBool = YES;
}

#pragma mark - MKMapViewDelegate

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    if (!haveGetUserLocation) {
        if (self.mapView.userLocationVisible) {
            haveGetUserLocation = YES;
            selectLocation = mapView.userLocation.location;
            [self parseLocalInfo:mapView.userLocation.location searchKey:nil];
            [self addCenterLocationViewWithCenterPoint:self.mapView.center];
        }
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    if (imgView && (spanBool||pinchBool)) {
        CGPoint mapCenter = self.mapView.center;
        CLLocationCoordinate2D coordinate = [self.mapView convertPoint:mapCenter toCoordinateFromView:self.mapView];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
        selectLocation = location;
        [self parseLocalInfo:location searchKey:nil];
        imgView.center = CGPointMake(mapCenter.x, mapCenter.y-15);
        [UIView animateWithDuration:0.2 animations:^{
            imgView.center = mapCenter;
        }completion:^(BOOL finished){
            if (finished) {
                [UIView animateWithDuration:0.05 animations:^{
                    imgView.transform = CGAffineTransformMakeScale(1.0, 0.8);
                }completion:^(BOOL finished){
                    if (finished) {
                        [UIView animateWithDuration:0.1 animations:^{
                            imgView.transform = CGAffineTransformIdentity;
                        }completion:^(BOOL finished){
                            if (finished) {
                                spanBool = NO;
                            }
                        }];
                    }
                }];
            }
        }];
    }
}

#pragma mark - table delegate datasource 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:localcellid];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:localcellid];
    }
    LocationInfo *info = self.dataList[indexPath.row];
    cell.textLabel.text = info.name;
    cell.detailTextLabel.text = info.thoroughfare;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewHeaderFooterView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:localheaderid];
    UISearchBar *bar = [view viewWithTag:100];
    if (!bar || [bar isKindOfClass:[UISearchBar class]]) {
        [view addSubview:self.searchBar];
    }
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.completeLocal) {
        LocationInfo *info = self.dataList[indexPath.row];
        info.nowCond = nowCond;
        self.completeLocal(info);
    }
    [self goBack];
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchAroundWithKeyword:[searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self searchAroundWithKeyword:[searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
}
#pragma mark - search method
- (void)searchAroundWithKeyword:(NSString *)keyword{
    [self parseLocalInfo:selectLocation searchKey:keyword];
}

- (LocationManager *)lcManager {
    if (!_lcManager) {
        _lcManager = [[LocationManager alloc] init];
    }
    return _lcManager;
}

- (MKMapView *)mapView {
    if (_mapView) {
        return _mapView;
    }
    _mapView = [[MKMapView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, 315)];
    _mapView.mapType = MKMapTypeStandard;
    //设置地图可缩放
    _mapView.zoomEnabled = YES;
    //设置地图可滚动
    _mapView.scrollEnabled = YES;
    //设置地图可旋转
    _mapView.rotateEnabled = YES;
    //设置显示用户显示位置
    _mapView.showsUserLocation = YES;
    _mapView.showsPointsOfInterest = YES;
    _mapView.userTrackingMode = MKUserTrackingModeFollowWithHeading;
    //为MKMapView设置delegate
    _mapView.delegate = self;
    
    for (UIView *view in self.mapView.subviews) {
        NSString *viewName = NSStringFromClass([view class]);
        if ([viewName isEqualToString:@"_MKMapContentView"]) {
            UIView *contentView = view;//[self.mapView valueForKey:@"_contentView"];
            for (UIGestureRecognizer *gestureRecognizer in contentView.gestureRecognizers) {
                if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
                    [gestureRecognizer addTarget:self action:@selector(mapViewSpanGesture:)];
                }
                if ([gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]) {
                    [gestureRecognizer addTarget:self action:@selector(mapViewPinchGesture:)];
                }
            }
            
        }
    }
    self.edgesForExtendedLayout = UIRectEdgeNone;
    return _mapView;
}

-(void)addCenterLocationViewWithCenterPoint:(CGPoint)point {
    if (!imgView) {
        imgView = [ViewFactory getImageView:CM(SCREEN_WIDTH/2, 0, 18, 38) image:getImage(@"public_hobnail")];
        imgView.center = point;
        imgView.center = self.mapView.center;
        [self.view addSubview:imgView];
    }
}


- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, 45)];
        _searchBar.backgroundImage = [[UIImage alloc] init];
        _searchBar.barTintColor = WhiteColor;
        _searchBar.delegate = self;
        _searchBar.placeholder = @"输入地点";
    }
    return _searchBar;
}

#pragma mark - 天气解析
- (void)parseWeather:(NSString *)cityName {
    NSString *charactersToEscape = @"?!@#$^&%*+,:;='\"`<>()[]{}/\\| ";
    NSCharacterSet *allowedCharacters = [NSCharacterSet characterSetWithCharactersInString:charactersToEscape];
    NSString *httpUrl = [NSString stringWithFormat:@"https://free-api.heweather.com/v5/weather?city=%@&key=7fcf60e8809f4c96a66ee7d1bb3c6fc0",[cityName stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpUrl]];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        NSArray *jsonArr = [jsonDict objectForKey:@"HeWeather5"];
        NSDictionary *jsonDictionary = [jsonArr objectAtIndex:0];
        nowCond = jsonDictionary[@"now"][@"cond"][@"txt"];
    }];
    [task resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
