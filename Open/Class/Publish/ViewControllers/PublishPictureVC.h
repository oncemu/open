//
//  PublishPictureVC.h
//  Open
//
//  Created by mfp on 17/8/21.
//  Copyright © 2017年 彭彬. All rights reserved.
//  发布美图

#import "BaseVC.h"

@interface PublishPictureVC : BaseVC
@property(nonatomic, strong) UIImage *image;
@end
