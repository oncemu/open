//
//  TakePhotoVC.m
//  Open
//
//  Created by mfp on 17/8/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "TakePhotoVC.h"
#import "LLSimpleCamera.h"
#import "CropToolBar.h"
#import "SnapTabView.h"
#import "PublishPictureVC.h"

@interface TakePhotoVC ()

@property(nonatomic, strong) LLSimpleCamera *camera;//相机
@property(nonatomic, strong) UIButton *overturnBtn;//翻转btn
@property(nonatomic, strong) UIButton *flashButton;//闪光灯
@property(nonatomic, strong) SnapBtnView *snapBtnView;//拍照设置按钮view
@property(nonatomic, strong) UIImage *image;//拍照图片
@property(nonatomic, strong) CropToolBar *toolbar;//设置view
@property(nonatomic, strong) UIImageView *cropView;//拍照后显示图片view


@end

@implementation TakePhotoVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [self.camera start];
    } else {
        [self goBack];
        return;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    [self InitializeCamera];
    [self setViewUI];
}

- (void)setViewUI {
    _overturnBtn = [ViewFactory getButton:CM(0, 0, 44, 44) selectImage:getImage(@"publish_turn") norImage:getImage(@"publish_turn") target:self action:@selector(TurnThelens:)];
    self.navigationItem.titleView = _overturnBtn;
    
    _flashButton = [ViewFactory getButton:CM(0, 0, 44, 44) selectImage:getImage(@"publish_flash_on") norImage:getImage(@"publish_flash_off") target:self action:@selector(openFlash:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_flashButton];
    
    self.snapBtnView = [[SnapBtnView alloc] initWithFrame:CM(0, SCREEN_HEIGHT - 110, SCREEN_WIDTH, 110)];
    WS(weakSelf);
    self.snapBtnView.SnapBlock = ^() {
        [weakSelf takePhoto];
    };
    self.snapBtnView.ReSnapBlock = ^() {
        [weakSelf.cropView removeFromSuperview];
        _cropView = nil;
        weakSelf.overturnBtn.hidden = NO;
        weakSelf.flashButton.hidden = NO;
    };
    self.snapBtnView.DoneBlock = ^() {
        NSData *data = UIImageJPEGRepresentation(weakSelf.image, 1);
        NSInteger length = data.length/1000;
        if (self.type == PublishTypeNormal) {
            if (weakSelf.headerPhotoBlock) {
                [weakSelf goBack];
                weakSelf.headerPhotoBlock(weakSelf.image);
            }
        }else {
            if (length<1000) {
                NSLog(@"图片小于1M");
            }else{
                if (weakSelf.addPhotoBlock) {
                    [weakSelf goBack];
                    weakSelf.addPhotoBlock(weakSelf.image);
                }else{
                    [weakSelf nextPublish];
                }
            }
        }
    };
    [self.view addSubview:self.snapBtnView];
}

- (void)setPhoto:(BOOL)selected {
    self.toolbar.hidden = NO;
    if (selected) {
        [UIView animateWithDuration:0.3 animations:^{
            self.toolbar.frameOriginY = SCREEN_HEIGHT - 110 - 78;
            self.toolbar.frameSizeHeight = 78;
        }];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            self.toolbar.frameOriginY = SCREEN_HEIGHT - 110;
            self.toolbar.frameSizeHeight = 0;
        }];
    }
}

//拍照
- (void)takePhoto {
    WS(weakSelf);
    [self.camera capture:^(LLSimpleCamera *camera, UIImage *image, NSDictionary *metadata, NSError *error) {
        if(!error) {
            weakSelf.image = image;
            [weakSelf.view addSubview:weakSelf.cropView];
            weakSelf.overturnBtn.hidden = YES;
            weakSelf.flashButton.hidden = YES;
            [weakSelf.snapBtnView snap:YES];
        }
        else {
            NSLog(@"An error has occured: %@", error);
        }
    } exactSeenImage:YES];
}

//翻转镜头
- (void)TurnThelens:(UIButton *)button {
    [self.camera togglePosition];
}

//开启灯光
- (void)openFlash:(UIButton *)button {
    if(self.camera.flash == LLCameraFlashOff) {
        BOOL done = [self.camera updateFlashMode:LLCameraFlashOn];
        if(done) {
            self.flashButton.selected = YES;
            self.flashButton.tintColor = [UIColor yellowColor];
        }
    }else {
        BOOL done = [self.camera updateFlashMode:LLCameraFlashOff];
        if(done) {
            self.flashButton.selected = NO;
            self.flashButton.tintColor = [UIColor whiteColor];
        }
    }
}

- (void)nextPublish {
    PublishPictureVC *publishVC = [[PublishPictureVC alloc] initWithShowBackButton:YES];
    publishVC.image = self.image;
    [self pushVC:publishVC animated:YES];
}

-(void)InitializeCamera{
    CGRect screenRect = CM(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 110 - 64);
    // 创建一个相机
    self.camera = [[LLSimpleCamera alloc] initWithQuality:AVCaptureSessionPresetHigh  position:LLCameraPositionRear
                                             videoEnabled:YES];
    
    [self.camera attachToViewController:self withFrame:screenRect];
    self.camera.fixOrientationAfterCapture = NO;
    
    
    __weak typeof(self) weakSelf = self;
    [self.camera setOnDeviceChange:^(LLSimpleCamera *camera, AVCaptureDevice * device) {
        if([camera isFlashAvailable]) {
            weakSelf.flashButton.hidden = NO;
            if(camera.flash == LLCameraFlashOff) {
                weakSelf.flashButton.selected = NO;
            }else {
                weakSelf.flashButton.selected = YES;
            }
        }else {
            weakSelf.flashButton.hidden = YES;
        }
    }];
    
    [self.camera setOnError:^(LLSimpleCamera *camera, NSError *error) {
        NSLog(@"Camera error: %@", error);
        
        if([error.domain isEqualToString:LLSimpleCameraErrorDomain]) {
            if(error.code == LLSimpleCameraErrorCodeCameraPermission) {
                DLog(@"未获取相机权限");
            }
        }
    }];
}

- (UIImageView *)cropView {
    if (_cropView == nil) {
        _cropView = [[UIImageView alloc] initWithImage:self.image];
        _cropView.frame = CM(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 110 - 64);
    }
    return _cropView;
}

- (CropToolBar *)toolbar {
    if (_toolbar == nil) {
        _toolbar = [[CropToolBar alloc] initWithFrame:CM(19, SCREEN_HEIGHT - 110 - 78, SCREEN_WIDTH - 38, 0)];
        [self.view addSubview:_toolbar];
    }
    return _toolbar;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
