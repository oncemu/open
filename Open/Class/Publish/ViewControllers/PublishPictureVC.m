//
//  PublishPictureVC.m
//  Open
//
//  Created by mfp on 17/8/21.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PublishPictureVC.h"
#import "SZTextView.h"
#import "PublishPictureView.h"
#import "PublishNetService.h"
#import "UserLocalVC.h"

@interface PublishPictureVC ()
@property(nonatomic, strong) UIScrollView *contentView;
@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) SZTextView *inputView;
@property(nonatomic, strong) PublishBar *bottomBar;
@property(nonatomic, strong) PublishNetService *publishService;
@property(nonatomic, strong) LocationInfo *localInfo;

@end

@implementation PublishPictureVC


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.inputView resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"发布美图";
    [self setupUI];
}

- (void)setupUI {
    self.contentView = [[UIScrollView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 50)];
    self.contentView.backgroundColor = WhiteColor;
    [self.view addSubview:self.contentView];
    
    self.imageView = [[UIImageView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, (self.image.size.height * SCREEN_WIDTH)/self.image.size.width)];
    self.imageView.image = self.image;
    [self.contentView addSubview:self.imageView];
    
    self.inputView = [[SZTextView alloc] initWithFrame:CM(0, CGRectGetMaxY(self.imageView.frame) + 10, SCREEN_WIDTH, 150)];
    _inputView.placeholder = @"分享美图......";
    _inputView.textColor = ColorWithHex(0x333333);
    _inputView.font = Font(16);
    [self.contentView addSubview:_inputView];
    self.contentView.contentSize = Size(0, CGRectGetMaxY(_inputView.frame));
    
    self.bottomBar = [[PublishBar alloc] initWithFrame:CM(0, SCREEN_HEIGHT - 50, SCREEN_WIDTH, 50)];
    WS(weakSelf);
    _bottomBar.publisBlock = ^() {//发布
        [weakSelf checkLoginStatus];
    };
    _bottomBar.localBlock = ^() {//定位
        [weakSelf local];
    };
    [self.view addSubview:_bottomBar];
}

- (void)checkLoginStatus {
    if ([self checkLogin:YES]) {
        [self publish];
    }
}

- (void)publish {
    [KVNProgress showProgress:0.0f status:@"正在发布..."];
    NSString *title = @"";
    NSString *description = self.inputView.text ? self.inputView.text : @"";
    NSString *country = self.localInfo.country ? self.localInfo.country : @"";
    NSString *city = self.localInfo.city ? self.localInfo.city : @"";
    NSString *detailedAddress = self.localInfo.name ? self.localInfo.name : @"";
    NSString *weather = self.localInfo.nowCond ? self.localInfo.nowCond : @"";
    CGFloat longitude = self.localInfo.location.coordinate.longitude;
    CGFloat latitude = self.localInfo.location.coordinate.latitude;
    WS(weakSelf);
    [self.publishService uploadImage:@[self.image] params:@{@"category":@"PHOTO",@"title":title,@"description":description,@"country":country,@"city":city,@"longitude":@(longitude),@"latitude":@(latitude),@"weather":weather,@"detailedAddress":detailedAddress} url:kPost progress:^(NSProgress *progress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *pro = [NSString stringWithFormat:@"%.1f",progress.fractionCompleted];
            [KVNProgress showProgress:pro.floatValue status:@"正在发布..."];
        });
    } success:^(NSString *requestInfo, id responseObject) {
        [KVNProgress showSuccessWithStatus:@"发布成功！"];
        [weakSelf goBack];
        [[NSNotificationCenter defaultCenter] postNotificationName:kPublishAlbumSuccess object:nil];
    } failure:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:responseObject[@"message"]];
    }];
}

- (void)loginSucceed {
    [self publish];
}

- (void)local {
    WS(weakSelf);
    UserLocalVC *vc = [[UserLocalVC alloc] initWithShowBackButton:YES];
    vc.completeLocal = ^(LocationInfo *info) {
        weakSelf.localInfo = info;
        weakSelf.bottomBar.localName = info.city;
    };

    [self pushVC:vc animated:YES];
}

- (PublishNetService *)publishService {
    if (!_publishService) {
        _publishService = [[PublishNetService alloc] init];
    }
    return _publishService;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
