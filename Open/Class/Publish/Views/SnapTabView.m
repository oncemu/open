//
//  SnapTabView.m
//  Open
//
//  Created by mfp on 17/8/28.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "SnapTabView.h"

@implementation SnapTabView

@end

@interface SnapBtnView ()
@property(nonatomic, strong) UIButton *snapButton;//拍照按钮
@property(nonatomic, strong) UIButton *setButton;//重拍按钮
@property(nonatomic, strong) UIButton *doneButton;

@end

@implementation SnapBtnView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.snapButton = [ViewFactory getButton:CM(frame.size.width/2 - 44, frame.size.height/2 - 44, 88, 88) selectImage:getImage(@"publish_snap") norImage:getImage(@"publish_snap") target:self action:@selector(takePhoto)];
        [self addSubview:_snapButton];
        
        self.setButton = [ViewFactory getButton:CM(19, frame.size.height - 61 - 25, 61, 61) selectImage:getImage(@"snap_back") norImage:getImage(@"snap_back") target:self action:@selector(reSnap)];
        [self addSubview:_setButton];
        
        self.doneButton = [ViewFactory getButton:CM(SCREEN_WIDTH - 61 - 19, frame.size.height - 61 - 25, 61, 61) selectImage:getImage(@"snap_ok") norImage:getImage(@"snap_ok") target:self action:@selector(next)];

        [self addSubview:_doneButton];
        _doneButton.hidden = _setButton.hidden = YES;
    }
    return self;
}

- (void)takePhoto {
    if (self.SnapBlock) {
        self.SnapBlock();
    }
}

- (void)reSnap {
    if (self.ReSnapBlock) {
        [self snap:NO];
        self.ReSnapBlock();
    }
}

- (void)next {
    if (self.DoneBlock) {
        self.DoneBlock();
    }
}

- (void)snap:(BOOL)success {
    self.setButton.hidden = self.doneButton.hidden = !success;
    self.snapButton.hidden = success;
}
@end

@implementation EditBtnView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

@end

@implementation CropTabView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

@end
