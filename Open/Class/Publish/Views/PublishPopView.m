//
//  PublishPopView.m
//  Open
//
//  Created by mfp on 17/8/21.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PublishPopView.h"

@interface PublishPopView ()

@property(nonatomic, strong) UIButton *button1;
@property(nonatomic, strong) UIButton *button2;
@property(nonatomic, strong) UIButton *closeBtn;

@property(nonatomic, strong) UIButton *albumBtn;
@property(nonatomic, strong) UIButton *cameraBtn;
@property(nonatomic, strong) UIButton *noteBtn;

@end

@implementation PublishPopView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = ClearColor;
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.startPoint = Point(0, 0);
        gradient.endPoint = Point(0, 1);
        UIColor *color1 = [UIColor colorWithWhite:1 alpha:0];
        UIColor *color2 = [UIColor colorWithWhite:1 alpha:1];
        UIColor *color3 = WhiteColor;
        NSArray *colors = [NSArray arrayWithObjects:(id)color1.CGColor, color2.CGColor,color3.CGColor, nil];
        gradient.colors = colors;
        gradient.frame = CM(0, 0, frame.size.width, frame.size.height);
        [self.layer addSublayer:gradient];
        
        self.button1 = [ViewFactory getButton:CM(SCREEN_WIDTH/2 - 85 - 41, SCREEN_HEIGHT, 85, 113) selectImage:getImage(@"publish_btnpic") norImage:getImage(@"publish_btnpic") target:self action:@selector(buttonClick:)];
        [self.button1 setBackgroundImage:getImage(@"publish_buttonbg") forState:UIControlStateNormal];
        [self.button1 setTitle:@"美图" forState:UIControlStateNormal];
        self.button1.titleLabel.font = Font(19);
        [self.button1 setTitleColor:ColorWithHex(0x000000) forState:UIControlStateNormal];
        self.button1.imageEdgeInsets = UIEdgeInsetsMake(-20, 10, 20, -10);
        self.button1.titleEdgeInsets = UIEdgeInsetsMake(30, -50, -30, 0);
        
        self.button2 = [ViewFactory getButton:CM(SCREEN_WIDTH/2 + 41, self.button1.frameOriginY, 85, 113) selectImage:getImage(@"publish_btnnote") norImage:getImage(@"publish_btnnote") target:self action:@selector(buttonClick:)];
        [self.button2 setBackgroundImage:getImage(@"publish_buttonbg") forState:UIControlStateNormal];
        [self.button2 setTitle:@"游记" forState:UIControlStateNormal];
        self.button2.titleLabel.font = Font(19);
        [self.button2 setTitleColor:ColorWithHex(0x000000) forState:UIControlStateNormal];
        self.button2.imageEdgeInsets = UIEdgeInsetsMake(-20, 20, 20, -20);
        self.button2.titleEdgeInsets = UIEdgeInsetsMake(30, -27, -30, 0);
        
        self.closeBtn = [ViewFactory getButton:CM(SCREEN_WIDTH/2 - 10.5f, SCREEN_HEIGHT - kTabBarHeight - 21, 21, 21) selectImage:getImage(@"publish_close") norImage:getImage(@"publish_close") target:self action:@selector(hide)];

        [self addSubview:self.button1];
        [self addSubview:self.button2];
        [self addSubview:self.closeBtn];
        
    }
    return self;
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self hide];
}

- (void)buttonClick:(UIButton *)button {
    if (!self.publishBlock) {
        return;
    }
    [self hide];
    if ([button isEqual:self.button1]) {
        self.publishBlock(PublishTypePicture);
    }else {
        self.publishBlock(PublishTypeTravelNotes);
    }
}

- (void)show {
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    [window addSubview:self];
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.alpha = 1;
        self.button1.frameOriginY = self.button2.frameOriginY = SCREEN_HEIGHT - kTabBarHeight - self.button1.frameSizeHeight;
    } completion:nil];
}

- (void)hide {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.alpha = 0;
        self.button1.frameOriginY = self.button2.frameOriginY = SCREEN_HEIGHT;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (UIButton *)albumBtn {
    if (_albumBtn == nil) {
        _albumBtn = [ViewFactory getButton:CM(-55, SCREEN_HEIGHT - kTabBarHeight - self.button1.frameSizeHeight, 55, 55) selectImage:getImage(@"publish_album") norImage:getImage(@"publish_album") target:self action:@selector(buttonClick:)];
        [self addSubview:_albumBtn];
    }
    return _albumBtn;
}

- (UIButton *)cameraBtn {
    if (_cameraBtn == nil) {
        _cameraBtn = [ViewFactory getButton:CM(-55, SCREEN_HEIGHT - kTabBarHeight - self.button1.frameSizeHeight, 55, 55) selectImage:getImage(@"publish_camera") norImage:getImage(@"publish_camera") target:self action:@selector(buttonClick:)];
        [self addSubview:_cameraBtn];
    }
    return _cameraBtn;
}

- (UIButton *)noteBtn {
    if (_noteBtn == nil) {
        _noteBtn = [ViewFactory getButton:CM(-55, SCREEN_HEIGHT - kTabBarHeight - self.button1.frameSizeHeight, 55, 55) selectImage:getImage(@"publish_note") norImage:getImage(@"publish_note") target:self action:@selector(buttonClick:)];
        [self addSubview:_noteBtn];
    }
    return _noteBtn;
}

@end
