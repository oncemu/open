//
//  PublishPopView.h
//  Open
//
//  Created by mfp on 17/8/21.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublishPopView : UIView

@property(nonatomic,copy)void (^publishBlock)(PublishType type);

- (void)show;

- (void)hide;

@end
