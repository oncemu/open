//
//  PhotoAlbumView.m
//  Open
//
//  Created by mfp on 17/9/3.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PhotoAlbumView.h"
#import <Photos/Photos.h>

@implementation PhotoAlbumView


@end


@interface AlbumPhotoCell ()
@property (nonatomic, strong) UIImageView   *thumbnailImageView;
@property (nonatomic, strong) UIButton      *selectBtn;
@property (nonatomic, assign) CGFloat   contrastImageWidth;
@property (nonatomic, strong) UIButton  *imageButton;
@property(nonatomic, strong) UILabel *snaplabel;

@end

@implementation AlbumPhotoCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.thumbnailImageView = [ViewFactory getImageView:CGRectZero image:nil];
        self.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.thumbnailImageView.userInteractionEnabled = YES;
        [self.contentView addSubview:self.thumbnailImageView];
        self.contrastImageWidth = (SCREEN_WIDTH-20)/4;
        self.selectBtn = [ViewFactory getButton:CGRectMake(0, 0, 20, 20) selectImage:[UIImage imageNamed:@"publish_albumselect"] norImage:[UIImage imageNamed:@"publish_albumunselect"] target:self action:@selector(buttonClicked:)];
        [self.contentView addSubview:self.selectBtn];
        
        self.imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.imageButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.thumbnailImageView addSubview:self.imageButton];
        
        self.snaplabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@"拍摄"];
        _snaplabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_snaplabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.thumbnailImageView.frame = self.bounds;
    self.imageButton.frame = self.thumbnailImageView.bounds;
    self.snaplabel.frame = CM(0, self.frameSizeHeight/2 + 12, self.frameSizeWidth, 20);
    self.selectBtn.frame = CM(self.frameSizeWidth-20, 0, 20, 20);
}

- (void)setAsset:(PHAsset *)asset {
    if ([asset isKindOfClass:[PHAsset class]]) {
        _asset = asset;
        self.thumbnailImageView.image = nil;
        if ([self.imageViewModel.selectMediaDataArray containsObject:asset]) {
            self.selectBtn.selected = YES;
        }else{
            self.selectBtn.selected = NO;
        }
        if (!self.asset || self.asset.mediaType != PHAssetMediaTypeImage || self.isCamera) {
            self.selectBtn.hidden = self.imageButton.hidden = YES;
            self.thumbnailImageView.userInteractionEnabled = NO;
            self.thumbnailImageView.alpha = 0.5f;
        }else{
            self.selectBtn.hidden = self.imageButton.hidden = NO;
            self.thumbnailImageView.userInteractionEnabled = YES;
            self.thumbnailImageView.alpha = 1;
        }
        
        __unsafe_unretained typeof(self)weakSelf = self;
        @autoreleasepool {
            PHImageRequestOptions *opt = [[PHImageRequestOptions alloc]init];
            opt.synchronous = NO;
            opt.resizeMode = PHImageRequestOptionsResizeModeFast;
            opt.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
            
            PHImageManager *imageManager = [PHImageManager defaultManager];
            [imageManager requestImageForAsset:asset targetSize:CGSizeMake(self.contrastImageWidth, self.contrastImageWidth) contentMode:PHImageContentModeAspectFill options:opt resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                if (result) {
                    weakSelf.thumbnailImageView.image = result;
                }
            }];
        }
    }
}

- (void)setIsCamera:(BOOL)isCamera {
    _isCamera = isCamera;
    self.snaplabel.hidden = !isCamera;
    if (isCamera) {
        self.thumbnailImageView.image = [UIImage imageNamed:@"publish_albumsnap"];
        self.selectBtn.hidden = self.imageButton.hidden = YES;
        self.thumbnailImageView.contentMode = UIViewContentModeCenter;
    }else{
        self.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
}

- (void)buttonClicked:(UIButton *)button {
    button.selected = [self.imageViewModel selectionAnyMediaItem:self.asset];
    self.selectBtn.selected = button.selected;
    if (self.ClickSelectedButtonBlock) {
        self.ClickSelectedButtonBlock(self.asset);
    }
}

- (void)clickImageBackgroundButton:(UIButton *)button {
    self.selectBtn.selected = [self.imageViewModel selectionAnyMediaItem:self.asset];
    if (self.ClickSelectedButtonBlock) {
        self.ClickSelectedButtonBlock(self.asset);
    }
}


@end
