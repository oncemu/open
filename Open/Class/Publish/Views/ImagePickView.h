//
//  ImagePickView.h
//  Open
//
//  Created by mfp on 17/9/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickView : UIView

@property(nonatomic, strong) NSMutableArray *images;
@property(nonatomic, copy) void(^selectBlock)(UIImage *img);

- (instancetype)initWithFrame:(CGRect)frame target:(UIViewController *)target;

@end

@interface ImageCell : UICollectionViewCell
@property(nonatomic, assign) BOOL add;//是否为添加
@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) UIButton *deleteBtn;
@property(nonatomic, copy) void(^deleteBlock)();

@end
