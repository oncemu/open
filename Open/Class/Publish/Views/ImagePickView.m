//
//  ImagePickView.m
//  Open
//
//  Created by mfp on 17/9/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "ImagePickView.h"
#import "PhotoAlbumVC.h"
#import "RTRootNavigationController.h"

static NSString *imagecellid = @"imagecellid";

@interface ImagePickView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, weak) UIViewController *target;
@end

@implementation ImagePickView

- (instancetype)initWithFrame:(CGRect)frame target:(UIViewController *)target {
    self = [super initWithFrame:frame];
    if (self) {
        self.target = target;
        self.images = [NSMutableArray array];
        self.backgroundColor = WhiteColor;
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 2;
        layout.minimumInteritemSpacing = 2;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.itemSize = Size((self.frameSizeWidth - 4)/3, (self.frameSizeWidth - 4)/3);

        _collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
        _collectionView.backgroundColor = WhiteColor;
        [_collectionView registerClass:[ImageCell class] forCellWithReuseIdentifier:imagecellid];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [self addSubview:_collectionView];
    }
    return self;
}

- (void)reloadImageData {
    [self.collectionView reloadData];
    [self setNeedsLayout];
}

- (void)deleteImage:(NSIndexPath *)indexPath {
    [self.images removeObjectAtIndex:indexPath.item];
    [self.collectionView performBatchUpdates:^{
        if (self.images.count == 8) { //如果开始是9张图，删除一张后要在最后添加一个加号cell，所以cell数量是不变，不能做delete操作，只能reload
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        }else{ //不够9张的正常删除
            [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
        }
    } completion:^(BOOL finished) {
        if (finished) {
            //对indexpath重新编号排序，
            [self.collectionView reloadData];
        }
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.frameSizeHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height;
    self.collectionView.frameSizeHeight = self.frameSizeHeight;
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.images.count < 9) {
        return self.images.count + 1;
    }else{
        return self.images.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCell *cell = (ImageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:imagecellid forIndexPath:indexPath];
    if (indexPath.item<self.images.count) {
        cell.imageView.image = self.images[indexPath.item];
        cell.add = NO;
    }else{
        cell.add = YES;
        cell.imageView.image = getImage(@"publish_imageadd");
    }
    WS(weakSelf);
    cell.deleteBlock = ^() {
        [weakSelf deleteImage:indexPath];
    };
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item == self.images.count) {
        WS(weakSelf);
        PhotoAlbumVC *vc = [[PhotoAlbumVC alloc] initWithShowBackButton:YES];
        vc.type = PublishTypeTravelNotes;
        vc.imageCount = self.images.count;
        vc.maxCount = 9;
        vc.addPhotoBlock = ^(NSArray *images) {
            if (images) {
                [weakSelf.images addObjectsFromArray:images];
                [weakSelf reloadImageData];
            }
        };
        RTRootNavigationController *nav = [[RTRootNavigationController alloc] initWithRootViewController:vc];
        [self.target presentViewController:nav animated:YES completion:nil];
    }else{
        if (self.selectBlock) {
            self.selectBlock(self.images[indexPath.item]);
        }
    }
}


@end


@implementation ImageCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _imageView = [ViewFactory getImageView:CGRectZero image:nil];
        _imageView.layer.cornerRadius = 8;
        _imageView.layer.masksToBounds = YES;
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_imageView];
        
        _deleteBtn = [ViewFactory getButton:CGRectZero selectImage:getImage(@"publish_deleteimg") norImage:getImage(@"publish_deleteimg") target:self action:@selector(deleteAction)];
        [self addSubview:_deleteBtn];
    }
    return self;
}

- (void)setAdd:(BOOL)add {
    self.deleteBtn.hidden = add;
}

- (void)deleteAction {
    if (self.deleteBlock) {
        self.deleteBlock();
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _imageView.frame = self.bounds;
    _deleteBtn.frame = CM(self.frameSizeWidth - 16, 0, 16, 16);
}
@end
