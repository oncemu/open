//
//  SnapTabView.h
//  Open
//
//  Created by mfp on 17/8/28.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SnapTabView : UIView

@end

//显示拍照设置按钮view
@interface SnapBtnView : UIView

@property(nonatomic, copy) void(^SnapBlock)();
@property(nonatomic, copy) void(^ReSnapBlock)();
@property(nonatomic, copy) void(^DoneBlock)();

- (void)snap:(BOOL)success;
@end

@interface EditBtnView : UIView//显示裁剪等功能view

@property(nonatomic, strong) UIButton *CropBtn;
@property(nonatomic, strong) UIButton *WrodBtn;//文字
@property(nonatomic, strong) UIButton *TipBtn;//贴纸
@property(nonatomic, strong) UIButton *LabelBtn;//标签

@end

//裁剪功能tab
@interface CropTabView : UIView

@end
