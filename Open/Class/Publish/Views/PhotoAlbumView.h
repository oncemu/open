//
//  PhotoAlbumView.h
//  Open
//
//  Created by mfp on 17/9/3.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickImageViewModel.h"

@interface PhotoAlbumView : UIView

@end

@interface AlbumPhotoCell : UICollectionViewCell
@property (weak,nonatomic) PickAlbumViewModel *albumViewModel;
@property (nonatomic, weak) PickImageViewModel  *imageViewModel;

@property (nonatomic, assign) BOOL          isCamera;
@property (nonatomic, strong) PHAsset *asset;
@property (nonatomic, copy) void(^ClickSelectedButtonBlock)(PHAsset *asset);
@end
