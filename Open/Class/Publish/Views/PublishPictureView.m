//
//  PublishPictureView.m
//  Open
//
//  Created by mfp on 17/8/29.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PublishPictureView.h"

@implementation PublishPictureView

@end

@interface PublishBar ()
@property(nonatomic, strong) UIImageView *localImage;
@property(nonatomic, strong) UIButton *localButton;
@property(nonatomic, strong) UIButton *publishBtn;
@property(nonatomic, strong) UIButton *placeButton;

@end

@implementation PublishBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        UIView *line = [[UIView alloc] initWithFrame:CM(0, 0, frame.size.width, 0.5f)];
        line.backgroundColor = grayLineColor;
        [self addSubview:line];
        self.localImage = [[UIImageView alloc] initWithFrame:CM(18, (frame.size.height - 16)/2 , 12, 16)];
        _localImage.image = getImage(@"publish_local");
        [self addSubview:_localImage];
        
        self.localButton = [ViewFactory getButton:CM(CGRectGetMaxX(self.localImage.frame) + 5, self.localImage.frameOriginY, frame.size.width - 40 -100, 16) font:Font(15) selectTitle:@"" norTitle:@"你在哪里?" sColor:ColorWithHex(0x999999) norColor:ColorWithHex(0x999999) target:self action:@selector(loacal:)];
        _localButton.showsTouchWhenHighlighted = NO;
        _localButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self addSubview:_localButton];
        
        self.publishBtn = [ViewFactory getButton:CM(frame.size.width - 60 - 20, (frame.size.height - 25)/2, 60, 25) font:Font(15) selectTitle:@"" norTitle:@"发布" sColor:ColorWithHex(0x000000) norColor:ColorWithHex(0x000000) target:self action:@selector(publish)];
        _publishBtn.backgroundColor = LightLemonYellow;
        _publishBtn.layer.cornerRadius = 4;
        _publishBtn.layer.masksToBounds = YES;
        [self addSubview:_publishBtn];
    }
    return self;
}

- (void)setLocalName:(NSString *)localName {
    _localName = localName;
    self.localButton.hidden = YES;
    [self addSubview:self.placeButton];
    CGFloat len = [StringUtil getLabelLength:localName font:Font(11) height:20] + 30;
    [self.placeButton setTitle:localName forState:UIControlStateNormal];
    self.placeButton.frame = CM(CGRectGetMaxX(self.localImage.frame) + 5, (self.frameSizeHeight - 20)/2,len , 20);
    self.placeButton.imageEdgeInsets = UIEdgeInsetsMake(0, len - 25, 0, -(len - 25));
    self.placeButton.titleEdgeInsets = UIEdgeInsetsMake(0, -(len - 30), 0, 0);

}
- (void)loacal:(UIButton *)button {
    if (self.localBlock) {
        self.localBlock();
    }
}

- (void)deletePlacebtn {
    [self.placeButton removeFromSuperview];
    self.localButton.hidden = NO;
}

- (void)publish {
    if (self.publisBlock) {
        self.publisBlock();
    }
}

- (UIButton *)placeButton {
    if (!_placeButton) {
        UIImage *image = [getImage(@"publish_localnamebg") stretchableImageWithLeftCapWidth:5 topCapHeight:5];
        UIImage *delete = getImage(@"publish_localdelete");
        _placeButton = [ViewFactory getButton:CGRectZero selectImage:delete norImage:delete target:self action:@selector(deletePlacebtn)];
        [_placeButton setTitleColor:WhiteColor forState:UIControlStateNormal];
        _placeButton.titleLabel.font = Font(11);
        [_placeButton setBackgroundImage:image forState:UIControlStateNormal];
    }
    return _placeButton;
}
@end
