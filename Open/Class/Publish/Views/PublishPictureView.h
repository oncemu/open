//
//  PublishPictureView.h
//  Open
//
//  Created by mfp on 17/8/29.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublishPictureView : UIView

@end

@interface PublishBar : UIView

@property(nonatomic, copy) NSString *localName;
@property(nonatomic, copy) void(^publisBlock)();
@property(nonatomic, copy) void(^localBlock)();

@end
