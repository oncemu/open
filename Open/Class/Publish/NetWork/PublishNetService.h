//
//  PublishNetService.h
//  Open
//
//  Created by mfp on 17/9/5.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseNetService.h"

@interface PublishNetService : BaseNetService

/* POST /posts 发布 - 发布美图 OR 发布游记
 category 发布类型: PHOTO-美图；JOURNEY-游记

 title  标题：长度不超过20个字符

 description  描述：长度不超过200个字符
 
 country 国家：长度不超过10个字符
 
 city 城市：长度不超过20个字符

 */

- (void)postAlbum:(UIImage *)image
           params:(NSDictionary *)params
          success:(responseSuccess)success
          failure:(responseFailure)failure;

@end
