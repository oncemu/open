//
//  SearchView.m
//  Open
//
//  Created by mfp on 17/7/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "SearchView.h"
#import "SearchRecordsManager.h"
#import "GreedoCollectionViewLayout.h"

static NSString *recordcellid = @"recordcellid";
static NSString *recordheaderid = @"recordheaderid";
static NSString *recordfooterid = @"recordfooterid";

@implementation SearchView


@end

@interface SearchRecordsView ()<GreedoCollectionViewLayoutDataSource,UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic, strong) UICollectionView *collectionView;
@property (strong, nonatomic) GreedoCollectionViewLayout *collectionViewSizeCalculator;
@property(nonatomic, strong) NSMutableArray *historyList;
@end

@implementation SearchRecordsView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.historyList =[NSMutableArray array];
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumInteritemSpacing = 8.0f;
        layout.minimumLineSpacing = 8.0f;
        layout.sectionInset = UIEdgeInsetsMake(0, 17, 0, 17);
        layout.headerReferenceSize = Size(SCREEN_WIDTH, 45);
        layout.footerReferenceSize = Size(SCREEN_WIDTH, 60);
        self.collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
        self.collectionView.backgroundColor = WhiteColor;
        [self.collectionView registerClass:[RecordCell class] forCellWithReuseIdentifier:recordcellid];
        [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:recordheaderid];
        [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:recordfooterid];

        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self addSubview:_collectionView];
        self.collectionViewSizeCalculator.rowMaximumHeight = 30;
        self.collectionViewSizeCalculator.fixedHeight = YES;
    }
    return self;
}

- (void)clearRecords {
    [[SearchRecordsManager defaultManager] deleteAllHistory];
    [self reloadHistoryView];
}

- (void)reloadHistoryView {
    [self.historyList removeAllObjects];
    NSArray *list = [[SearchRecordsManager defaultManager] getHistory];
    if (list && list.count > 0) {
        [self.historyList addObjectsFromArray:list];
        self.collectionView.hidden = NO;
    }else{
        self.collectionView.hidden = YES;
    }
    [self.collectionViewSizeCalculator clearCache];
    [self.collectionView reloadData];
}

#pragma mark UICollectionViewDataSource,UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.historyList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RecordCell *cell = (RecordCell *)[collectionView dequeueReusableCellWithReuseIdentifier:recordcellid forIndexPath:indexPath];
    cell.textLabel.text = self.historyList[indexPath.item];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:recordheaderid forIndexPath:indexPath];
        view.backgroundColor = WhiteColor;
        UILabel *label = [view viewWithTag:100];
        if (!label) {
            label = [ViewFactory getLabel:CM(17, 0, SCREEN_WIDTH, 45) font:Font(16) textColor:ColorWithHex(0x999999) text:@"历史记录"];
            [view addSubview:label];
        }
        return view;
    }else{
        UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:recordfooterid forIndexPath:indexPath];
        view.backgroundColor = WhiteColor;
        UIButton *btn = [view viewWithTag:200];
        if (!btn) {
            btn = [ViewFactory getButton:CM(SCREEN_WIDTH/2 - 50, 15, 100, 30) font:Font(14) selectTitle:@"清空历史记录" norTitle:@"清空历史记录" sColor:ColorWithHex(0x999999) norColor:ColorWithHex(0x999999) target:self action:@selector(clearRecords)];
            btn.layer.cornerRadius = 4;
            btn.layer.borderColor = ColorWithHex(0x999999).CGColor;
            btn.layer.borderWidth = 0.5f;
            btn.layer.masksToBounds = YES;
            [view addSubview:btn];
        }
        return view;
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchHistory) {
        self.searchHistory(self.historyList[indexPath.item]);
    }
}

#pragma mark - <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.collectionViewSizeCalculator sizeForPhotoAtIndexPath:indexPath];
}

#pragma mark - <GreedoCollectionViewLayoutDataSource>

- (CGSize)greedoCollectionViewLayout:(GreedoCollectionViewLayout *)layout originalImageSizeAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item < self.historyList.count) {
        NSString *record = self.historyList[indexPath.item];
        CGFloat len = [StringUtil getLabelLength:record font:Font(16) height:30];
        return Size(len + 50, 30);
    }
    return CGSizeMake(0, 0);
}

- (GreedoCollectionViewLayout *)collectionViewSizeCalculator {
    if (!_collectionViewSizeCalculator) {
        _collectionViewSizeCalculator = [[GreedoCollectionViewLayout alloc] initWithCollectionView:self.collectionView];
        _collectionViewSizeCalculator.dataSource = self;
    }
    return _collectionViewSizeCalculator;
}


@end

@interface RecordCell ()

@end

@implementation RecordCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.textLabel = [ViewFactory getLabel:CGRectZero font:Font(16) textColor:ColorWithHex(0x999999) text:nil];
        _textLabel.backgroundColor = ColorWithHex(0xf2f2f2);
        _textLabel.layer.cornerRadius = 5;
        _textLabel.layer.masksToBounds = YES;
        _textLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_textLabel];
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = self.bounds;
}

@end

@interface UserCell ()
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, strong) UIButton *followBtn;
@end

@implementation UserCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = ColorWithHex(0xf7f7f7);
        self.textLabel.textColor = ColorWithHex(0x000000);
        self.textLabel.font = Font(14);
        self.detailLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x999999) text:nil];
        self.detailLabel.backgroundColor = ColorWithHex(0xf7f7f7);
        self.followBtn = [ViewFactory getButton:CGRectZero font:Font(15) selectTitle:@"关注" norTitle:@"关注" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(follow)];
        [self.followBtn setBackgroundImage:getImage(@"pic_send_comment") forState:UIControlStateNormal];
        [self.contentView addSubview:self.detailLabel];
        [self.contentView addSubview:self.followBtn];
        self.textLabel.text = @"EHTHA";
        self.detailLabel.text = @"100作品 100粉丝";
    }
    return self;
}

- (void)setObject:(id)object {
    _object = object;
    self.textLabel.text = @"EHTHA";
    self.detailLabel.text = @"100作品 100粉丝";
}
- (void)follow {
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CM(17, self.frameSizeHeight/2 - 15, 30, 30);
    self.textLabel.frame = CM(CGRectGetMaxX(self.imageView.frame) + 5, self.imageView.frameOriginY + 2, self.frameSizeWidth - 35 - 80, 15);
    self.detailLabel.frame = CM(self.textLabel.frameOriginX, CGRectGetMaxY(self.textLabel.frame), self.textLabel.frameSizeWidth, 14);
    self.followBtn.frame = CM(self.frameSizeWidth - 63 - 15, self.frameSizeHeight/2 - 13, 63, 25);
}
@end
