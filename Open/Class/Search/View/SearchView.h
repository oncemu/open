//
//  SearchView.h
//  Open
//
//  Created by mfp on 17/7/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchView : UIView

@end


//搜索历史记录
@interface SearchRecordsView : UIView

@property(nonatomic, copy) void (^searchHistory)(NSString *history);

- (void)reloadHistoryView;

@end

//搜索记录cell
@interface RecordCell : UICollectionViewCell

@property(nonatomic, strong) UILabel *textLabel;

@end

//用户cell
@interface UserCell : UITableViewCell

@property (nonatomic, strong) id object;

@end
