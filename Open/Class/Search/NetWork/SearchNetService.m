//
//  SearchNetService.m
//  Open
//
//  Created by mfp on 17/9/5.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "SearchNetService.h"

@implementation SearchNetService


//首页搜索 title city nickname
- (void)search:(NSDictionary *)params
              more:(BOOL)more
           success:(responseSuccess)success
           failure:(responseFailure)failure {
    if (!more) {
        SearchResultIndex = 1;
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:params];
    [dic setObject:@(SearchResultIndex) forKey:@"page"];
    [self requestGETwithUrl:kHomeSearch parameters:dic success:^(NSString *requestInfo, id responseObject) {
        SearchResultIndex += 1;
        NSArray *list = [AlbumModel albumList:responseObject];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}
@end
