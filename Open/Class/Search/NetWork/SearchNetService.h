//
//  SearchNetService.h
//  Open
//
//  Created by mfp on 17/9/5.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseNetService.h"
#import "AlbumModel.h"

#define kHomeSearch @"home/search"// 首页 - 搜索（待测试）

@interface SearchNetService : BaseNetService
{
    NSInteger SearchResultIndex;
}
//首页搜索 title city nickname
- (void)search:(NSDictionary *)params
          more:(BOOL)more
       success:(responseSuccess)success
       failure:(responseFailure)failure;
@end
