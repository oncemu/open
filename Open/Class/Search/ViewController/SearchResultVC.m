//
//  SearchResultVC.m
//  Open
//
//  Created by mfp on 17/7/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "SearchResultVC.h"
#import "PictureCell.h"
#import "DateTools.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "PictureDetailVC.h"
#import "PictureCollectionVC.h"
#import "SearchNetService.h"

static NSString *const picturefooterId = @"picturefooterId";

@interface SearchResultVC ()<CHTCollectionViewDelegateWaterfallLayout,PictureCellDelegate>

@property(nonatomic, strong) UISegmentedControl *segment;
@property(nonatomic, strong) SearchNetService *searchService;
@end

@implementation SearchResultVC


- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchService = [[SearchNetService alloc] init];
//    self.segment = [[UISegmentedControl alloc] initWithItems:@[@"用户",@"作品"]];
//    _segment.frame = CM(0, 0, 160, 29);
//    _segment.selectedSegmentIndex = 0;
//    _segment.tintColor = ColorWithHex(0xfedf34);
//    [_segment setTitleTextAttributes:@{NSForegroundColorAttributeName:ColorWithHex(0x333333)} forState:UIControlStateSelected];
//    [_segment setTitleTextAttributes:@{NSForegroundColorAttributeName:ColorWithHex(0x333333)} forState:UIControlStateNormal];
//    [_segment addTarget:self action:@selector(selectedSegmentAtIndex:) forControlEvents:UIControlEventValueChanged];
//    self.navigationItem.titleView = _segment;
    self.collectionView.backgroundColor = WhiteColor;
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.minimumInteritemSpacing = 5;
    self.collectionView.collectionViewLayout = layout;
    [self.collectionView registerClass:[PictureCell class] forCellWithReuseIdentifier:pictureCellId];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter withReuseIdentifier:picturefooterId];
}

- (void)selectedSegmentAtIndex:(UISegmentedControl *)segment {
    DLog(@"%ld",(long)segment.selectedSegmentIndex);
}

- (void)loadData {
    [self searchResult:NO];
}

- (void)loadMoreData {
    [self searchResult:YES];
}

- (void)searchResult:(BOOL)more {
    WS(weakSelf);
    [self.searchService search:@{@"title":self.searchKey} more:more success:^(NSString *requestInfo, id responseObject) {
        [weakSelf changeRefreshStatus:!more data:responseObject];
    } failure:^(NSError *error, id responseObject) {
        [weakSelf changeRefreshStatus:!more data:nil];
    }];
}

#pragma mark UICollectionViewDataSource UICollectionViewDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.dataList.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PictureCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:pictureCellId forIndexPath:indexPath];
    cell.object = self.dataList[indexPath.section];
    cell.delegate = self;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    PictureDetailVC *vc = [[PictureDetailVC alloc] initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
    vc.object = self.dataList[indexPath.section];
    [self pushVC:vc animated:YES];
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    AlbumModel *mode = self.dataList[indexPath.section];
    CGFloat height = [mode cellHeight];
    return Size(SCREEN_WIDTH, height);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout columnCountForSection:(NSInteger)section {
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section {
    return 0.01f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForFooterInSection:(NSInteger)section {
    return 0.5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 17, 0, 17);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumColumnSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

#pragma mark - PictureCellDelegate
- (void)tapUserInfo:(id)object {
 //   OtherCenterVC *vc = [[OtherCenterVC alloc] initWithShowBackButton:YES];
//    [self.parentVC pushVC:vc animated:YES];
}

- (void)toolBarAction:(NSInteger)index {
    
}
- (void)followButtonClick {
    
}
- (void)typeButtonClick {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
