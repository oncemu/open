//
//  SearchVC.m
//  Open
//
//  Created by mfp on 17/7/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "SearchVC.h"
#import "SearchView.h"
#import "SearchResultVC.h"
#import "SearchNetService.h"
#import "PictureCell.h"
#import "DateTools.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "PictureDetailVC.h"
#import "PictureCollectionVC.h"


static NSString *const picturefooterId = @"picturefooterId";
static NSString *const searchusercellId = @"searchusercellId";
static NSString *const searchheaderId = @"searchheaderId";

@interface SearchVC ()<UISearchBarDelegate,CHTCollectionViewDelegateWaterfallLayout,PictureCellDelegate>
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) SearchRecordsView *historyView;
@property(nonatomic, strong) SearchNetService *searchService;

@end

@implementation SearchVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.searchBar becomeFirstResponder];
    [self.historyView reloadHistoryView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.searchBar resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH - 100, 40)];
    self.searchBar.delegate = self;
    self.searchBar.showsCancelButton = YES;
    self.searchBar.backgroundColor = WhiteColor;
    self.searchBar.barTintColor = WhiteColor;
    self.searchBar.placeholder = @"输入地点、目的地、关键词";
    UIButton *cancelBtn = [self.searchBar valueForKey:@"cancelButton"];
    [cancelBtn setTitleColor:ColorWithHex(0x666666) forState:UIControlStateNormal];
    UIView *searchTextField = [[[self.searchBar.subviews firstObject] subviews] lastObject];
    searchTextField.backgroundColor = ColorWithHex(0xdfdfdf);
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTitle:@"取消"];

    self.navigationItem.titleView = self.searchBar;
    
    self.historyView = [[SearchRecordsView alloc] initWithFrame:CM(0, StatusBar_HEIGHT + NavigationBar_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - StatusBar_HEIGHT - NavigationBar_HEIGHT)];
    WS(weakSelf);
    _historyView.searchHistory = ^(NSString *history) {
        weakSelf.searchBar.text = history;
        [weakSelf searchResult:NO];
    };
    [self.view addSubview:_historyView];
    [self setUpCollectionView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardHide {
    UIButton *cancelBtn = [self.searchBar valueForKey:@"cancelButton"];
    cancelBtn.enabled = YES;
}

//- (void)pushToSearchResultVC:(NSString *)searchKey {
//    [[SearchRecordsManager defaultManager] saveSearchHistory:searchKey];
//    SearchResultVC *vc = [[SearchResultVC alloc] initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
//    vc.searchKey = searchKey;
//    [self pushVC:vc animated:YES];
//}

#pragma mark - UISearchBarDelegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self goBack];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (![StringUtil isNONil:searchBar.text]) {
        return;
    }
    [searchBar resignFirstResponder];
    [self searchResult:NO];
}

- (void)loadData {
    [self searchResult:NO];
}

- (void)loadMoreData {
    [self searchResult:YES];
}

- (void)searchResult:(BOOL)more {
    WS(weakSelf);
    [self.searchService search:@{@"title":self.searchBar.text} more:more success:^(NSString *requestInfo, id responseObject) {
        if (!more) {
            [weakSelf.dataList removeAllObjects];
        }
        [weakSelf.dataList addObjectsFromArray:responseObject];
        [weakSelf changeRefreshStatus:!more data:responseObject];
    } failure:^(NSError *error, id responseObject) {
        [weakSelf changeRefreshStatus:!more data:nil];
    }];
    [self.searchService search:@{@"city":self.searchBar.text} more:more success:^(NSString *requestInfo, id responseObject) {
        if (!more) {
            [weakSelf.dataList removeAllObjects];
        }
        [weakSelf.dataList addObjectsFromArray:responseObject];
        [weakSelf changeRefreshStatus:!more data:responseObject];
    } failure:^(NSError *error, id responseObject) {
        [weakSelf changeRefreshStatus:!more data:nil];
    }];
    [self.searchService search:@{@"nickname":self.searchBar.text} more:more success:^(NSString *requestInfo, id responseObject) {
        if (!more) {
            [weakSelf.dataList removeAllObjects];
        }
        [weakSelf.dataList addObjectsFromArray:responseObject];
        [weakSelf changeRefreshStatus:!more data:responseObject];
    } failure:^(NSError *error, id responseObject) {
        [weakSelf changeRefreshStatus:!more data:nil];
    }];
}

- (void)changeRefreshStatus:(BOOL)isHeader data:(NSArray *)data {
    [super changeRefreshStatus:isHeader data:data];
    self.historyView.hidden = YES;
}

#pragma mark UICollectionViewDataSource UICollectionViewDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.dataList.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PictureCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:pictureCellId forIndexPath:indexPath];
    cell.object = self.dataList[indexPath.section];
    cell.delegate = self;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    PictureDetailVC *vc = [[PictureDetailVC alloc] initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
    vc.object = self.dataList[indexPath.section];
    [self pushVC:vc animated:YES];
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    AlbumModel *mode = self.dataList[indexPath.section];
    CGFloat height = [mode cellHeight];
    return Size(SCREEN_WIDTH, height);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout columnCountForSection:(NSInteger)section {
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section {
    return 0.01f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForFooterInSection:(NSInteger)section {
    return 0.5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 17, 0, 17);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumColumnSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

#pragma mark - PictureCellDelegate
- (void)tapUserInfo:(id)object {
    //   OtherCenterVC *vc = [[OtherCenterVC alloc] initWithShowBackButton:YES];
    //    [self.parentVC pushVC:vc animated:YES];
}

- (void)toolBarAction:(NSInteger)index {
    
}
- (void)followButtonClick {
    
}
- (void)typeButtonClick {
    
}

- (void)setUpCollectionView {
    self.collectionView.backgroundColor = WhiteColor;
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.minimumInteritemSpacing = 5;
    self.collectionView.collectionViewLayout = layout;
    [self.collectionView registerClass:[PictureCell class] forCellWithReuseIdentifier:pictureCellId];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter withReuseIdentifier:picturefooterId];
}

- (SearchNetService *)searchService {
    if (_searchService == nil) {
        _searchService = [[SearchNetService alloc] init];
    }
    return _searchService;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
