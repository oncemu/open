//
//  SearchResultVC.h
//  Open
//
//  Created by mfp on 17/7/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CollectionBaseVC.h"

@interface SearchResultVC : CollectionBaseVC

@property (nonatomic, copy) NSString *searchKey;
@property(nonatomic, copy) NSString *city;
@property(nonatomic, copy) NSString *usernick;

@end
