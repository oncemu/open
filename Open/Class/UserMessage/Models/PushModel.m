//
//  PushModel.m
//  Open
//
//  Created by mfp on 17/9/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PushModel.h"

@implementation PushModel

- (NSString *)displaystarPush {
    _displaystarPush = [self displayName:_starPush];
    return _displaystarPush;
}

- (NSString *)displayscommentPush {
    _displaystarPush = [self displayName:_commentPush];
    return _displaystarPush;
}

- (NSString *)displayName:(NSString *)eng {
    NSString *display = @"";
    //返回的sting带空格，呵呵
    NSString *engdis = [eng stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([engdis isEqualToString:@"ALL"]) {
        display = @"所有人";
    }
    if ([engdis isEqualToString:@"FRIENDS"]) {
        display = @"我的好友";
    }
    if ([engdis isEqualToString:@"NONE"]) {
        display = @"全部拒绝";
    }
    return display;
}

@end
