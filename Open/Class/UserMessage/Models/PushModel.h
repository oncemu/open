//
//  PushModel.h
//  Open
//
//  Created by mfp on 17/9/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 activityPush (integer, optional): 是否接收活动消息推送: 1-是、0-否 ,
 commentPush (string, optional): 评论推送类型: ALL-所有人、FRIENDS-我的好友、NONE-全部拒绝 ,
 likePush (integer, optional): 是否接收被关注消息推送: 1-是、0-否 ,
 starPush (string, optional): 点赞推送类型: ALL-所有人、FRIENDS-我的好友、NONE-全部拒绝 ,
 systemPush (integer, optional): 是否接收系统消息推送: 1-是、0-否 ,

 */

@interface PushModel : NSObject

@property(nonatomic, copy) NSString *starPush;
@property(nonatomic, copy) NSString *commentPush;
@property(nonatomic, assign) NSInteger activityPush;
@property(nonatomic, assign) NSInteger likePush;
@property(nonatomic, assign) NSInteger systemPush;
@property(nonatomic, assign) NSInteger userId;
//显示的文字
@property(nonatomic, copy) NSString *displaystarPush;
@property(nonatomic, copy) NSString *displayscommentPush;




@end
