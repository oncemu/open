//
//  UserMessageView.h
//  Open
//
//  Created by mfp on 17/9/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserMessageView : UITableViewCell

@end

@interface UserMsgCell : UserMessageView
@property(nonatomic, strong) UILabel *countLabel;

@end

@interface ClearMsgCell : UserMessageView
@property(nonatomic, strong) UIButton *clearBtn;
@property(nonatomic, copy) void (^clearMsg)();

@end

@interface SystemMsgCell : UserMessageView
@property(nonatomic, strong) UILabel *timeLabel;
@property(nonatomic, strong) UITextView *contentLabel;
@property(nonatomic, strong) UILabel *checkDetailLabel;

@end

@interface MessageSetCell : UserMessageView
@property(nonatomic, strong) UILabel *leftLabel;
@property(nonatomic, strong) UIImageView *leftImgV;

@end

@interface MessageSwitchSetCell : UserMessageView
@property(nonatomic, strong) UISwitch *setSwitch;
@property(nonatomic, copy) void (^switchBlock)(MessageSwitchSetCell *scell);

@end
