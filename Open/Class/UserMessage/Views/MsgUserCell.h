//
//  MsgUserCell.h
//  Open
//
//  Created by mfp on 17/9/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

typedef NS_ENUM(NSUInteger, MsgUserType) {
    MsgUserTypeFavorite = 0,//点赞的
    MsgUserTypeFollowMe,//关注我的人
    MsgUserTypeFans,//他的粉丝
    MsgUserTypeFollowed //他关注的
};

@interface MsgUserCell : UITableViewCell

@property(nonatomic, strong) NSDictionary *model;
@property(nonatomic, assign) MsgUserType type;
@property(nonatomic, copy) void (^followBlock)(NSDictionary *user);


@end
