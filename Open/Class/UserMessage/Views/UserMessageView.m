//
//  UserMessageView.m
//  Open
//
//  Created by mfp on 17/9/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserMessageView.h"

@implementation UserMessageView

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.backgroundColor = WhiteColor;
        self.textLabel.textColor = ColorWithHex(0x666666);
        self.textLabel.font = Font(15);
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}
@end

@implementation UserMsgCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.countLabel = [ViewFactory getLabel:CGRectZero font:Font(13) textColor:ColorWithHex(0xffdd1c) text:nil];
        self.countLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.countLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.countLabel.frame = CM(self.frameSizeWidth - 17 - 150 , (self.frameSizeHeight - 20)/2, 150, 20);
}

@end

@implementation ClearMsgCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.text = @"系统消息";
        self.clearBtn = [ViewFactory getButton:CGRectZero font:Font(14) selectTitle:@"清空" norTitle:@"清空" sColor:LightLemonYellow norColor:LightLemonYellow target:self action:@selector(clear)];
        _clearBtn.layer.cornerRadius = 5;
        _clearBtn.layer.borderColor = LightLemonYellow.CGColor;
        _clearBtn.layer.borderWidth = 0.5f;
        _clearBtn.layer.masksToBounds = YES;
        [self.contentView addSubview:_clearBtn];
    }
    return self;
}

- (void)clear {
    if (self.clearMsg) {
        self.clearMsg();
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.clearBtn.frame = CM(self.frameSizeWidth - 17 - 50 , (self.frameSizeHeight - 20)/2, 50, 20);
}

@end

@interface SystemMsgCell ()
@property(nonatomic, strong) UIView *line1;
@end
@implementation SystemMsgCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.font = Font(14);
        self.textLabel.text = @"系统消息";
        self.separatorInset = UIEdgeInsetsMake(17, 0, 0, 0);
        self.timeLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@"昨天 15:00"];
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.timeLabel];
        
        self.contentLabel =  [[UITextView alloc] init];
        _contentLabel.textColor = ColorWithHex(0x666666);
        _contentLabel.font = Font(14);
        _contentLabel.userInteractionEnabled = NO;
        _contentLabel.text = @"这一条测试的系统消息，不够字数啊啊啊啊啊啊啊啊啊啊案发的发的发生打发打发啊打多少";
        [self.contentView addSubview:self.contentLabel];
        
        self.checkDetailLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@"点击查看详情"];
        _checkDetailLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.checkDetailLabel];
        self.line1 = [[UIView alloc] init];
        _line1.backgroundColor = ColorWithHex(0xf2f2f2);
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = CM(17, 0, 100, 43);
    self.timeLabel.frame = CM(self.frameSizeWidth - 17 - 150 , 0, 150, 43);
    self.line1.frame = CM(17, 43, self.frameSizeWidth, 0.5f);
    self.contentLabel.frame = CM(17, CGRectGetMaxY(self.line1.frame)+5, self.frameSizeWidth - 34, 90);
    self.checkDetailLabel.frame = CM(self.frameSizeWidth - 17 - 150, self.frameSizeHeight - 40, 150, 40);
}

@end

@implementation MessageSetCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.leftLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:LightLemonYellow text:@"所有人"];
        _leftLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.leftLabel];
        
        self.leftImgV = [ViewFactory getImageView:CGRectZero image:getImage(@"pic_scanzan")];
        [self.contentView addSubview:self.leftImgV];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.leftImgV.frame = CM(self.frameSizeWidth - 17 - 11, (self.frameSizeHeight - 18)/2, 11, 18);
    self.leftLabel.frame = CM(CGRectGetMinX(self.leftImgV.frame) - 30 - 60, 5, 60, self.frameSizeHeight- 10);
}
@end


@implementation MessageSwitchSetCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.setSwitch = [[UISwitch alloc] init];
        _setSwitch.onTintColor = LightLemonYellow;
        _setSwitch.tintColor = ColorWithHex(0xf2f2f2);
        _setSwitch.layer.cornerRadius = 15.5f;
        _setSwitch.layer.masksToBounds = YES;
        _setSwitch.backgroundColor = ColorWithHex(0xf2f2f2);
        [_setSwitch addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.contentView addSubview:_setSwitch];
    }
    return self;
}

- (void)valueChanged:(UISwitch *)sender {
    if (self.switchBlock) {
        self.switchBlock(self);
    }
}
- (void)layoutSubviews {
    [super layoutSubviews];
    self.setSwitch.frame = CM(self.frameSizeWidth - 17 - 60, (self.frameSizeHeight - 31)/2, 51, 31);
}
@end
