//
//  MsgUserCell.m
//  Open
//
//  Created by mfp on 17/9/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MsgUserCell.h"

@interface MsgUserCell ()
@property(nonatomic, strong) UIImageView *userIcon;
@property(nonatomic, strong) UILabel *userName;
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) UIImageView *albumView;
@property(nonatomic, strong) UIButton *followBtn;

@end

@implementation MsgUserCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.userIcon = [ViewFactory getImageView:CM(17, 12, 32, 32) image:getImage(@"headDefault")];
        _userIcon.layer.cornerRadius = 16;
        _userIcon.layer.masksToBounds = YES;
        _userIcon.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_userIcon];
        
        self.userName = [ViewFactory getLabel:CM(CGRectGetMaxX(self.userIcon.frame) + 10, self.userIcon.frameOriginY, SCREEN_WIDTH - 60 - 60, 16) font:Font(15) textColor:ColorWithHex(0x666666) text:@"用户名字"];
        [self.contentView addSubview:_userName];
        
        self.dateLabel = [ViewFactory getLabel:CM(CGRectGetMaxX(self.userIcon.frame) + 10, CGRectGetMaxY(self.userName.frame), SCREEN_WIDTH - 60 - 60, 16) font:Font(12) textColor:ColorWithHex(0x999999) text:@"2017-9-19"];
        [self.contentView addSubview:_dateLabel];
    }
    return self;
}

- (void)setModel:(NSDictionary *)model {
    _model = model;
    if (self.type == MsgUserTypeFavorite) {
        self.albumView.hidden = NO;
    }else{
        [self.followBtn setTitle:@"关注" forState:UIControlStateNormal];
    }
    self.userName.text = model[@"nickname"];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:model[@"avatar"]] placeholderImage:nil];
    self.dateLabel.text = [StringUtil timeWithTimeIntervalString:[model[@"createdAt"] doubleValue]];
    NSArray *photos = model[@"photos"];
    NSString *url = [photos firstObject];
    if (url) {
        [self.albumView sd_setImageWithURL:[NSURL URLWithString:url]];
    }
}

- (void)followAction:(UIButton *)sender {
    if (self.followBlock) {
        self.followBlock(self.model);
    }
}

- (UIImageView *)albumView {
    if (!_albumView) {
        _albumView = [ViewFactory getImageView:CM(SCREEN_WIDTH - 45 - 20, 10, 45, 35) image:nil];
        [self.contentView addSubview:_albumView];
    }
    return _albumView;
}

- (UIButton *)followBtn {
    if (!_followBtn) {
        _followBtn = [ViewFactory getButton:CM(SCREEN_WIDTH - 20 - 60, 15, 60, 25) selectImage:getImage(@"pic_send_comment") norImage:getImage(@"pic_send_comment") target:self action:@selector(followAction:)];
        [self.contentView addSubview:_followBtn];
    }
    return _followBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
