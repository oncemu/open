//
//  MsgCommentListCell.m
//  Open
//
//  Created by mfp on 17/9/25.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MsgCommentListCell.h"

@interface MsgCommentListCell ()
@property(nonatomic, strong) UIImageView *userIcon;
@property(nonatomic, strong) UILabel *userNamel;
@property(nonatomic, strong) UILabel *timeL;
@property(nonatomic, strong) UIButton *responseBtn;
@property(nonatomic, strong) UILabel *replyL;
@property(nonatomic, strong) UILabel *originUserNameL;
@property(nonatomic, strong) UILabel *originUserCommentL;

@end

@implementation MsgCommentListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
