//
//  MsgCommentListVC.m
//  Open
//
//  Created by mfp on 17/9/14.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MsgCommentListVC.h"
#import "HomeNetService.h"

@interface MsgCommentListVC ()
@property(nonatomic, strong) HomeNetService *homeService;

@end

@implementation MsgCommentListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"评论";
    self.homeService = [[HomeNetService alloc] init];
}

- (void)loadData {
    [self loadData:NO];
}

- (void)loadMoreData {
    [self loadData:YES];
}

- (void)loadData:(BOOL)more {
    WS(weakSelf);
    [self.homeService getHomeMsgMore:more success:^(NSString *requestInfo, id responseObject) {
        if (!more) {
            [weakSelf.dataList removeAllObjects];
        }
        [weakSelf.dataList addObjectsFromArray:responseObject];
        [weakSelf loadDataFinishWithHeader:!more withData:responseObject];
    } failure:^(NSError *error, id responseObject) {
        [weakSelf loadDataFinishWithHeader:!more withData:nil];

    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
