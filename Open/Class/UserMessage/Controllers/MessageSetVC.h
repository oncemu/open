//
//  MessageSetVC.h
//  Open
//
//  Created by mfp on 17/9/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseVC.h"
#import "TableBaseVC.h"
#import "PushModel.h"


typedef NS_ENUM(NSUInteger, MsgSetType) {
    MsgSetTypefComment = 0,     //评论设置
    MsgSetTypefFavorite = 1  //点赞设置
};

//push消息设置
@interface MessageSetVC : BaseVC

@end

//选择推送限制条件  所有人，好友或者全部拒绝
@interface MsgSetVC : TableBaseVC

@property(nonatomic, assign) MsgSetType type;
@property(nonatomic, strong) PushModel *model;
@property(nonatomic, copy) void (^setBlock)(PushModel *mode);

@end
