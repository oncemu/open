//
//  FavoriteVC.h
//  Open
//
//  Created by mfp on 17/9/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "TableBaseVC.h"
#import "MsgUserCell.h"


@interface MsgUserVC : TableBaseVC

@property(nonatomic, assign) MsgUserType type;

@end
