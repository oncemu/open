//
//  FavoriteVC.m
//  Open
//
//  Created by mfp on 17/9/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MsgUserVC.h"
#import "MsgUserCell.h"
#import "HomeNetService.h"

static NSString *favoritecellid = @"favoritecellid";

@interface MsgUserVC ()
@property(nonatomic, strong) HomeNetService *homeService;

@end

@implementation MsgUserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    self.homeService = [[HomeNetService alloc] init];
    if (self.type == MsgUserTypeFavorite) {
        self.navigationItem.title = @"已赞";
    }else if (self.type == MsgUserTypeFollowMe) {
        self.navigationItem.title = @"关注我的人";
    }
    self.tableView.separatorColor = ColorWithHex(0xf2f2f2);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.rowHeight = 56;
    [self.tableView registerClass:[MsgUserCell class] forCellReuseIdentifier:favoritecellid];
}

- (void)rightButtonItemAction {
    [self.dataList removeAllObjects];
    [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)loadData {
    [self loadWithType:NO];
}

- (void)loadMoreData {
    [self loadWithType:YES];
}

- (void)loadWithType:(BOOL)more {
    WS(weakSelf);
    if (self.type == MsgUserTypeFavorite) {
        //首页消息列表 点赞 accessToken
        [self.homeService userStarMsgMore:more success:^(NSString *requestInfo, id responseObject) {
            if (!more) {
                [weakSelf.dataList removeAllObjects];
            }
            [weakSelf.dataList addObjectsFromArray:responseObject];
            [weakSelf loadDataFinishWithHeader:!more withData:responseObject];
        } failure:^(NSError *error, id responseObject) {
            
        }];
    }else{
        //首页消息列表 关注 accessToken
        [self.homeService getUserLikeMsgMore:more success:^(NSString *requestInfo, id responseObject) {
            if (!more) {
                [weakSelf.dataList removeAllObjects];
            }
            [weakSelf.dataList addObjectsFromArray:responseObject];
            [weakSelf loadDataFinishWithHeader:!more withData:responseObject];
        } failure:^(NSError *error, id responseObject) {
            
        }];
    }
}

#pragma mark - tableview delegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MsgUserCell *cell = (MsgUserCell *)[tableView dequeueReusableCellWithIdentifier:favoritecellid];
    cell.type = self.type;
    cell.model = self.dataList[indexPath.row];
    WS(weakSelf);
    cell.followBlock = ^(NSDictionary *user) {
        [weakSelf followUser:[user[@"userId"] integerValue] like:NO success:^{
            
        } failed:^{
            
        }];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc]initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
