//
//  MessageSetVC.m
//  Open
//
//  Created by mfp on 17/9/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MessageSetVC.h"
#import "UserMessageView.h"
#import "HomeNetService.h"

static NSString *setcellone = @"setcellone";
static NSString *setcelltwo = @"setcelltwo";

@interface MessageSetVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *section1;
    NSArray *section2;
}
@property(nonatomic, strong) HomeNetService *homeService;
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) PushModel *model;

@end

@implementation MessageSetVC

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getPushSeting];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"推送消息设置";
    self.homeService = [[HomeNetService alloc] init];
    section1 = @[@"消息评论",@"点赞消息"];
    section2 = @[@"被关注消息推送",@"活动消息推送",@"系统消息推送"];
    [self setUI];
}

- (void)switchValueChange:(MessageSwitchSetCell *)scell {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:scell];
    NSDictionary *params = nil;
    switch (indexPath.row) {
        case 0://是否接收被关注消息推送: 1-是、0-否
            params = @{@"likePush":@(scell.setSwitch.isOn)};
            break;
        case 1://是否接收活动消息推送: 1-是、0-否
            params = @{@"activityPush":@(scell.setSwitch.isOn)};
            break;
        case 2://是否接收系统消息推送: 1-是、0-否
            params = @{@"systemPush":@(scell.setSwitch.isOn)};
            break;
    }
    [self setPush:params];
}

- (void)setPush:(NSDictionary *)params {
    [self.homeService setUserMsgGet:params success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
    } failure:^(NSError *error, id responseObject) {
        DLog(@"%@",error);
    }];
}

//获取消息设置
- (void)getPushSeting {
    WS(weakSelf);
    [self.homeService getUserMsgGet:^(NSString *requestInfo, id responseObject) {
        weakSelf.model = responseObject;
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

- (void)setUI {
    _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    _tableView.backgroundColor = ColorWithHex(0xf2f2f2);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = ColorWithHex(0xf2f2f2);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 43;
    [_tableView registerClass:[MessageSetCell class] forCellReuseIdentifier:setcellone];
    [_tableView registerClass:[MessageSwitchSetCell class] forCellReuseIdentifier:setcelltwo];
    [self.view addSubview:_tableView];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }else {
        return 3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        MessageSetCell *cell = [tableView dequeueReusableCellWithIdentifier:setcellone];
        cell.textLabel.text = section1[indexPath.row];
        if (indexPath.row == 0) {
            cell.leftLabel.text = self.model.displayscommentPush;
        }else{
            cell.leftLabel.text = self.model.displaystarPush;
        }
        return cell;
    }else{
        MessageSwitchSetCell *cell = [tableView dequeueReusableCellWithIdentifier:setcelltwo];
        cell.textLabel.text = section2[indexPath.row];
        switch (indexPath.row) {
            case 0:
                cell.setSwitch.on = self.model.likePush;
                break;
            case 1:
                cell.setSwitch.on = self.model.activityPush;
                break;
            case 2:
                cell.setSwitch.on = self.model.systemPush;
                break;
        }
        WS(weakSelf);
        cell.switchBlock = ^(MessageSwitchSetCell *scell) {
            [weakSelf switchValueChange:scell];
        };
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        MsgSetVC *vc = [[MsgSetVC alloc] initWithShowBackButton:YES];
        vc.type = indexPath.row;
        vc.model = self.model;
        WS(weakSelf);
        vc.setBlock = ^(PushModel *mode) {
            NSDictionary *params = nil;
            if (indexPath.row == 0) {//评论推送类型: ALL-所有人、FRIENDS-我的好友、NONE-全部拒绝
                params = @{@"commentPush":mode.commentPush};
            }else{//点赞推送类型: ALL-所有人、FRIENDS-我的好友、NONE-全部拒绝
                params = @{@"starPush":mode.starPush};
            }
            [weakSelf setPush:params];
        };
        [self pushVC:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 7.5;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end


@interface MsgSetVC ()

@end

@implementation MsgSetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    PushModel *mode1 = [[PushModel alloc] init];
    mode1.starPush = @"ALL";
    mode1.commentPush = @"ALL";
    PushModel *mode2 = [[PushModel alloc] init];
    mode2.starPush = @"FRIENDS";
    mode2.commentPush = @"FRIENDS";
    PushModel *mode3 = [[PushModel alloc] init];
    mode3.starPush = @"NONE";
    mode3.commentPush = @"NONE";
    [self.dataList addObjectsFromArray:@[mode1,mode2,mode3]];

    if (self.type == MsgSetTypefComment) {
        self.navigationItem.title = @"评论设置";
    }else{
        self.navigationItem.title = @"点赞设置";
    }
    
    self.tableView.backgroundColor = ColorWithHex(0xf2f2f2);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorColor = ColorWithHex(0xf2f2f2);
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.rowHeight = 43;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:setcellone];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:setcellone];
    cell.textLabel.font = Font(14);
    cell.textLabel.textColor = ColorWithHex(0x999999);
    PushModel *mode = self.dataList[indexPath.row];
    cell.tintColor = [mode.displayscommentPush isEqualToString:self.model.displayscommentPush]?LightLemonYellow:BACKGROUND_COLOR;
    cell.accessoryType = [mode.displayscommentPush isEqualToString:self.model.displayscommentPush]?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
    cell.textLabel.text = mode.displaystarPush;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.setBlock) {
        self.setBlock(self.dataList[indexPath.row]);
        [self goBack];
    }
}

@end
