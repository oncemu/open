//
//  CommentVC.m
//  Open
//
//  Created by mfp on 17/8/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CommentVC.h"
#import "UITextView+Placeholder.h"
#import "AlbumNetService.h"

@interface CommentVC ()
@property(nonatomic, strong) UITextView *inputView;
@property(nonatomic, strong) AlbumNetService   *albumService;

@end

@implementation CommentVC

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.inputView becomeFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    self.navigationItem.title = self.isReplyComment?@"回复评论" : @"发表评论";
    [self setBarButtonItemWithImage:nil title:@"发送" isLeft:NO];
    _inputView = [[UITextView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, 500)];
    _inputView.font = Font(14);
    _inputView.textColor = ColorWithHex(0x666666);
    _inputView.placeholder = @"请输入内容";
    _inputView.placeholderColor = ColorWithHex(0x999999);
    [self.view addSubview:_inputView];
}

- (void)rightButtonItemAction {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(self.albumModel.albumId) forKey:@"album_id"];
    NSString *content = self.inputView.text ? self.inputView.text : @"";
    [params setObject:content forKey:@"content"];
    if (self.isReplyComment) {
        [params setObject:@(self.commentModel.commentId) forKey:@"parentId"];
    }
    WS(weakSelf);
    [self.albumService sendAlbumsComments:params success:^(NSString *requestInfo, id responseObject) {
        [KVNProgress showSuccessWithStatus:@"评论成功!"];
        [weakSelf goBack];
    } failure:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:@"评论失败"];
    }];
}

- (AlbumNetService *)albumService {
    if (!_albumService) {
        _albumService = [[AlbumNetService alloc] init];
    }
    return _albumService;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
