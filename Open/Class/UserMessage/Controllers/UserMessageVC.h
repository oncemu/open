//
//  UserMessageVC.h
//  Open
//
//  Created by mfp on 17/9/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//  首页->我的消息

#import "BaseVC.h"
#import "CommentModel.h"

@interface UserMessageVC : BaseVC

@end

//查看系统消息详情
@interface UserMsgDetailVC : BaseVC
@property(nonatomic, strong) CommentModel *model;
@end
