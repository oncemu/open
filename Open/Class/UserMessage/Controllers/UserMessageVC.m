//
//  UserMessageVC.m
//  Open
//
//  Created by mfp on 17/9/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserMessageVC.h"
#import "UserMessageView.h"
#import "MessageSetVC.h"
#import "HomeNetService.h"
#import "MsgCommentListVC.h"
#import "MsgUserVC.h"

static NSString *usermsgcellid = @"usermsgcellid";
static NSString *clearmsgcellid = @"clearmsgcellid";
static NSString *systemmsgcellid = @"systemmsgcellid";

@interface UserMessageVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong) HomeNetService *homeService;
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *commentMsgList;//评论
@property(nonatomic, strong) NSMutableArray *favoriteMsgList;//已赞
@property(nonatomic, strong) NSMutableArray *followMsgList;//关注我的
@property(nonatomic, strong) NSMutableArray *systemMsgList;//系统消息
@property(nonatomic, strong) NSArray *titleList;
@end

@implementation UserMessageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的消息";
    self.homeService = [[HomeNetService alloc] init];
    [self setBarButtonItemWithImage:getImage(@"setting") title:nil isLeft:NO];
    self.titleList = @[@"评论",@"已赞",@"关注我的"];
    self.systemMsgList = [NSMutableArray array];
    self.commentMsgList = [NSMutableArray array];
    self.favoriteMsgList = [NSMutableArray array];
    self.followMsgList = [NSMutableArray array];
    [self setUI];
    [self loadMessageData];
}

- (void)setUI {
    _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    _tableView.backgroundColor = ColorWithHex(0xf2f2f2);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = ColorWithHex(0xf2f2f2);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:[UserMsgCell class] forCellReuseIdentifier:usermsgcellid];
    [_tableView registerClass:[ClearMsgCell class] forCellReuseIdentifier:clearmsgcellid];
    [_tableView registerClass:[SystemMsgCell class] forCellReuseIdentifier:systemmsgcellid];
    [self.view addSubview:_tableView];
}

- (void)rightButtonItemAction {
    MessageSetVC *vc = [[MessageSetVC alloc] initWithShowBackButton:YES];
    [self pushVC:vc animated:YES];
}

- (void)loadMessageData {
    WS(weakSelf);
    //首页 系统消息列表accessToken
    [self.homeService homeSystemMsg:^(NSString *requestInfo, id responseObject) {
        [weakSelf.systemMsgList addObjectsFromArray:responseObject];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error, id responseObject) {
        
    }];
    //首页消息列表 评论 accessToken
    [self.homeService getHomeMsgMore:NO success:^(NSString *requestInfo, id responseObject) {
        [weakSelf.commentMsgList addObjectsFromArray:responseObject];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error, id responseObject) {
        
    }];
    
    //首页消息列表 关注 accessToken
    [self.homeService getUserLikeMsgMore:NO success:^(NSString *requestInfo, id responseObject) {
        [weakSelf.followMsgList addObjectsFromArray:responseObject];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error, id responseObject) {
        
    }];
    
    //首页消息列表 点赞 accessToken
    [self.homeService userStarMsgMore:NO success:^(NSString *requestInfo, id responseObject) {
        [weakSelf.favoriteMsgList addObjectsFromArray:responseObject];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error, id responseObject) {
        
    }];
    
}

//清除消息
- (void)clearMsg {
    [self.systemMsgList removeAllObjects];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    }else {
        return self.systemMsgList.count + 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        UserMsgCell *cell = [tableView dequeueReusableCellWithIdentifier:usermsgcellid];
        cell.textLabel.text = self.titleList[indexPath.row];
        switch (indexPath.row) {
            case 0:
                if (self.commentMsgList.count > 0) {
                    cell.countLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.commentMsgList.count];
                }else{
                    cell.countLabel.text = @"";
                }
                break;
            case 1:
                if (self.favoriteMsgList.count > 0) {
                    cell.countLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.favoriteMsgList.count];
                }else{
                    cell.countLabel.text = @"";
                }                break;
            case 2:
                if (self.followMsgList.count > 0) {
                    cell.countLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.followMsgList.count];
                }else{
                    cell.countLabel.text = @"";
                }                break;
        }
        return cell;
    }else{
        if (indexPath.row == 0) {
            ClearMsgCell *cell = [tableView dequeueReusableCellWithIdentifier:clearmsgcellid];
            WS(weakSelf);
            cell.clearMsg = ^() {
                [weakSelf clearMsg];
            };
            return cell;
        }else{
            SystemMsgCell *cell = [tableView dequeueReusableCellWithIdentifier:systemmsgcellid];
            CommentModel *mode = self.systemMsgList[indexPath.row - 1];
            cell.contentLabel.text = mode.content;
            return cell;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
            {
                MsgCommentListVC *vc = [[MsgCommentListVC alloc] initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
                [self pushVC:vc animated:YES];
            }
                break;
            case 1:
            {
                MsgUserVC *vc = [[MsgUserVC alloc] initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
                vc.type  = MsgUserTypeFavorite;
                [self pushVC:vc animated:YES];
            }
                break;
            case 2:
            {
                MsgUserVC *vc = [[MsgUserVC alloc] initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
                vc.type  = MsgUserTypeFollowMe;
                [self pushVC:vc animated:YES];
            }
                break;
        }
    }
    if (indexPath.section == 1 && indexPath.row > 0) {
        UserMsgDetailVC *vc = [[UserMsgDetailVC alloc] initWithShowBackButton:YES];
        vc.model = self.systemMsgList[indexPath.row - 1];
        [self pushVC:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 7.5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1 && indexPath.row >0) {
        return 180;
    }
    return 43;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end


@interface UserMsgDetailVC ()
@property(nonatomic, strong) UILabel *contentLabel;
@end

@implementation UserMsgDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的消息";
    self.view.backgroundColor = WhiteColor;
    self.contentLabel = [ViewFactory getLabel:self.view.bounds font:Font(14) textColor:ColorWithHex(0x999999) text:self.model.content];
    _contentLabel.backgroundColor = WhiteColor;
    [self.view addSubview:_contentLabel];
}

@end
