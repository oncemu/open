//
//  CommentVC.h
//  Open
//
//  Created by mfp on 17/8/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//  发表评论

#import "BaseVC.h"
#import "AlbumModel.h"
#import "CommentModel.h"

@interface CommentVC : BaseVC
@property(nonatomic, assign)BOOL isReplyComment; //YES:回复评论，NO:发表评论，default:NO
@property(nonatomic, strong) AlbumModel   *albumModel;
@property(nonatomic, strong) CommentModel *commentModel;

@end


