//
//  MallView.m
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MallView.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "MallRightCell.h"

@implementation MallView

@end


@interface LeftView ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong) UITableView *tableView;
@end
@implementation LeftView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.list = [NSMutableArray array];
        _tableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorColor = ColorWithHex(0xf2f2f2f2);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        [_tableView registerClass:[LeftCell class] forCellReuseIdentifier:NSStringFromClass([LeftCell class])];
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = 57;
        [self addSubview:_tableView];
    }
    return self;
}

- (void)reloadList:(NSArray *)categories {
    [self.list removeAllObjects];
    [self.list addObjectsFromArray:categories];
    [self.tableView reloadData];
    if (categories.count > 0) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
        if (self.selectIndexBlock) {
            self.selectIndexBlock(self.list[0]);
        }
    }
}

#pragma mark - UITableViewDelegate,UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LeftCell *cell = (LeftCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LeftCell class])];
    cell.model = self.list[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectIndexBlock) {
        self.selectIndexBlock(self.list[indexPath.row]);
    }
}

@end

@implementation LeftCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.separatorInset = UIEdgeInsetsZero;
        self.backgroundColor = WhiteColor;
        self.textLabel.font = Font(14);
        self.textLabel.textColor = ColorWithHex(0x333333);
    }
    return self;
}
- (void)setModel:(MallCategoryModel *)model {
    _model = model;
    self.imageView.image = getImage(model.categoryName);
    self.textLabel.text = model.categoryName;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CM(8, (self.frameSizeHeight - 19)/2, 19, 19);
    self.textLabel.frame = CM(CGRectGetMaxX(self.imageView.frame)+4, 2, self.frameSizeWidth - 31, self.frameSizeHeight - 4);
}

@end

@interface RightView ()<CHTCollectionViewDelegateWaterfallLayout,UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) NSMutableArray *itemList;
@property(nonatomic, strong) UIButton *noDataButton;
@end
@implementation RightView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.itemList = [NSMutableArray array];
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc]init];
        layout.columnCount = 2;
        layout.minimumColumnSpacing = 3;
        layout.minimumContentHeight = 0;
        layout.minimumInteritemSpacing = 3;
        layout.sectionInset = UIEdgeInsetsMake(5, 4, 0, 4);
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CM(0, 64, frame.size.width, frame.size.height - 64) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        [_collectionView registerClass:[MallRightCell class] forCellWithReuseIdentifier:NSStringFromClass([MallRightCell class])];
        [self addSubview:_collectionView];
    }
    return self;
}

- (void)setHeader:(MJRefreshNormalHeader *)header {
    _header = header;
    self.collectionView.mj_header = header;
}

- (void)setFooter:(MJRefreshAutoStateFooter *)footer {
    _footer = footer;
    self.collectionView.mj_footer = footer;
}

- (void)reloadList:(NSArray *)list more:(BOOL)more {
    if (!more) {
        [self.itemList removeAllObjects];
    }
    [self.itemList addObjectsFromArray:list];
    [self.collectionView reloadData];
    self.noDataButton.hidden = self.itemList.count > 0;
    [self.header endRefreshing];
    [self.footer endRefreshing];
    if (list.count < 10) {
        self.footer.hidden = YES;
    }else{
        self.footer.hidden = NO;
    }
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.itemList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MallRightCell *cell = (MallRightCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MallRightCell class]) forIndexPath:indexPath];
    cell.model = self.itemList[indexPath.item];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return Size((SCREEN_HEIGHT - 120 - 11)/2, (SCREEN_HEIGHT - 120 - 11)/2 + 52);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectAlbum) {
        self.selectAlbum(self.itemList[indexPath.item]);
    }
}

- (void)reloadData {
    
}

- (UIButton *)noDataButton {
    if (!_noDataButton) {
        _noDataButton = [ViewFactory getButton:CM(0, 0, self.frameSizeWidth, self.frameSizeHeight - 100) font:Font(16) selectTitle:@"没有数据" norTitle:@"没有数据" sColor:ColorWithHex(0x999999) norColor:ColorWithHex(0x999999) target:self action:@selector(reloadData)];
        [self addSubview:_noDataButton];
    }
    return _noDataButton;
}

@end
