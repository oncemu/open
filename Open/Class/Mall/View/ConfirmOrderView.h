//
//  ConfirmOrderView.h
//  Open
//
//  Created by mfp on 17/9/21.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MallGoodsModel.h"
#import "UsualAddressModel.h"

@interface ConfirmOrderView : UIView

@end

//收获地址
@interface ShippingAddressCell : UITableViewCell
@property(nonatomic, strong) UsualAddressModel *model;
@end

@interface ConfirmOrderCell : UITableViewCell

@property(nonatomic, strong) UILabel *count;
@property(nonatomic, strong) UILabel *messageLabel;//买家留言
@property(nonatomic, weak) MallGoodsModel *model;
@property(nonatomic, copy) void (^countBlock)(NSInteger count);

@end
