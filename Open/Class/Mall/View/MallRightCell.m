//
//  MallRightCell.m
//  Open
//
//  Created by onceMu on 2017/8/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MallRightCell.h"

@interface MallRightCell ()

@property (nonatomic, strong) UIImageView *bgImageVIew;
@property (nonatomic, strong) UILabel *lblName;
@property (nonatomic, strong) UILabel *lblPrice;

@end

@implementation MallRightCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _bgImageVIew = [[UIImageView alloc]init];
    _bgImageVIew.backgroundColor = [UIColor colorWithHex:0xf8f8f8];
    _bgImageVIew.layer.masksToBounds = YES;
    [self.contentView addSubview:_bgImageVIew];
    [_bgImageVIew mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(self.contentView.mas_width);
    }];

    _lblName = [[UILabel alloc]init];
    _lblName.font = [UIFont systemFontOfSize:14];
    _lblName.textColor = [UIColor colorWithHex:0x010101];
    [self.contentView addSubview:_lblName];
    [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView.mas_left).offset(8);
        make.top.mas_equalTo(self.bgImageVIew.mas_bottom).offset(5);
        make.right.mas_equalTo(self.bgImageVIew.mas_right).offset(-8);
        make.height.mas_equalTo(14);
    }];
    _lblName.text = @"A4 210*220";

    _lblPrice = [[UILabel alloc]init];
    _lblPrice.font = [UIFont systemFontOfSize:12.0f];
    _lblPrice.textColor = [UIColor colorWithHex:0xb3b3b3];
    [self.contentView addSubview:_lblPrice];
    [_lblPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView.mas_left).offset(8);
        make.top.mas_equalTo(self.lblName.mas_bottom).offset(5);
        make.right.mas_equalTo(self.contentView.mas_right).offset(-8);
        make.height.mas_equalTo(12);
    }];


}

- (void)setModel:(MallGoodsModel *)model {
    _model = model;
    NSString *url = [model.photos firstObject];
    [self.bgImageVIew sd_setImageWithURL:[NSURL URLWithString:url]];
    NSString *text =[NSString stringWithFormat:@"¥ %.2f",model.price];
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:text];
    [att setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHex:0xfedf03]} range:NSMakeRange(0, 1)];
    [att setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHex:0xb3b3b3]} range:NSMakeRange(1, text.length-1)];
    _lblPrice.attributedText = att;
    self.lblName.text = model.itemName;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _bgImageVIew.image = nil;
    _lblName.text = nil;
    _lblPrice.text = nil;
}

@end
