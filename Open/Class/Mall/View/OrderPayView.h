//
//  OrderPayView.h
//  Open
//
//  Created by mfp on 17/9/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MallGoodsModel.h"
#import "OrderModel.h"

@interface OrderPayView : UIView

@end

@interface OrderPayCell : UITableViewCell

@property(nonatomic, strong) DetailModel *model;

@end

@interface PayModelCell : UITableViewCell

@end

@interface OrderPayFooter : UITableViewHeaderFooterView
@property(nonatomic, strong) UIButton *payButton;
@property(nonatomic, copy) void (^payBlock)();

@end

@interface PaySuccessHeader : UICollectionReusableView
@property(nonatomic, strong) UILabel *priceLable;
@property(nonatomic, copy) void (^headerBlock)(BOOL check);//yes:查看订单 ， NO:返回首页

@end
