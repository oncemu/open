//
//  ConfirmOrderView.m
//  Open
//
//  Created by mfp on 17/9/21.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "ConfirmOrderView.h"

@implementation ConfirmOrderView

@end

@implementation ShippingAddressCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = WhiteColor;
        self.accessoryView = [[UIImageView alloc] initWithImage:getImage(@"pic_scanzan")];
        self.textLabel.text = @"请填写你的收获地址";
        self.textLabel.textColor = ColorWithHex(0x333333);
        self.imageView.image = getImage(@"cart_local");
    }
    return self;
}

- (void)setModel:(UsualAddressModel *)model {
    _model = model;
    if (model) {
        self.textLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@",model.province,model.city,model.region,model.street];
    }
}
@end


@interface ConfirmOrderCell ()<UITextViewDelegate>

@property(nonatomic, strong) UIImageView *itemImage;
@property(nonatomic, strong) UILabel *itemName;
@property(nonatomic, strong) UILabel *itemPrice;
@property(nonatomic, strong) UILabel *itemCount;
@property(nonatomic, strong) UIButton *plusBtn;
@property(nonatomic, strong) UIButton *minusBtn;
@property(nonatomic, strong) UILabel *deliveryLabel;//配送方式
@property(nonatomic, strong) UILabel *delivery;
@property(nonatomic, strong) UITextView *inputView;

@end

@implementation ConfirmOrderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = WhiteColor;
        self.itemImage = [ViewFactory getImageView:CGRectZero image:nil];
        [self.contentView addSubview:self.itemImage];
        [self.itemImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self.contentView).offset(19);
            make.size.mas_equalTo(Size(55, 55));
        }];
        self.itemName = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@"商品名称"];
        [self.contentView addSubview:_itemName];
        [self.itemName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.itemImage.mas_right).offset(15);
            make.top.equalTo(self.contentView).offset(25);
            make.right.equalTo(self.contentView).offset(-19);
            make.height.mas_equalTo(17);
        }];
        
        self.itemPrice = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@""];
        [self.contentView addSubview:_itemPrice];
        [self.itemPrice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.itemName.mas_left);
            make.top.equalTo(self.itemName.mas_bottom).offset(10);
            make.bottom.equalTo(self.itemImage.mas_bottom).offset(-5);
            make.right.equalTo(self.contentView).offset(-70);
        }];
        
        UIView *line1 = [[UIView alloc] init];
        line1.backgroundColor = BACKGROUND_COLOR;
        [self.contentView addSubview:line1];
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.itemImage.mas_left);
            make.right.equalTo(self.contentView).offset(-19);
            make.height.mas_equalTo(0.5);
            make.top.equalTo(self.contentView).offset(92);
        }];
        
        self.itemCount = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@"购买数量"];
        [self.contentView addSubview:_itemCount];
        [self.itemCount mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(line1);
            make.top.equalTo(line1.mas_bottom);
            make.size.mas_equalTo(Size(100, 44));
        }];
        self.minusBtn = [ViewFactory getButton:CGRectZero font:Font(14) selectTitle:@"-" norTitle:@"-" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(countButtonClick:)];
        [self.contentView addSubview:_minusBtn];
        [self.minusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-19);
            make.top.equalTo(line1.mas_bottom);
            make.size.mas_equalTo(Size(25, 44));
        }];
        
        self.count = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@""];
        _count.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_count];
        [self.count mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.minusBtn.mas_left);
            make.top.bottom.equalTo(self.minusBtn);
            make.width.mas_equalTo(30);
        }];
        
        self.plusBtn = [ViewFactory getButton:CGRectZero font:Font(14) selectTitle:@"+" norTitle:@"+" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(countButtonClick:)];
        [self.contentView addSubview:_plusBtn];
        [self.plusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.count.mas_left);
            make.top.bottom.equalTo(self.count);
            make.size.mas_equalTo(Size(25, 44));
        }];
        
        UIView *line2 = [[UIView alloc] init];
        line2.backgroundColor = BACKGROUND_COLOR;
        [self.contentView addSubview:line2];
        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.itemImage.mas_left);
            make.right.equalTo(self.contentView).offset(-19);
            make.height.mas_equalTo(0.5);
            make.top.equalTo(self.itemCount.mas_bottom);
        }];
        
        self.deliveryLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@"配送方式"];
        [self.contentView addSubview:_deliveryLabel];
        [self.deliveryLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(line2);
            make.top.equalTo(line2.mas_bottom);
            make.size.mas_equalTo(Size(100, 44));
        }];
        
        self.delivery = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@""];
        _delivery.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_delivery];
        [self.delivery mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(line2.mas_right);
            make.top.bottom.equalTo(self.deliveryLabel);
            make.left.equalTo(self.deliveryLabel.mas_right);
        }];
        
        UIView *line3 = [[UIView alloc] init];
        line3.backgroundColor = BACKGROUND_COLOR;
        [self.contentView addSubview:line3];
        [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.itemImage.mas_left);
            make.right.equalTo(self.contentView).offset(-19);
            make.height.mas_equalTo(0.5);
            make.top.equalTo(self.deliveryLabel.mas_bottom);
        }];
        
        self.messageLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@"买家留言"];
        [self.contentView addSubview:_messageLabel];
        [self.messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(line3);
            make.top.equalTo(line3.mas_bottom);
            make.size.mas_equalTo(Size(100, 44));
        }];
        
        UIView *line4 = [[UIView alloc] init];
        line4.backgroundColor = BACKGROUND_COLOR;
        [self.contentView addSubview:line4];
        [line4 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.itemImage.mas_left);
            make.right.equalTo(self.contentView).offset(-19);
            make.height.mas_equalTo(0.5);
            make.top.equalTo(self.messageLabel.mas_bottom);
        }];
        
        self.inputView = [[UITextView alloc] init];
        _inputView.layer.borderColor = BACKGROUND_COLOR.CGColor;
        _inputView.layer.borderWidth = 0.5f;
        _inputView.layer.masksToBounds = YES;
        _inputView.delegate = self;
        [self.contentView addSubview:_inputView];
        [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(line4);
            make.top.equalTo(line4.mas_bottom);
            make.bottom.equalTo(self.contentView.mas_bottom).offset(-10);
            make.right.equalTo(self.contentView).offset(-19);
        }];
    }
    return self;
}

- (void)setModel:(MallGoodsModel *)model {
    _model = model;
    self.itemName.text = model.itemName;
    self.itemPrice.text = [NSString stringWithFormat:@"¥%.2f",model.price];
    self.delivery.text = [NSString stringWithFormat:@"快递%.2f元",model.expressFee];
    id image = [model.itemPhotos firstObject];
    if ([image isKindOfClass:[NSString class]]) {
        [self.itemImage sd_setImageWithURL:[NSURL URLWithString:image]];
    }else if ([image isKindOfClass:[UIImage class]]){
        self.itemImage.image = image;
    }
    self.count.text = [NSString stringWithFormat:@"%ld",model.num];
}

- (void)countButtonClick:(UIButton *)sender {
    NSInteger count = self.count.text.integerValue;
    if ([sender isEqual:self.plusBtn]) {
        count += 1;
    }else{
        if (count>1) {
            count -= 1;
        }else{
            return;
        }
    }
    self.count.text = [NSString stringWithFormat:@"%ld",count];
    self.model.num = count;
    if (self.countBlock) {
        self.countBlock(count);
    }
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    self.model.message = textView.text;
}

@end
