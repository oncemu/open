//
//  AlbumDetailView.h
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MallGoodsModel.h"

//相册详情顶部
@interface AlbumDetailView : UIView
@property(nonatomic, strong) MallGoodsModel *mode;
@end

@interface AlbumInfoCell : UITableViewCell

@end

@interface AlbumTipView : UITableViewCell
@property(nonatomic, strong) UILabel *tipLabel;

@end

@interface AlbumDetailHeader : UITableViewHeaderFooterView

@end
