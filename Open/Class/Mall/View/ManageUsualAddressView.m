//
//  ManageUsualAddressView.m
//  Open
//
//  Created by mfp on 17/9/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "ManageUsualAddressView.h"

@implementation ManageUsualAddressView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

@end

@interface ManageUsualAddressCell ()<UITextViewDelegate>
@property(nonatomic, strong) UIImageView *rightImage;
@end

@implementation ManageUsualAddressCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.textLabel.font = Font(15);
        self.textLabel.textColor = ColorWithHex(0x666666);
    }
    return self;
}

- (void)setMode:(AddressCellMode)mode {
    _mode = mode;
    switch (mode) {
        case AddressCellModeName:
            self.accessoryView = self.addressText;
            break;
        case AddressCellModeMobile:
            self.accessoryView = self.addressText;
            self.addressText.keyboardType = UIKeyboardTypePhonePad;
            break;
        case AddressCellModeProvince:
        case AddressCellModeCity:
        case AddressCellModeSubLocality:
            self.accessoryView = self.rightImage;
            [self rightLabel];
            break;
        case AddressCellModeDetail:
            self.accessoryView = self.addressText;
            break;
        case AddressCellModeDefault:
            self.accessoryView = self.defaultSwitch;
            break;
    }
}

- (void)setAddressModel:(UsualAddressModel *)addressModel {
    _addressModel = addressModel;
    switch (self.mode) {
        case AddressCellModeName:
            self.addressText.text = self.addressModel.consignee;
            break;
        case AddressCellModeMobile:
            self.addressText.text = self.addressModel.cellphone;
            break;
        case AddressCellModeProvince:
            self.rightLabel.text = self.addressModel.province;
            break;
        case AddressCellModeCity:
            self.rightLabel.text = self.addressModel.city;
            break;
        case AddressCellModeSubLocality:
            self.rightLabel.text = self.addressModel.region;
            break;
        case AddressCellModeDetail:
            self.addressText.text = self.addressModel.street;
            break;
        case AddressCellModeDefault:
            self.defaultSwitch.on = self.addressModel.isDefault;
            break;
    }
}

- (void)valueChanged:(UISwitch *)as {
    self.addressModel.isDefault = as.on;
}

- (UIImageView *)rightImage {
    if (!_rightImage) {
        _rightImage = [[UIImageView alloc] initWithImage:getImage(@"pic_scanzan")];
    }
    return _rightImage;
}

- (UITextView *)addressText {
    if (!_addressText) {
        _addressText = [[UITextView alloc] initWithFrame:CGRectZero];
        _addressText.textAlignment = NSTextAlignmentRight;
        _addressText.font = Font(14);
        _addressText.textColor = ColorWithHex(0x666666);
        _addressText.delegate = self;
    }
    return _addressText;
}

- (UILabel *)rightLabel {
    if (!_rightLabel) {
        _rightLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:nil];
        _rightLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_rightLabel];
    }
    return _rightLabel;
}

- (UISwitch *)defaultSwitch {
    if (!_defaultSwitch) {
        _defaultSwitch = [[UISwitch alloc] init];
        [_defaultSwitch addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
        [_defaultSwitch setOn:YES];
    }
    return _defaultSwitch;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = CM(18, 0, 100, 44);
    _addressText.frame = CM(110, 2, self.frameSizeWidth - 128, self.frameSizeHeight - 4);
    _rightLabel.frame = CM(110, 0, self.frameSizeWidth - 110 - 45 - 8, self.frameSizeHeight);
}

#pragma mark - textview delegate 
- (void)textViewDidEndEditing:(UITextView *)textView {
    switch (self.mode) {
        case AddressCellModeName:
            self.addressModel.consignee = textView.text;
            break;
        case AddressCellModeMobile:
            self.addressModel.cellphone = textView.text;
            break;
        case AddressCellModeDetail:
            self.addressModel.street = textView.text;
            break;
        default:
            break;
    }
}

@end

@interface AddressPicker ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property(nonatomic, strong) UIPickerView *picker;
@property(nonatomic, strong) NSArray *addressList;
@property(nonatomic, assign) NSInteger provinceSelectIndex;
@property(nonatomic, assign) NSInteger citySelectIndex;
@property(nonatomic, assign) NSInteger subLocalitySelectIndex;
@property(nonatomic, strong) UIView *selectBar;
@property(nonatomic, strong) UIButton *selectBtn;
@property(nonatomic, strong) AddressModel *selectModel;

@end

@implementation AddressPicker

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithHex:0x000000 alpha:0.5];
        _selectBar = [[UIView alloc] initWithFrame:CM(0, SCREEN_HEIGHT - 240, SCREEN_WIDTH, 40)];
        _selectBar.backgroundColor = WhiteColor;
        [self addSubview:_selectBar];
        
        _selectBtn = [ViewFactory getButton:CM(_selectBar.frameSizeWidth - 60, 0, 50, 40) font:Font(14) selectTitle:@"选择" norTitle:@"选择" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(didSelect)];
        [_selectBar addSubview:_selectBtn];
        
        _picker = [[UIPickerView alloc] initWithFrame:CM(0, CGRectGetMaxY(_selectBar.frame), frame.size.width, 200)];
        _picker.delegate = self;
        _picker.dataSource = self;
        _picker.backgroundColor = WhiteColor;
        [self addSubview:_picker];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
        [self addGestureRecognizer:tap];
        
        NSData *JSONData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"address" ofType:@"json"]];
        NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingAllowFragments error:nil];
        
        self.addressList = [NSArray yy_modelArrayWithClass:[AddressModel class] json:dataArray];
    
    }
    return self;
}

- (void)setKind:(AddressPickerKind)kind {
    _kind = kind;
    [self.picker reloadAllComponents];
}

- (void)didSelect {
    if (self.didSelectBlock) {
        self.didSelectBlock(self.selectModel);
        [self dismiss];
    }
}

- (void)show {
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    [window addSubview:self];
    self.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1;
        self.selectBar.frameOriginY = SCREEN_HEIGHT - 240;
        self.picker.frameOriginY = SCREEN_HEIGHT - 200;
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
        self.selectBar.frameOriginY = SCREEN_HEIGHT ;
        self.picker.frameOriginY = SCREEN_HEIGHT + 40;
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

#pragma mark - UIPickerViewDelegate,UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (self.kind) {
        case AddressPickerKindProvince:
            return self.addressList.count;
            break;
        case AddressPickerKindCity:
        {
            AddressModel *mode = self.addressList[self.provinceSelectIndex];
            return mode.sub.count;
        }
            break;
        case AddressPickerKindSubLocality:
        {
            AddressModel *mode = self.addressList[self.provinceSelectIndex];
            AddressModel *pmode = mode.sub[self.citySelectIndex];
            return pmode.sub.count;
        }
            break;
        default:
            break;
    }
    return 0;
}

// 返回选中的行
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    AddressModel *model;
    switch (self.kind) {
        case AddressPickerKindProvince:
        {
            model = self.addressList[row];
            self.provinceSelectIndex = row;
        }
            break;
        case AddressPickerKindCity:
        {
            AddressModel *mode = self.addressList[self.provinceSelectIndex];
            model = mode.sub[row];
            self.citySelectIndex = row;
        }
            break;
        case AddressPickerKindSubLocality:
        {
            AddressModel *mode = self.addressList[self.provinceSelectIndex];
            AddressModel *cmode = mode.sub[self.citySelectIndex];
            model = cmode.sub[row];
            self.subLocalitySelectIndex = row;
        }
            break;
        default:
            break;
    }
    self.selectModel = model;
}

//返回当前行的内容,此处是将数组中数值添加到滚动的那个显示栏上
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    AddressModel *model;
    switch (self.kind) {
        case AddressPickerKindProvince:
        {
            model = self.addressList[row];
        }
            break;
        case AddressPickerKindCity:
        {
            AddressModel *mode = self.addressList[self.provinceSelectIndex];
            model = mode.sub[row];
        }
            break;
        case AddressPickerKindSubLocality:
        {
            AddressModel *mode = self.addressList[self.provinceSelectIndex];
            AddressModel *cmode = mode.sub[self.citySelectIndex];
            model = cmode.sub[row];
        }
            break;
        default:
            break;
    }
    return model.name;
}

@end

@interface UsualAddressCell ()
@property(nonatomic, strong) UILabel *nameLabel;
@property(nonatomic, strong) UILabel *mobileLabel;
@property(nonatomic, strong) UILabel *detailLabel;
@property(nonatomic, strong) UIButton *defaultBtn;
@property(nonatomic, strong) UIButton *editBtn;
@property(nonatomic, strong) UIButton *deleteBtn;
@end

@implementation UsualAddressCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = WhiteColor;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _mobileLabel = [ViewFactory getLabel:CGRectZero font:Font(13) textColor:ColorWithHex(0x666666) text:nil];
        _mobileLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_mobileLabel];
        [_mobileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.right.equalTo(self.contentView).offset(-15);
            make.size.mas_equalTo(Size(100, 20));
        }];
        _nameLabel = [ViewFactory getLabel:CGRectZero font:Font(13) textColor:ColorWithHex(0x666666) text:nil];
        [self.contentView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(15);
            make.top.equalTo(self.contentView);
            make.right.equalTo(self.mobileLabel.mas_left);
            make.height.equalTo(self.mobileLabel.mas_height);
        }];
        
        _detailLabel = [ViewFactory getLabel:CGRectZero font:Font(13) textColor:ColorWithHex(0x666666) text:nil];
        [self.contentView addSubview:_detailLabel];
        [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(15);
            make.right.equalTo(self.contentView).offset(-15);
            make.top.equalTo(self.nameLabel.mas_bottom).offset(2);
            make.height.mas_equalTo(20);
        }];

    }
    return self;
}

@end
