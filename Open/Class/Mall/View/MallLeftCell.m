//
//  MallLeftCell.m
//  Open
//
//  Created by onceMu on 2017/8/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MallLeftCell.h"

@interface MallLeftCell ()

@property (nonatomic, strong) UILabel *lblName;
@property (nonatomic, strong) UIImageView *logImageView;
@property (nonatomic, strong) UIView *rightLine;
@property (nonatomic, strong) UIView *spaceLine;
@property (nonatomic, strong) UIView *topLine;
@property (nonatomic, strong) UIView *bottomView;

@end

@implementation MallLeftCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _topLine = [[UIView alloc]init];
    [self.contentView addSubview:_topLine];
    _topLine.hidden = YES;
    _topLine.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [_topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.top.mas_equalTo(self.mas_top);
        make.height.mas_equalTo(0.5);
    }];

    _bottomView = [[UIView alloc]init];
    [self.contentView addSubview:_bottomView];
    _bottomView.hidden = YES;
    _bottomView.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];

    _rightLine = [[UIView alloc]init];
    [self.contentView addSubview:_rightLine];
    _rightLine.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [_rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-0.5);
        make.top.mas_equalTo(self.mas_top);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.width.mas_equalTo(0.5);
    }];

    _spaceLine = [[UIView alloc]init];
    [self.contentView addSubview:_spaceLine];
    _spaceLine.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [_spaceLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-5);
        make.left.mas_equalTo(self.mas_left).offset(5);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];

    _logImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_logImageView];
    [_logImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView.mas_left).offset(12);
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(24, 24));
    }];

    _lblName = [[UILabel alloc]init];
    _lblName.textColor = [UIColor colorWithHex:0xa3a3a3];
    _lblName.font = [UIFont systemFontOfSize:14.0f];
    [self.contentView addSubview:_lblName];
    [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.logImageView.mas_right).offset(5);
        make.right.mas_equalTo(self.mas_right);
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
        make.height.mas_equalTo(14);
    }];
}

- (void)assignIndex:(MallCategoryModel *)model {
    self.logImageView.image = getImage(model.categoryName);
    self.lblName.text = model.categoryName;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _logImageView.image = nil;
    _lblName.text = nil;
}



@end
