//
//  AlbumDetailView.m
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "AlbumDetailView.h"

@interface AlbumDetailView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic, strong) UIView *bgView;
@property(nonatomic, strong) UIImageView *displayView;;
@property(nonatomic, strong) UICollectionView *collectionView;
@end

@implementation AlbumDetailView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.bgView = [[UIView alloc] initWithFrame:CM(18, 0, frame.size.width - 36, frame.size.height - (SCREEN_WIDTH - 36 - 24)/4)];
        _bgView.backgroundColor = BACKGROUND_COLOR;
        [self addSubview:_bgView];
        
        self.displayView = [[UIImageView alloc] initWithFrame:CM(18, 0, frame.size.width - 36, frame.size.height - (SCREEN_WIDTH - 36 - 24)/4)];
        _displayView.contentMode = UIViewContentModeScaleAspectFill;
        _displayView.layer.masksToBounds = YES;
        
        [self addSubview:_displayView];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.itemSize = Size((SCREEN_WIDTH - 36 - 24)/4, (SCREEN_WIDTH - 36 - 24)/4);
        layout.sectionInset = UIEdgeInsetsMake(0, 18, 0, 18);
        _collectionView = [[UICollectionView alloc]initWithFrame:CM(0, CGRectGetMaxY(_displayView.frame) + 13, frame.size.width, self.frameSizeHeight - CGRectGetMaxY(_displayView.frame) - 13) collectionViewLayout:layout];
        _collectionView.backgroundColor = WhiteColor;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
        [self addSubview:_collectionView];
    }
    return self;
}

- (void)setMode:(MallGoodsModel *)mode {
    _mode = mode;
    NSString *url = [mode.photos firstObject];
    [self.displayView sd_setImageWithURL:[NSURL URLWithString:url]];
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.mode.photos.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class]) forIndexPath:indexPath];
    UIImageView *view = [cell.contentView viewWithTag:100];
    if (!view) {
        view = [[UIImageView alloc] initWithFrame:CM(0, 0, (SCREEN_WIDTH - 36 - 24)/4, (SCREEN_WIDTH - 36 - 24)/4)];
        
        [cell.contentView addSubview:view];
    }
    NSString *url = self.mode.photos[indexPath.item];
    [view sd_setImageWithURL:[NSURL URLWithString:url]];
    return cell;
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    return Size((SCREEN_HEIGHT - 120 - 11)/2, (SCREEN_HEIGHT - 120 - 11)/2 + 52);
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.displayView.image = nil;
    NSString *url = self.mode.photos[indexPath.item];
    [self.displayView sd_setImageWithURL:[NSURL URLWithString:url]];
}

@end

@implementation AlbumInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

@end


@implementation AlbumTipView

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.tipLabel = [ViewFactory getLabel:CGRectZero font:Font(16) textColor:ColorWithHex(0x666666) text:nil];
        _tipLabel.numberOfLines = 0;
        _tipLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.contentView addSubview:_tipLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.tipLabel.frame = CM(18, 5, self.frameSizeWidth - 36, self.frameSizeHeight-10);
}

@end

@implementation AlbumDetailHeader

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.backgroundColor = WhiteColor;
        self.textLabel.font = Font(15);
        self.textLabel.textColor = ColorWithHex(0x333333);
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = CM(19, 0, self.frameSizeWidth, self.frameSizeHeight);
}

@end
