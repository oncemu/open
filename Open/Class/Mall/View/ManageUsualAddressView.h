//
//  ManageUsualAddressView.h
//  Open
//
//  Created by mfp on 17/9/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"
#import "UsualAddressModel.h"

typedef NS_ENUM(NSInteger,AddressCellMode) {
    AddressCellModeName = 0,
    AddressCellModeMobile = 1,
    AddressCellModeProvince = 2,
    AddressCellModeCity = 3,
    AddressCellModeSubLocality = 4,
    AddressCellModeDetail = 5,
    AddressCellModeDefault = 6
};

typedef NS_ENUM(NSInteger,AddressPickerKind) {
    AddressPickerKindProvince = 2,
    AddressPickerKindCity = 3,
    AddressPickerKindSubLocality = 4
};

@interface ManageUsualAddressView : UIView

@end

@interface ManageUsualAddressCell : UITableViewCell
@property(nonatomic, strong) UITextView *addressText;
@property(nonatomic, strong) UILabel *rightLabel;
@property(nonatomic, strong) UISwitch *defaultSwitch;
@property(nonatomic, assign) AddressCellMode mode;
@property(nonatomic, weak) UsualAddressModel *addressModel;
@end

@interface AddressPicker : UIView
@property(nonatomic, assign) AddressPickerKind kind;
@property(nonatomic, copy) void (^didSelectBlock)(AddressModel *mode);

- (void)show;
@end

@interface UsualAddressCell : UITableViewCell

@property(nonatomic, strong) UsualAddressModel *model;

@end
