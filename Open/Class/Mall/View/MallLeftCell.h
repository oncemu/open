//
//  MallLeftCell.h
//  Open
//
//  Created by onceMu on 2017/8/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MallCategoryModel.h"

@interface MallLeftCell : UICollectionViewCell

- (void)assignIndex:(MallCategoryModel *)model;

@end
