//
//  UserCartView.m
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserCartView.h"
#import "MallGoodsModel.h"
#import "CHTCollectionViewWaterfallLayout.h"

@interface UserCartView ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic, strong) UITableView *tableView;
@end

@implementation UserCartView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.list = [NSMutableArray array];
        self.selectList = [NSMutableArray array];

        _tableView = [[UITableView alloc] initWithFrame:CM(0, 0, frame.size.width, frame.size.height - 49) style:UITableViewStylePlain];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorColor = ColorWithHex(0xf2f2f2f2);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorInset = UIEdgeInsetsZero;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = 160;
        _tableView.allowsMultipleSelection = YES;
        [_tableView registerClass:[UserCartCell class] forCellReuseIdentifier:NSStringFromClass([UserCartCell class])];
        [self addSubview:_tableView];
        
        _bottomView = [[CartBottomView alloc] initWithFrame:CM(0, frame.size.height - 49, frame.size.width, 49)];
        [self addSubview:_bottomView];
        
        WS(weakSelf);
        _bottomView.selectBlock = ^(BOOL selected) {//全选
            [weakSelf.selectList removeAllObjects];
            [weakSelf.list enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (selected) {
                    [weakSelf.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
                }
            }];
            if (selected) {
                [weakSelf.selectList addObjectsFromArray:weakSelf.list];
            }else{
                [weakSelf.tableView reloadData];
            }
            [weakSelf reloadBottomView];
        };
        //删除或确认订单
        _bottomView.actionBlock = ^(BOOL del) {
            
        };
    }
    return self;
}

- (void)setHeader:(MJRefreshNormalHeader *)header {
    _header = header;
    self.tableView.mj_header = header;
}

- (void)setFooter:(MJRefreshAutoStateFooter *)footer {
    _footer = footer;
    self.tableView.mj_footer = footer;
}

- (void)setEditing:(BOOL)editing {
    _editing = editing;
    self.bottomView.editting = editing;
    [self.tableView reloadData];
}

- (void)reloadCartView:(NSArray *)list more:(BOOL)more {
    [self.list removeAllObjects];
    [self.list addObjectsFromArray:list];
    [self.tableView reloadData];
    
    [self.header endRefreshing];
    [self.footer endRefreshing];
    [self reloadBottomView];
}

- (void)reloadBottomView {
    __block double total = 0;
    [self.selectList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MallGoodsModel *item = (MallGoodsModel *)obj;
        total += (item.price * item.num);
    }];
    self.bottomView.price = total;
    self.bottomView.confirmBtn.enabled = self.selectList.count > 0;
    if (self.list.count < 5) {
        self.footer.hidden = YES;
    }else{
        self.footer.hidden = NO;
    }
}

- (void)deleteItem:(NSInteger)index {
    [self.tableView beginUpdates];
    [self.list removeObjectAtIndex:index];
    [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableView endUpdates];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserCartCell *cell = (UserCartCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UserCartCell class])];
    cell.model = self.list[indexPath.row];
    WS(weakSelf);
    cell.deleteBlock = ^() {
        if (weakSelf.deleteBlock) {
            weakSelf.deleteBlock(indexPath.row);
            [weakSelf deleteItem:indexPath.row];
        }
    };
    cell.countBlock = ^(NSInteger count) {
        if (weakSelf.countBlock) {
            weakSelf.countBlock(count,indexPath.row);
            [weakSelf reloadBottomView];
        }
    };
    cell.edit = self.editing;
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MallGoodsModel *object = self.list[indexPath.row];
    if (![self.selectList containsObject:object]) {
        [self.selectList addObject:object];
    }
    [self reloadBottomView];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    MallGoodsModel *object = self.list[indexPath.row];
    if ([self.selectList containsObject:object]) {
        [self.selectList removeObject:object];
    }
    [self reloadBottomView];
}

@end

@interface UserCartCell ()
@property(nonatomic, strong) UIImageView *dotView;
@property(nonatomic, strong) UIImageView *goodImageV;
@property(nonatomic, strong) UILabel *nameLabel;
@property(nonatomic, strong) UILabel *priceLabel;
@property(nonatomic, strong) UILabel *countLabel;
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) UIButton *editBtn;

//编辑时控件
@property(nonatomic, strong) UIButton *plusBtn;
@property(nonatomic, strong) UIButton *minusBtn;
@property(nonatomic, strong) UIButton *deleteBtn;
@property(nonatomic, strong) UILabel *count;

@end

@implementation UserCartCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _dotView = [ViewFactory getImageView:CM(18, 74, 12.5, 12.5) image:getImage(@"cart_dotunselect")];
        [self.contentView addSubview:_dotView];
        [_dotView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(18);
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.size.mas_equalTo(Size(12.5, 12.5));
        }];
        
        _goodImageV = [ViewFactory getImageView:CM(47, 10, 140, 140) image:nil];
        _goodImageV.backgroundColor = BACKGROUND_COLOR;
        [self.contentView addSubview:_goodImageV];
        [_goodImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_dotView.mas_right).offset(17);
            make.top.equalTo(self.contentView).offset(10);
            make.bottom.equalTo(self.contentView).offset(-10);
            make.width.equalTo(self.contentView.mas_height).offset(-20);
        }];
        
        _editBtn = [ViewFactory getButton:CGRectZero selectImage:nil norImage:getImage(@"cart_edit") target:self action:@selector(editGood:)];
        _editBtn.titleLabel.font = Font(14);
        [_editBtn setTitle:@"完成" forState:UIControlStateSelected];
        [_editBtn setTitle:@"" forState:UIControlStateNormal];
        [_editBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
        [self.contentView addSubview:_editBtn];
        [_editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView);
            make.top.equalTo(self.contentView).offset(14);
            make.size.mas_equalTo(Size(18+34, 22));
        }];
        
        _nameLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:nil];
        [self.contentView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodImageV.mas_right).offset(21);
            make.top.equalTo(self.contentView).offset(17);
            make.height.mas_equalTo(@15);
            make.right.equalTo(_editBtn.mas_left).offset(-5);
        }];
        
        _countLabel = [ViewFactory getLabel:CGRectZero font:Font(12) textColor:ColorWithHex(0x666666) text:@"x1"];
        _countLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_countLabel];
        
        [_countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_editBtn.mas_bottom).offset(26);
            make.right.equalTo(self.contentView).offset(-17);
            make.size.mas_equalTo(Size(45, 15));
        }];
        
        _priceLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:RedColor text:nil];
        [self.contentView addSubview:_priceLabel];
        
        [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_nameLabel.mas_bottom).offset(25);
            make.right.equalTo(_countLabel.mas_left).offset(-5);
            make.height.mas_equalTo(@15);
        }];
        
        _dateLabel = [ViewFactory getLabel:CGRectZero font:Font(12) textColor:ColorWithHex(0x666666) text:@"最后编辑日期:2017-05-01"];
        [self.contentView addSubview:_dateLabel];
        [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel.mas_left);
            make.bottom.equalTo(_goodImageV.mas_bottom).offset(-2);
            make.right.equalTo(self.contentView).offset(-17);
            make.height.mas_equalTo(@15);
        }];
        
        self.deleteBtn = [ViewFactory getButton:CGRectZero font:Font(14) selectTitle:@"删除" norTitle:@"删除" sColor:WhiteColor norColor:WhiteColor target:self action:@selector(deleteItem:)];
        _deleteBtn.backgroundColor = LightLemonYellow;
        [self.contentView addSubview:_deleteBtn];
        [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView);
            make.top.equalTo(self.editBtn.mas_bottom).offset(10);
            make.bottom.equalTo(self.dateLabel.mas_top).offset(-18);
            make.left.equalTo(self.editBtn);
        }];
        
        self.minusBtn = [ViewFactory getButton:CGRectZero font:Font(14) selectTitle:@"-" norTitle:@"-" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(countButtonClick:)];
        [self.contentView addSubview:_minusBtn];
        [self.minusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.deleteBtn.mas_left).offset(-1);
            make.centerY.equalTo(self.deleteBtn.mas_centerY);
            make.size.mas_equalTo(Size(25, 44));
        }];
        
        self.plusBtn = [ViewFactory getButton:CGRectZero font:Font(14) selectTitle:@"+" norTitle:@"+" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(countButtonClick:)];
        [self.contentView addSubview:_plusBtn];
        [self.plusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel);
            make.centerY.equalTo(self.minusBtn.mas_centerY);
            make.size.mas_equalTo(Size(25, 44));
        }];
        
        self.count = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@""];
        _count.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_count];
        [self.count mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.minusBtn.mas_left);
            make.centerY.equalTo(self.minusBtn.mas_centerY);
            make.left.equalTo(self.plusBtn.mas_right);
            make.height.mas_equalTo(@20);
        }];
        self.minusBtn.hidden = self.deleteBtn.hidden = self.plusBtn.hidden = self.count.hidden = YES;
}
    return self;
}

- (void)setModel:(MallGoodsModel *)model {
    _model = model;
    self.nameLabel.text = model.itemName;
    self.priceLabel.text = [NSString stringWithFormat:@"¥ %.2f",model.price];
    NSString *url = [model.itemPhotos firstObject];
    [self.goodImageV sd_setImageWithURL:[NSURL URLWithString:url]];
    self.countLabel.text = [NSString stringWithFormat:@"x%ld",(long)model.num];
    self.dateLabel.text = [NSString stringWithFormat:@"%@",[StringUtil timeWithTimeIntervalString:model.updatedAt]];
}

- (void)editGood:(UIButton *)button {
    button.selected = !button.selected;
    self.editing = button.selected;
}

//单个编辑
- (void)setEditing:(BOOL)editing {
    [super setEditing:editing];
    self.editBtn.backgroundColor = editing?LightLemonYellow:ClearColor;
    [self.editBtn setImage:editing?nil:getImage(@"cart_edit") forState:UIControlStateNormal];
    self.minusBtn.hidden = self.deleteBtn.hidden = self.plusBtn.hidden = self.count.hidden = !editing;
    self.priceLabel.hidden = self.countLabel.hidden = editing;
    self.count.text = [NSString stringWithFormat:@"%ld",(long)self.model.num];
    [self.deleteBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView);
        make.top.equalTo(self.editBtn.mas_bottom).offset(10);
        make.bottom.equalTo(self.dateLabel.mas_top).offset(-18);
        make.left.equalTo(self.editBtn);
    }];
}

//全部编辑
- (void)setEdit:(BOOL)edit {
    _edit = edit;
    self.editing = edit;
    self.editBtn.hidden = edit;
    [self.deleteBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(14);
        make.right.equalTo(self.contentView);
        make.bottom.equalTo(self.dateLabel.mas_top).offset(-18);
        make.left.equalTo(self.editBtn);
    }];
}

- (void)deleteItem:(UIButton *)btn {
    if (self.deleteBlock) {
        self.deleteBlock();
    }
}

- (void)countButtonClick:(UIButton *)sender {
    NSInteger count = self.count.text.integerValue;
    if ([sender isEqual:self.plusBtn]) {
        count += 1;
    }else{
        if (count>1) {
            count -= 1;
        }else{
            return;
        }
    }
    self.count.text = [NSString stringWithFormat:@"%ld",count];
    self.countLabel.text = [NSString stringWithFormat:@"x%ld",count];
    self.model.num = count;
    if (self.countBlock) {
        self.countBlock(count);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.dotView.image = selected?getImage(@"cart_dotselect"):getImage(@"cart_dotunselect");
}

@end


@interface EmptyCartView ()<CHTCollectionViewDelegateWaterfallLayout,UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) NSMutableArray *itemList;
@property(nonatomic, strong) UIButton *noDataButton;

@end

@implementation EmptyCartView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.itemList = [NSMutableArray array];
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc]init];
        layout.columnCount = 2;
        layout.minimumColumnSpacing = 3;
        layout.minimumContentHeight = 0;
        layout.minimumInteritemSpacing = 3;
        layout.sectionInset = UIEdgeInsetsMake(5, 4, 0, 4);
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CM(0, 0, frame.size.width, frame.size.height) collectionViewLayout:layout];
        _collectionView.backgroundColor = BACKGROUND_COLOR;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        [_collectionView registerClass:[EmptyCartCell class] forCellWithReuseIdentifier:NSStringFromClass([EmptyCartCell class])];
        [_collectionView registerClass:[EmptyCartHeader class] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([EmptyCartHeader class])];
        [self addSubview:_collectionView];
    }
    return self;
}

- (void)setHeader:(MJRefreshNormalHeader *)header {
    _header = header;
    self.collectionView.mj_header = header;
}

- (void)setFooter:(MJRefreshAutoStateFooter *)footer {
    _footer = footer;
    self.collectionView.mj_footer = footer;
}

- (void)reloadList:(NSArray *)list more:(BOOL)more {
    if (!more) {
        [self.itemList removeAllObjects];
    }
    [self.itemList addObjectsFromArray:list];
    [self.collectionView reloadData];
    self.noDataButton.hidden = self.itemList.count > 0;
    [self.header endRefreshing];
    [self.footer endRefreshing];
    if (list.count < 10) {
        self.footer.hidden = YES;
    }else{
        self.footer.hidden = NO;
    }
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.itemList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    EmptyCartCell *cell = (EmptyCartCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([EmptyCartCell class]) forIndexPath:indexPath];
    cell.model = self.itemList[indexPath.item];
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section {
    return 240;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    EmptyCartHeader *header = (EmptyCartHeader *)[collectionView dequeueReusableSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([EmptyCartHeader class]) forIndexPath:indexPath];
    return header;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return Size((SCREEN_HEIGHT - 120 - 11)/2, (SCREEN_HEIGHT - 120 - 11)/2 + 52);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectBlock) {
        self.selectBlock(self.itemList[indexPath.item]);
    }
}

- (UIButton *)noDataButton {
    if (!_noDataButton) {
        _noDataButton = [ViewFactory getButton:CM(0, 0, self.frameSizeWidth, self.frameSizeHeight - 100) font:Font(16) selectTitle:@"没有数据" norTitle:@"没有数据" sColor:ColorWithHex(0x999999) norColor:ColorWithHex(0x999999) target:self action:@selector(reloadData)];
        [self addSubview:_noDataButton];
    }
    return _noDataButton;
}

@end

@implementation EmptyCartHeader

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = BACKGROUND_COLOR;
        UIView *bgview = [[UIView alloc] initWithFrame:CGRectZero];
        bgview.backgroundColor = WhiteColor;
        [self addSubview:bgview];
        [bgview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(self);
            make.bottom.equalTo(self).offset(-50);
        }];
        
        UIImageView *imagev = [ViewFactory getImageView:CGRectZero image:getImage(@"cart_empty")];
        [self addSubview:imagev];
        imagev.contentMode = UIViewContentModeCenter;
        [imagev mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self);
            make.height.mas_equalTo(100);
        }];
        
        UILabel *emptyTip = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x999999) text:@"您的购物车内没有商品!"];
        emptyTip.textAlignment = NSTextAlignmentCenter;
        [self addSubview:emptyTip];
        [emptyTip mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(imagev.mas_bottom).offset(19);
            make.height.mas_equalTo(@15);
        }];
        
        UILabel *emptyTip1 = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x999999) text:@"您可能需要的"];
        emptyTip1.backgroundColor = BACKGROUND_COLOR;
        emptyTip1.textAlignment = NSTextAlignmentCenter;
        [self addSubview:emptyTip1];
        [emptyTip1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bgview.mas_bottom);
            make.bottom.equalTo(self);
            make.centerX.equalTo(self.mas_centerX);
        }];
        [emptyTip1 setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        [emptyTip1 setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        UIImageView *imagev1 = [ViewFactory getImageView:CGRectZero image:getImage(@"cart_idea")];
        [self addSubview:imagev1];
        imagev1.contentMode = UIViewContentModeCenter;
        [imagev1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(emptyTip1);
            make.right.equalTo(emptyTip1.mas_left).offset(-11);
            make.width.mas_equalTo(@40);
        }];
        
    }
    return self;
}

@end

@interface EmptyCartCell ()
@property(nonatomic, strong) UIImageView *goodImage;
@property(nonatomic, strong) UILabel *nameLabel;
@property(nonatomic, strong) UILabel *priceLabel;

@end

@implementation EmptyCartCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = WhiteColor;
        _goodImage = [ViewFactory getImageView:CGRectZero image:nil];
        [self.contentView addSubview:_goodImage];
        [_goodImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView).offset(-45);
        }];
        
        _priceLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0xf85454) text:@"10"];
        [self.contentView addSubview:_priceLabel];
        _priceLabel.textAlignment = NSTextAlignmentRight;
        [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-5);
            make.top.equalTo(_goodImage.mas_bottom);
            make.bottom.equalTo(self.contentView);
        }];
        
        [_priceLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        [_priceLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        _nameLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x333333) text:nil];
        [self.contentView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(8);
            make.top.equalTo(_goodImage.mas_bottom);
            make.bottom.equalTo(self.contentView);
            make.right.equalTo(_priceLabel.mas_left).offset(-5);
        }];

    }
    return self;
}

- (void)setModel:(MallGoodsModel *)model {
    NSString *url = [model.photos firstObject];
    [self.goodImage sd_setImageWithURL:[NSURL URLWithString:url]];
    self.nameLabel.text = model.itemName;
    self.priceLabel.text = [NSString stringWithFormat:@"¥%.2f",model.price];
}

@end

@interface CartBottomView ()

@property(nonatomic, strong) UILabel *hejiL;
@property(nonatomic, strong) UILabel *priceLabel;

@end

@implementation CartBottomView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        
        _dotButton = [ViewFactory getButton:CGRectZero selectImage:getImage(@"cart_dotselect") norImage:getImage(@"cart_dotunselect") target:self action:@selector(didSelectAll)];
        [_dotButton setTitle:@"全选" forState:UIControlStateNormal];
        [_dotButton setTitleColor:ColorWithHex(0x666666) forState:UIControlStateNormal];
        _dotButton.titleLabel.font = Font(14);
        _dotButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5);
        [self addSubview:_dotButton];
        [_dotButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(19);
            make.top.bottom.equalTo(self);
            make.width.mas_equalTo(@60);
        }];
        
        _confirmBtn = [ViewFactory getButton:CGRectZero font:Font(14) selectTitle:@"删除" norTitle:@"确认订单" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(buttonClick)];
        _confirmBtn.backgroundColor = ColorWithHex(0xfce233);
        [self addSubview:_confirmBtn];
        [_confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.right.equalTo(self);
            make.width.mas_equalTo(@100);
        }];
        
        _priceLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0xf90f1b) text:@"0.00"];
        [self addSubview:_priceLabel];
        [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_confirmBtn.mas_left).offset(-25);
            make.top.bottom.equalTo(self);
        }];
        [_priceLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        [_priceLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        _hejiL = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:@"合计¥"];
        _hejiL.textAlignment = NSTextAlignmentRight;
        [self addSubview:_hejiL];
        [_hejiL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_priceLabel.mas_left).offset(-2);
            make.top.bottom.equalTo(self);
            make.width.mas_equalTo(@(50));
        }];
        
        UIView *topLine = [[UIView alloc] initWithFrame:CM(0, 0, frame.size.width, 0.5f)];
        topLine.backgroundColor = ColorWithHex(0xf2f2f2);
        [self addSubview:topLine];
        self.editting = NO;
    }
    return self;
}

- (void)setPrice:(double)price {
    _price = price;
    self.priceLabel.text = [NSString stringWithFormat:@"%.2f",price];
}

- (void)setEditting:(BOOL)editting {
    _editting = editting;
    self.confirmBtn.selected = editting;
}

- (void)didSelectAll {
    self.dotButton.selected = !self.dotButton.selected;
    if (self.selectBlock) {
        self.selectBlock(self.dotButton.selected);
    }
}

- (void)buttonClick {
    if (self.actionBlock) {
        self.actionBlock(_editting);
    }
}

@end
