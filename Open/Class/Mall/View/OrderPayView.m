//
//  OrderPayView.m
//  Open
//
//  Created by mfp on 17/9/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "OrderPayView.h"

@implementation OrderPayView

@end

@interface OrderPayCell ()
@property(nonatomic, strong) UIImageView *goodImageV;
@property(nonatomic, strong) UILabel *nameLabel;
@property(nonatomic, strong) UILabel *priceLabel;
@property(nonatomic, strong) UILabel *countLabel;

@end

@implementation OrderPayCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _goodImageV = [ViewFactory getImageView:CM(47, 10, 140, 140) image:nil];
        _goodImageV.backgroundColor = BACKGROUND_COLOR;
        [self.contentView addSubview:_goodImageV];
        [_goodImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(19);
            make.top.equalTo(self.contentView).offset(15);
            make.bottom.equalTo(self.contentView).offset(-15);
            make.width.equalTo(self.contentView.mas_height).offset(-30);
        }];
        
        _nameLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:nil];
        [self.contentView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodImageV.mas_right).offset(19);
            make.top.equalTo(self.contentView).offset(21);
            make.height.mas_equalTo(@15);
            make.right.equalTo(self.contentView).offset(-5);
        }];
        
        _countLabel = [ViewFactory getLabel:CGRectZero font:Font(12) textColor:ColorWithHex(0x666666) text:@"x1"];
        _countLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_countLabel];
        
        [_countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_nameLabel.mas_bottom).offset(18);
            make.right.equalTo(self.contentView).offset(-19);
            make.size.mas_equalTo(Size(45, 15));
        }];

        _priceLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:RedColor text:nil];
        [self.contentView addSubview:_priceLabel];
        
        [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_nameLabel.mas_bottom).offset(18);
            make.right.equalTo(_countLabel.mas_left).offset(-15);
            make.height.mas_equalTo(@15);
        }];

    }
    return self;
}

- (void)setModel:(DetailModel *)model {
    _model = model;
    self.nameLabel.text = model.name;
    self.priceLabel.text = [NSString stringWithFormat:@"¥ %@",model.price];
    NSString *url = [model.photos firstObject];
    [self.goodImageV sd_setImageWithURL:[NSURL URLWithString:url]];
    self.countLabel.text = [NSString stringWithFormat:@"x%ld",(long)model.num];
}

@end

@interface PayModelCell ()
@property(nonatomic, strong) UIImageView *dotView;

@end

@implementation PayModelCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _dotView = [ViewFactory getImageView:CM(0, 0, 14, 14) image:getImage(@"cart_dotunselect")];
        self.accessoryView = _dotView;
        self.textLabel.font = Font(14);
        self.textLabel.textColor = ColorWithHex(0x666666);
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.dotView.image = selected?getImage(@"cart_dotselect"):getImage(@"cart_dotunselect");
}

@end

@implementation OrderPayFooter

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = WhiteColor;
        _payButton = [ViewFactory getButton:CM(19, 30, SCREEN_WIDTH - 38, 47) font:Font(17) selectTitle:@"确认支付" norTitle:@"确认支付" sColor:ColorWithHex(0xffffff) norColor:WhiteColor target:self action:@selector(payorder)];
        _payButton.backgroundColor = LightLemonYellow;
        _payButton.layer.cornerRadius = 23.5f;
        _payButton.layer.masksToBounds = YES;
        [self.contentView addSubview:_payButton];
    }
    return self;
}

- (void)payorder {
    if (self.payBlock) {
        self.payBlock();
    }
}
@end

@interface PaySuccessHeader ()
@property(nonatomic, strong) UIButton *checkBtn;
@property(nonatomic, strong) UIButton *backBtn;

@end

@implementation PaySuccessHeader

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        _priceLable = [ViewFactory getLabel:CM(0, 24, SCREEN_WIDTH, 22) font:Font(20) textColor:ColorWithHex(0xf85454) text:nil];
        _priceLable.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_priceLable];
        [_priceLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self).offset(24);
            make.height.mas_equalTo(20);
        }];
        
        _checkBtn = [ViewFactory getButton:CGRectZero font:Font(14) selectTitle:@"" norTitle:@"查看订单" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(buttonClick:)];
        [self addSubview:_checkBtn];
        [_checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(72);
            make.top.equalTo(_priceLable.mas_bottom).offset(24);
            make.size.mas_equalTo(Size(65, 14));
        }];
        
        _backBtn = [ViewFactory getButton:CGRectZero font:Font(14) selectTitle:@"" norTitle:@"返回首页" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(buttonClick:)];
        [self addSubview:_backBtn];
        [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_checkBtn);
            make.size.equalTo(_checkBtn);
            make.right.equalTo(self).offset(-72);
        }];
        
        UIView *bottomView = [[UIView alloc] init];
        bottomView.backgroundColor = BACKGROUND_COLOR;
        [self addSubview:bottomView];
        [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.bottom.equalTo(self);
            make.height.mas_equalTo(50);
        }];
        
        UILabel *emptyTip1 = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x999999) text:@"您可能需要的"];
        emptyTip1.backgroundColor = BACKGROUND_COLOR;
        emptyTip1.textAlignment = NSTextAlignmentCenter;
        [self addSubview:emptyTip1];
        [emptyTip1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bottomView.mas_top);
            make.bottom.equalTo(self);
            make.centerX.equalTo(self.mas_centerX);
        }];
        [emptyTip1 setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        [emptyTip1 setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        UIImageView *imagev1 = [ViewFactory getImageView:CGRectZero image:getImage(@"cart_idea")];
        [self addSubview:imagev1];
        imagev1.contentMode = UIViewContentModeCenter;
        [imagev1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(emptyTip1.mas_left).offset(-11);
            make.width.mas_equalTo(@40);
            make.centerY.equalTo(bottomView.mas_centerY);
        }];
    }
    return self;
}

- (void)buttonClick:(UIButton *)sender {
    if (self.headerBlock) {
        self.headerBlock([sender isEqual:self.checkBtn]);
    }
}

@end
