//
//  UserCartView.h
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"
#import "MallGoodsModel.h"

@class CartBottomView;

//购物车view
@interface UserCartView : UIView
@property(nonatomic, strong) MJRefreshNormalHeader *header;
@property(nonatomic, strong) MJRefreshAutoStateFooter *footer;
@property(nonatomic, strong) CartBottomView *bottomView;
@property(nonatomic, assign) BOOL editing;
@property(nonatomic, strong) NSMutableArray *list;
@property(nonatomic, strong) NSMutableArray *selectList;//选中的

@property(nonatomic, copy) void (^countBlock)(NSInteger count,NSInteger index);
@property(nonatomic, copy) void (^deleteBlock)(NSInteger index);


- (void)reloadCartView:(NSArray *)list more:(BOOL)more;

@end

//购物车cell
@interface UserCartCell : UITableViewCell
@property(nonatomic, weak) MallGoodsModel *model;
@property(nonatomic, assign) BOOL edit;//全部编辑标识 编辑还分单个和全部，fuck
@property(nonatomic, copy) void (^deleteBlock)();
@property(nonatomic, copy) void (^countBlock)(NSInteger count);

@end

//购物车为空时view
@interface EmptyCartView : UIView
@property(nonatomic, strong) MJRefreshNormalHeader *header;
@property(nonatomic, strong) MJRefreshAutoStateFooter *footer;
@property(nonatomic, copy) void (^selectBlock)(MallGoodsModel *model);

- (void)reloadList:(NSArray *)list more:(BOOL)more;

@end

@interface EmptyCartHeader : UICollectionReusableView

@end

@interface EmptyCartCell : UICollectionViewCell

@property(nonatomic, strong) MallGoodsModel *model;

@end

//底部确认条
@interface CartBottomView : UIView

@property(nonatomic, assign) BOOL editting;
@property(nonatomic, assign) double price;
@property(nonatomic, strong) UIButton *dotButton;
@property(nonatomic, strong) UIButton *confirmBtn;

@property(nonatomic, copy) void(^actionBlock)(BOOL del);
@property(nonatomic, copy) void(^selectBlock)(BOOL selected);

@end
