//
//  MallRightCell.h
//  Open
//
//  Created by onceMu on 2017/8/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MallGoodsModel.h"

@interface MallRightCell : UICollectionViewCell

@property(nonatomic, strong) MallGoodsModel *model;

@end
