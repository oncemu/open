//
//  OrderDetailView.m
//  Open
//
//  Created by mfp on 17/9/25.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "OrderDetailView.h"
#import "MallGoodsModel.h"

@interface OrderDetailView ()
@property(nonatomic, strong) UILabel *nameL;
@property(nonatomic, strong) UILabel *addressL;
@property(nonatomic, strong) UIImageView *imageV;
@property(nonatomic, strong) UILabel *goodsNameL;
@property(nonatomic, strong) UILabel *priceL;
@property(nonatomic, strong) UILabel *countL;
@property(nonatomic, strong) UILabel *detailL;

@end

@implementation OrderDetailView

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = WhiteColor;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    
        _nameL = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:nil];
        [self.contentView addSubview:_nameL];
        [_nameL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(19);
            make.top.equalTo(self.contentView).offset(10);
            make.right.equalTo(self.contentView).offset(-19);
            make.height.mas_equalTo(15);
        }];
        
        _addressL = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:nil];
        [self.contentView addSubview:_addressL];
        [_addressL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameL);
            make.top.equalTo(_nameL.mas_bottom).offset(13);
            make.size.equalTo(_nameL);
        }];
        
        _imageV = [ViewFactory getImageView:CGRectZero image:nil];
        [self.contentView addSubview:_imageV];
        [_imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameL);
            make.top.equalTo(_addressL.mas_bottom).offset(9);
            make.size.mas_equalTo(Size(130, 130));
        }];
        
        _goodsNameL = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:nil];
        [self.contentView addSubview:_goodsNameL];
        [_goodsNameL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_imageV.mas_right).offset(21);
            make.top.equalTo(_imageV.mas_top).offset(9);
            make.right.equalTo(self.contentView).offset(19);
            make.height.mas_equalTo(15);
        }];
        
        _countL = [ViewFactory getLabel:CGRectZero font:Font(13) textColor:ColorWithHex(0x666666) text:nil];
        _countL.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_countL];
        [_countL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-50);
            make.top.equalTo(_goodsNameL.mas_bottom).offset(25);
            make.size.mas_equalTo(Size(50, 15));
        }];
        
        _priceL = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0xf85454) text:nil];
        [self.contentView addSubview:_priceL];
        [_priceL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsNameL);
            make.top.bottom.equalTo(_countL);
            make.right.equalTo(_countL.mas_left);
        }];
        
        _detailL = [ViewFactory getLabel:CGRectZero font:Font(12) textColor:ColorWithHex(0x999999) text:nil];
        [self.contentView addSubview:_detailL];
        [_detailL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsNameL);
            make.right.equalTo(self.contentView).offset(-19);
            make.bottom.equalTo(self.imageV).offset(-7);
            make.height.mas_equalTo(14);
        }];
        
    }
    return self;
}

- (void)setModel:(OrderModel *)model {
    _model = model;
    DetailModel *detail = [model.details firstObject];
    [self.imageV sd_setImageWithURL:[NSURL URLWithString:[detail.photos firstObject]]];
    self.goodsNameL.text = detail.name;
    self.priceL.text = detail.price;
    self.countL.text = [NSString stringWithFormat:@"x%ld",detail.num];
    NSString *des = [NSString stringWithFormat:@"共%ld件商品 合计：",model.details.count];
    NSString *express = [NSString stringWithFormat:@"(含运费%@)",model.totalExpressFee];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@",des,model.totalPrice,express]];
    self.detailL.attributedText = str;
    self.nameL.text = [NSString stringWithFormat:@"收货人：%@",model.consignee];
    self.addressL.text = [NSString stringWithFormat:@"收货地址：%@",model.deliveryAddress];
}

@end

@implementation OrderDetailTop
- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.font = Font(13);
    self.detailTextLabel.font = Font(12);
    self.textLabel.textColor = ColorWithHex(0x999999);
    self.detailTextLabel.textColor = ColorWithHex(0x999999);
    self.textLabel.frame = CM(19, 20, self.frameSizeWidth - 38, 15);
    self.detailTextLabel.frame = CM(19, CGRectGetMaxY(self.textLabel.frame)+13, self.frameSizeWidth-38, 15);
}
@end

@implementation OrderDetailBottom

- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.font = Font(13);
    self.detailTextLabel.font = Font(12);
    self.textLabel.textColor = ColorWithHex(0x999999);
    self.detailTextLabel.textColor = ColorWithHex(0x999999);
    self.textLabel.frame = CM(19, 10, self.frameSizeWidth - 38, 15);
    self.detailTextLabel.frame = CM(19, CGRectGetMaxY(self.textLabel.frame)+13, self.frameSizeWidth-38, 15);
}

@end
