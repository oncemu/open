//
//  MallView.h
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MJRefresh.h>
#import "MallCategoryModel.h"
#import "MallGoodsModel.h"

@interface MallView : UIView

@end

@interface LeftView : UIView
@property(nonatomic, copy) void (^selectIndexBlock)(MallCategoryModel *model);
@property(nonatomic, strong) NSMutableArray *list;

- (void)reloadList:(NSArray *)categories;

@end

@interface LeftCell : UITableViewCell
@property(nonatomic, strong) MallCategoryModel *model;
@end

@interface RightView : UIView

@property(nonatomic, strong) MJRefreshNormalHeader *header;
@property(nonatomic, strong) MJRefreshAutoStateFooter *footer;
@property(nonatomic, copy) void (^selectAlbum)(MallGoodsModel *model);
@property(nonatomic, copy) void (^reloadDataBlock)();

- (void)reloadList:(NSArray *)list more:(BOOL)more;

@end
