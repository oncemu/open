//
//  OrderDetailView.h
//  Open
//
//  Created by mfp on 17/9/25.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderModel.h"

@interface OrderDetailView : UITableViewCell
@property(nonatomic, strong) OrderModel *model;
@end


@interface OrderDetailTop : UITableViewCell

@end

@interface OrderDetailBottom : UITableViewCell

@end
