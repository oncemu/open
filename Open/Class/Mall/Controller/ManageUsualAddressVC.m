//
//  ManageUsualAddressVC.m
//  Open
//
//  Created by mfp on 17/9/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "ManageUsualAddressVC.h"
#import "ManageUsualAddressView.h"
#import "MallNetService.h"

@interface ManageUsualAddressVC ()
{
    NSArray *titleList;
}
@property(nonatomic, strong) MallNetService *mallService;
@property(nonatomic, strong) AddressPicker *picker;
@property(nonatomic, strong) NSIndexPath *selectIndexPath;
@property(nonatomic, strong) UIButton *saveBtn;



@end

@implementation ManageUsualAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"管理收货地址";
    titleList = @[@"收货人姓名",@"手机号码",@"请选择省份",@"请选择城市",@"请选择区/县",@"详情地址",@"设置为默认"];
    [self.tableView registerClass:[ManageUsualAddressCell class] forCellReuseIdentifier:NSStringFromClass([ManageUsualAddressCell class])];
    self.tableView.separatorColor = BACKGROUND_COLOR;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    if (self.addressModel == nil) {
        self.addressModel = [[UsualAddressModel alloc] init];
    }
    [self saveBtn];
}

#pragma mark - method
- (void)saveAddress {
    if (self.addressModel.consignee.length == 0) {
        [KVNProgress showErrorWithStatus:@"请填写姓名"];
        return;
    }
    if (![StringUtil checkIsPhoneNumber:self.addressModel.cellphone]) {
        [KVNProgress showErrorWithStatus:@"请填写正确手机号"];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.addressModel.consignee forKey:@"consignee"];
    [dict setObject:self.addressModel.cellphone forKey:@"cellphone"];

    if (self.addressModel.province.length > 0) {
        [dict setObject:self.addressModel.province forKey:@"province"];
    }
    if (self.addressModel.city.length > 0) {
        [dict setObject:self.addressModel.city forKey:@"city"];
    }
    if (self.addressModel.region.length > 0) {
        [dict setObject:self.addressModel.region forKey:@"region"];
    }
    if (self.addressModel.street.length > 0) {
        [dict setObject:self.addressModel.street forKey:@"street"];
    }
    if (self.addressModel.isDefault) {
        [dict setObject:@"1" forKey:@"isDefault"];
    }else{
        [dict setObject:@"0" forKey:@"isDefault"];
    }
    DLog(@"%@",dict);
    WS(weakSelf);
    [self.mallService editUserAddress:dict success:^(NSString *requestInfo, id responseObject) {
        [KVNProgress showSuccessWithStatus:responseObject[@"message"]];
        [weakSelf goBack];
    } failure:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:responseObject[@"message"]];
    }];
}

#pragma mark - table Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return titleList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 5) {
        return 105;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ManageUsualAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ManageUsualAddressCell class])];
    cell.textLabel.text = titleList[indexPath.row];
    cell.mode = indexPath.row;
    cell.addressModel = self.addressModel;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > 1 && indexPath.row < 5) {
        self.selectIndexPath = indexPath;
        self.picker.kind = indexPath.row;
        [self.picker show];
    }
}

- (AddressPicker *)picker {
    if (!_picker) {
        _picker = [[AddressPicker alloc] initWithFrame:self.view.bounds];
        WS(weakSelf);
        _picker.didSelectBlock = ^(AddressModel *model) {
            switch (weakSelf.selectIndexPath.row) {
                case 2:
                    if (![weakSelf.addressModel.province isEqualToString:model.name]) {
                        weakSelf.addressModel.city = @"";
                        weakSelf.addressModel.region = @"";
                    }
                    weakSelf.addressModel.province = model.name;
                    break;
                case 3:
                    if (![weakSelf.addressModel.city isEqualToString:model.name]) {
                        weakSelf.addressModel.region = @"";
                    }
                    weakSelf.addressModel.city = model.name;
                    break;
                case 4:
                    weakSelf.addressModel.region = model.name;
                    break;
                    
                default:
                    break;
            }
            [weakSelf.tableView reloadData];
        };
    }
    return _picker;
}

- (UIButton *)saveBtn {
    if (_saveBtn == nil) {
        _saveBtn = [ViewFactory getButton:CM(0, SCREEN_HEIGHT - kTabBarHeight, SCREEN_WIDTH, kTabBarHeight) font:Font(18) selectTitle:@"保存" norTitle:@"保存" sColor:ColorWithHex(0xffffff) norColor:WhiteColor target:self action:@selector(saveAddress)];
        _saveBtn.backgroundColor = LightLemonYellow;
        [self.view addSubview:_saveBtn];
    }
    return _saveBtn;
}

- (MallNetService *)mallService {
    if (!_mallService) {
        _mallService = [[MallNetService alloc] init];
     }
    return _mallService;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
