//
//  PaySuccessVC.m
//  Open
//
//  Created by mfp on 17/9/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PaySuccessVC.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "UserCartView.h"
#import "AlbumDetailVC.h"
#import "OrderPayView.h"
#import "MallNetService.h"
#import "OrderDetailVC.h"


@interface PaySuccessVC ()<CHTCollectionViewDelegateWaterfallLayout>
@property(nonatomic, strong) MallNetService *mallService;
@end

@implementation PaySuccessVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"支付成功";
    self.mallService = [[MallNetService alloc] init];
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc]init];
    layout.columnCount = 2;
    layout.minimumColumnSpacing = 3;
    layout.minimumContentHeight = 0;
    layout.minimumInteritemSpacing = 3;
    layout.sectionInset = UIEdgeInsetsMake(5, 4, 0, 4);
    
    self.collectionView.collectionViewLayout = layout;
    [self.collectionView registerClass:[EmptyCartCell class] forCellWithReuseIdentifier:NSStringFromClass([EmptyCartCell class])];
    [self.collectionView registerClass:[PaySuccessHeader class] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([PaySuccessHeader class])];
    [self loadRecommendAlbulm];
}

- (void)goBack {
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

//加载推荐
- (void)loadRecommendAlbulm {
    WS(weakSelf);
    [self.mallService cartRecommend:NO success:^(NSString *requestInfo, id responseObject) {
        [weakSelf.dataList removeAllObjects];
        [weakSelf.dataList addObjectsFromArray:responseObject];
        [weakSelf changeRefreshStatus:YES data:responseObject];
    } failure:^(NSError *error, id responseObject) {
        
    }];

}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    EmptyCartCell *cell = (EmptyCartCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([EmptyCartCell class]) forIndexPath:indexPath];
    cell.model = self.dataList[indexPath.item];
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section {
    return 145;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    PaySuccessHeader *header = (PaySuccessHeader *)[collectionView dequeueReusableSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([PaySuccessHeader class]) forIndexPath:indexPath];
    header.priceLable.text = self.model.totalPrice;
    WS(weakSelf);
    header.headerBlock = ^(BOOL check) {
        if (check) {
            OrderDetailVC *vc = [[OrderDetailVC alloc] initWithShowBackButton:YES];
            [self pushVC:vc animated:YES];
        }else{
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        }
    };
    return header;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return Size((SCREEN_HEIGHT - 120 - 11)/2, (SCREEN_HEIGHT - 120 - 11)/2 + 52);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    AlbumDetailVC *vc = [[AlbumDetailVC alloc] initWithShowBackButton:YES];
    vc.model = self.dataList[indexPath.item];
    [self pushVC:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
