//
//  OrderPayVC.m
//  Open
//
//  Created by mfp on 17/9/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "OrderPayVC.h"
#import "OrderPayView.h"
#import "PaySuccessVC.h"
#import "MallNetService.h"
#import "PaymentUtil.h"
#import "AlbumMakeObject.h"

@interface OrderPayVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *payModeList;
    NSInteger payIndex;//支付方式
    PaymentUtil *payUtil;
}
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) MallNetService *mallService;

@end

@implementation OrderPayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单支付";
    payModeList = @[@"支付宝支付",@"微信支付"];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = WhiteColor;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionFooterHeight = 0;
    self.tableView.sectionHeaderHeight = 0;
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorColor = BACKGROUND_COLOR;
    [self.tableView registerClass:[OrderPayCell class] forCellReuseIdentifier:NSStringFromClass([OrderPayCell class])];
    [self.tableView registerClass:[PayModelCell class] forCellReuseIdentifier:NSStringFromClass([PayModelCell class])];
    [self.tableView registerClass:[OrderPayFooter class] forHeaderFooterViewReuseIdentifier:NSStringFromClass([OrderPayFooter class])];
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:NSStringFromClass([UITableViewHeaderFooterView class])];
    [self.view addSubview:self.tableView];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] animated:NO scrollPosition:UITableViewScrollPositionNone];
    payIndex = 0;
    payUtil = [PaymentUtil sharedUtil];
    WS(weakSelf);
    payUtil.successBlock = ^() {
        [weakSelf pushToSuccessVC];
    };
    payUtil.failureBlock = ^(NSString *msg) {
        [KVNProgress showErrorWithStatus:msg];
    };
}

- (void)pay {
    NSString *channel = (payIndex == 0)?@"ALIPAY":@"WX";
    [self.mallService payOrder:@{@"channel":channel,@"order_id":@(self.model.orderId)} success:^(NSString *requestInfo, id responseObject) {
        [payUtil payOrder:responseObject];
    } failure:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:responseObject[@"message"]];
    }];
}

- (void)pushToSuccessVC {
    [[AlbumMakeData shareData] deleteAlbumData];
    [[NSNotificationCenter defaultCenter] postNotificationName:kAlbumMakeSuccess object:nil];
    PaySuccessVC *vc = [[PaySuccessVC alloc] initWithShowBackButton:YES];
    vc.model = self.model;
    [self pushVC:vc animated:YES];
}

#pragma mark - tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.model.details.count;
    }else{
        return payModeList.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 86;
    }else{
        return 55;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        OrderPayCell *cell = (OrderPayCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([OrderPayCell class])];
        cell.model = self.model.details[indexPath.row];
        return cell;
    }else{
        PayModelCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PayModelCell class])];
        cell.imageView.image = getImage(payModeList[indexPath.row]);
        cell.textLabel.text = payModeList[indexPath.row];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        
    }else{
        payIndex = indexPath.row;
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return [UIView new];
    }
    OrderPayFooter *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([OrderPayFooter class])];
    [footer.payButton setTitle:[NSString stringWithFormat:@"确认支付¥%@",self.model.totalPrice] forState:UIControlStateNormal];
    WS(weakSelf);
    footer.payBlock = ^() {
        [weakSelf pay];
    };
    return footer;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return [UIView new];
    }else{
        UITableViewHeaderFooterView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([UITableViewHeaderFooterView class])];
        header.contentView.backgroundColor = BACKGROUND_COLOR;
        return header;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.01;
    }else{
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return 0.01;
    }else{
        return 107;
    }
}

- (MallNetService *)mallService {
    if (!_mallService) {
        _mallService = [[MallNetService alloc] init];
    }
    return _mallService;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
