//
//  ConfirmOrderVC.m
//  Open
//
//  Created by mfp on 17/9/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "ConfirmOrderVC.h"
#import "MJRefresh.h"
#import "ConfirmOrderView.h"
#import "UserCartView.h"
#import "MallNetService.h"
#import "UsualAddressListVC.h"
#import "OrderDetailVC.h"
#import "PaySuccessVC.h"
#import "MakeNetService.h"

@interface ConfirmOrderVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong) UITableView *tableView;
@property (nonatomic,strong) MJRefreshNormalHeader *header;
@property(nonatomic, strong) CartBottomView *bottomView;
@property(nonatomic, strong) MallNetService *mallService;
@property(nonatomic, strong) MakeNetService *makeService;
@property(nonatomic, strong) UsualAddressModel *defaultAddress;

@end

@implementation ConfirmOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"确认订单";
    self.tableView = [[UITableView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kTabBarHeight) style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = BACKGROUND_COLOR;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionFooterHeight = 0;
    self.tableView.sectionHeaderHeight = 0;
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView registerClass:[ShippingAddressCell class] forCellReuseIdentifier:NSStringFromClass([ShippingAddressCell class])];
    [self.tableView registerClass:[ConfirmOrderCell class] forCellReuseIdentifier:NSStringFromClass([ConfirmOrderCell class])];

    [self.view addSubview:self.tableView];
    self.bottomView = [[CartBottomView alloc] initWithFrame:CM(0, SCREEN_HEIGHT - kTabBarHeight, SCREEN_WIDTH, kTabBarHeight)];
    _bottomView.dotButton.hidden = YES;
    WS(weakSelf);
    _bottomView.actionBlock = ^(BOOL del) {
        [weakSelf summitOrder];
    };
    [self.view addSubview:_bottomView];
    [self reloadBottomView];
    [self loadDefaultAddress];
}

//加载默认地址
- (void)loadDefaultAddress {
    WS(weakSelf);
    [self.mallService getUserAddressSuccess:^(NSString *requestInfo, id responseObject) {
        NSArray *list = (NSArray *)responseObject;
        [list enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UsualAddressModel *mode = (UsualAddressModel *)obj;
            if (mode.isDefault) {
                weakSelf.defaultAddress = mode;
                [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
            }
        }];
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

//提交订单
- (void)summitOrder {
    if (!self.defaultAddress) {
        [KVNProgress showErrorWithStatus:@"请选择收货地址"];
        return;
    }
    if (self.type == AlbumConfirmTypeAlbum) {
        [self getMakeAlbumId];
    }else if(self.type == AlbumConfirmTypePicture){
        [self confconfirmUserMakePicture];
    }else{
        [self confirmBuyGoods];
    }
}

//提交购买商品
- (void)confirmBuyGoods {
    NSMutableArray *itemIds = [NSMutableArray array];
    NSMutableArray *itemCounts = [NSMutableArray array];
    NSMutableArray *itemMessages = [NSMutableArray array];
    [self.goodsList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MallGoodsModel *mode = (MallGoodsModel *)obj;
        [itemIds addObject:@(mode.itemId)];
        [itemCounts addObject:@(mode.num)];
        [itemMessages addObject:mode.message?mode.message:@""];
    }];
    
    NSDictionary *dict = @{@"itemIds":[itemIds componentsJoinedByString:@","],@"itemCounts":[itemCounts componentsJoinedByString:@","],@"itemMessages":[itemMessages componentsJoinedByString:@"|"],@"addressId":@(self.defaultAddress.addressId)};
    WS(weakSelf);
    
    [self.mallService orderConfirm:dict success:^(NSString *requestInfo, id responseObject) {
        [weakSelf pushToOrderDetailVC:responseObject];
    } failure:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:responseObject[@"message"]];
    }];
}

//提交用户制作相册
- (void)confirmUserMakeAlbum:(NSString *)madeItemId {
    MallGoodsModel *mode = [self.goodsList firstObject];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@(mode.itemId) forKey:@"itemId"];
    [dic setObject:@(self.defaultAddress.addressId) forKey:@"addressId"];
    WS(weakSelf);
    [self.makeService albumMake:mode.itemPhotos params:dic progress:^(NSProgress *progress) {
        
    } success:^(NSString *requestInfo, id responseObject) {
        dispatch_async(dispatch_get_main_queue(), ^{
            OrderModel *model = [OrderModel yy_modelWithJSON:responseObject[@"content"]];
            [weakSelf pushToOrderDetailVC:model];
        });
    } failure:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:responseObject[@"message"]];
    }];
}

- (void)confconfirmUserMakePicture {
    MallGoodsModel *mode = [self.goodsList firstObject];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@(mode.itemId) forKey:@"itemId"];
    [dic setObject:@(self.defaultAddress.addressId) forKey:@"addressId"];
    [dic setObject:@(mode.categoryId) forKey:@"album_id"];
    WS(weakSelf);
    [self.makeService postAlbumsMake:nil success:^(NSString *requestInfo, id responseObject) {
        dispatch_async(dispatch_get_main_queue(), ^{
            OrderModel *model = [OrderModel yy_modelWithJSON:responseObject[@"content"]];
            [weakSelf pushToOrderDetailVC:model];
        });
    } failure:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:responseObject[@"message"]];
    }];
}

//获取制作相册id
- (void)getMakeAlbumId {
    MallGoodsModel *mode = [self.goodsList firstObject];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@(mode.itemId) forKey:@"item_id"];
    [dic setObject:@(self.defaultAddress.addressId) forKey:@"addressId"];
    WS(weakSelf);
    [self.makeService itemsMake:dic success:^(NSString *requestInfo, id responseObject) {
        [weakSelf confirmUserMakeAlbum:responseObject[@"content"]];
    } failure:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:responseObject[@"message"]];
    }];
}

//编辑数量
- (void)editItemCount:(NSInteger)count item:(MallGoodsModel *)model {
    [self.mallService editCartsNum:@{@"cart_id":@(model.cartId),@"num":@(count)} success:^(NSString *requestInfo, id responseObject) {
        
    } failure:^(NSError *error, id responseObject) {
        
    }];
    [self reloadBottomView];
}

//进入订单详情
- (void)pushToOrderDetailVC:(OrderModel *)om {
    OrderDetailVC *vc = [[OrderDetailVC alloc] initWithShowBackButton:YES];
    vc.model = om;
    [self pushVC:vc animated:YES];
}

- (void)reloadBottomView {
    __block double total = 0;
    [self.goodsList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MallGoodsModel *item = (MallGoodsModel *)obj;
        total += (item.price * item.num);
    }];
    self.bottomView.price = total;
    self.bottomView.confirmBtn.enabled = self.goodsList.count > 0;
}

#pragma mark - UITableViewDataSource,UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.goodsList.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ShippingAddressCell *cell = (ShippingAddressCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ShippingAddressCell class])];
        cell.model = self.defaultAddress;
        return cell;
    }else{
        ConfirmOrderCell *cell = (ConfirmOrderCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ConfirmOrderCell class])];
        cell.model = self.goodsList[indexPath.section - 1];
        WS(weakSelf);
        cell.countBlock = ^(NSInteger count) {
            MallGoodsModel *model = weakSelf.goodsList[indexPath.section - 1];
            [weakSelf editItemCount:count item:model];
        };
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        UsualAddressListVC *vc = [[UsualAddressListVC alloc] initWithShowBackButton:YES];
        WS(weakSelf);
        vc.selectAddressBlock = ^(UsualAddressModel *model) {
            weakSelf.defaultAddress = model;
            [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
        };
        [self pushVC:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 0)?0.01:10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.section == 0)?67:(92 + 44 * 3 + 49);
}

- (MallNetService *)mallService {
    if (!_mallService) {
        _mallService = [[MallNetService alloc] init];
    }
    return _mallService;
}

- (MakeNetService *)makeService {
    if (!_makeService) {
        _makeService = [[MakeNetService alloc] init];
    }
    return _makeService;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
