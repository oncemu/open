//
//  PaySuccessVC.h
//  Open
//
//  Created by mfp on 17/9/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CollectionBaseVC.h"
#import "OrderModel.h"

@interface PaySuccessVC : CollectionBaseVC
@property(nonatomic, strong) OrderModel *model;
@end
