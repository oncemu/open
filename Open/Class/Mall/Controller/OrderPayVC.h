//
//  OrderPayVC.h
//  Open
//
//  Created by mfp on 17/9/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseVC.h"
#import "OrderModel.h"

@interface OrderPayVC : BaseVC

@property(nonatomic, strong) OrderModel *model;

@end
