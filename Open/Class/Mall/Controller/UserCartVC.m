//
//  UserCartVC.m
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserCartVC.h"
#import "MallNetService.h"
#import "UserCartView.h"
#import "ConfirmOrderVC.h"
#import "CancelOrderVC.h"
#import "AlbumDetailVC.h"

@interface UserCartVC ()
@property(nonatomic, strong) MallNetService *mallService;
@property(nonatomic, strong) UserCartView *cartView;
@property(nonatomic, strong) EmptyCartView *emptyView;
@property(nonatomic, strong) UIButton *rightBtn;

@end

@implementation UserCartVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"购物车";
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.mallService = [[MallNetService alloc] init];
    [self loadCartData:NO];
}

- (void)rightButtonClick {
    self.rightBtn.selected = !self.rightBtn.selected;
    self.cartView.editing = self.rightBtn.selected;
}

//加载购物车商品
- (void)loadCartData {
    [self loadCartData:NO];
}

- (void)loadMoreCartData {
    [self loadCartData:YES];
}

- (void)loadCartData:(BOOL)more {
    WS(weakSelf);
    [self.mallService getUserCarts:more success:^(NSString *requestInfo, id responseObject) {
        CartGoodsModel *mode = (CartGoodsModel *)responseObject;
        if (mode.content.count > 0) {
            [_emptyView removeFromSuperview];
            weakSelf.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
            [weakSelf.cartView reloadCartView:mode.content more:more];
            [weakSelf.view addSubview:weakSelf.cartView];
        }else{
            [_cartView removeFromSuperview];
            weakSelf.navigationItem.rightBarButtonItem = nil;
            [weakSelf.view addSubview:weakSelf.emptyView];
            [weakSelf loadRecommend];
        }
    } failure:^(NSError *error, id responseObject) {
        weakSelf.noDataButton.hidden = NO;
        weakSelf.noDataAction = ^() {
            [weakSelf loadCartData:NO];
        };
    }];
}

//删除购物车
- (void)deleteCarts {
    WS(weakSelf);
    [self.cartView.selectList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MallGoodsModel *model = (MallGoodsModel *)obj;
        [weakSelf.mallService deleteUserCarts:@{@"cart_id":@(model.cartId)} success:^(NSString *requestInfo, id responseObject) {
            
        } failure:^(NSError *error, id responseObject) {
            
        }];
    }];
    self.navigationItem.rightBarButtonItem = nil;
    [self.view addSubview:weakSelf.emptyView];
    [self loadRecommend];
}

//加载推荐商品
- (void)loadRecommend {
    [self loadRecommend:NO];
}

- (void)loadMoreRecommend {
    [self loadRecommend:YES];
}

- (void)loadRecommend:(BOOL)more {
    WS(weakSelf);
    [self.mallService cartRecommend:more success:^(NSString *requestInfo, id responseObject) {
        [weakSelf.emptyView reloadList:responseObject more:more];
    } failure:^(NSError *error, id responseObject) {
        weakSelf.noDataButton.hidden = NO;
    }];
}

- (void)pushToConfirmVC {
    ConfirmOrderVC *vc = [[ConfirmOrderVC alloc] initWithShowBackButton:YES];
    vc.goodsList = self.cartView.selectList;
    [self pushVC:vc animated:YES];
}

#pragma mark - cartView method
//删除商品
- (void)deleteItem:(NSInteger)index {
    WS(weakSelf);
    MallGoodsModel *model = self.cartView.list[index];
    [self.mallService deleteUserCarts:@{@"cart_id":@(model.cartId)} success:^(NSString *requestInfo, id responseObject) {
        if (weakSelf.cartView.list.count == 0) {
            [weakSelf loadCartData];
        }
    } failure:^(NSError *error, id responseObject) {
        
    }];

}

//编辑数量
- (void)editItemCount:(NSInteger)count item:(MallGoodsModel *)model {
    [self.mallService editCartsNum:@{@"cart_id":@(model.cartId),@"num":@(count)} success:^(NSString *requestInfo, id responseObject) {
        
    } failure:^(NSError *error, id responseObject) {
        
    }];
    [self reloadBottomView];
}

- (void)reloadBottomView {
    
}

- (UserCartView *)cartView {
    if (!_cartView) {
        _cartView = [[UserCartView alloc] initWithFrame:CM(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 64)];
        _cartView.header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadCartData)];
        _cartView.footer = [MJRefreshAutoStateFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreCartData)];
        WS(weakSelf);
        _cartView.bottomView.actionBlock = ^(BOOL del) {
            if (del) {
                [weakSelf deleteCarts];
            }else{
                [weakSelf pushToConfirmVC];
            }
        };
        _cartView.deleteBlock = ^(NSInteger index) {
            [weakSelf deleteItem:index];
        };
        _cartView.countBlock = ^(NSInteger count,NSInteger index) {
            MallGoodsModel *mode = weakSelf.cartView.list[index];
            [weakSelf editItemCount:count item:mode];
        };
    }
    return _cartView;
}

- (EmptyCartView *)emptyView {
    if (!_emptyView) {
        _emptyView = [[EmptyCartView alloc] initWithFrame:CM(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 64)];
        _emptyView.header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadCartData)];
        _emptyView.footer = [MJRefreshAutoStateFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreRecommend)];
        WS(weakSelf);
        _emptyView.selectBlock = ^(MallGoodsModel *model){
            AlbumDetailVC *vc = [[AlbumDetailVC alloc] initWithShowBackButton:YES];
            vc.model = model;
            [weakSelf pushVC:vc animated:YES];
        };
    }
    return _emptyView;
}

- (UIButton *)rightBtn {
    if (_rightBtn == nil) {
        _rightBtn = [ViewFactory getButton:CM(0, 0, 50, 44) font:Font(15) selectTitle:@"完成" norTitle:@"编辑" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(rightButtonClick)];
    }
    return _rightBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
