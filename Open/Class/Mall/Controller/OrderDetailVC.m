//
//  OrderDetailVC.m
//  Open
//
//  Created by mfp on 17/9/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "OrderDetailVC.h"
#import "OrderDetailView.h"
#import "PictureDetailView.h"
#import "CancelOrderVC.h"
#import "OrderPayVC.h"

@interface OrderDetailVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) DetailBottomBar *bottomView;
@end

@implementation OrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单详情";
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = BACKGROUND_COLOR;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionFooterHeight = 0;
    self.tableView.sectionHeaderHeight = 0;
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorColor = BACKGROUND_COLOR;
    [self.tableView registerClass:[OrderDetailView class] forCellReuseIdentifier:NSStringFromClass([OrderDetailView class])];

    [self.view addSubview:self.tableView];
    
    self.bottomView = [[DetailBottomBar alloc] initWithFrame:CM(0, SCREEN_HEIGHT - kTabBarHeight, SCREEN_WIDTH, kTabBarHeight) items:@[@"取消订单",@"付款"]];
    
    WS(weakSelf);
    _bottomView.clickBlock = ^(NSInteger index) {
        if (index == 0) {
            CancelOrderVC *vc = [[CancelOrderVC alloc] initWithShowBackButton:YES];
            vc.model = weakSelf.model;
            [weakSelf pushVC:vc animated:YES];
        }else{
            [weakSelf pay];
        }
    };
    [self.view addSubview:_bottomView];
}

#pragma mark - method
- (void)pay {
    OrderPayVC *vc = [[OrderPayVC alloc] initWithShowBackButton:YES];
    vc.model = self.model;
    [self pushVC:vc animated:YES];
}

#pragma mark - tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 76;
    }else if(indexPath.section == 1){
        return 206;
    }else {
        return 61;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        OrderDetailTop *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([OrderDetailTop class])];
        if (cell == nil) {
            cell = [[OrderDetailTop alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:NSStringFromClass([OrderDetailTop class])];
        }
        cell.textLabel.text = @"等待买家付款";
        cell.detailTextLabel.text = @"剩2天23小时自动关闭";
        return cell;
    }else if (indexPath.section == 1) {
        OrderDetailView *cell = (OrderDetailView *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([OrderDetailView class])];
        cell.model = self.model;
        return cell;
    }else {
        OrderDetailBottom *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([OrderDetailBottom class])];
        if (cell == nil) {
            cell = [[OrderDetailBottom alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:NSStringFromClass([OrderDetailBottom class])];
        }
        cell.textLabel.text = [NSString stringWithFormat:@"订单编号：%@",self.model.orderNo];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"创建时间：%@",self.model.createdAt];
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
