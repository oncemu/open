//
//  AlbumDetailVC.m
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "AlbumDetailVC.h"
#import "MallNetService.h"
#import "AlbumDetailView.h"
#import "PictureDetailView.h"
#import "MJRefresh.h"
#import "AlbumNetService.h"
#import "PhotoAlbumVC.h"
#import "PostCardEditListVC.h"
#import "MyAlbumVC.h"
#import "MakeNetService.h"

@interface AlbumDetailVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong) MallNetService *mallService;
@property(nonatomic, strong) AlbumNetService *albumService;
@property(nonatomic, strong) MakeNetService *makeService;
@property(nonatomic, strong) NSMutableArray *commentList;
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) AlbumDetailView *headView;
@property(nonatomic, strong) DetailBottomBar *bottomView;

@end

@implementation AlbumDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"相册详情";
    self.mallService = [[MallNetService alloc] init];
    self.albumService = [[AlbumNetService alloc] init];
    [self setBarButtonItemWithImage:getImage(@"pic_shopcart") title:nil isLeft:NO];
    self.commentList = [NSMutableArray array];
    self.tableView = [[UITableView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kTabBarHeight) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableView registerClass:[AlbumInfoCell class] forCellReuseIdentifier:NSStringFromClass([AlbumInfoCell class])];
    [_tableView registerClass:[AlbumTipView class] forCellReuseIdentifier:NSStringFromClass([AlbumTipView class])];
    [_tableView registerClass:[CommentCell class] forCellReuseIdentifier:NSStringFromClass([CommentCell class])];
    [_tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:NSStringFromClass([UITableViewHeaderFooterView class])];
    [self.view addSubview:_tableView];
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    _tableView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    CGFloat h = (SCREEN_WIDTH - 36 - 24)/4;
    CGFloat h1 = ((SCREEN_WIDTH - 36) * 360)/676.0f;
    
    _headView = [[AlbumDetailView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, h+h1+13)];
    _headView.mode = self.model;
    self.tableView.tableHeaderView = _headView;
    
    self.bottomView = [[DetailBottomBar alloc] initWithFrame:CM(0, SCREEN_HEIGHT - kTabBarHeight, SCREEN_WIDTH, kTabBarHeight) items:@[@"加入购物车",@"立即制作"]];
    WS(weakSelf);
    self.bottomView.clickBlock = ^(NSInteger index) {
        if (index == 0) {//加入购物车
            [weakSelf rightButtonItemAction];
        }else{//立即制作
            if (weakSelf.model.canMake) {
                [weakSelf showSelect];
            }
        }
    };

    [self.view addSubview:self.bottomView];
    [self.tableView.mj_header beginRefreshing];
}

//立即制作相册
- (void)makeAlbumNow:(AlbumFormatType)type {
    AlbumMakeData *data = [AlbumMakeData shareData];
    if ([data hasExistAlbum:type]) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"编辑上次未完成的相册？" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self pushToMakeAlbumVC:type];
        }];
        [alertVC addAction:cancelAction];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
    }else{
        [self pushToMakeAlbumVC:type];
    }
}

- (void)pushToMakeAlbumVC:(AlbumFormatType)type {
    MyAlbumVC *vc = [[MyAlbumVC alloc] initWithShowBackButton:YES];
    vc.type = type;
    vc.model = self.model;
    [self pushVC:vc animated:YES];
}

- (void)showSelect {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"选择%@种类",self.categoryModel.categoryName] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    switch (self.categoryModel.categoryId) {
        case 1://相册
        {
            [alertVC addAction:[UIAlertAction actionWithTitle:@"精装10寸大方册模板" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makeAlbumNow:AlbumFormatType_10_Asset];
            }]];
            [alertVC addAction:[UIAlertAction actionWithTitle:@"12寸竖册模板" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makeAlbumNow:AlbumFormatType_12_Portrait];
                
            }]];
            [alertVC addAction:[UIAlertAction actionWithTitle:@"7X10寸横册模板" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makeAlbumNow:AlbumFormatType_7X10_Landscape];
            }]];
        }
            break;
        case 2://宝丽莱照片
        {
            [alertVC addAction:[UIAlertAction actionWithTitle:@"宝丽来定制6寸照片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makePostCard:AlbumFormatType_BLL_6];
            }]];
            [alertVC addAction:[UIAlertAction actionWithTitle:@"宝丽来定制7寸照片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makePostCard:AlbumFormatType_BLL_7];
                
            }]];
            [alertVC addAction:[UIAlertAction actionWithTitle:@"宝丽来定制8寸照片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makePostCard:AlbumFormatType_BLL_8];
            }]];
            [alertVC addAction:[UIAlertAction actionWithTitle:@"宝丽来定制10寸照片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makePostCard:AlbumFormatType_BLL_10];
            }]];
        }
            break;
        case 3://摆台
        {
            [alertVC addAction:[UIAlertAction actionWithTitle:@"摆台" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makePostCard:AlbumFormatType_Talbe];
            }]];
        }
            break;
        case 4://画框
        {
            [alertVC addAction:[UIAlertAction actionWithTitle:@"画框" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makePostCard:AlbumFormatType_Frame];
            }]];
        }
            break;
        case 5://台历
        {
            [alertVC addAction:[UIAlertAction actionWithTitle:@"台历" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makePostCard:AlbumFormatType_DestCalendar];
            }]];
        }
            break;
        case 6://明信片
        {
            [alertVC addAction:[UIAlertAction actionWithTitle:@"明信片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makePostCard:AlbumFormatType_PostCart];
            }]];
        }
            break;
        case 7://公仔
        {
            [alertVC addAction:[UIAlertAction actionWithTitle:@"公仔" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self makePostCard:AlbumFormatType_BLL_10];
            }]];
        }
            break;
            
        default:
            break;
    }
    [alertVC addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    [self presentViewController:alertVC animated:YES completion:nil];
}

//美图明信片制作
- (void)makePostCard:(AlbumFormatType)type {
    PhotoAlbumVC *vc = [[PhotoAlbumVC alloc] initWithShowBackButton:YES];
    WS(weakSelf);
    vc.addPhotoBlock = ^(NSArray *images) {
        PostCardEditListVC *listVC = [[PostCardEditListVC alloc] initWithShowBackButton:YES];
        listVC.model = self.model;
        listVC.type = type;
        listVC.categoryModel = self.categoryModel;
        for (UIImage *image in images) {
            AlbumMakeObject *obj = [[AlbumMakeObject alloc] init];
            obj.image = image;
            obj.type = type;
            BaseAlbumObject *imageobj = [BaseAlbumObject getAlbumImage:image frame:CGRectZero];
            [obj addObject:imageobj];
            [listVC.dataList addObject:obj];
        }
        [weakSelf pushVC:listVC animated:YES];
    };
    CustomNavigationController *nav = [[CustomNavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

//加入购物车
- (void)rightButtonItemAction {
    [self.mallService addCarts:@{@"itemId":@(self.model.itemId)} success:^(NSString *requestInfo, id responseObject) {
        [KVNProgress showSuccessWithStatus:responseObject[@"message"]];
    } failure:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:responseObject[@"message"]];
    }];
}

- (void)loadData {
    [self loadAlbumDetailInfo];
    [self loadAlbumComments:NO];
}

- (void)loadMoreData {
    [self loadAlbumComments:YES];
}

//加载详情
- (void)loadAlbumDetailInfo {
    WS(weakSelf);
    [self.mallService getAlbumDetail:@{@"itemId":@(self.model.itemId)} success:^(NSString *requestInfo, id responseObject) {
        weakSelf.model = responseObject;
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

//加载评论
- (void)loadAlbumComments:(BOOL)more {
    WS(weakSelf);
    [self.albumService getAlbumsComments:@{@"albumId":@(self.model.itemId)} more:more success:^(NSString *requestInfo, id responseObject) {
        CommentData *data = (CommentData *)responseObject;
        if (!more) {
            [weakSelf.commentList removeAllObjects];
        }
        [weakSelf.commentList addObjectsFromArray:data.content];
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if (more && data.content.count == 0) {
            weakSelf.tableView.mj_footer.hidden = YES;
        }else{
            weakSelf.tableView.mj_footer.hidden = NO;
        }
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
        case 1:
            return 1;
            break;
        case 2:
            return self.commentList.count;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            AlbumInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AlbumInfoCell class])];
            cell.textLabel.text = [NSString stringWithFormat:@"%.2f",self.model.price];
            return cell;
        }
            break;
        case 1:
        {
            AlbumTipView *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AlbumTipView class])];
            cell.tipLabel.text = self.model.tips;
            return cell;
        }
            break;
        case 2:
        {
            CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CommentCell class])];
            cell.object = self.commentList[indexPath.item];
            return cell;
        }
            break;
    }
    return nil;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([UITableViewHeaderFooterView class])];
    header.contentView.backgroundColor = WhiteColor;
    header.textLabel.font = Font(15);
    switch (section) {
        case 0:
            header.textLabel.text = self.model.itemName;
            break;
        case 1:
            header.textLabel.text = @"温馨提示";
            break;
        case 2:
            header.textLabel.text = @"评价";
            break;
    }
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return 35;
            break;
        case 1:
        {
            CGFloat h = [StringUtil getLabelHeight:self.model.tips font:Font(16) width:SCREEN_WIDTH - 36];
            return h + 10;
        }
            break;
        case 2:
            return 99;
            break;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 37;
}

#pragma mark - get method

- (MakeNetService *)makeService {
    if (!_makeService) {
        _makeService = [[MakeNetService alloc] init];
    }
    return _makeService;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
