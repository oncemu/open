//
//  UsualAddressListVC.h
//  Open
//
//  Created by mfp on 17/9/21.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "TableBaseVC.h"
#import "UsualAddressModel.h"

@interface UsualAddressListVC : TableBaseVC
@property(nonatomic, copy) void (^selectAddressBlock)(UsualAddressModel *model);

@end
