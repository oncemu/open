//
//  ManageUsualAddressVC.h
//  Open
//
//  Created by mfp on 17/9/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "TableBaseVC.h"
#import "UsualAddressModel.h"

@interface ManageUsualAddressVC : TableBaseVC

//如果 addressModel 存在，说明是更改，否则是添加新地址
@property(nonatomic, strong) UsualAddressModel *addressModel;

@end
