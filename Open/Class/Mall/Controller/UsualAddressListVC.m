//
//  UsualAddressListVC.m
//  Open
//
//  Created by mfp on 17/9/21.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UsualAddressListVC.h"
#import "MallNetService.h"
#import "AddressModel.h"
#import "ManageUsualAddressVC.h"

@interface UsualAddressListVC ()

@property(nonatomic, strong) MallNetService *mallService;
@property(nonatomic, strong) NSMutableArray *addressList;
@end

@implementation UsualAddressListVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"常用地址";
    [self setBarButtonItemWithImage:nil title:@"添加地址" isLeft:NO];
    self.mallService = [[MallNetService alloc] init];
    self.addressList = [NSMutableArray array];
    self.tableView.rowHeight = 60;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
}

- (void)rightButtonItemAction {
    ManageUsualAddressVC *vc = [[ManageUsualAddressVC alloc] initWithShowBackButton:YES];
    vc.addressModel = [[UsualAddressModel alloc] init];
    [self pushVC:vc animated:YES];
}

- (void)loadData {
    WS(weakSelf);
    [self.mallService getUserAddressSuccess:^(NSString *requestInfo, id responseObject) {
        [weakSelf.addressList removeAllObjects];
        [weakSelf.addressList addObjectsFromArray:responseObject];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

//删除地址
- (void)deleteUsualAddress:(NSIndexPath *)indexPath {
    [self.tableView beginUpdates];
    [self.addressList removeObjectAtIndex:indexPath.row];
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    [self.tableView reloadData];
}

#pragma mark - table delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.addressList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:NSStringFromClass([UITableViewCell class])];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    UsualAddressModel *model = self.addressList[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",model.consignee,model.cellphone];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@",model.province,model.city,model.region,model.street];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    ManageUsualAddressVC *vc = [[ManageUsualAddressVC alloc] initWithShowBackButton:YES];
//    vc.addressModel = self.addressList[indexPath.row];
//    [self pushVC:vc animated:YES];
    if (self.selectAddressBlock) {
        self.selectAddressBlock(self.addressList[indexPath.row]);
        [self goBack];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteUsualAddress:indexPath];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
