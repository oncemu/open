//
//  MallVC.m
//  Open
//
//  Created by onceMu on 2017/7/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MallVC.h"
#import "MallNetService.h"
#import "MallView.h"
#import "AlbumDetailVC.h"
#import "UserCartVC.h"

@interface MallVC ()

@property(nonatomic, strong) LeftView       *leftView;
@property(nonatomic, strong) RightView      *rightView;
@property(nonatomic, strong) MallNetService *mallService;
@property(nonatomic, strong) NSMutableArray *categories;//分类列表
@property(nonatomic, strong) NSMutableArray *itemList;//分类下作品列表
@property(nonatomic, strong) MallCategoryModel *currentModel;
@end

@implementation MallVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.leftView.list.count == 0) {
        [self loadMallCategories];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"商店";
    self.mallService = [[MallNetService alloc] init];
    self.categories = [NSMutableArray array];
    self.itemList = [NSMutableArray array];
    [self setBarButtonItemWithImage:getImage(@"mallCart") title:nil isLeft:NO];
    [self setLeftView];
    [self setRightView];
}

//进入购物车
- (void)rightButtonItemAction {
    if ([self checkLogin:YES]) {
        [self pushToCartVC];
    }
}

- (void)loginSucceed {
    [self pushToCartVC];
}

- (void)pushToCartVC {
    UserCartVC *vc = [[UserCartVC alloc] initWithShowBackButton:YES];
    [self pushVC:vc animated:YES];
}

//加载相册分类
- (void)loadMallCategories {
    WS(weakSelf);
    [self.mallService getMallCategories:^(NSString *requestInfo, id responseObject) {
        weakSelf.rightView.hidden = NO;
        [weakSelf.leftView reloadList:responseObject];
    } failure:^(NSError *error, id responseObject) {
        weakSelf.noDataButton.hidden = NO;
        weakSelf.rightView.hidden = YES;
        weakSelf.noDataAction = ^() {
            [weakSelf loadMallCategories];
        };
    }];
}

//设置相册分类UI
- (void)setLeftView {
    WS(weakSelf);
    self.leftView = [[LeftView alloc] initWithFrame:CM(0, 0, 120, SCREEN_HEIGHT - kTabBarHeight)];
    self.leftView.selectIndexBlock = ^(MallCategoryModel *model) {
        weakSelf.currentModel = model;
        [weakSelf loadItems:NO];
    };
    [self.view addSubview:_leftView];
}

//设置相册图片列表UI
- (void)setRightView {
    self.rightView = [[RightView alloc] initWithFrame:CM(120, 0, SCREEN_WIDTH - 120, SCREEN_HEIGHT - kTabBarHeight)];
    [self.view addSubview:_rightView];
    self.rightView.header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    self.rightView.footer = [MJRefreshAutoStateFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    WS(weakSelf);
    self.rightView.selectAlbum = ^(MallGoodsModel *model) {
        [weakSelf pushToAlbumDetail:model];
    };
    self.rightView.reloadDataBlock = ^() {
        [weakSelf loadData];
    };
}
//加载分类相册list
- (void)loadData {
    [self loadItems:NO];
}

- (void)loadMoreData {
    [self loadItems:YES];
}

- (void)loadItems:(BOOL)more {
    WS(weakSelf);
    [self.mallService getMallCategoriesItems:more params:@{@"category_id":@(self.currentModel.categoryId)} success:^(NSString *requestInfo, id responseObject) {
        [weakSelf.rightView reloadList:responseObject more:more];
    } failure:^(NSError *error, id responseObject) {
        [weakSelf.rightView reloadList:@[] more:more];
    }];
}

//进入相册详情
- (void)pushToAlbumDetail:(MallGoodsModel *)model {
    AlbumDetailVC *vc = [[AlbumDetailVC alloc] initWithShowBackButton:YES];
    vc.model = model;
    vc.categoryModel = self.currentModel;
    [self pushVC:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
