//
//  ConfirmOrderVC.h
//  Open
//
//  Created by mfp on 17/9/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseVC.h"

typedef NS_ENUM(NSInteger, AlbumConfirmType) {
    AlbumConfirmTypeBuy,//直接购买商品
    AlbumConfirmTypeAlbum,//用户制作的相册
    AlbumConfirmTypePicture//用户制作图片
};

@interface ConfirmOrderVC : BaseVC

@property(nonatomic, assign) AlbumConfirmType type;
@property(nonatomic, strong) NSArray *goodsList;

@end
