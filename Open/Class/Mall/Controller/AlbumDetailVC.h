//
//  AlbumDetailVC.h
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseVC.h"
#import "MallNetService.h"

@interface AlbumDetailVC : BaseVC

@property(nonatomic, strong) MallCategoryModel *categoryModel;
@property(nonatomic, strong) MallGoodsModel *model;

@end
