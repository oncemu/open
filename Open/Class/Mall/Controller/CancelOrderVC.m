//
//  CancelOrderVC.m
//  Open
//
//  Created by mfp on 17/9/25.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CancelOrderVC.h"
#import "UITextView+Placeholder.h"
#import "PictureDetailView.h"
#import "MallNetService.h"

@interface CancelOrderVC ()
@property(nonatomic, strong) UIView *topView;
@property(nonatomic, strong) UITextView *inputView;
@property(nonatomic, strong) DetailBottomBar *bottomView;
@property(nonatomic, strong) MallNetService *mallService;
@end

@implementation CancelOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"取消订单";
    _topView = [[UIView alloc] initWithFrame:CM(0, 64, SCREEN_WIDTH, 78)];
    _topView.backgroundColor = WhiteColor;
    [self.view addSubview:_topView];
    
    UILabel *label = [ViewFactory getLabel:CM(19, 11, 200, 16) font:Font(14) textColor:ColorWithHex(0x333333) text:@"取消订单原因"];
    [_topView addSubview:label];
    
    NSArray *list = @[@"不喜欢",@"拍错了",@"其他"];
    for (NSInteger i = 0; i<list.count; i++) {
        UIButton *btn = [ViewFactory getButton:CM(18 + ((SCREEN_WIDTH - 36)/3) * i, CGRectGetMaxY(label.frame) + 20, (SCREEN_WIDTH - 36)/3, 30) selectImage:getImage(@"cart_dotselect") norImage:getImage(@"cart_dotunselect") target:self action:@selector(buttonClicked:)];
        [btn setTitleColor:ColorWithHex(0x333333) forState:UIControlStateNormal];
        [btn setTitle:list[i] forState:UIControlStateNormal];
        btn.titleLabel.font = Font(14);
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 25);
        btn.tag = i;
        [_topView addSubview:btn];
    }
    
    _inputView = [[UITextView alloc] initWithFrame:CM(0, CGRectGetMaxY(_topView.frame) + 10, SCREEN_WIDTH, 210)];
    _inputView.backgroundColor = WhiteColor;
    _inputView.textColor = ColorWithHex(0x666666);
    _inputView.font = Font(14);
    _inputView.contentInset = UIEdgeInsetsMake(0, 19, 0, 19);
    _inputView.placeholder = @"请填写其他原因";
    _inputView.placeholderColor = ColorWithHex(0x999999);
    [self.view addSubview:_inputView];
    
    self.bottomView = [[DetailBottomBar alloc] initWithFrame:CM(0, SCREEN_HEIGHT - kTabBarHeight, SCREEN_WIDTH, kTabBarHeight) items:@[@"取消",@"确定"]];
    WS(weakSelf);
    _bottomView.clickBlock = ^(NSInteger index) {
        if (index == 0) {
            [weakSelf goBack];
        }else{
            [weakSelf summit];
        }
    };
    [self.view addSubview:_bottomView];
}

- (void)summit {
    NSMutableString *reason = [NSMutableString string];
    for (UIButton *btn in _topView.subviews) {
        if ([btn isKindOfClass:[UIButton class]]) {
            if (btn.selected) {
                [reason appendString:btn.currentTitle];
            }
        }
    }
    if (_inputView.text) {
        [reason appendString:_inputView.text];
    }
    WS(weakSelf);
    [self.mallService cancelOrder:@{@"order_id":@(self.model.orderId),@"reason":reason} success:^(NSString *requestInfo, id responseObject) {
        [KVNProgress showSuccessWithStatus:responseObject[@"message"]];
        [weakSelf.navigationController popToRootViewControllerAnimated:YES];
    } failure:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:responseObject[@"message"]];
    }];
}

- (void)buttonClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (MallNetService *)mallService {
    if (!_mallService) {
        _mallService = [[MallNetService alloc] init];
    }
    return _mallService;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
