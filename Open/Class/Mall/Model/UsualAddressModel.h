//
//  UsualAddressModel.h
//  Open
//
//  Created by mfp on 17/9/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseModel.h"

@interface UsualAddressModel : BaseModel
@property(nonatomic, assign) NSInteger addressId;
@property(nonatomic, copy) NSString *consignee;//收货人姓名
@property(nonatomic, copy) NSString *cellphone;
@property(nonatomic, copy) NSString *province;
@property(nonatomic, copy) NSString *city;
@property(nonatomic, copy) NSString *region;//区县
@property(nonatomic, copy) NSString *street;//详细地址
@property(nonatomic, assign) BOOL isDefault; //默认地址

+ (NSArray *)parseAddress:(id)object;

@end
