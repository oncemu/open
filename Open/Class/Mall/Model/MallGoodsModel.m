//
//  MallGoodsModel.m
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MallGoodsModel.h"

@implementation MallGoodsModel

@end

@implementation CartGoodsModel

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{ @"content" : [MallGoodsModel class]};
}

@end
