//
//  OrderModel.h
//  Open
//
//  Created by mfp on 17/9/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderModel : NSObject

@property(nonatomic, assign) double cellphone;
@property(nonatomic, copy) NSString * consignee;
@property(nonatomic, copy) NSString * createdAt;
@property(nonatomic, copy) NSString * deliveryAddress;
@property(nonatomic, copy) NSArray * details;
@property(nonatomic, assign) NSInteger orderId;
@property(nonatomic, copy) NSString * orderNo;
@property(nonatomic, copy) NSString * status;
@property(nonatomic, copy) NSString * statusText;
@property(nonatomic, copy) NSString * totalExpressFee;
@property(nonatomic, copy) NSString * totalPrice;

@end

@interface DetailModel : NSObject
@property(nonatomic, assign) NSInteger detailId;
@property(nonatomic, copy) NSString * expressFee;
@property(nonatomic, assign) NSInteger itemId;
@property(nonatomic, copy) NSString * message;
@property(nonatomic, copy) NSString * name;
@property(nonatomic, assign) NSInteger num;
@property(nonatomic, copy) NSArray * photos;
@property(nonatomic, copy) NSString * price;

@end
