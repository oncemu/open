//
//  UsualAddressModel.m
//  Open
//
//  Created by mfp on 17/9/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UsualAddressModel.h"

@implementation UsualAddressModel

+ (NSArray *)parseAddress:(id)object {
    if (![object objectForKey:@"content"]) {
        return @[];
    }
    id data = object[@"content"];
    if ([data isKindOfClass:[NSArray class]]) {
        return [NSArray yy_modelArrayWithClass:[self class] json:data];
    }else{
        return @[];
    }
    return @[];
}

@end
