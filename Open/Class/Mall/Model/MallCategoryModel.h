//
//  MallCategoryModel.h
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseModel.h"

@interface MallCategoryModel : BaseModel
@property(nonatomic, assign) NSInteger categoryId;
@property(nonatomic, copy) NSString *categoryName;

@end
