//
//  OrderModel.m
//  Open
//
//  Created by mfp on 17/9/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "OrderModel.h"

@implementation OrderModel

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{ @"details" : [DetailModel class]};
}

@end

@implementation DetailModel

@end
