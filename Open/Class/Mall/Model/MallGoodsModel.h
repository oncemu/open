//
//  MallGoodsModel.h
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseModel.h"

@interface MallGoodsModel : BaseModel

@property(nonatomic, assign) BOOL canMake;
@property(nonatomic, assign) NSInteger categoryId;
@property(nonatomic, assign) double expressFee;//快递费
@property(nonatomic, assign) NSInteger itemId;
@property(nonatomic, copy) NSString  *itemName;
@property(nonatomic, strong) NSArray *photos;
@property(nonatomic, assign) double price;;
@property(nonatomic, copy) NSString *shipAddress;
@property(nonatomic, copy) NSString *tips;

@property(nonatomic, assign) NSInteger cartId;//加入购物车后的id
@property(nonatomic, strong) NSArray *itemPhotos;
@property(nonatomic, assign) NSInteger num;
@property(nonatomic, assign) double updatedAt;

@property(nonatomic, copy) NSString *message;//买家留言


@end

@interface CartGoodsModel : BaseModel

@end
