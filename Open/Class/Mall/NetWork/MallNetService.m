//
//  MallNetService.m
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MallNetService.h"

@implementation MallNetService
//获取商城 - 分类
- (void)getMallCategories:(responseSuccess)success
                  failure:(responseFailure)failure {
    [self requestGETwithUrl:kMallCategories parameters:nil success:^(NSString *requestInfo, id responseObject) {
        NSArray *list = [MallCategoryModel parseListModel:responseObject];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//获取商城分类商品
- (void)getMallCategoriesItems:(BOOL)more
                        params:(NSDictionary *)params
                       success:(responseSuccess)success
                       failure:(responseFailure)failure {
    if (!more) {
        MallCategoriesItemsIndex = 1;
    }
    NSString *categoryid = params[@"category_id"];
    NSString *url = [NSString stringWithFormat:@"mall/categories/%@/items",categoryid];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:params];
    [dic setObject:@(MallCategoriesItemsIndex) forKey:@"page"];
    [self requestGETwithUrl:url parameters:dic success:^(NSString *requestInfo, id responseObject) {
        MallCategoriesItemsIndex += 1;
        NSArray *list = [MallGoodsModel parseListModel:responseObject[@"content"]];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//获取商品详情
- (void)getAlbumDetail:(NSDictionary *)params
               success:(responseSuccess)success
               failure:(responseFailure)failure {
    NSString *itemid = params[@"itemId"];
    NSString *url = [NSString stringWithFormat:@"items/%@",itemid];
    [self requestGETwithUrl:url parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        MallGoodsModel *mode = [MallGoodsModel parseBaseModel:responseObject];
        success(requestInfo,mode);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}
//获取收货地址
- (void)getUserAddressSuccess:(responseSuccess)success
                      failure:(responseFailure)failure {
    [self requestGETwithUrl:kMallUserAddress parameters:nil success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
        NSArray *list = [UsualAddressModel parseAddress:responseObject];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//编辑收货地址
/*
 consignee  收货人姓名
 cellphone  手机号码
 province  省
 city      城市
 region    区/县
 street     详细地址
 */
- (void)editUserAddress:(NSDictionary *)params
                success:(responseSuccess)success
                failure:(responseFailure)failure {
    [self requestPOSTwithUrl:kMallUserAddress parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//GET 获取购物车
- (void)getUserCarts:(BOOL)more
             success:(responseSuccess)success
             failure:(responseFailure)failure {
    if (!more) {
        UserCartsIndex = 1;
    }
    [self requestGETwithUrl:kMallUserCarts parameters:@{@"page":@(UserCartsIndex)}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
        UserCartsIndex += 1;
        CartGoodsModel *model = [CartGoodsModel parseBaseModel:responseObject];
        success(requestInfo,model);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//POST 添加到购物车 itemId 商品ID
- (void)addCarts:(NSDictionary *)params
         success:(responseSuccess)success
         failure:(responseFailure)failure {
    [self requestPOSTwithUrl:kMallUserCarts parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//post 删除购物车 cart_id
- (void)deleteUserCarts:(NSDictionary *)params
                success:(responseSuccess)success
                failure:(responseFailure)failure {
    NSString *cart_id = params[@"cart_id"];
    NSString *url = [NSString stringWithFormat:@"mall/user/carts/%@/delete",cart_id];
    [self requestPOSTwithUrl:url parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//post 编辑购物车商品数量 cart_id
- (void)editCartsNum:(NSDictionary *)params
              success:(responseSuccess)success
              failure:(responseFailure)failure {
    NSString *cart_id = params[@"cart_id"];
    NSString *url = [NSString stringWithFormat:@"mall/user/carts/%@/item_num",cart_id];
    [self requestPOSTwithUrl:url parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//GET /mall/recommended_items 商城 - 购物车 - 商品推荐
- (void)cartRecommend:(BOOL)more
              success:(responseSuccess)success
              failure:(responseFailure)failure {
    if (!more) {
        CartRecommendIndex = 1;
    }
    [self requestGETwithUrl:kGetCartRecommend parameters:@{@"page":@(CartRecommendIndex)}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
        CartRecommendIndex += 1;
        NSArray *list = [MallGoodsModel parseListModel:responseObject[@"content"]];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//POST /user/orders 确认订单
- (void)orderConfirm:(NSDictionary *)params
             success:(responseSuccess)success
             failure:(responseFailure)failure {
    [self requestPOSTwithUrl:kOrderConfirm parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        OrderModel *model = [OrderModel yy_modelWithJSON:responseObject[@"content"]];
        success(requestInfo,model);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//POST /user/orders/{order_id}/payments 支付订单
- (void)payOrder:(NSDictionary *)params
         success:(responseSuccess)success
         failure:(responseFailure)failure {
    NSString *url = [NSString stringWithFormat:@"user/orders/%@/payments",params[@"order_id"]];
    [self requestPOSTwithUrl:url parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//POST /user/orders/{order_id}/cancel 取消订单
- (void)cancelOrder:(NSDictionary *)params
            success:(responseSuccess)success
            failure:(responseFailure)failure {
    NSString *url = [NSString stringWithFormat:@"user/orders/%@/cancel",params[@"order_id"]];
    [self requestPOSTwithUrl:url parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

@end
