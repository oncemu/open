//
//  MallNetService.h
//  Open
//
//  Created by mfp on 17/9/15.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseNetService.h"
#import "MallCategoryModel.h"
#import "MallGoodsModel.h"
#import "UsualAddressModel.h"
#import "OrderModel.h"

//GET /mall/categories 商城 - 分类
#define kMallCategories  @"mall/categories"

//GET /mall/categories/{category_id}/items 商城 - 分类商品 -- 商品详情数据可以从列表带入

//GET /mall/user/addresses 商城 - 获取 收货地址

//POST /mall/user/addresses 商城 - 新增收货地址

#define kMallUserAddress @"mall/user/addresses"

//GET /mall/user/carts 商城 - 购物车
//POST /mall/user/carts 商城 - 添加到购物车
#define kMallUserCarts @"mall/user/carts"

//POST /mall/user/carts/{cart_id}/delete 商城 - 删除购物车
//POST /mall/user/carts/{cart_id}/item_num 商城 - 编辑购物车商品数量

//GET /mall/recommended_items 商城 - 购物车 - 商品推荐
#define kGetCartRecommend  @"mall/recommended_items"

//POST /user/orders 确认订单
#define kOrderConfirm @"user/orders"

//POST /user/orders/{order_id}/payments 支付订单

@interface MallNetService : BaseNetService
{
    NSInteger MallCategoriesItemsIndex; //获取商城分类商品分页
    NSInteger UserCartsIndex;           //购物车分页
    NSInteger CartRecommendIndex;       //购物车推荐商品分页

}
//获取商城 - 分类
- (void)getMallCategories:(responseSuccess)success
                  failure:(responseFailure)failure;

//获取商城分类商品
- (void)getMallCategoriesItems:(BOOL)more
                        params:(NSDictionary *)params
                       success:(responseSuccess)success
                       failure:(responseFailure)failure;

//获取商品详情
- (void)getAlbumDetail:(NSDictionary *)params
               success:(responseSuccess)success
               failure:(responseFailure)failure;

//获取收货地址
- (void)getUserAddressSuccess:(responseSuccess)success
                      failure:(responseFailure)failure;

//编辑收货地址
/*
consignee  收货人姓名
cellphone  手机号码
province  省
city      城市
region    区/县
street     详细地址
*/
- (void)editUserAddress:(NSDictionary *)params
                success:(responseSuccess)success
                failure:(responseFailure)failure;

//GET 获取购物车
- (void)getUserCarts:(BOOL)more
             success:(responseSuccess)success
             failure:(responseFailure)failure;

//POST 添加到购物车 itemId 商品ID
- (void)addCarts:(NSDictionary *)params
         success:(responseSuccess)success
         failure:(responseFailure)failure;

//post 删除购物车 cart_id
- (void)deleteUserCarts:(NSDictionary *)params
                success:(responseSuccess)success
                failure:(responseFailure)failure;

//post 编辑购物车商品数量 cart_id
- (void)editCartsNum:(NSDictionary *)params
              success:(responseSuccess)success
              failure:(responseFailure)failure;

//GET /mall/recommended_items 商城 - 购物车 - 商品推荐
- (void)cartRecommend:(BOOL)more
              success:(responseSuccess)success
              failure:(responseFailure)failure;

//POST /user/orders 确认订单
- (void)orderConfirm:(NSDictionary *)params
             success:(responseSuccess)success
             failure:(responseFailure)failure;

//POST /user/orders/{order_id}/payments 支付订单
- (void)payOrder:(NSDictionary *)params
             success:(responseSuccess)success
             failure:(responseFailure)failure;

//POST /user/orders/{order_id}/cancel 取消订单
- (void)cancelOrder:(NSDictionary *)params
            success:(responseSuccess)success
            failure:(responseFailure)failure;

@end
