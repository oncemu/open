//
//  UserNetService.h
//  Open
//
//  Created by onceMu on 2017/7/25.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseNetService.h"

typedef NS_ENUM(NSInteger,CaptchaType) {
    CaptchaTypeRegister,//注册
    CaptchaTypeBindingPhone,//绑定手机号
    CaptchaTypeModifyPwd,// 修改手机号
    CaptchaTypeLostPwd//忘记密码
};

typedef NS_ENUM(NSInteger,SignType) {
    SignTypeNormal,//普通手机号注册
    SignTypeQQ,//QQ
    SignTypeWeChat //微信
};

@interface UserNetService : BaseNetService




/**
 获取用户验证码

 @param mobile 电话号码    SIGN_UP_ADD_IMPROVE_DATA-注册；BIND-绑定手机号；MODIFY_PWD-修改密码；LOST_PWD-忘记密码   验证码类型
 @param success 成功
 @param failed 失败
 */
- (void)fetchCaptcha:(NSString *)mobile
                type:(CaptchaType)type
             success:(responseSuccess)success
              failed:(responseFailure)failed;



/**
 注册

 @param type 用户来源
 @param parames 参数，mobile、password、openId
 @param success 成功
 @param failed 失败
 */
- (void)signType:(SignType)type
         parames:(NSMutableDictionary *)parames
         success:(responseSuccess)success
          failed:(responseFailure)failed;



/**
 手机号注册

 @param parames mobile，password,confirmPassword,captcha
 @param success 成功
 @param failed 失败
 */
- (void)signUp:(NSMutableDictionary *)parames
       success:(responseSuccess)success
        failed:(responseFailure)failed;


/**
 获取用户信息

 @param parames 参数
 @param success 成功
 @param failed 失败
 */
- (void)getUserInfo:(NSMutableDictionary *)parames
            success:(responseSuccess)success
             failed:(responseFailure)failed;


/**
 更新用户信息

 @param parames 参数
 @param success 成功
 @param failed 失败
 */
- (void)updateUserInfo:(NSMutableDictionary *)parames
               success:(responseSuccess)success
                failed:(responseFailure)failed;


/**
 用户退出登录

 @param parames 参数
 @param success 成功
 @param failed 失败
 */
- (void)userLoginOut:(NSMutableDictionary *)parames
             success:(responseSuccess)success
              failed:(responseFailure)failed;


/**
 获取用户点赞列表

 @param parames 参数
 @param success 成功
 @param failed 失败
 */
- (void)getUserPrasieList:(NSMutableDictionary *)parames
                  success:(responseSuccess)success
                   failed:(responseFailure)failed;


/**
 获取用户发布列表

 @param parames 参数
 @param success 成功
 @param failed 失败
 */
- (void)getUserPublishList:(NSMutableDictionary *)parames
                   success:(responseSuccess)success
                    failed:(responseFailure)failed;



/**
 获取用户美图

 @param parames 参数
 @param success 成功
 @param failed 失败
 */
- (void)getUserCommonPhotoList:(NSMutableDictionary *)parames
                       success:(responseSuccess)success
                        failed:(responseFailure)failed;

/**
 获取用户上架美图

 @param parames 参数
 @param success 成功
 @param faield 失败
 */
- (void)getUserPhotoList:(NSMutableDictionary *)parames
                 success:(responseSuccess)success
                  failed:(responseFailure)faield;

//更新用户的背景图
- (void)updateUserBackGround:(NSMutableDictionary *)parames
                     success:(responseSuccess)success
                      failed:(responseFailure)failed;
@end
