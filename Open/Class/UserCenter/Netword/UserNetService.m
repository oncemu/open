//
//  UserNetService.m
//  Open
//
//  Created by onceMu on 2017/7/25.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserNetService.h"
#import "User.h"
#import "UserPrasieItem.h"
#import "UserWorkItem.h"
#import "AlbumModel.h"

@implementation UserNetService

- (void)fetchCaptcha:(NSString *)mobile
                type:(CaptchaType)type
             success:(responseSuccess)success
              failed:(responseFailure)failed {
    NSMutableDictionary *parames = [NSMutableDictionary dictionaryWithCapacity:1];
    [parames setObject:mobile forKey:@"mobile"];
    switch (type) {
        case CaptchaTypeLostPwd:
        {
            [parames setObject:@"LOST_PWD" forKey:@"type"];
        }
            break;
        case CaptchaTypeRegister:
        {
            [parames setObject:@"SIGN_UP_ADD_IMPROVE_DATA" forKey:@"type"];
        }
            break;
        case CaptchaTypeModifyPwd:
        {
            [parames setObject:@"MODIFY_PWD" forKey:@"type"];
        }
            break;
        case CaptchaTypeBindingPhone:
        {
            [parames setObject:@"BIND" forKey:@"type"];
        }
            break;
        default:
            break;
    }
    [self requestPOSTwithUrl:kCaptcha parameters:parames success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
    } failure:^(NSError *error, id responseObject) {
        failed(error,responseObject);
    }];
}

- (void)signType:(SignType)type
         parames:(NSMutableDictionary *)parames
         success:(responseSuccess)success
          failed:(responseFailure)failed {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:1];
    if (parames) {
        [dict addEntriesFromDictionary:parames];
    }
    switch (type) {
        case SignTypeQQ:
        {
            [dict setObject:@"QQ" forKey:@"source"];
        }
            break;
        case SignTypeNormal:
        {
            [dict setObject:@"NORMAL" forKey:@"source"];
        }
            break;
        case SignTypeWeChat:
        {
            [dict setObject:@"WECHAT" forKey:@"source"];
        }
            break;
        default:
            break;
    }
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    [self requestPOSTwithUrl:kSignIn parameters:dict success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);

        NSString *token = [[responseObject objectForKey:@"content"] objectForKey:@"accessToken"];
        strongSelf.gdm.accessToken = token;
        [strongSelf.gdm archive];

        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failed(error,responseObject);
    }];
}

- (void)signUp:(NSMutableDictionary *)parames
       success:(responseSuccess)success
        failed:(responseFailure)failed {
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    [self requestPOSTwithUrl:kSignUp parameters:parames success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
        NSString *token = [[responseObject objectForKey:@"content"] objectForKey:@"accessToken"];
        strongSelf.gdm.accessToken = token;
        [strongSelf.gdm archive];
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failed(error,responseObject);
    }];
}

- (void)getUserInfo:(NSMutableDictionary *)parames
            success:(responseSuccess)success
             failed:(responseFailure)failed {
    [self requestGETwithUrl:kUserInfo parameters:parames success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
        User *user = [User yy_modelWithJSON:[responseObject objectForKey:@"content"]];
        success(requestInfo,user);
    } failure:^(NSError *error, id responseObject) {
        failed(error,responseObject);
    }];
}

- (void)userLoginOut:(NSMutableDictionary *)parames
             success:(responseSuccess)success
              failed:(responseFailure)failed {
    [self requestPOSTwithUrl:kLoiginOut parameters:parames success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failed(error,responseObject);
    }];
}

- (void)getUserPrasieList:(NSMutableDictionary *)parames
                  success:(responseSuccess)success
                   failed:(responseFailure)failed {
    [self requestGETwithUrl:kUserPrasieList parameters:parames success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
        success(requestInfo,[self converParsieList:[[responseObject objectForKey:@"content"] objectForKey:@"content"]]);
    } failure:^(NSError *error, id responseObject) {
        DLog(@"%@",responseObject);
        failed(error,responseObject);
    }];
}

- (void)getUserPublishList:(NSMutableDictionary *)parames
                   success:(responseSuccess)success
                    failed:(responseFailure)failed {
    [self requestGETwithUrl:kUserPublishList parameters:parames success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
        success(requestInfo,[self converWordList:[[responseObject objectForKey:@"content"] objectForKey:@"content"]]);
    } failure:^(NSError *error, id responseObject) {
        failed(error,responseObject);
    }];
}

- (void)getUserCommonPhotoList:(NSMutableDictionary *)parames
                       success:(responseSuccess)success
                        failed:(responseFailure)failed {
    [self requestGETwithUrl:kUserPhotoList parameters:parames success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,[self converWordList:[[responseObject objectForKey:@"content"] objectForKey:@"content"]]);
    } failure:^(NSError *error, id responseObject) {
        failed(error,responseObject);
    }];
}

- (void)getUserPhotoList:(NSMutableDictionary *)parames
                 success:(responseSuccess)success
                  failed:(responseFailure)faield {
    [self requestGETwithUrl:kUserPhotoListForSale parameters:parames success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,[self converWordList:[responseObject objectForKey:@"content"]]);
    } failure:^(NSError *error, id responseObject) {
        faield(error,responseObject);
    }];
}

- (void)updateUserInfo:(NSMutableDictionary *)parames
               success:(responseSuccess)success
                failed:(responseFailure)failed {
    if ([parames objectForKey:@"avatar"]) {
        UIImage *image = (UIImage *)[parames objectForKey:@"avatar"];
        NSArray *images = @[image];
        [parames removeObjectForKey:@"avatar"];
        [self requestUpLoadFileWithURLString:kUserInfo images:images parameters:parames filePath:@"avatar" success:^(NSString *requestInfo, id responseObject) {
            success(requestInfo,responseObject);
        } failed:^(NSError *error, id responseObject) {
            failed(error,responseObject);
        }];
    }else {
        [self requestPOSTwithUrl:kUserInfo parameters:parames success:^(NSString *requestInfo, id responseObject) {
            DLog(@"%@",responseObject);
            success(requestInfo,responseObject);
        } failure:^(NSError *error, id responseObject) {
            DLog(@"%@",responseObject);
            failed(error,responseObject);
        }];
    }
}

- (void)updateUserBackGround:(NSMutableDictionary *)parames
                     success:(responseSuccess)success
                      failed:(responseFailure)failed {
    if ([parames objectForKey:@"background"]) {
        UIImage *image = [parames objectForKey:@"background"];
        NSArray *images = @[image];
//        [parames removeObjectForKey:@"background"];
        [parames setObject:@"" forKey:@"background"];
        [self requestUpLoadFileWithURLString:kUserBackGround images:images parameters:parames filePath:@"background" success:^(NSString *requestInfo, id responseObject) {
            success(requestInfo,responseObject);
        } failed:^(NSError *error, id responseObject) {
            failed(error,responseObject);
        }];
    }
}

- (NSMutableArray *)converParsieList:(NSArray *)data {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:1];
    for(NSDictionary *dict in data) {
        AlbumModel *item = [AlbumModel yy_modelWithDictionary:dict];
        [result addObject:item];
    }
    return result;
}

- (NSMutableArray *)converWordList:(NSArray *)data {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:1];
    for(NSDictionary *dict in data) {
        AlbumModel *item = [AlbumModel yy_modelWithJSON:dict];
        [result addObject:item];
    }
    return result;
}

@end
