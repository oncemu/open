//
//  UserFansCell.m
//  Open
//
//  Created by onceMu on 2017/7/31.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserFansCell.h"

@implementation UserFansCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _headerImageView = [[UIImageView alloc]init];
    _headerImageView.clipsToBounds = YES;
    [self.contentView addSubview:_headerImageView];
    [_headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    _headerImageView.layer.cornerRadius = 15.0f;

    _followBtn = [FollowButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:_followBtn];
    [_followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(65, 25));
    }];

    _lblName = [[UILabel alloc]init];
    _lblName.font = [UIFont systemFontOfSize:13.0f];
    _lblName.textColor = [UIColor colorWithHex:0x000000];
    [self.contentView addSubview:_lblName];
    [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headerImageView.mas_right).offset(10);
        make.top.mas_equalTo(self.mas_top).offset(14);
        make.right.mas_equalTo(self.followBtn.mas_left).offset(-10);
        make.height.mas_equalTo(13);
    }];


    _lblContent = [[UILabel alloc]init];
    _lblContent.font = [UIFont systemFontOfSize:13.0f];
    _lblContent.textColor = [UIColor colorWithHex:0x999999];
    [self.contentView addSubview:_lblContent];
    [_lblContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headerImageView.mas_right).offset(10);
        make.top.mas_equalTo(self.lblName.mas_bottom).offset(4);
        make.right.mas_equalTo(self.followBtn.mas_left).offset(-10);
        make.height.mas_equalTo(13);
    }];

    _lineView = [[UIView alloc]init];
    _lineView.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [self.contentView addSubview:_lineView];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-0);
        make.height.mas_equalTo(0.5);
    }];

}

- (void)assignFans:(id)fansObject hiddenLine:(BOOL)hidden {
    _lineView.hidden = hidden;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _headerImageView.image = nil;
    _lblName.text = nil;
    _lblContent.text = nil;
}

@end


@implementation FollowButton

- (instancetype)init {
    if (self = [super init]) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.backgroundColor = [UIColor colorWithHex:0xfde133];
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    self.layer.cornerRadius = 4.0f;
    [self setTitleColor:[UIColor colorWithHex:0x323232] forState:UIControlStateNormal];
    [self addTarget:self action:@selector(tapSelf:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)tapSelf:(UIButton *)sender {
    NSString *title = sender.titleLabel.text;
    if ([title isEqualToString:@"互相关注"]) {
        title = @"取消关注";
    }
    if (self.block) {
        self.block(title);
    }
}

- (void)changeFollowType:(FollowType)type {
    _followType = type;
    switch (type) {
        case FollowTypeFollow:
        {
            [self setTitle:@"取消关注" forState:UIControlStateNormal];
        }
            break;
        case FollowTypeUnFollow:
        {
            [self setTitle:@"加关注" forState:UIControlStateNormal];
        }
            break;
        case FollowTypeAllFollow:
        {
            [self setTitle:@"互相关注" forState:UIControlStateNormal];
        }
            break;
        case FollowButtonAllUnFollow:
        {
            [self setTitle:@"加关注" forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
}

@end
