//
//  UserFansCell.h
//  Open
//
//  Created by onceMu on 2017/7/31.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FollowButton;
@interface UserFansCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *headerImageView;
@property (nonatomic, strong) UILabel *lblName;
@property (nonatomic, strong) UILabel *lblContent;
@property (nonatomic, strong) FollowButton *followBtn;
@property (nonatomic, strong) UIView *lineView;

- (void)assignFans:(id)fansObject
        hiddenLine:(BOOL)hidden;

@end



typedef NS_ENUM(NSInteger,FollowType) {
    FollowButtonAllUnFollow,//全部未关注
    FollowTypeFollow,//关注
    FollowTypeUnFollow,//未关注
    FollowTypeAllFollow,//互相关注
};

//成功block回调
typedef void(^FollowButtonBlock)(NSString *actionTitle);

@interface FollowButton : UIButton
@property (nonatomic, assign) FollowType followType;
@property (nonatomic, copy) FollowButtonBlock block;

- (void)changeFollowType:(FollowType)type;

@end
