//
//  RankCell.h
//  Open
//
//  Created by onceMu on 2017/8/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RankCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *rankImage;
@property (nonatomic, strong) UILabel *lblName;
@property (nonatomic, strong) UILabel *lblContent;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIView *spaceView;

- (void)assignObejct:(id)object
               index:(NSInteger)index;

@end
