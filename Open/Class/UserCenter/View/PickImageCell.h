//
//  QXPickImageCell.h
//  ShownailSeller
//
//  Created by mfp on 16/11/5.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import "PickImageViewModel.h"

typedef void(^ClickSelectedButtonBlock)(PHAsset *asset);

@interface PickImageCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView   *thumbnailImageView;
@property (nonatomic, strong) UIButton      *selectBtn;
@property (weak,nonatomic) PickAlbumViewModel *albumViewModel;
@property (nonatomic, assign) BOOL          isCamera;

@property (nonatomic, strong) PHAsset *asset;
@property (nonatomic, weak) PickImageViewModel  *imageViewModel;
@property (nonatomic, copy) ClickSelectedButtonBlock clickSelectedButtonBlock;

@end
