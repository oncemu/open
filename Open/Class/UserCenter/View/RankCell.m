//
//  RankCell.m
//  Open
//
//  Created by onceMu on 2017/8/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "RankCell.h"

@implementation RankCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _rankImage = [[UIImageView alloc]init];
    [self.contentView addSubview:_rankImage];
    [_rankImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];

    _lineView = [[UIView alloc]init];
    _lineView.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [self.contentView addSubview:_lineView];

    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(120);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.height.mas_equalTo(0.5);
    }];

    _lblName = [[UILabel alloc]init];
    _lblName.font = [UIFont systemFontOfSize:14.0f];
    _lblName.textColor = [UIColor colorWithHex:0x343333];
    [self.contentView addSubview:_lblName];
    [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(120);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.height.mas_equalTo(14);
    }];

    _lblContent = [[UILabel alloc]init];
    _lblContent.font = [UIFont systemFontOfSize:14];
    _lblContent.textColor = [UIColor colorWithHex:0x343333];
    _lblContent.numberOfLines = 0;
    [self.contentView addSubview:_lblContent];
    [_lblContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.lblName.mas_left);
        make.top.mas_equalTo(self.lblName.mas_bottom).offset(10);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-10);
        make.right.mas_equalTo(self.mas_right).offset(-20);
    }];

    _spaceView = [[UIView alloc]init];
    _spaceView.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [self.contentView addSubview:_spaceView];
    [_spaceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(40);
        make.top.mas_equalTo(self.rankImage.mas_bottom).offset(10);
        make.bottom.mas_equalTo(self.mas_bottom).offset(10);
        make.width.mas_equalTo(4);
    }];
}

- (void)assignObejct:(id)object index:(NSInteger)index {
    if (index == 4) {
        _spaceView.hidden = YES;
        _lineView.hidden = YES;
    }else {
        _spaceView.hidden = NO;
        _lineView.hidden = NO;
    }
    _rankImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"rank%ld",(index+1)]];
    _lblName.text = [NSString stringWithFormat:@"LV %ld",(index+1)];
    _lblContent.text = @"积分哈世纪东方好机会打分卡死发货的发哈就是发环境设计安居房";
}

@end
