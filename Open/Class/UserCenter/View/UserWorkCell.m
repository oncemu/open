//
//  UserWorkCell.m
//  Open
//
//  Created by onceMu on 2017/9/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserWorkCell.h"

@implementation UserWorkCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    self.backgroundColor = [UIColor whiteColor];
    _workImageView = [[UserWorkImageView alloc]init];
    [self.contentView addSubview:_workImageView];
    [_workImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.top.mas_equalTo(self.mas_top).offset(20);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.height.mas_equalTo(180);
    }];


    _lblTitle = [[UILabel alloc]init];
    _lblTitle.font = [UIFont systemFontOfSize:15.0f];
    _lblTitle.textColor = [UIColor colorWithHex:0x676767];
    [self.contentView addSubview:_lblTitle];
    [_lblTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.top.mas_equalTo(self.workImageView.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(100, 16));
    }];

    _cateforyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _cateforyButton.backgroundColor = [UIColor colorWithHex:0xffde00];
    [_cateforyButton setTitleColor:[UIColor colorWithHex:0x333333] forState:UIControlStateNormal];
    _cateforyButton.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    [self.contentView addSubview:_cateforyButton];
    [_cateforyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.lblTitle.mas_right).offset(5);
        make.top.mas_equalTo(self.lblTitle.mas_top);
        make.size.mas_equalTo(CGSizeMake(40, 16));
    }];

    _lblComment = [[UILabel alloc]init];
    _lblComment.textColor = [UIColor colorWithHex:0xd0d0d0];
    _lblComment.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_lblComment];
    [_lblComment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.top.mas_equalTo(self.workImageView.mas_bottom).offset(14);
        make.size.mas_equalTo(CGSizeMake(40, 13));
    }];

    _commentImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_commentImageView];
    [_commentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.lblComment.mas_left).offset(-5);
        make.top.mas_equalTo(self.workImageView.mas_bottom).offset(12);
        make.size.mas_equalTo(CGSizeMake(16, 16));
    }];

    _lblLike = [[UILabel alloc]init];
    _lblLike.textColor = [UIColor colorWithHex:0xd0d0d0];
    _lblLike.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_lblLike];
    [_lblLike mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.commentImageView.mas_left).offset(-5);
        make.top.mas_equalTo(self.workImageView.mas_bottom).offset(14);
        make.size.mas_equalTo(CGSizeMake(40, 13));
    }];

    _likeImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_likeImageView];
    [_likeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.lblLike.mas_left).offset(-5);
        make.top.mas_equalTo(self.workImageView.mas_bottom).offset(13);
        make.size.mas_equalTo(CGSizeMake(17, 15));
    }];

    _lineView = [[UIView alloc]init];
    _lineView.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [self.contentView addSubview:_lineView];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.right.mas_equalTo(self.mas_right);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)setItem:(AlbumModel *)item {
    _item = item;
    [_workImageView assignImages:item.photos];
    NSString *title = [NSString stringWithFormat:@"摄影 | %@",item.nickname];
    CGSize titleSize = [title boundingRectWithSize:CGSizeMake(MAXFLOAT, 16) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:_lblTitle.font} context:nil].size;
    [_lblTitle mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(ceilf(titleSize.width), 16));
    }];
    _lblTitle.text = title;
    [_cateforyButton setTitle:item.categoryText forState:UIControlStateNormal];

    NSString *comment = [NSString stringWithFormat:@"%@",@(item.commentCount)];
    CGSize commentSize = [comment boundingRectWithSize:CGSizeMake(MAXFLOAT, 13) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:_lblComment.font} context:nil].size;
    [_lblComment mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceilf(commentSize.width)+1);
    }];
    _lblComment.text = comment;

    [_commentImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_lblComment.mas_left).offset(-5);
    }];

    NSString *like = [NSString stringWithFormat:@"%@",@(item.favoriteCount)];
    CGSize likeSize = [like boundingRectWithSize:CGSizeMake(MAXFLOAT, 13) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:_lblComment.font} context:nil].size;
    [_lblLike mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceilf(likeSize.width)+1);
    }];

    _lblLike.text = like;

    [_likeImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_lblLike.mas_left).offset(-5);
    }];
    _likeImageView.image = [UIImage imageNamed:@"home_heart"];
    _commentImageView.image = [UIImage imageNamed:@"home_comment"];
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _lblLike.text = nil;
    _lblComment.text = nil;
    _lblTitle.text = nil;
    [_workImageView preparForResue];
    _likeImageView.image = nil;
    _commentImageView.image = nil;
}

@end


@implementation UserWorkImageView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    self.backgroundColor = [UIColor clearColor];
}

- (void)preparForResue {
    for(UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
}

- (void)assignImages:(NSArray *)images {
    for(UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    if (images.count == 1) {
        UIImageView *imageView1 = [[UIImageView alloc]init];
        [self addSubview:imageView1];
        [imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left);
            make.top.mas_equalTo(self.mas_top);
            make.right.mas_equalTo(self.mas_right);
            make.bottom.mas_equalTo(self.mas_bottom);
        }];
        PhotoItem *item = images[0];
        [imageView1 sd_setImageWithURL:[NSURL URLWithString:item.url]];
    }

    if (images.count == 2) {
        UIImageView *imageView1 = [[UIImageView alloc]init];
        [self addSubview:imageView1];
        CGFloat width = (SCREEN_WIDTH-50)/2;
        [imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left);
            make.top.mas_equalTo(self.mas_top);
            make.bottom.mas_equalTo(self.mas_bottom);
            make.width.mas_equalTo(width);
        }];
        PhotoItem *item = images[0];
        [imageView1 sd_setImageWithURL:[NSURL URLWithString:item.url]];

        UIImageView *imageView2 = [[UIImageView alloc]init];
        [self addSubview:imageView2];
        [imageView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(imageView1.mas_right).offset(10);
            make.top.mas_equalTo(self.mas_top);
            make.bottom.mas_equalTo(self.mas_bottom);
            make.width.mas_equalTo(width);
        }];
        PhotoItem *item1 = images[1];
        [imageView2 sd_setImageWithURL:[NSURL URLWithString:item1.url]];

    }
    if (images.count >= 3) {
        UIImageView *imageView1 = [[UIImageView alloc]init];
        [self addSubview:imageView1];
        CGFloat width = (SCREEN_WIDTH-50)/3;
        [imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left);
            make.top.mas_equalTo(self.mas_top);
            make.bottom.mas_equalTo(self.mas_bottom);
            make.width.mas_equalTo(ceilf(width*2));
        }];
        PhotoItem *item = images[0];
        [imageView1 sd_setImageWithURL:[NSURL URLWithString:item.url]];

        UIImageView *imageView2 = [[UIImageView alloc]init];
        [self addSubview:imageView2];
        [imageView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(imageView1.mas_right).offset(10);
            make.top.mas_equalTo(self.mas_top);
            make.height.mas_equalTo(85);
            make.width.mas_equalTo(width);
        }];
        PhotoItem *item1 = images[1];
        [imageView2 sd_setImageWithURL:[NSURL URLWithString:item1.url]];

        UIImageView *imageView3 = [[UIImageView alloc]init];
        [self addSubview:imageView3];
        [imageView3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(imageView1.mas_right).offset(10);
            make.top.mas_equalTo(imageView2.mas_bottom).offset(10);
            make.height.mas_equalTo(85);
            make.width.mas_equalTo(width);
        }];
        PhotoItem *item2 = images[2];
        [imageView3 sd_setImageWithURL:[NSURL URLWithString:item2.url]];
    }
}

@end
