//
//  QXMyVoucherSelectHeader.h
//  ShowNail
//
//  Created by mengfanpeng on 14-9-23.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, numPosition) {
    positionDown = 0,//数字显示在文字下方
    positionLeft = 1,// 数字在文字右侧
    positionTopRight = 2 // 数字在文字右上角
};
typedef void(^tipNumBlock)(UIButton *btn);
typedef void(^movetipNumBlock)(UIButton *btn);

@protocol SelectedHeaderDelegate <NSObject>
-(void)didSelectedItemIndex:(NSInteger)index;
@optional
-(void)selectedAgainItemIndex:(NSInteger)index; //点击同一按钮

@end
@interface UserCenterSelectHeader : UIView
@property(nonatomic,weak)id<SelectedHeaderDelegate>delegate;
@property(nonatomic,strong)UIView *floatView;   //浮动下划线
@property(nonatomic,strong)NSMutableArray *buttons;
@property(nonatomic,strong)NSMutableArray *titlesArray;
@property(nonatomic,strong)NSMutableArray *countArray;
@property(nonatomic,strong)NSMutableArray *floatViewOffsets;
@property(nonatomic,assign)BOOL getRandomColor;  //当前选中标题是否随即颜色 default NO
@property(nonatomic,assign)BOOL zoomCurrentSelectedTitle; //当前选中标题是否放大 default NO
@property(nonatomic,assign)BOOL isNeedLineView;//当前视图是否需要加下划线、Default NO
@property(nonatomic,assign)BOOL isNeedBlur;//透明模糊效果 Default NO  >ios7
@property(nonatomic,assign)NSInteger commentNum;
@property(nonatomic,assign)NSInteger likeNum;
@property(nonatomic,strong)UIView *bottomline;
@property(nonatomic,assign)NSInteger selectIndex;
@property(nonatomic,assign)BOOL isNeedNum;
@property(nonatomic,assign)NSInteger currentNum; //需要显示数字时 当前选项的数量
@property(nonatomic,copy)tipNumBlock tipHandler;
@property(nonatomic,copy)movetipNumBlock moveHandler;
- (id)initWithFrame:(CGRect)frame titles:(NSArray *)titles;
- (id)initWithFrame:(CGRect)frame titles:(NSArray *)titles isNeedNum:(BOOL)isNeedNum;//是否需要显示数量
-(void)setNumber:(NSInteger)count index:(NSInteger)index withIndexPostition:(numPosition)position;
-(void)setTipHandler:(tipNumBlock)tipHandler;
-(void)setMoveHandler:(movetipNumBlock)moveHandler;
@end
