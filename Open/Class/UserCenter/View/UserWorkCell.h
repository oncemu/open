//
//  UserWorkCell.h
//  Open
//
//  Created by onceMu on 2017/9/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//  用户作品

#import <UIKit/UIKit.h>
#import "UserWorkItem.h"
#import "AlbumModel.h"

@class UserWorkImageView;

@interface UserWorkCell : UICollectionViewCell

@property (nonatomic, strong) UserWorkImageView *workImageView;


@property (nonatomic, strong) UILabel *lblTitle;
@property (nonatomic, strong) UIButton *cateforyButton;
@property (nonatomic, strong) UILabel *lblComment;
@property (nonatomic, strong) UIImageView *commentImageView;
@property (nonatomic, strong) UILabel *lblLike;
@property (nonatomic, strong) UIImageView *likeImageView;
@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) AlbumModel *item;

@end


@interface UserWorkImageView : UIView

- (void)assignImages:(NSArray *)images;
- (void)preparForResue;

@end
