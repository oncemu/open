//
//  UserWorkCell.h
//  Open
//
//  Created by onceMu on 2017/9/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserPrasieItem.h"
#import "CustomImageView.h"
#import "AlbumModel.h"

@interface UserPrasieCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *coverImage;
@property (nonatomic, strong) UILabel *lblTitle;
@property (nonatomic, strong) UILabel *lblContent;
@property (nonatomic, strong) UILabel *lblComment;
@property (nonatomic, strong) UIImageView *commentImage;
@property (nonatomic, strong) UILabel *lblLike;
@property (nonatomic, strong) UIImageView *likeImage;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) AlbumModel *item;

@end
