//
//  CustomImageTextView.m
//  Open
//
//  Created by onceMu on 2017/7/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CustomImageTextView.h"

@implementation CustomImageTextView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
                         type:(ContentType)type
                     fontSize:(CGFloat)fontSize
                         font:(UIFont *)font {
    self = [super initWithFrame:frame];
    if (self) {
        _type = type;
        _fontSize = fontSize;
        _font = font;
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _imageView = [[UIImageView alloc]init];
    [self addSubview:_imageView];
    _lblTitle = [[UILabel alloc]init];
    _lblTitle.font = _font;
    [self addSubview:_lblTitle];
    
}

- (void)setFont:(UIFont *)font {
    _lblTitle.font = font;
}

- (void)setLeftImage:(UIImage *)leftImage {
    _leftImage = leftImage;
    [self updateFrames];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    [self updateFrames];
}

- (void)updateFrames {
    switch (_type) {
        case ContentTypeLeft:
        {
            [self leftFrames];
        }
            break;
        case ContentTypeCenter:
        {
            [self centerFrames];
        }
            break;
        case ContentTypeRight:
        {
            [self rightFrames];
        }
            break;
        default:
            break;
    }
}


- (void)leftFrames {
    CGFloat height = 11;
    if (_leftImage) {
        _imageView.frame = CGRectMake(5, (self.frameSizeHeight-_leftImage.size.height)/2, _leftImage.size.width, _leftImage.size.height);
        height = _leftImage.size.height;
        _imageView.image = _leftImage;
    }
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(5);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(_leftImage.size.width, _leftImage.size.height));
    }];
//    _lblTitle.frame = CGRectMake(CGRectGetMaxX(_imageView.bounds)+5, (self.frameSizeHeight-self.fontSize)/2, self.frameSizeWidth-5-CGRectGetMaxX(_lblTitle.bounds), self.fontSize);
    [_lblTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.imageView.mas_right).offset(5);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.right.mas_equalTo(self.mas_right).offset(-5);
        make.height.mas_equalTo(height);
    }];
//    _lblTitle.font = [UIFont systemFontOfSize:height];
    _lblTitle.text = _title;
}

- (void)centerFrames {
    CGFloat width = 0;
    if (_leftImage) {
        width = _leftImage.size.width;
    }
    if (_title) {
        CGSize bounds = [self.title boundingRectWithSize:CGSizeMake(MAXFLOAT, self.fontSize) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.font} context:nil].size;
        width += ceil(bounds.width) + 5;
    }

    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(5);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(_leftImage.size.width, _leftImage.size.height));
    }];
    _imageView.image = _leftImage;

    [_lblTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.imageView.mas_right).offset(2);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.right.mas_equalTo(self.mas_right);
        make.height.mas_equalTo(11);
    }];
    _lblTitle.text = _title;
}

- (void)rightFrames {
    CGFloat width = 0;
    if (_title) {
        CGSize bounds = [self.title boundingRectWithSize:CGSizeMake(MAXFLOAT, self.fontSize) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.font} context:nil].size;
        width += ceil(bounds.width) + 5;
    }

    _lblTitle.text = _title;
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(5);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(_leftImage.size.width, _leftImage.size.height));
    }];
    _imageView.image = _leftImage;
    [_lblTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.imageView.mas_right).offset(5);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.right.mas_equalTo(self.mas_right).offset(-5);
        make.height.mas_equalTo(11);
    }];
}




@end
