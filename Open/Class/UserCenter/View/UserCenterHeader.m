//
//  UserCenterHeader.m
//  Open
//
//  Created by onceMu on 2017/7/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserCenterHeader.h"

#define kHeaderButtonTag 1000

@implementation UserCenterHeader

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

//@property (nonatomic, strong) UIView *contentView;
//@property (nonatomic, strong) UIImageView *backImageview;
//@property (nonatomic, strong) UIButton *senttingBtn;
//@property (nonatomic, strong) UIButton *signBtn;
//@property (nonatomic, strong) UIImageView *logImageview;
//@property (nonatomic, strong) UILabel *lblStatus;
//@property (nonatomic, strong) UIImageView *headerImage;
//@property (nonatomic, strong) CustomImageTextView *goldView;
//@property (nonatomic, strong) CustomImageTextView *likeView;
//@property (nonatomic, strong) UILabel *lblSign;

- (void)setupViews {
    self.backgroundColor = [UIColor colorWithHex:0xefefec];
    _lastTag = 0;
    _contentView = [[UIView alloc]init];
    _contentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_contentView];
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.top.mas_equalTo(self.mas_top);
        make.right.mas_equalTo(self.mas_right);
        make.bottom.mas_equalTo(self.mas_bottom).offset(0);
    }];

    _backImageview = [[UIImageView alloc]init];
    _backImageview.clipsToBounds = YES;
    _backImageview.contentMode = UIViewContentModeScaleToFill;
    [self.contentView addSubview:_backImageview];
    [_backImageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.top.mas_equalTo(self.mas_top);
        make.right.mas_equalTo(self.mas_right);
        make.height.mas_equalTo(200);
    }];

    _backImageview.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapBackgroudImage)];
    [_backImageview addGestureRecognizer:tap];
//    [self.contentView sendSubviewToBack:_backImageview];

    //30 * 30

    _settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_settingBtn setBackgroundImage:[UIImage imageNamed:@"setting"] forState:UIControlStateNormal];
    [self.contentView addSubview:_settingBtn];
    [_settingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView.mas_left).offset(16);
        make.top.mas_equalTo(self.contentView.mas_top).offset(12);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    UIView *settingView = [[UIView alloc]init];
    [self.contentView addSubview:settingView];
    settingView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapBack = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSettingButtonClick)];
    [settingView addGestureRecognizer:tapBack];
    
    [settingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.top.mas_equalTo(self.mas_top);
        make.size.mas_equalTo(CGSizeMake(60, 50));
    }];

    _signBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _signBtn.layer.cornerRadius = 5.0f;
    _signBtn.backgroundColor = [UIColor colorWithHex:0xffe000];
    _signBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [_signBtn setTitleColor:[UIColor colorWithHex:0x343434] forState:UIControlStateNormal];
    [_signBtn setTitle:@"签到" forState:UIControlStateNormal];
    _signBtn.userInteractionEnabled = NO;
    [self.contentView addSubview:_signBtn];
    [_signBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-16);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.size.mas_equalTo(CGSizeMake(40, 20));
    }];

    UIView *signView = [[UIView alloc]init];
    [self.contentView addSubview:signView];
    signView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapSign = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSignButtonClicked)];
    [signView addGestureRecognizer:tapSign];
    [signView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right);
        make.top.mas_equalTo(self.mas_top);
        make.size.mas_equalTo(CGSizeMake(60, 50));
    }];

    _shopBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_shopBtn setBackgroundImage:[UIImage imageNamed:@"shopCart"] forState:UIControlStateNormal];
    [_shopBtn addTarget:self action:@selector(shopCartClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_shopBtn];
    [_shopBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.signBtn.mas_left).offset(-20);
        make.top.mas_equalTo(self.signBtn.mas_top);
        make.size.mas_equalTo(CGSizeMake(24, 24));
    }];

    //116
    _logImageview = [[UIImageView alloc]init];
    [self.contentView addSubview:_logImageview];
    [_logImageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.top.mas_equalTo(self.mas_top).offset(56);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    _lblStatus = [[UILabel alloc]init];
    _lblStatus.font = [UIFont systemFontOfSize:14.0f];
    _lblStatus.textColor = [UIColor colorWithHex:0xffe000];
    [self.contentView addSubview:_lblStatus];
    [_lblStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.logImageview.mas_top).offset(13);
        make.left.mas_equalTo(self.logImageview.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(200, 14));
    }];

    _headerImage = [[UIImageView alloc]init];
    _headerImage.clipsToBounds = YES;
    [self.contentView addSubview:_headerImage];
    [_headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset((SCREEN_WIDTH-45)/2);
        make.top.mas_equalTo(self.logImageview.mas_bottom).offset(5);
        make.size.mas_equalTo(CGSizeMake(45, 45));
    }];
    _headerImage.layer.cornerRadius = 45/2.0f;
    _headerImage.image = [UIImage imageNamed:@"headerDefault"];

    _nameView = [[CustomImageTextView alloc]initWithFrame:CGRectZero type:ContentTypeCenter fontSize:11 font:[UIFont systemFontOfSize:11.0f]];
    _nameView.layer.cornerRadius = 3.0f;
    _nameView.backgroundColor = [UIColor whiteColor];
    _nameView.alpha = 0.85;
    [self.contentView addSubview:_nameView];

    UIImage *nameImage = [UIImage imageNamed:@"femal"];
    NSString *name = @"";
    CGSize size1 = [name boundingRectWithSize:CGSizeMake(MAXFLOAT, 11) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11.0]} context:nil].size;
    CGFloat widt = nameImage.size.width;
    widt += 12;
    widt += ceil(size1.width);

    [_nameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView.mas_left).offset((SCREEN_WIDTH-widt)/2);
        make.top.mas_equalTo(self.headerImage.mas_bottom).offset(20);
        make.size.mas_equalTo(CGSizeMake(widt, 22));
    }];
//    _nameView.frame = CGRectMake((SCREEN_WIDTH-widt)/2, CGRectGetMaxY(self.headerImage.bounds)+20, widt, 22);
    _nameView.leftImage = nameImage;
    _nameView.title = name;

    _line = [[UIView alloc]init];
    _line.backgroundColor = [UIColor colorWithHex:0x343434];
    [self.contentView addSubview:_line];
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(SCREEN_WIDTH/2);
        make.top.mas_equalTo(self.backImageview.mas_bottom).offset(22);
        make.size.mas_equalTo(CGSizeMake(0.5, 12));
    }];
    [self.contentView addSubview:_line];

    _goldView = [[CustomImageTextView alloc]initWithFrame:CGRectZero type:ContentTypeLeft fontSize:11.0 font:[UIFont systemFontOfSize:11.0f]];
    _goldView.userInteractionEnabled = YES;
    UITapGestureRecognizer *glodTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goldViewClick)];
    [_goldView addGestureRecognizer:glodTap];
    [self.contentView addSubview:_goldView];

    _likeView = [[CustomImageTextView alloc]initWithFrame:CGRectZero type:ContentTypeRight fontSize:11.0 font:[UIFont systemFontOfSize:11.0f]];
    _likeView.userInteractionEnabled = YES;
    UITapGestureRecognizer *likeTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(likeTapClick)];
    [_likeView addGestureRecognizer:likeTap];
    [self.contentView addSubview:_likeView];


    NSString *gold = @"";
    UIImage *image = [UIImage imageNamed:@"gold"];
    CGFloat width = image.size.width;
    width +=10;
    CGSize bounds = [gold boundingRectWithSize:CGSizeMake(MAXFLOAT, 11) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]} context:nil].size;
    width += ceil(bounds.width)+5;
    [_goldView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.line.mas_left).offset(-5);
        make.top.mas_equalTo(self.line.mas_top);
        make.size.mas_equalTo(CGSizeMake(width, 12));
    }];
//    _goldView.frame = CGRectMake(SCREEN_WIDTH/2-width, self.line.frameOriginY, width, 12);
    _goldView.leftImage = image;
    _goldView.title = gold;

    NSString *gold2 = @"";
    UIImage *image2 = [UIImage imageNamed:@"Male-User"];
    CGFloat width2 = image2.size.width;
    width2 +=10;
    CGSize bounds2 = [gold2 boundingRectWithSize:CGSizeMake(MAXFLOAT, 11) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]} context:nil].size;
    width2 += ceil(bounds2.width)+5;
//    _likeView.backgroundColor = [UIColor redColor];
    [_likeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.line.mas_right).offset(5);
        make.top.mas_equalTo(self.line.mas_top);
        make.size.mas_equalTo(CGSizeMake(width2, 12));
    }];
//    _likeView.frame = CGRectMake(SCREEN_WIDTH/2+5, self.line.frameOriginY, width2, 12);

    _likeView.leftImage = image2;
    _likeView.title = gold2;

    _lblSign = [[UILabel alloc]init];
    _lblSign.textAlignment = NSTextAlignmentCenter;
    _lblSign.font = [UIFont systemFontOfSize:12.0f];
    _lblSign.textColor = [UIColor colorWithHex:0x343434];
    [self.contentView addSubview:_lblSign];
    [_lblSign mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-15);
        make.top.mas_equalTo(self.line.mas_bottom).offset(16);
        make.height.mas_equalTo(12);
    }];

    _bottomLine = [[UIView alloc]init];
    _bottomLine.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [self.contentView addSubview:_bottomLine];
    [_bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-0.5);
        make.height.mas_equalTo(0.5);
    }];

    CGFloat buttonWidth = (SCREEN_WIDTH-184)/3;
    _publishButton = [[UserCenterHeaderButton alloc]initWithFrame:CGRectMake(42, self.frameSizeHeight-44, buttonWidth, 44) type:UserCenterHeaderButtonTypeRight];
    _publishButton.tag = kHeaderButtonTag;
    _publishButton.userInteractionEnabled = YES;
    [self addSubview:_publishButton];

    _prasieButton = [[UserCenterHeaderButton alloc]initWithFrame:CGRectMake(_publishButton.frameOriginX+_publishButton.frameSizeWidth+50, self.frameSizeHeight-44, buttonWidth, 44) type:UserCenterHeaderButtonTypeMiddle];
    _prasieButton.tag = kHeaderButtonTag+1;
    _prasieButton.userInteractionEnabled = YES;
    [self addSubview:_prasieButton];

    _photoButton = [[UserCenterHeaderButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-42-buttonWidth, self.frameSizeHeight-44, buttonWidth, 44) type:UserCenterHeaderButtonTypeLeft];
    _photoButton.tag = kHeaderButtonTag+2;
    _photoButton.userInteractionEnabled = YES;
    [self addSubview:_photoButton];

    UITapGestureRecognizer *tapHeaderButton = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHeaderButton)];
    UITapGestureRecognizer *tapHeaderButton1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHeaderButton1)];
    UITapGestureRecognizer *tapHeaderButton2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHeaderButton2)];
    [_publishButton addGestureRecognizer:tapHeaderButton];
    [_prasieButton addGestureRecognizer:tapHeaderButton1];
    [_photoButton addGestureRecognizer:tapHeaderButton2];

    _scrollLine = [[UIView alloc]initWithFrame:CGRectMake(42, self.frameSizeHeight-2, buttonWidth, 2)];
    _scrollLine.backgroundColor = [UIColor colorWithHex:0xf4e86b];
    [self addSubview:_scrollLine];
    _offX = 42;
}

- (void)tapHeaderButton {
    if (_lastTag == _publishButton.tag - kHeaderButtonTag) {
        return;
    }
    _lastTag = _publishButton.tag - kHeaderButtonTag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickHeaderWithIndex:)]) {
        [self.delegate clickHeaderWithIndex:_lastTag];
    }
//    CGFloat width = (SCREEN_WIDTH-184)/3;
    CGFloat width = _publishButton.frameSizeWidth;
    [UIView animateWithDuration:0.3 animations:^{
        _scrollLine.frameOriginX = _offX;
        _scrollLine.frameSizeWidth = width;
    }];
}

- (void)tapHeaderButton1 {
    if (_lastTag == _prasieButton.tag - kHeaderButtonTag) {
        return;
    }
    _lastTag = _prasieButton.tag - kHeaderButtonTag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickHeaderWithIndex:)]) {
        [self.delegate clickHeaderWithIndex:_lastTag];
    }
    CGFloat width = _prasieButton.frameSizeWidth;
    CGFloat offX = _publishButton.frameSizeWidth+_publishButton.frameOriginX+50;
    [UIView animateWithDuration:0.3 animations:^{
        _scrollLine.frameOriginX = offX;
        _scrollLine.frameSizeWidth = width;
    }];
}

- (void)tapHeaderButton2 {
    if (_lastTag == _photoButton.tag - kHeaderButtonTag) {
        return;
    }
    _lastTag = _photoButton.tag - kHeaderButtonTag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickHeaderWithIndex:)]) {
        [self.delegate clickHeaderWithIndex:_lastTag];
    }
    CGFloat width = _photoButton.frameSizeWidth;
    CGFloat offX = _prasieButton.frameSizeWidth+_prasieButton.frameOriginX+50;
    [UIView animateWithDuration:0.3 animations:^{
        _scrollLine.frameOriginX = offX;
        _scrollLine.frameSizeWidth = width;
    }];
}


- (void)onSignButtonClicked {
    if (self.delegate && [self.delegate respondsToSelector:@selector(signBtnClick)]) {
        [self.delegate signBtnClick];
    }
}

- (void)shopCartClick {
    if (self.delegate && [self.delegate respondsToSelector:@selector(shopCartBtnClick)]) {
        [self.delegate shopCartBtnClick];
    }
}

- (void)onSettingButtonClick {
    if (self.delegate && [self.delegate respondsToSelector:@selector(settingBtnClick)]) {
        [self.delegate settingBtnClick];
    }
}

- (void)tapBackgroudImage {
    if (self.delegate && [self.delegate respondsToSelector:@selector(changeBackgroundImage)]) {
        [self.delegate changeBackgroundImage];
    }
}

- (void)likeTapClick {
    if (self.delegate && [self.delegate respondsToSelector:@selector(followBtnClick)]) {
        [self.delegate followBtnClick];
    }
}

- (void)goldViewClick {
    if (self.delegate && [self.delegate respondsToSelector:@selector(earningBtnClick)]) {
        [self.delegate earningBtnClick];
    }
}

- (void)setUser:(User *)user {
    _user = user;
    UIImage *nameImage;
    NSString *name = user.gender;
    if ([name isEqualToString:@"男"]) {
        nameImage = [UIImage imageNamed:@"Male"];
    }else {
        nameImage = [UIImage imageNamed:@"femal"];
    }
    CGSize size1 = [user.nickname boundingRectWithSize:CGSizeMake(MAXFLOAT, 11) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11.0]} context:nil].size;
    CGFloat widt = nameImage.size.width;
    widt += 12;
    widt += ceil(size1.width);

    [_nameView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView.mas_left).offset((SCREEN_WIDTH-widt)/2);
        make.top.mas_equalTo(self.headerImage.mas_bottom).offset(20);
        make.size.mas_equalTo(CGSizeMake(widt, 22));
    }];
    _nameView.leftImage = nameImage;
    _nameView.title = user.nickname;

    NSString *gold = [NSString stringWithFormat:@"%ld",user.gold];
    UIImage *image = [UIImage imageNamed:@"gold"];
    CGFloat width = image.size.width;
    width +=10;
    CGSize bounds = [gold boundingRectWithSize:CGSizeMake(MAXFLOAT, 11) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]} context:nil].size;
    width += ceil(bounds.width)+5;
    [_goldView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.line.mas_left).offset(-5);
        make.top.mas_equalTo(self.line.mas_top);
        make.size.mas_equalTo(CGSizeMake(width, 12));
    }];
    //    _goldView.frame = CGRectMake(SCREEN_WIDTH/2-width, self.line.frameOriginY, width, 12);
    _goldView.leftImage = image;
    _goldView.title = gold;


    NSString *gold2 = [NSString stringWithFormat:@"%ld",user.fansCount];
    UIImage *image2 = [UIImage imageNamed:@"Male-User"];
    CGFloat width2 = image2.size.width;
    width2 +=10;
    CGSize bounds2 = [gold2 boundingRectWithSize:CGSizeMake(MAXFLOAT, 11) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]} context:nil].size;
    width2 += ceil(bounds2.width)+5;
    [_likeView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.line.mas_right).offset(5);
        make.top.mas_equalTo(self.line.mas_top);
        make.size.mas_equalTo(CGSizeMake(width2, 12));
    }];
    //    _likeView.frame = CGRectMake(SCREEN_WIDTH/2+5, self.line.frameOriginY, width2, 12);

    _likeView.leftImage = image2;
    _likeView.title = gold2;

    _lblSign.text = user.signature;

    [_headerImage sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"headerDefault"]];

    NSInteger rank = user.level;
    if (rank == 0) {
        return;
    }
    UIImage *rankImage = [UIImage imageNamed:[NSString stringWithFormat:@"rank%ld",rank]];
    CGSize size = [user.levelName boundingRectWithSize:CGSizeMake(MAXFLOAT, 14) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil].size;
    CGFloat offWidth = rankImage.size.width + ceil(size.width) + 5;
    CGFloat offX = (SCREEN_WIDTH-offWidth)/2;
    [_logImageview mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(offX);
        make.top.mas_equalTo(self.mas_top).offset(56);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];

    _logImageview.image = rankImage;
    _lblStatus.text = user.levelName;
    [_backImageview sd_setImageWithURL:[NSURL URLWithString:user.background]];
//    [_backImageview sd_setImageWithURL:[NSURL URLWithString:@"http://ov76eg20o.bkt.clouddn.com/photo/Xmb1u0BnJrFrMbUnNZLipwNylYA.jpg"]];

    [self setupHeaderButton];
}

- (void)setupHeaderButton {
    _publishButton.lblName.text = [NSString stringWithFormat:@"发布 %@",@(_user.postCount)];
    _prasieButton.lblName.text = [NSString stringWithFormat:@"点赞 %@",@(_user.favoriteCount)];
    _photoButton.lblName.text = [NSString stringWithFormat:@"美图 %@",@(_user.photoCount)];

    CGFloat pubulishWidth = 25;
    UIFont *font = _publishButton.lblName.font;
    CGSize size = [_publishButton.lblName.text boundingRectWithSize:CGSizeMake(MAXFLOAT, 14) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    pubulishWidth += ceilf(size.width);

    CGFloat prasieWidth = 24;
    CGSize prasieSize = [_prasieButton.lblName.text boundingRectWithSize:CGSizeMake(MAXFLOAT, 14) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    prasieWidth += ceilf(prasieSize.width);

    CGFloat photoWidth = 26;
    CGSize photoSize = [_photoButton.lblName.text boundingRectWithSize:CGSizeMake(MAXFLOAT, 14) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    photoWidth += ceilf(photoSize.width);

    CGFloat allWidth = pubulishWidth + prasieWidth + photoWidth + 100;
    CGFloat offX = (SCREEN_WIDTH-allWidth)/2;
    _offX = offX;
    [UIView animateWithDuration:0.3 animations:^{
        _publishButton.frame = CGRectMake(_offX, self.frameSizeHeight-44, pubulishWidth, 44);
        _prasieButton.frame = CGRectMake(_publishButton.frameOriginX+_publishButton.frameSizeWidth+50, self.frameSizeHeight-44, prasieWidth, 44);
        _photoButton.frame = CGRectMake(_prasieButton.frameSizeWidth+_prasieButton.frameOriginX+50, self.frameSizeHeight-44, photoWidth, 44);
    }];
    [UIView animateWithDuration:0.3 animations:^{
        [_publishButton updateWidth:ceilf(size.width)];
        [_prasieButton updateWidth:ceilf(prasieSize.width)];
        [_photoButton updateWidth:ceilf(photoSize.width)];
    }];
}

- (void)didSelctedIndex:(NSInteger)index {
    if (index == 0) {
        [self tapHeaderButton];
    }else if (index == 1) {
        [self tapHeaderButton1];
    }else {
        [self tapHeaderButton2];
    }
}


@end


@implementation UserCenterHeaderButton

- (instancetype)initWithFrame:(CGRect)frame
                         type:(UserCenterHeaderButtonType)type {
    self = [super initWithFrame:frame];
    if (self) {
        _type = type;
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _lblName = [[UILabel alloc]init];
    _lblName.font = [UIFont systemFontOfSize:14.0f];
    _lblName.textColor = [UIColor colorWithHex:0x333333];
    [self addSubview:_lblName];
    _headerImage = [[UIImageView alloc]init];
    [self addSubview:_headerImage];
    switch (_type) {
        case UserCenterHeaderButtonTypeRight:
        {
            [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(self.mas_right);
                make.centerY.mas_equalTo(self.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(40, 14));
            }];
            [_headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(self.lblName.mas_left).offset(-7);
                make.centerY.mas_equalTo(self.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(18, 18));
            }];
            _headerImage.image = [UIImage imageNamed:@"userCup"];
            _lblName.text = @"发布";
        }
            break;
        case UserCenterHeaderButtonTypeMiddle:
        {
            CGFloat width = (SCREEN_WIDTH-184)/3;
            CGFloat allWidth = 64;
            [_headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.mas_left).offset((width-allWidth)/2);
                make.centerY.mas_equalTo(self.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(17, 15));
            }];
            [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(_headerImage.mas_right).offset(7);
                make.centerY.mas_equalTo(self.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(40, 14));
            }];
            _headerImage.image = [UIImage imageNamed:@"userPrasie"];
            _lblName.text = @"点赞";
        }
            break;
        case UserCenterHeaderButtonTypeLeft:
        {
            [_headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.mas_left);
                make.centerY.mas_equalTo(self.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(19, 15));
            }];

            [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(_headerImage.mas_right).offset(7);
                make.centerY.mas_equalTo(self.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(40, 14));
            }];
            _headerImage.image = [UIImage imageNamed:@"userPhoto"];
            _lblName.text = @"美图";
        }
            break;
        default:
            break;
    }

}

- (void)updateWidth:(CGFloat)width {
    [_lblName mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(width);
    }];
}

@end
