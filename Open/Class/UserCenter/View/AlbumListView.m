//
//  QXAlbumListView.m
//  ShownailSeller
//
//  Created by mfp on 16/11/4.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import "AlbumListView.h"
#import "PickImageViewModel.h"
#import "StringUtil.h"
#import "ViewFactory.h"

#define  ROW_HEIGHT     60
static   NSString       *cellIdentify = @"albumCell";

@implementation AlbumListView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = RGBACOLOR(0, 0, 0, 0.5);
        self.albumList = [NSMutableArray array];
        _albumTableView = [[UITableView alloc]initWithFrame:CM(0, 0, SCREEN_WIDTH, 300)];
        _albumTableView.delegate = self;
        _albumTableView.dataSource = self;
        _albumTableView.showsHorizontalScrollIndicator = NO;
        _albumTableView.showsVerticalScrollIndicator = YES;
        _albumTableView.scrollEnabled = YES;
        _albumTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _albumTableView.rowHeight = ROW_HEIGHT;
        _albumTableView.backgroundColor = WhiteColor;
        [_albumTableView registerClass:[AlbumListCell class] forCellReuseIdentifier:cellIdentify];
        _albumTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        [self addSubview:_albumTableView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide:)];
        tap.delegate = self;
        [self addGestureRecognizer:tap];
    }
    return self;
}

#pragma mark - UIGestureRecognizerDelegate
//判断点击事件在tableview上则忽略
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView:self];
    point = [self.layer convertPoint:point toLayer:self.albumTableView.layer];
    if ([self.albumTableView.layer containsPoint:point]) {
        return NO;
    }else{
        return YES;
    }
}


- (void)hide:(UITapGestureRecognizer *)gesture{
    if ([self.delegate respondsToSelector:@selector(hideListView)]) {
        [self.delegate hideListView];
    }
}


- (void)setAlbumList:(NSMutableArray *)albumList
{
    _albumList = albumList;
    [self.albumTableView reloadData];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.albumList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlbumListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    cell.entity = self.albumList[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(didSelectAlbumIndex:)]) {
        [self.delegate didSelectAlbumIndex:indexPath.row];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end


@implementation AlbumListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.contentView.backgroundColor = WhiteColor;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.albumImage = [ViewFactory getImageView:CM(10, 5, 50, 50) image:nil];
        self.albumImage.clipsToBounds = YES;
        self.albumImage.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:self.albumImage];
        
        self.albumName = [ViewFactory getLabel:CM(70, 0, 100, 50) font:Font(15) textColor:ColorWithHex(0x434348) text:nil];
        [self.contentView addSubview:self.albumName];
        
        self.picCount = [ViewFactory getLabel:CM(CGRectGetMaxX(self.albumName.frame)+10, 0, 100, ROW_HEIGHT) font:Font(14) textColor:ColorWithHex(0x737378) text:nil];
        [self.contentView addSubview:self.picCount];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.albumImage.frame = CM(10, 5, 50, 50);
    CGFloat nameLen = [StringUtil getLabelLength:self.albumName.text font:self.albumName.font height:ROW_HEIGHT]+10;
    self.albumName.frame = CM(CGRectGetMaxX(self.albumImage.frame)+10, 0, nameLen, ROW_HEIGHT);
    self.picCount.frameOriginX = CGRectGetMaxX(self.albumName.frame)+10;
    self.albumName.backgroundColor = ClearColor;
    self.albumName.opaque = NO;
}

- (void)setEntity:(AlbumEntity *)entity
{
    if ([entity isKindOfClass:[AlbumEntity class]]) {
        _entity = entity;
        __unsafe_unretained typeof(self)weakSelf = self;
        self.albumName.text = entity.assetCollection.localizedTitle;
        self.picCount.text = [NSString stringWithFormat:@"(%ld)",(unsigned long)entity.fetchResult.count];
        @autoreleasepool {
            PHAsset *asset = entity.fetchResult.lastObject;
            PHImageRequestOptions *opt = [[PHImageRequestOptions alloc]init];
            opt.synchronous = NO;
            opt.resizeMode = PHImageRequestOptionsResizeModeFast;
            opt.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
            PHImageManager *imageManager = [PHImageManager defaultManager];
            [imageManager requestImageForAsset:asset targetSize:CGSizeMake(80, 80) contentMode:PHImageContentModeAspectFill options:opt resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                if (result) {
                    weakSelf.albumImage.image = result;
                }
            }];
        }
    }
}

@end
