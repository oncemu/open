//
//  CustomTextFiled.m
//  Open
//
//  Created by onceMu on 2017/7/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CustomTextFiled.h"

@implementation CustomTextFiled

-(CGRect)editingRectForBounds:(CGRect)bounds
{
    CGRect rect = [super textRectForBounds:bounds];

    UIEdgeInsets insets = UIEdgeInsetsMake(5, 0, 5,0);
    return UIEdgeInsetsInsetRect(rect, insets);
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect rect = [super textRectForBounds:bounds];

    UIEdgeInsets insets = UIEdgeInsetsMake(5, 0, 5,0);
    return UIEdgeInsetsInsetRect(rect, insets);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
