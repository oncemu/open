//
//  RankReusableView.m
//  Open
//
//  Created by onceMu on 2017/8/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "RankReusableView.h"

@implementation RankReusableView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
//    self.backgroundColor = [UIColor whiteColor];
    _bkImageView = [[UIImageView alloc]initWithFrame:self.bounds];
    [self addSubview:_bkImageView];
    _bkImageView.image = [UIImage imageNamed:@"rankBgImage"];

    _headerBgImageView = [[UIImageView alloc]init];
    [_bkImageView addSubview:_headerBgImageView];
    [_headerBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(50, 118/2));
    }];
    _headerBgImageView.image = [UIImage imageNamed:@"rankHeaderBg"];

    _headerImage = [[UIImageView alloc]init];
    _headerImage.clipsToBounds = YES;
    [_headerBgImageView addSubview:_headerImage];
    [_headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headerBgImageView.mas_left).offset(1);
        make.right.mas_equalTo(self.headerBgImageView.mas_right).offset(-1);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-1);
        make.height.mas_equalTo(48);
    }];
    _headerImage.layer.cornerRadius  =24.0f;

    _rankImage = [[UIImageView alloc]init];
    [_bkImageView addSubview:_rankImage];
    [_rankImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.top.mas_equalTo(self.headerBgImageView.mas_bottom).offset(30);
        make.size.mas_equalTo(CGSizeMake(12, 12));
    }];
    _rankImage.image = [UIImage imageNamed:@"rankLog"];

    _lblRank = [[UILabel alloc]init];
    _lblRank.font = [UIFont systemFontOfSize:14.f];
    _lblRank.textColor = [UIColor colorWithHex:0x343333];
    [_bkImageView addSubview:_lblRank];
    [_lblRank mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.rankImage.mas_right).offset(5);
        make.top.mas_equalTo(self.rankImage.mas_top);
        make.size.mas_equalTo(CGSizeMake(100, 14));
    }];
    _lblRank.text = @"我的头像";

    _lblName = [[UILabel alloc]init];
    _lblName.font = [UIFont systemFontOfSize:14.0f];
    _lblName.textColor = [UIColor colorWithHex:0x343333];
    _lblName.textAlignment = NSTextAlignmentRight;
    [_bkImageView addSubview:_lblName];
    [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.top.mas_equalTo(self.lblRank.mas_top);
        make.size.mas_equalTo(CGSizeMake(80, 14));
    }];
    _lblName.text = @"熟驴客";

    _rankStatusImage = [[UIImageView alloc]init];
    [_bkImageView addSubview:_rankStatusImage];
    [_rankStatusImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.lblName.mas_left).offset(20);
        make.top.mas_equalTo(self.headerBgImageView.mas_bottom).offset(2);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    _rankStatusImage.image = [UIImage imageNamed:@"rank1"];

    _lineView = [[UIView alloc]init];
    _lineView.backgroundColor = [UIColor colorWithHex:0xe9c601];
    [_bkImageView addSubview:_lineView];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.top.mas_equalTo(self.lblName.mas_bottom).offset(5);
        make.height.mas_equalTo(0.5);
    }];

    _progressBgImage = [[UIImageView alloc]init];
    [_bkImageView addSubview:_progressBgImage];
    [_progressBgImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.top.mas_equalTo(self.lineView.mas_bottom).offset(20);
        make.height.mas_equalTo(20);
    }];
    _progressBgImage.clipsToBounds = YES;
    _progressBgImage.backgroundColor = [UIColor colorWithHex:0xf3791f];
    _progressBgImage.layer.cornerRadius = 8.0f;
    _progressImage = [[UIImageView alloc]init];
    [_progressBgImage addSubview:_progressImage];
    [_progressImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.progressBgImage.mas_left).offset(1);
        make.top.mas_equalTo(self.progressBgImage.mas_top).offset(1);
        make.bottom.mas_equalTo(self.progressBgImage.mas_bottom).offset(-1);
        make.width.mas_equalTo(100);
    }];
//    [image resizableImageWithCapInsets:UIEdgeInsetsMake(image.size.height/2, image.size.width/2, image.size.height/2, image.size.width/2) resizingMode:UIImageResizingModeStretch];
    UIImage *image = [UIImage imageNamed:@"Progress-Bar"];
    _progressImage.image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(image.size.height/4, image.size.width/4, image.size.height/4, image.size.width/4) resizingMode:UIImageResizingModeStretch];

    _lblFoot = [[UILabel alloc]init];
    _lblFoot.textColor = [UIColor colorWithHex:0x343333];
    _lblFoot.font = [UIFont systemFontOfSize:14.0f];
    [_bkImageView addSubview:_lblFoot];
    [_lblFoot mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.top.mas_equalTo(self.progressBgImage.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(100, 14));
    }];
    _lblFoot.text = @"我的脚步";

    _lblExpress = [[UILabel alloc]init];
    _lblExpress.textColor = [UIColor colorWithHex:0x343333];
    _lblExpress.font = [UIFont systemFontOfSize:14.0f];
    _lblExpress.textAlignment = NSTextAlignmentRight;
    [_bkImageView addSubview:_lblExpress];
    [_lblExpress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.top.mas_equalTo(self.lblFoot.mas_top);
        make.size.mas_equalTo(CGSizeMake(200, 14));
    }];
    _lblExpress.text = @"25EXP / 50EXP";

    _lblSign = [[UILabel alloc]init];
    _lblSign.textAlignment = NSTextAlignmentCenter;
    _lblSign.font = [UIFont systemFontOfSize:16.0f];
    _lblSign.textColor = [UIColor colorWithHex:0x343333];
    [_bkImageView addSubview:_lblSign];
    [_lblSign mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.bottom.mas_equalTo(self.bkImageView.mas_bottom).offset(-25);
        make.height.mas_equalTo(16);
    }];
    _lblSign.text = @"头衔介绍";
}

@end
