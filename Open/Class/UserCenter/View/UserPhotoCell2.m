//
//  UserPhotoCell2.m
//  Open
//
//  Created by onceMu on 2017/9/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserPhotoCell2.h"


@implementation UserPhotoCell2

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    self.backgroundColor = [UIColor whiteColor];
    _coverImage = [[UIImageView alloc]init];
    _coverImage.backgroundColor = [UIColor colorWithHex:0x999999];
    [self.contentView addSubview:_coverImage];
    CGFloat width = (SCREEN_WIDTH-50)/2;
    [_coverImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.top.mas_equalTo(self.mas_top).offset(5);
        make.size.mas_equalTo(CGSizeMake(width, width));
    }];

    _lblName = [[UILabel alloc]init];
    _lblName.textColor = [UIColor colorWithHex:0xff0000];
    _lblName.font = [UIFont systemFontOfSize:13.0f];
    [self.contentView addSubview:_lblName];
    [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.top.mas_equalTo(self.coverImage.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(100, 13));
    }];

    _lblLike = [[UILabel alloc]init];
    _lblLike.textAlignment = NSTextAlignmentRight;
    _lblLike.font = [UIFont systemFontOfSize:13.0f];
    _lblLike.textColor = [UIColor colorWithHex:0x999999];
    [self.contentView addSubview:_lblLike];
    [_lblLike mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-2);
        make.top.mas_equalTo(self.coverImage.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(60, 13));
    }];
    _likeImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_likeImageView];
    [_likeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.lblLike.mas_left).offset(-2);
        make.top.mas_equalTo(self.coverImage.mas_bottom).offset(8.5);
        make.size.mas_equalTo(CGSizeMake(16, 16));
    }];
}

- (void)setItem:(AlbumModel *)item {
    _item = item;
    if (item.photos.count>=1) {
        PhotoItem *photo = item.photos[0];
        [_coverImage sd_setImageWithURL:[NSURL URLWithString:photo.url]];
    }
    _lblName.text = [NSString stringWithFormat:@"￥%.2f",item.price];
    _lblLike.text = [NSString stringWithFormat:@"%@",@(item.favoriteCount)];
    CGSize size = [_lblLike.text boundingRectWithSize:CGSizeMake(MAXFLOAT, 13) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:_lblLike.font} context:nil].size;
    [_lblLike mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceilf(size.width));
    }];
    _likeImageView.image = [UIImage imageNamed:@"home_heart"];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _coverImage.image = nil;
    _lblLike.text = nil;
    _lblName.text = nil;
    _likeImageView.image = nil;
}

@end
