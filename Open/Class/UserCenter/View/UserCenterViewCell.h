//
//  UserCenterViewCell.h
//  Open
//
//  Created by onceMu on 2017/7/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserCenterViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *channelImageView;
@property (nonatomic, strong) UILabel *lblChannelName;
@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) UIView *bottomView;

- (void)assignChannelIndex:(NSInteger)index;

@end
