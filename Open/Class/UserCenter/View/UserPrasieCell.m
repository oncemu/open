//
//  UserWorkCell.m
//  Open
//
//  Created by onceMu on 2017/9/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserPrasieCell.h"

@implementation UserPrasieCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    self.backgroundColor = [UIColor whiteColor];
    _coverImage = [[UIImageView alloc]init];
    _coverImage.contentMode = UIViewContentModeScaleToFill;
//    _coverImage.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:_coverImage];


    _lblTitle = [[UILabel alloc]init];
    _lblTitle.font = [UIFont systemFontOfSize:14.0f];
    _lblTitle.textColor = [UIColor colorWithHex:0x323333];
    [self.contentView addSubview:_lblTitle];

    _lblContent = [[UILabel alloc]init];
    _lblContent.font = [UIFont systemFontOfSize:12];
    _lblContent.numberOfLines = 0;
    _lblContent.textColor = [UIColor colorWithHex:0x3233333];
    [self.contentView addSubview:_lblContent];

    _lblComment = [[UILabel alloc]init];
    _lblComment.font = [UIFont systemFontOfSize:11];
    _lblComment.textColor = [UIColor colorWithHex:0xd0d0d0];
    [self.contentView addSubview:_lblComment];

    _commentImage = [[UIImageView alloc]init];
    [self.contentView addSubview:_commentImage];

    _lblLike = [[UILabel alloc]init];
    _lblLike.font = [UIFont systemFontOfSize:11.0f];
    _lblLike.textColor = [UIColor colorWithHex:0xd0d0d0];
    [self.contentView addSubview:_lblLike];

    _likeImage = [[UIImageView alloc]init];
    [self.contentView addSubview:_likeImage];

    _lineView = [[UIView alloc]init];
    _lineView.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [self.contentView addSubview:_lineView];

    [_coverImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.top.mas_equalTo(self.mas_top).offset(20);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(274/2);
    }];
    //    _coverImage.frame = CGRectMake(20, 20, 130, 274/2);
    [_lblTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.coverImage.mas_right).offset(20);
        make.top.mas_equalTo(self.mas_top).offset(25);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.height.mas_equalTo(16);
    }];


    [_lblComment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-32);
        make.size.mas_equalTo(CGSizeMake(10, 14));
    }];
    [_commentImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(_lblComment.mas_centerY);
        make.right.mas_equalTo(_lblComment.mas_left).offset(-5);
        make.size.mas_equalTo(CGSizeMake(16, 16));
    }];

    [_lblContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.lblTitle.mas_left);
        make.top.mas_equalTo(self.lblTitle.mas_bottom).offset(10);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.bottom.mas_equalTo(self.lblComment.mas_top).offset(-40);
    }];


    [_lblLike mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_commentImage.mas_left).offset(-5);
        make.bottom.mas_equalTo(_lblComment.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(10, 14));
    }];
    [_likeImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_lblLike.mas_left).offset(-5);
        make.centerY.mas_equalTo(_commentImage.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(17, 15));
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _lblLike.text = nil;
    _lblComment.text = nil;
    _lblContent.text = nil;
    _lblTitle.text = nil;
    _coverImage.image = nil;
    _likeImage.image = nil;
    _commentImage.image = nil;
}

- (void)setItem:(AlbumModel *)item {
    _item = item;
    if (item.photos.count>=1) {
        PhotoItem *photo = item.photos[0];
        [_coverImage sd_setImageWithURL:[NSURL URLWithString:photo.url]];
    }
    _lblTitle.text = item.title;
    _lblContent.text = item.description;
    _lblLike.text = [NSString stringWithFormat:@"%@",@(item.favoriteCount)];
    _lblComment.text = [NSString stringWithFormat:@"%@",@(item.collectionCount)];
    _likeImage.image = [UIImage imageNamed:@"home_heart"];
    _commentImage.image = [UIImage imageNamed:@"home_comment"];

    NSString *text = _lblComment.text;
    UIFont *font = _lblComment.font;
    CGSize commentSize = [text boundingRectWithSize:CGSizeMake(MAXFLOAT, 14) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;

    [_lblComment mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceilf(commentSize.width));
    }];
    NSString *like = _lblLike.text;
    CGSize likeSize = [like boundingRectWithSize:CGSizeMake(MAXFLOAT, 14) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    [_lblLike mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceilf(likeSize.width));
    }];
}


@end
