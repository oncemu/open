//
//  UserPhotoCell1.h
//  Open
//
//  Created by onceMu on 2017/9/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserWorkItem.h"

@class UserPhotCell1Content;

@interface UserPhotoCell1 : UICollectionViewCell<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataList;

- (void)assignData:(NSArray *)data;

@end


@interface UserPhotoCell1ReuseableView : UICollectionReusableView

@property (nonatomic, strong) UILabel *lblName;


@end


@interface UserPhotCell1Content : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *lblPrice;
@property (nonatomic, strong) UILabel *lblLike;
@property (nonatomic, strong) UIImageView *likeImageView;
@property (nonatomic, strong) UserWorkItem *item;

@end
