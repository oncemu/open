//
//  RankReusableView.h
//  Open
//
//  Created by onceMu on 2017/8/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RankReusableView : UICollectionReusableView

@property (nonatomic, strong) UIImageView *headerImage;
@property (nonatomic, strong) UIImageView *headerBgImageView;
@property (nonatomic, strong) UIImageView *rankImage;
@property (nonatomic, strong) UILabel *lblRank;
@property (nonatomic, strong) UIImageView *rankStatusImage;
@property (nonatomic, strong) UILabel *lblName;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIImageView *progressBgImage;
@property (nonatomic, strong) UIImageView *progressImage;
@property (nonatomic, strong) UILabel *lblFoot;
@property (nonatomic, strong) UILabel *lblExpress;
@property (nonatomic, strong) UILabel *lblSign;
@property (nonatomic, strong) UIImageView *bkImageView;

@end
