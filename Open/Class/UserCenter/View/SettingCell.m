//
//  SettingCell.m
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "SettingCell.h"

static NSString * const kSettingInSideIdentifier = @"SettingInSideIdentifier";
static NSString * const kSettingInSideHeaderIdentifier = @"SettingInSideHeaderIdentifier";

@interface SettingCell ()<UITableViewDelegate,UITableViewDataSource,SettingHeaderDelegate>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation SettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.layer.cornerRadius = 4.0f;
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.frameSizeWidth, self.frameSizeHeight) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_tableView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.tableView.frame = CGRectMake(0, 0, self.frameSizeWidth, self.frameSizeHeight);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_indexPath) {
        if (_indexPath.section == 0 ) {
            return 4;
        }
        if ( _indexPath.section == 2) {
            return 3;
        }
        return 2;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_indexPath.section == 0 && indexPath.row == 0) {
        SettingHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:kSettingInSideHeaderIdentifier];
        if (cell == nil) {
            cell = [[SettingHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kSettingInSideHeaderIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.delegate = self;
        [cell assignHeader:self.user];
        return cell;
    }
    SettingContentCell *cell = [tableView dequeueReusableCellWithIdentifier:kSettingInSideIdentifier];
    if (cell == nil) {
        cell = [[SettingContentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kSettingInSideIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell assignSection:_indexPath.section index:indexPath.row user:self.user];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_indexPath) {
        if (_indexPath.section == 0 && indexPath.row == 0 ) {
            return 62;
        }
    }
    return 48.0f;
}


- (void)assignIndex:(NSIndexPath *)indexPath user:(User *)user {
    _indexPath = indexPath;
    _user = user;
    [self.tableView reloadData];
}

- (void)updateIndex:(NSInteger)index string:(NSString *)string {
    SettingContentCell *cell = (SettingContentCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    cell.lblRight.text = string;
}

- (void)updateHeader:(UIImage *)image {
    SettingHeaderCell *cell = (SettingHeaderCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    cell.headerImae.image = image;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.indexPath.section == 0 && indexPath.row == 0) {
//        [self clickModifyHeader];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickSection:index:)]) {
        [self.delegate clickSection:self.indexPath.section index:indexPath.row];
    }
}

- (void)clickModifyHeader {
    if (self.delegate && [self.delegate respondsToSelector:@selector(modifyHeader)]) {
        [self.delegate modifyHeader];
    }
}


@end


@implementation SettingHeaderCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _headerImae = [[UIImageView alloc]init];
    _headerImae.clipsToBounds = YES;
    [self.contentView addSubview:_headerImae];
    [_headerImae mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(30);
        make.top.mas_equalTo(self.mas_top).offset(10);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    _headerImae.layer.cornerRadius = 25.0f;
    _headerImae.layer.borderColor = [UIColor colorWithHex:0xf3df01].CGColor;
    _headerImae.layer.borderWidth = 2.0f;

//    _modifyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_modifyBtn setBackgroundImage:[UIImage imageNamed:@"Note"] forState:UIControlStateNormal];
//    [self.contentView addSubview:_modifyBtn];
//    [_modifyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(self.mas_right).offset(-30);
//        make.top.mas_equalTo(self.mas_top).offset(20);
//        make.size.mas_equalTo(CGSizeMake(18, 18));
//    }];

    UIView *backView = [[UIView alloc]init];
    [self.contentView addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right);
        make.top.mas_equalTo(self.mas_top).offset(10);
        make.size.mas_equalTo(CGSizeMake(50, 40));
    }];
    backView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapModify)];
    [backView addGestureRecognizer:tap];


    //fedf01
}

- (void)tapModify {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickModifyHeader)]) {
        [self.delegate clickModifyHeader];
    }
}

- (void)assignHeader:(User *)user {
    [_headerImae sd_setImageWithURL:[NSURL URLWithString:user.avatar]];
}

@end


@implementation SettingContentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _leftImage = [[UIImageView alloc]init];
    [self.contentView addSubview:_leftImage];

    _lblLeft = [[UILabel alloc]init];
    _lblLeft.font = [UIFont systemFontOfSize:14.0f];
    _lblLeft.textColor = [UIColor colorWithHex:0x333333];
    [self.contentView addSubview:_lblLeft];


    _lblRight = [[UILabel alloc]init];
    _lblRight.font = [UIFont systemFontOfSize:14.0f];
    _lblRight.textColor = [UIColor colorWithHex:0x333333];
    _lblRight.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_lblRight];


    _lineView = [[UIView alloc]init];
    _lineView.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [self.contentView addSubview:_lineView];
    _lineView.hidden = YES;

    _rightImage = [[UIImageView alloc]init];
    [self.contentView addSubview:_rightImage];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.lineView.frame = CGRectMake(30, self.frameSizeHeight-0.5, self.frameSizeWidth-30, 0.5);
}

- (void)assignSection:(NSInteger)section index:(NSInteger)index user:(User *)user{
    if (section == 0) {
        if (index == 3) {
            _lineView.hidden = YES;
        }
    }else {
        _lineView.hidden = NO;
    }
    if (section == 1) {
        if (index == 1) {
            _lineView.hidden = YES;
        }
    }else {
        _lineView.hidden = NO;
    }
    if (section == 2) {
        if (index == 2) {
            _lineView.hidden = YES;
        }
    }else {
        _lineView.hidden = NO;
    }
    if (section == 3) {
        if (index == 2) {
            _lineView.hidden = YES;
        }
    }else {
        _lineView.hidden = NO;
    }

    if (section == 0) {
        _rightImage.hidden = YES;
        [_lblLeft mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.leftImage.mas_right).offset(30);
            make.top.mas_equalTo(self.mas_top).offset(17);
            make.size.mas_equalTo(CGSizeMake(100, 14));
        }];
        [_lblRight mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.lblLeft.mas_right).offset(5);
            make.top.mas_equalTo(self.lblLeft.mas_top);
            make.right.mas_equalTo(self.mas_right).offset(-30);
            make.height.mas_equalTo(14);
        }];
        if (index == 1) {
            _lblLeft.text = @"修改昵称";
            _lblRight.text = user.nickname;
        }else if(index == 2) {
            _lblLeft.text = @"性别";
            _lblRight.text = user.gender;
        }else {
            _lblRight.text = user.signature;
            _lblLeft.text = @"签名";
        }
    }
    if (section == 1) {
        //Lock-Open //密码
        //globe //缓存
        UIImage *image = nil;

        if (index == 0) {
            image = [UIImage imageNamed:@"Lock-Open"];
            _lblLeft.text = @"重置密码";
        }else {
            image = [UIImage imageNamed:@"globe"];
            _lblLeft.text = @"清除缓存";
            [self countMemory];
        }
        [_leftImage mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(30);
            make.top.mas_equalTo(self.mas_top).offset((48-image.size.height)/2);
            make.size.mas_equalTo(CGSizeMake(image.size.width, image.size.height));
        }];
        _leftImage.image = image;

        [_lblLeft mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.leftImage.mas_right).offset(10);
            make.top.mas_equalTo(self.mas_top).offset(17);
            make.size.mas_equalTo(CGSizeMake(100, 14));
        }];
        [_lblRight mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.lblLeft.mas_right).offset(5);
            make.top.mas_equalTo(self.mas_top).offset(17);
            make.right.mas_equalTo(self.mas_right).offset(-30);
            make.height.mas_equalTo(14);
        }];
        _rightImage.hidden = YES;
        if (index == 0) {
            _rightImage.hidden = NO;
            [_rightImage mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(self.mas_right).offset(-30);
                make.top.mas_equalTo(self.mas_top).offset(12);
                make.size.mas_equalTo(CGSizeMake(11, 20));
            }];
            _rightImage.image = [UIImage imageNamed:@"rightBack"];
        }
    }

    if (section == 2) {
        //End-call //dianhua
        //aboutUs //Playlist
        UIImage *image = nil;

        if (index == 0) {
            image = [UIImage imageNamed:@"水印-4"];
            _lblLeft.text = @"选择水印";
        }else if(index == 1) {
            image = [UIImage imageNamed:@"aboutUs"];
            _lblLeft.text = @"关于我们";
        }else {
            image = [UIImage imageNamed:@"End-call"];
            _lblLeft.text = @"客服电话";
            _lblRight.text = @"400-877-9795";
        }
        [_leftImage mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(30);
            make.top.mas_equalTo(self.mas_top).offset((48-image.size.height)/2);
            make.size.mas_equalTo(CGSizeMake(image.size.width, image.size.height));
        }];
        _leftImage.image = image;

        [_lblLeft mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.leftImage.mas_right).offset(10);
            make.top.mas_equalTo(self.mas_top).offset(17);
            make.size.mas_equalTo(CGSizeMake(100, 14));
        }];
        [_lblRight mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.lblLeft.mas_right).offset(5);
            make.top.mas_equalTo(self.lblLeft.mas_top);
            make.right.mas_equalTo(self.mas_right).offset(-30);
            make.height.mas_equalTo(14);
        }];
        _rightImage.hidden = NO;
        [_rightImage mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_right).offset(-30);
            make.top.mas_equalTo(self.mas_top).offset(12);
            make.size.mas_equalTo(CGSizeMake(11, 20));
        }];
        _rightImage.image = [UIImage imageNamed:@"rightBack"];
        if (index == 2) {
            _rightImage.hidden = YES;
        }
    }

    if (section == 3) {
        //End-call //dianhua
        //aboutUs //Playlist
        UIImage *image = nil;
        _rightImage.hidden = YES;
        if (index == 0) {
            image = [UIImage imageNamed:@"Arrow_left"];
            _lblLeft.text = @"分享APP";
        }else {
            image = [UIImage imageNamed:@"Login"];
            _lblLeft.text = @"登出";
        }
        [_leftImage mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(30);
            make.top.mas_equalTo(self.mas_top).offset((48-image.size.height)/2);
            make.size.mas_equalTo(CGSizeMake(image.size.width, image.size.height));
        }];
        _leftImage.image = image;

        [_lblLeft mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.leftImage.mas_right).offset(10);
            make.top.mas_equalTo(self.mas_top).offset(17);
            make.size.mas_equalTo(CGSizeMake(100, 14));
        }];
        [_lblRight mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.lblLeft.mas_right).offset(5);
            make.top.mas_equalTo(self.lblLeft.mas_top);
            make.right.mas_equalTo(self.mas_right).offset(-30);
            make.height.mas_equalTo(14);
        }];
    }

}

- (void)prepareForReuse {
    [super prepareForReuse];
    _lblLeft.text = nil;
    _lblRight.text = nil;
    _leftImage.image = nil;
}

- (void)countMemory {
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:cachePath];
    double sizeBig = 0;
    for (NSString *p in files) {
        NSString *path = [cachePath stringByAppendingString:[NSString stringWithFormat:@"/%@",p]];
        NSFileManager *fileManager=[NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:path]) {
            long long size=[fileManager attributesOfItemAtPath:path error:nil].fileSize;
            sizeBig += size/1024.0/1024.0;
        }
    }
    _lblRight.text = [NSString stringWithFormat:@"%.2fM",sizeBig];
}



@end
