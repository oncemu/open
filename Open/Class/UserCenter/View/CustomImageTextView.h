//
//  CustomImageTextView.h
//  Open
//
//  Created by onceMu on 2017/7/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,ContentType) {
    ContentTypeLeft,//左
    ContentTypeCenter,//中
    ContentTypeRight//右
};

@interface CustomImageTextView : UIView

- (instancetype)initWithFrame:(CGRect)frame
                         type:(ContentType)type
                     fontSize:(CGFloat)fontSize
                         font:(UIFont *)font;


@property (nonatomic, assign) ContentType type;
@property (nonatomic, assign) CGFloat cornerRadius;
@property (nonatomic, assign) CGFloat fontSize;
@property (nonatomic, strong) UIImage *leftImage;
@property (nonatomic, strong) UILabel *lblTitle;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) NSString *title;


@end
