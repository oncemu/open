//
//  UserPhotoCell1.m
//  Open
//
//  Created by onceMu on 2017/9/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserPhotoCell1.h"

@implementation UserPhotoCell1

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    self.backgroundColor = [UIColor whiteColor];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 0;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;

    _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.showsHorizontalScrollIndicator = NO;
    [_collectionView registerClass:[UserPhotCell1Content class] forCellWithReuseIdentifier:@"1234"];
    [self addSubview:_collectionView];
}

- (void)assignData:(NSArray *)data {
    [_dataList removeAllObjects];
    [_dataList addObjectsFromArray:data];
    [_collectionView reloadData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (__kindof UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UserPhotCell1Content *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"1234" forIndexPath:indexPath];
    cell.item = self.dataList[indexPath.item];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(90, 128);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 2.5, 0, 2.5);
}

@end


@implementation UserPhotoCell1ReuseableView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _lblName = [[UILabel alloc]init];
    _lblName.font = [UIFont systemFontOfSize:14];
    _lblName.textColor = [UIColor colorWithHex:0x333333];
    [self addSubview:_lblName];
    [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(20);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.size.mas_equalTo(CGSizeMake(100, 14));
    }];
}

@end


@implementation UserPhotCell1Content

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _imageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_imageView];
    _imageView.backgroundColor = [UIColor colorWithHex:0x999999];
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.top.mas_equalTo(self.mas_top);
        make.size.mas_equalTo(CGSizeMake(90, 95));
    }];

    _lblPrice = [[UILabel alloc]init];
    _lblPrice.font = [UIFont systemFontOfSize:12];
    _lblPrice.textColor = [UIColor colorWithHex:0xff0000];
    [self.contentView addSubview:_lblPrice];

    [_lblPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.top.mas_equalTo(self.imageView.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(60, 12));
    }];

    _lblLike = [[UILabel alloc]init];
    _lblLike.textAlignment = NSTextAlignmentRight;
    _lblLike.font = [UIFont systemFontOfSize:12];
    _lblLike.textColor = [UIColor colorWithHex:0x999999];
    [self.contentView addSubview:_lblLike];
    [_lblLike mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right);
        make.top.mas_equalTo(self.imageView.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(40, 12));
    }];

    _likeImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_likeImageView];
    [_likeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.lblLike.mas_left).offset(-2);
        make.top.mas_equalTo(self.imageView.mas_bottom).offset(-8);
        make.size.mas_equalTo(CGSizeMake(16, 16));
    }];
}

@end
