//
//  CustomCircleView.h
//  Open
//
//  Created by onceMu on 2017/7/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextFiled.h"
#import "CountDownButton.h"

typedef NS_ENUM(NSInteger,CircleType) {
    CircleTypePhone,//手机号
    CircleTypePwd,//密码
    CircleTypeButton,//按钮
    CircleTypeVerifyCode,//验证码
    CircleTypeShowPwd,//显示密码
    CircleTypeEnsurePwd//确认密码
};

@protocol CustomCircleViewDelegate <NSObject>
@optional
//改变密码显示状态
- (void)changeTextFiledSecturyStatus:(BOOL)status;
//登录
- (void)loginAcction;
//获取验证码
- (void)fetchCode;

@end

@interface CustomCircleView : UIView<UITextFieldDelegate>

- (instancetype)initWithFrame:(CGRect)frame
                         type:(CircleType)type
                    leftImage:(UIImage *)leftImage
                   rightImage:(UIImage *)rightImage
                        title:(NSString *)buttonTitle;

@property (nonatomic, weak) id<CustomCircleViewDelegate>delegate;
@property (nonatomic, assign) CircleType type;
@property (nonatomic, strong) UIImage *leftImage;
@property (nonatomic, strong) UIImage *rightImage;
@property (nonatomic, strong) UIView *circleView;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) CustomTextFiled *textFiled;
@property (nonatomic, strong) NSString *buttonTitle;
@property (nonatomic, strong) CountDownButton *codeButton;
@property (nonatomic, assign) NSInteger limitCount;
@property (nonatomic, strong) UIImageView *rightImageView;

- (void)startCode;

@end
