//
//  UserCenterViewCell.m
//  Open
//
//  Created by onceMu on 2017/7/18.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserCenterViewCell.h"

@implementation UserCenterViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
//    @property (nonatomic, strong) UIImageView *channelImageView;
//    @property (nonatomic, strong) UILabel *lblChannelName;
//    @property (nonatomic, strong) UIView *leftView;
//    @property (nonatomic, strong) UIView *bottomView;
    self.backgroundColor = [UIColor colorWithHex:0xffffff];
    _channelImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_channelImageView];
    //30 *30
    [_channelImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset((self.frameSizeHeight-36)/2);
        make.left.mas_equalTo(self.mas_left).offset((self.frameSizeWidth-15)/2);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];


    _lblChannelName = [[UILabel alloc]init];
    _lblChannelName.font = [UIFont systemFontOfSize:14.0f];
    _lblChannelName.textColor = [UIColor colorWithHex:0x7d7d7d];
    _lblChannelName.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_lblChannelName];
    [_lblChannelName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(1);
        make.top.mas_equalTo(self.channelImageView.mas_bottom).offset(9);
        make.right.mas_equalTo(self.mas_right).offset(-1);
        make.height.mas_equalTo(14);
    }];

    _leftView = [[UIView alloc]init];
    _leftView.hidden = YES;
    _leftView.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [self.contentView addSubview:_leftView];

    _bottomView = [[UIView alloc]init];
    _bottomView.hidden = YES;
    _bottomView.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [self.contentView addSubview:_bottomView];
}

- (void)assignChannelIndex:(NSInteger)index {
    _lblChannelName.text = [[self titles] objectAtIndex:index];
    _channelImageView.image = [[self imageArray] objectAtIndex:index];

//    UIImage *image = [[self imageArray] objectAtIndex:index];
//    [_channelImageView mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(image.size);
//    }];
    if (index == 0 || index>3) {
        _leftView.hidden = YES;
    }else {
        _leftView.hidden = NO;
    }
    if (index == 5) {
        _leftView.hidden = NO;
    }
    if (index>3) {
        _bottomView.hidden = YES;
    }else {
        _bottomView.hidden = NO;
    }
    [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.top.mas_equalTo(self.mas_top).offset(25);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-25);
        make.width.mas_equalTo(0.5);
    }];
    if (index == 0) {
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(25);
            make.bottom.mas_equalTo(self.mas_bottom).offset(0);
            make.right.mas_equalTo(self.mas_right);
            make.height.mas_equalTo(0.5);
        }];
    }
    if (index == 1 || index == 2) {
        [_bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(0);
            make.bottom.mas_equalTo(self.mas_bottom).offset(0);
            make.right.mas_equalTo(self.mas_right);
            make.height.mas_equalTo(0.5);
        }];
    }
    if (index == 3) {
        [_bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(0);
            make.bottom.mas_equalTo(self.mas_bottom).offset(0);
            make.right.mas_equalTo(self.mas_right).offset(-25);
            make.height.mas_equalTo(0.5);
        }];
    }
}

- (NSArray *)imageArray {
    return @[[UIImage imageNamed:@"myProduction"],
             [UIImage imageNamed:@"Like"],
             [UIImage imageNamed:@"Heart"],
             [UIImage imageNamed:@"Friends"],
             [UIImage imageNamed:@"myPicture"],
             [UIImage imageNamed:@"myRank"]];
}

- (NSArray *)titles {
    return @[@"我的作品",@"我赞过的",@"关注",@"粉丝",@"我的美图",@"我的头衔"];
}

@end
