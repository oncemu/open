//
//  QXMyVoucherSelectHeader.m
//  ShowNail
//
//  Created by mengfanpeng on 14-9-23.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import "UserCenterSelectHeader.h"
#import "UIColor+Hex.h"
//#import "StringConstant.h"
//#import "QXStringUtil.h"
//#import "JCRBlurView.h"

@interface UserCenterSelectHeader ()
{
    UIButton *perBtn;
    BOOL animating;
    NSInteger titleCount;
    CGFloat paddingWidth;
//    JCRBlurView *blur;
}
@end
@implementation UserCenterSelectHeader

- (id)initWithFrame:(CGRect)frame titles:(NSArray *)titles
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        if (!titles||titles.count==0) {
            return self;
        }
        self.backgroundColor= [UIColor colorWithHex:0xf2f2f2];
        self.layer.borderColor=[UIColor colorWithHex:0xcdcdcd].CGColor;
        self.layer.borderWidth=0.5;
        UIColor *color=[self getColor];
        
        _buttons=[NSMutableArray array];
        _floatViewOffsets=[NSMutableArray array];
        _titlesArray=[NSMutableArray arrayWithArray:titles];
        _countArray=[NSMutableArray array];
        titleCount=titles.count;
        paddingWidth= 0;
        for (NSInteger i=0; i<titleCount; i++) {
            [_countArray addObject:[NSNull null]];
            UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
            button.frame=CGRectMake(i*(self.frame.size.width/titleCount), 0, self.frame.size.width/titleCount, self.frame.size.height);
            button.tag=i+3000;
            [button setTitleColor:[UIColor colorWithHex:0x333333] forState:UIControlStateNormal];
            [button setTitleColor:color forState:UIControlStateSelected];
            [button addTarget:self action:@selector(didSelectedIndex:) forControlEvents:UIControlEventTouchUpInside];
//            button.titleLabel.font=[UIFont fontWithName:kBaseFontName size:15];
            button.titleLabel.font = [UIFont systemFontOfSize:15];
            button.titleLabel.textAlignment=NSTextAlignmentCenter;
            [button setTitle:titles[i] forState:UIControlStateNormal];
            if (i==0) {
                button.selected=YES;
                perBtn=button;
                _selectIndex=0;
            }
            
            [self addSubview:button];
            [_buttons addObject:button];
            
            
            NSString *title1=titles[i];
            CGFloat length=title1.length*18.0f + 30;
            CGPoint floatCenter=CGPointMake((self.frame.size.width/titleCount)*i+(self.frame.size.width/titleCount)/2.0f, self.frame.size.height-1);
            CGRect floatRect=CGRectMake(floatCenter.x-length/2.0f, self.frame.size.height-2, length, 2);
            [_floatViewOffsets addObject:[NSValue valueWithCGRect:floatRect]];
        }
        CGRect rect=[_floatViewOffsets[0] CGRectValue];
        _floatView=[[UIView alloc] initWithFrame:rect];
        _floatView.backgroundColor=color;
        [self addSubview:_floatView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame titles:(NSArray *)titles isNeedNum:(BOOL)isNeedNum
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        if (!titles||titles.count==0) {
            return self;
        }
        self.backgroundColor = [UIColor colorWithHex:0xf2f2f2];
        //        self.layer.shadowPath=[UIBezierPath bezierPathWithRect:self.bounds].CGPath;
//        self.layer.shadowOffset=CGSizeMake(0, 1);
//        self.layer.shadowColor=[UIColor lightGrayColor].CGColor;
//        self.layer.shadowOpacity=0.4;
        self.layer.borderColor=[UIColor colorWithHex:0xcdcdcd].CGColor;
        self.layer.borderWidth=0.5;
        UIColor *color=[self getColor];
        self.isNeedNum=isNeedNum;
        _buttons=[NSMutableArray array];
        _floatViewOffsets=[NSMutableArray array];
        _titlesArray=[NSMutableArray arrayWithArray:titles];
        _countArray =[NSMutableArray array];
        titleCount=titles.count;
        paddingWidth= 0;
        for (NSInteger i=0; i<titleCount; i++) {
            [_countArray addObject:[NSNull null]];
            UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
            button.frame=CGRectMake(i*(self.frame.size.width/titleCount), 0, self.frame.size.width/titleCount, self.frame.size.height);
            button.tag=i+3000;
            [button setTitleColor:[UIColor colorWithHex:0x333333] forState:UIControlStateNormal];
            [button setTitleColor:color forState:UIControlStateSelected];
            [button addTarget:self action:@selector(didSelectedIndex:) forControlEvents:UIControlEventTouchUpInside];
            button.titleLabel.font = [UIFont systemFontOfSize:13.0f];
            button.titleLabel.textAlignment=NSTextAlignmentCenter;
            [button setTitle:titles[i] forState:UIControlStateNormal];
            if (i==0) {
                button.selected=YES;
                perBtn=button;
            }
            if (isNeedNum) {
                button.titleLabel.numberOfLines=2;
                button.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
                [button setTitleColor:color forState:UIControlStateSelected];
            }
            NSString *title1=titles[i];
            CGFloat length=title1.length*18.0f;
            CGPoint floatCenter=CGPointMake((self.frame.size.width/titleCount)*i+(self.frame.size.width/titleCount)/2.0f, self.frame.size.height-1);
            CGRect floatRect=CGRectMake(floatCenter.x-length/2.0f, self.frame.size.height-2, length, 2);
//            if (isNeedNum) {
//                UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(55, 15, 35, 20)];
//                label.font = [UIFont systemFontOfSize:15.0f];
//                label.textColor = [UIColor colorWithHex:0xff4770];
//                label.tag = 1000;
//                [button addSubview:label];
//            }
            [self addSubview:button];
            [_buttons addObject:button];
            [_floatViewOffsets addObject:[NSValue valueWithCGRect:floatRect]];
        }
        CGRect rect=[_floatViewOffsets[0] CGRectValue];
        _floatView=[[UIView alloc] initWithFrame:rect];
        _floatView.backgroundColor=color;
        [self addSubview:_floatView];
    }
    return self;
}
-(void)setNumber:(NSInteger)count index:(NSInteger)index withIndexPostition:(numPosition)position
{
    if (index<0||index>=self.buttons.count) {
        return;
    }
    NSString *countStr=[NSString stringWithFormat:@"%ld",(long)count];
//    if (![[QXStringUtil shareInstance]isNONil:countStr]) {
//        countStr=@"0";
//    }
//    if (count>99) {
//        countStr=@"99";
//    }
    [_countArray replaceObjectAtIndex:index withObject:countStr];
    
    NSInteger countInt=[countStr intValue];
    UIButton *btn=self.buttons[index];
    NSString *pre=[NSString stringWithFormat:@"%@",self.titlesArray[index]];
    NSString *suf=[NSString stringWithFormat:@"(%@)",countStr];
    if (countInt ==0) {
        suf=@" ";
    }
    NSString *firstCount=@"0";
    if (![self.countArray[index] isEqual:[NSNull null]]) {
        firstCount=self.countArray[index];
    }
    if (firstCount.integerValue==0) {
        [btn setTitle:self.titlesArray[index] forState:UIControlStateNormal];
    }else{
        if (position == positionDown) {
            NSMutableAttributedString *string=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",pre,suf]];
//            NSRange range=NSMakeRange(pre.length+1, string.length-pre.length-1);
//            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:kBaseFontName size:13] range:range];
//            if (index==_selectIndex) {
//                [string addAttribute:NSForegroundColorAttributeName value:[self getColor] range:NSMakeRange(0, string.length)];
//                [btn setAttributedTitle:string forState:UIControlStateSelected];
//            }
            [btn setAttributedTitle:string forState:UIControlStateNormal];
        }else if (position == positionLeft){
            NSMutableAttributedString *string=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@(%@)",self.titlesArray[index],countStr]];
//            NSRange range=NSMakeRange([self.titlesArray[index] length], countStr.length-1);
//            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:kBaseFontName size:14] range:range];
            if (index==_selectIndex) {
                [string addAttribute:NSForegroundColorAttributeName value:[self getColor] range:NSMakeRange(0, string.length)];
                [btn setAttributedTitle:string forState:UIControlStateSelected];
            }
            [btn setAttributedTitle:string forState:UIControlStateNormal];
            [_countArray replaceObjectAtIndex:index withObject:countStr];
        }else if(position == positionTopRight)
        {
            [btn setTitle:self.titlesArray[index] forState:UIControlStateNormal];
            [self creatNumBtn:countStr subView:btn];
        }
    }
}
-(void)creatNumBtn:(NSString *)title subView:(UIButton *)sender
{
    [self removeTipNum:sender];
//    CGFloat textLen=[QXStringUtil getLabelLength:sender.currentTitle font:sender.titleLabel.font height:sender.titleLabel.frameSizeHeight];    
//    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
//    button.frame=CM((sender.frameSizeWidth-textLen)/2+textLen-3, 8, 16, 16);
//    button.backgroundColor=ColorWithHex(0xe50202);
//    button.titleLabel.font=[UIFont systemFontOfSize:10];//Font(7);
//    [button setTitle:title forState:UIControlStateNormal];
//    [button setTitleColor:WhiteColor forState:UIControlStateNormal];
//    button.tag=sender.tag+500;
//    button.layer.cornerRadius=button.frameSizeWidth * 0.5f;
//    button.layer.masksToBounds=YES;
//    [sender addSubview:button];
}
-(void)removeTipNum:(UIButton *)sender
{
    UIButton *exitL=(UIButton *)[sender viewWithTag:sender.tag+500];
    if (exitL&&[exitL isKindOfClass:[UIButton class]]) {
        [exitL removeFromSuperview];
    }
}

-(void)didSelectedIndex:(UIButton*)sender
{
    if ([self selectIndex:sender]) {
        if (_delegate&&[_delegate respondsToSelector:@selector(didSelectedItemIndex:)]) {
            [_delegate didSelectedItemIndex:sender.tag-3000];
        }
    }else{
        if (_delegate&&[_delegate respondsToSelector:@selector(selectedAgainItemIndex:)]) {
            [_delegate selectedAgainItemIndex:sender.tag-3000];
        }
    }
}
-(UIColor*)getColor
{
    if (_getRandomColor) {
//        NSInteger ramdom = arc4random()%5;
//        return [kColorArray objectAtIndex:ramdom];
        return [UIColor colorWithHex:0xff4770];
    }else{
        return [UIColor colorWithHex:0xff4770];
    }
}
-(void)setGetRandomColor:(BOOL)getRandomColor
{
    if (!getRandomColor) {
        return;
    }
    _getRandomColor=getRandomColor;
}
-(void)setZoomCurrentSelectedTitle:(BOOL)zoomCurrentSelectedTitle
{
    _zoomCurrentSelectedTitle=zoomCurrentSelectedTitle;
    if (_zoomCurrentSelectedTitle) {
        CATransform3D transform1=CATransform3DMakeScale(1.1, 1.1, 1.0);
        perBtn.layer.transform=transform1;
    }
}
-(UIView*)bottomline
{
    if (!_bottomline) {
        _bottomline = [[UIView alloc]initWithFrame:CGRectMake(0, self.frame.size.height-0.5, self.frame.size.width, 0.5)];
        _bottomline.backgroundColor = [UIColor colorWithHex:0xcdcdcd];
        _bottomline.tag = 100000;
        [self addSubview:_bottomline];
    }
    return _bottomline;
}
-(void)setIsNeedLineView:(BOOL)isNeedLineView
{
    _isNeedLineView = isNeedLineView;
    if (_isNeedLineView) {
        self.bottomline.hidden=NO;
    }else{
        self.bottomline.hidden=YES;
    }
}
-(void)setIsNeedBlur:(BOOL)isNeedBlur
{
    _isNeedBlur = isNeedBlur;
//    if (isNeedBlur) {
//        if (OSVersionIsAtLeastiOS7) {
//            self.backgroundColor = [UIColor clearColor];
////            if (!blur) {
////                blur = [[JCRBlurView alloc]initWithFrame:self.bounds];
////                [blur setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
////                [self addSubview:blur];
////                [self sendSubviewToBack:blur];
////            }
////            blur.hidden=NO;
//        }
//    }else{
////        if (blur) {
////            [blur removeFromSuperview];
////            blur = nil;
////        }
////        blur.hidden=YES;
//    }
}
-(void)setCommentNum:(NSInteger)commentNum
{
    if (commentNum==0) {
        UIButton *btn = (UIButton *)[self viewWithTag:3001];
        UILabel *lblCom = (UILabel *)[btn viewWithTag:1000];
        lblCom.text = nil;
    }else{
        UIButton *btn = (UIButton *)[self viewWithTag:3001];
        UILabel *lblCom = (UILabel *)[btn viewWithTag:1000];
        if (commentNum>99) {
            lblCom.text = [NSString stringWithFormat:@"(99+)"];
        }else{
            lblCom.text = [NSString stringWithFormat:@"(%ld)",(long)commentNum];
        }
        lblCom.textColor = [UIColor colorWithHex:0xff4770];
    }
}

-(void)setLikeNum:(NSInteger)likeNum
{
    if (likeNum ==0) {
        UIButton *btn = (UIButton *)[self viewWithTag:3002];
        UILabel *lblCom = (UILabel *)[btn viewWithTag:1000];
        lblCom.text = nil;
    }else{
        UIButton *btn = (UIButton *)[self viewWithTag:3002];
        UILabel *lblCom = (UILabel *)[btn viewWithTag:1000];
        if (likeNum>99) {
            lblCom.text = [NSString stringWithFormat:@"(99+)"];
        }else{
            lblCom.text = [NSString stringWithFormat:@"(%lu)",(long)likeNum];
        }
        lblCom.textColor = [UIColor colorWithHex:0xff4770];
    }
}


-(void)setSelectIndex:(NSInteger)selectIndex
{
    if (selectIndex<0||selectIndex>=self.buttons.count) {
        return;
    }
    _selectIndex=selectIndex;
    UIButton *btn=(UIButton *)self.buttons[selectIndex];
    [self selectIndex:btn];
}
-(BOOL)selectIndex:(UIButton *)sender
{
    if ([sender isEqual:perBtn]) {
        return NO;
    }
    if (animating) {
        return NO;
    }
    [self removeTipNum:sender];
    CATransform3D transform1=CATransform3DMakeScale(1.13, 1.13, 1.0);
    [UIView animateWithDuration:0.2 animations:^{
        self->perBtn.layer.transform=CATransform3DIdentity;
    }];
    animating=YES;
    perBtn.selected=!perBtn.selected;
    sender.selected=!sender.selected;
    UIColor *color=[self getColor];
    if (self.isNeedNum) {
        NSMutableAttributedString *currentTitle=[[NSMutableAttributedString alloc] initWithAttributedString:sender.currentAttributedTitle];
        [currentTitle addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, currentTitle.length)];
        [sender setAttributedTitle:currentTitle forState:UIControlStateNormal];
        if (perBtn) {
            NSMutableAttributedString *perTitle=[[NSMutableAttributedString alloc] initWithAttributedString:perBtn.currentAttributedTitle];
            [perTitle addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, perTitle.length)];
            [perBtn setAttributedTitle:perTitle forState:UIControlStateNormal];
        }
    }else{
        [sender setTitleColor:color forState:UIControlStateSelected];
    }
    perBtn=sender;

    _floatView.backgroundColor = color;
    
    [UIView animateWithDuration:0.2 animations:^{
        self->_floatView.frame=[self.floatViewOffsets[sender.tag-3000] CGRectValue];
        if (self->_zoomCurrentSelectedTitle) {
            sender.layer.transform=transform1;
        }
    } completion:^(BOOL finished) {
        self->animating=NO;
        self->_selectIndex=sender.tag-3000;
    }];
    return YES;
}
-(NSInteger)currentNum
{
    NSString *obj=self.countArray[_selectIndex];
    if (![obj isEqual:[NSNull null]]) {
        _currentNum=obj.integerValue;
    }else{
        _currentNum=0;
    }
    return _currentNum;
}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
