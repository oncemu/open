//
//  UserCenterHeader.h
//  Open
//
//  Created by onceMu on 2017/7/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomImageTextView.h"
#import "User.h"
@class UserCenterHeaderButton;

@protocol UserCenterHeaderDelegate <NSObject>

- (void)settingBtnClick;
- (void)signBtnClick;
- (void)changeBackgroundImage;
- (void)earningBtnClick;
- (void)followBtnClick;
- (void)clickHeaderWithIndex:(NSInteger)index;
- (void)shopCartBtnClick;

@end

@interface UserCenterHeader : UICollectionReusableView

@property (nonatomic, weak) id<UserCenterHeaderDelegate>delegate;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIImageView *backImageview;//背景
@property (nonatomic, strong) UIButton *settingBtn;//设置
@property (nonatomic, strong) UIButton *signBtn;//签到
@property (nonatomic, strong) UIButton *followBtn;
@property (nonatomic, strong) UIButton *shopBtn;
@property (nonatomic, strong) UIImageView *logImageview; //logo
@property (nonatomic, strong) UILabel *lblStatus;//身份级别
@property (nonatomic, strong) UIImageView *headerImage;
@property (nonatomic, strong) UIView *line;
@property (nonatomic, strong) CustomImageTextView *nameView;
@property (nonatomic, strong) CustomImageTextView *goldView;
@property (nonatomic, strong) CustomImageTextView *likeView;
@property (nonatomic, strong) UILabel *lblSign;
@property (nonatomic, strong) User *user;
@property (nonatomic, strong) UIView *bottomLine;
@property (nonatomic, strong) UserCenterHeaderButton *publishButton;
@property (nonatomic, strong) UserCenterHeaderButton *prasieButton;
@property (nonatomic, strong) UserCenterHeaderButton *photoButton;
@property (nonatomic, assign) NSInteger lastTag;
@property (nonatomic, strong) UIView *scrollLine;
@property (nonatomic, assign) CGFloat offX;

- (void)didSelctedIndex:(NSInteger)index;

@end


typedef NS_ENUM(NSInteger,UserCenterHeaderButtonType) {
    UserCenterHeaderButtonTypeRight,
    UserCenterHeaderButtonTypeMiddle,
    UserCenterHeaderButtonTypeLeft
};

@interface UserCenterHeaderButton : UIView

- (instancetype)initWithFrame:(CGRect)frame
                         type:(UserCenterHeaderButtonType)type;


@property (nonatomic, strong) UIImageView *headerImage;
@property (nonatomic, strong) UILabel *lblName;
@property (nonatomic, assign) UserCenterHeaderButtonType type;
- (void)updateWidth:(CGFloat)width;

@end

