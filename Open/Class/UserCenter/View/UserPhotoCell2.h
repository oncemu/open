//
//  UserPhotoCell2.h
//  Open
//
//  Created by onceMu on 2017/9/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumModel.h"

@interface UserPhotoCell2 : UICollectionViewCell

@property (nonatomic, strong) UILabel *lblName;
@property (nonatomic, strong) UIImageView *coverImage;
@property (nonatomic, strong) UILabel *lblLike;
@property (nonatomic, strong) UIImageView *likeImageView;
@property (nonatomic, strong) AlbumModel *item;
@end

