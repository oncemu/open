//
//  QXAlbumListView.h
//  ShownailSeller
//
//  Created by mfp on 16/11/4.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumEntity.h"

@protocol AlbumListViewDelegate <NSObject>

- (void)didSelectAlbumIndex:(NSInteger)index;
- (void)hideListView;

@end

@interface AlbumListView : UIView<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (nonatomic, strong) UITableView       *albumTableView;
@property (nonatomic, strong) NSMutableArray    *albumList;
@property (nonatomic, weak) id<AlbumListViewDelegate>delegate;

@end



@interface AlbumListCell : UITableViewCell

@property (nonatomic, strong) UIImageView       *albumImage;
@property (nonatomic, strong) UILabel           *albumName;
@property (nonatomic, strong) UILabel           *picCount;

@property (nonatomic, strong) AlbumEntity     *entity;



@end
