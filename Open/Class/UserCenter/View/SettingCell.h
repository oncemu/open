//
//  SettingCell.h
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@class SettingHeaderCell;
@class SettingContentCell;

@protocol SettingCellDelegate <NSObject>

- (void)modifyHeader;
- (void)clickSection:(NSInteger)section
               index:(NSInteger)index;

@end

@interface SettingCell : UITableViewCell

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) User *user;
@property (nonatomic, weak) id<SettingCellDelegate>delegate;
- (void)assignIndex:(NSIndexPath *)indexPath
               user:(User *)user;
- (void)updateIndex:(NSInteger)index
             string:(NSString *)string;
- (void)updateHeader:(UIImage *)image;

@end



@protocol SettingHeaderDelegate <NSObject>

- (void)clickModifyHeader;

@end

@interface SettingHeaderCell : UITableViewCell

@property (nonatomic, strong) UIImageView *headerImae;
@property (nonatomic, strong) UIButton *modifyBtn;
@property (nonatomic, weak) id<SettingHeaderDelegate>delegate;
- (void)assignHeader:(User *)user;

@end

@interface SettingContentCell : UITableViewCell

@property (nonatomic, strong) UIImageView *leftImage;
@property (nonatomic, strong) UILabel *lblLeft;
@property (nonatomic, strong) UILabel *lblRight;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIImageView *rightImage;


- (void)assignSection:(NSInteger)section
                index:(NSInteger)index
                 user:(User *)user;


@end
