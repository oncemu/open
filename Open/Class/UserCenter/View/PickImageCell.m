//
//  QXPickImageCell.m
//  ShownailSeller
//
//  Created by mfp on 16/11/5.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import "PickImageCell.h"
#import "ViewFactory.h"

@interface PickImageCell ()

@property (nonatomic, assign) CGFloat   contrastImageWidth;
@property (nonatomic, strong) UIButton  *imageButton;

@end

@implementation PickImageCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.thumbnailImageView = [ViewFactory getImageView:CGRectZero image:nil];
        self.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.thumbnailImageView.userInteractionEnabled = YES;
        [self.contentView addSubview:self.thumbnailImageView];
        self.contrastImageWidth = (SCREEN_WIDTH-20)/4;
        self.selectBtn = [ViewFactory getButton:CGRectMake(0, 0, 20, 20) selectImage:[UIImage imageNamed:@"pickphoto_hit"] norImage:[UIImage imageNamed:@"pickphoto_no"] target:self action:@selector(buttonClicked:)];
        [self.contentView addSubview:self.selectBtn];
        
        self.imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.imageButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.thumbnailImageView addSubview:self.imageButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.thumbnailImageView.frame = self.bounds;
    self.imageButton.frame = self.thumbnailImageView.bounds;
    self.selectBtn.frame = CM(self.frameSizeWidth-20, 0, 20, 20);
}

- (void)setAsset:(PHAsset *)asset {
    if ([asset isKindOfClass:[PHAsset class]]) {
        _asset = asset;
        self.thumbnailImageView.image = nil;
        if ([self.imageViewModel.selectMediaDataArray containsObject:asset]) {
            self.selectBtn.selected = YES;
        }else{
            self.selectBtn.selected = NO;
        }
        if (!self.asset || self.asset.mediaType != PHAssetMediaTypeImage || self.isCamera) {
            self.selectBtn.hidden = self.imageButton.hidden = YES;
            self.thumbnailImageView.userInteractionEnabled = NO;
            self.thumbnailImageView.alpha = 0.5f;
        }else{
            self.selectBtn.hidden = self.imageButton.hidden = NO;
            self.thumbnailImageView.userInteractionEnabled = YES;
            self.thumbnailImageView.alpha = 1;
        }
        
        __unsafe_unretained typeof(self)weakSelf = self;
        @autoreleasepool {
            PHImageRequestOptions *opt = [[PHImageRequestOptions alloc]init];
            opt.synchronous = NO;
            opt.resizeMode = PHImageRequestOptionsResizeModeFast;
            opt.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
            
            PHImageManager *imageManager = [PHImageManager defaultManager];
            [imageManager requestImageForAsset:asset targetSize:CGSizeMake(self.contrastImageWidth, self.contrastImageWidth) contentMode:PHImageContentModeAspectFill options:opt resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                if (result) {
                    weakSelf.thumbnailImageView.image = result;
                }
            }];
        }
    }
}

- (void)setIsCamera:(BOOL)isCamera {
    _isCamera = isCamera;
    if (isCamera) {
        self.thumbnailImageView.image = [UIImage imageNamed:@"camera_open"];
        self.selectBtn.hidden = self.imageButton.hidden = YES;
        self.thumbnailImageView.contentMode = UIViewContentModeCenter;
    }else{
        self.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
}

- (void)buttonClicked:(UIButton *)button {
//    button.selected = [self.imageViewModel selectionAnyMediaItem:self.asset];
    self.selectBtn.selected = !self.selectBtn.selected;
    self.clickSelectedButtonBlock(self.asset);
}

- (void)clickImageBackgroundButton:(UIButton *)button {
    self.selectBtn.selected = ! self.selectBtn.selected;
    self.selectBtn.selected = [self.imageViewModel selectionAnyMediaItem:self.asset];
    self.clickSelectedButtonBlock(self.asset);
}


@end
