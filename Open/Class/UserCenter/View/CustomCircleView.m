//
//  CustomCircleView.m
//  Open
//
//  Created by onceMu on 2017/7/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CustomCircleView.h"
#import "UIImage+UIColor.h"


@implementation CustomCircleView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
                         type:(CircleType)type
                    leftImage:(UIImage *)leftImage
                   rightImage:(UIImage *)rightImage
                        title:(NSString *)buttonTitle {
    self = [super initWithFrame:frame];
    if (self) {
        _type = type;
        _leftImage = leftImage;
        _rightImage = rightImage;
        _buttonTitle = buttonTitle;
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    self.layer.cornerRadius = 24.0f;
    self.backgroundColor = [UIColor clearColor];
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 0.5f;
    switch (_type) {
        case CircleTypePwd:
        {
            [self pwdView];
        }
            break;
        case CircleTypePhone:
        {
            [self phoneView];
        }
            break;
        case CircleTypeButton:
        {
            [self buttonView];
        }
            break;
        case CircleTypeShowPwd:
        {
            [self showPwdView];
        }
            break;
        case CircleTypeEnsurePwd:
        {
            [self ensurePwd];
        }
            break;
        case CircleTypeVerifyCode:
        {
            [self codeView];
        }
            break;
        default:
            break;
    }
}

- (void)codeView {
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor whiteColor];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(48);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-12);
        make.width.mas_equalTo(0.5);
    }];

    UIImageView *imageView = [[UIImageView alloc]init];
    CGFloat offX = (48-self.leftImage.size.width)/2;
    CGFloat offY = (48-self.leftImage.size.height)/2;
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(offX);
        make.top.mas_equalTo(self.mas_top).offset(offY);
        make.size.mas_equalTo(self.leftImage.size);
    }];
    imageView.image = self.leftImage;

    _codeButton = [CountDownButton buttonWithType:UIButtonTypeCustom];
    _codeButton.titleLabel.font = [UIFont systemFontOfSize:11.0f];
    [_codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    _codeButton.endTitle = @"获取验证码";
    _codeButton.titleLabel.textColor = [UIColor colorWithHex:0x737378];
    [_codeButton addTarget:self action:@selector(onCodeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_codeButton];
    [_codeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-12);
        make.width.mas_equalTo(70);
    }];
    _codeButton.layer.cornerRadius = 12.0f;
    _codeButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _codeButton.layer.borderWidth = 0.5f;

    _textFiled = [[CustomTextFiled alloc]init];
    _textFiled.tintColor = [UIColor whiteColor];
    _textFiled.delegate = self;
    _textFiled.textColor = [UIColor whiteColor];
    _textFiled.keyboardType = UIKeyboardTypePhonePad;
    _textFiled.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"验证码" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    _textFiled.font = [UIFont systemFontOfSize:12.0f];
    [self addSubview:_textFiled];
    [_textFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(line.mas_right).offset(15);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-12);
        make.right.mas_equalTo(self.codeButton.mas_left).offset(-5);
    }];
}

- (void)ensurePwd {
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor whiteColor];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(48);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-12);
        make.width.mas_equalTo(0.5);
    }];

    UIImageView *imageView = [[UIImageView alloc]init];
    CGFloat offX = (48-self.leftImage.size.width)/2;
    CGFloat offY = (48-self.leftImage.size.height)/2;
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(offX);
        make.top.mas_equalTo(self.mas_top).offset(offY);
        make.size.mas_equalTo(self.leftImage.size);
    }];
    imageView.image = self.leftImage;

    _textFiled = [[CustomTextFiled alloc]init];
    _textFiled.tintColor = [UIColor whiteColor];
    _textFiled.delegate = self;
    _textFiled.textColor = [UIColor whiteColor];
    _textFiled.font = [UIFont systemFontOfSize:12.0f];
    _textFiled.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"确认密码" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self addSubview:_textFiled];
    [_textFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(line.mas_right).offset(15);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-12);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
}

- (void)showPwdView {
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor whiteColor];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(48);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-12);
        make.width.mas_equalTo(0.5);
    }];

    UIImageView *imageView = [[UIImageView alloc]init];
    CGFloat offX = (48-self.leftImage.size.width)/2;
    CGFloat offY = (48-self.leftImage.size.height)/2;
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(offX);
        make.top.mas_equalTo(self.mas_top).offset(offY);
        make.size.mas_equalTo(self.leftImage.size);
    }];
    imageView.image = self.leftImage;


    UIView *rightView = [[UIView alloc]init];
    [self addSubview:rightView];
    [rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right);
        make.top.mas_equalTo(self.mas_top);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.width.mas_equalTo(48);
    }];

    _rightImageView = [[UIImageView alloc]init];
    CGFloat rightOffY = (48-self.rightImage.size.height)/2;
    [rightView addSubview:_rightImageView];
    [_rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-28);
        make.top.mas_equalTo(self.mas_top).offset(rightOffY);
        make.size.mas_equalTo(self.rightImage.size);
    }];
    _rightImageView.image = self.rightImage;
    rightView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapSecture)];
    [rightView addGestureRecognizer:tap];

    _textFiled = [[CustomTextFiled alloc]init];
    _textFiled.tintColor = [UIColor whiteColor];
    _textFiled.delegate = self;
    _textFiled.textColor = [UIColor whiteColor];
    _textFiled.font = [UIFont systemFontOfSize:12.0f];
    _textFiled.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"输入密码" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self addSubview:_textFiled];
    [_textFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(line.mas_right).offset(15);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-12);
        make.right.mas_equalTo(rightView.mas_left).offset(-5);
    }];
}

- (void)buttonView {
    self.layer.borderWidth = 0.0f;
    self.layer.borderColor = [UIColor clearColor].CGColor;
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    _button.frame = self.bounds;
    _button.clipsToBounds = YES;
    _button.layer.cornerRadius = 24.0f;
    [_button setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHex:0xffb200]] forState:UIControlStateNormal];
    [_button setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHex:0xffe672]] forState:UIControlStateSelected];
    _button.selected = NO;
    _button.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_button setTitle:self.buttonTitle forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(onLoginButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_button];
}

- (void)phoneView {
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor whiteColor];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(48);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-12);
        make.width.mas_equalTo(0.5);
    }];

    UILabel *lblTips = [[UILabel alloc]init];
    lblTips.textColor = [UIColor whiteColor];
    lblTips.font = [UIFont systemFontOfSize:11.0f];
    [self addSubview:lblTips];
    [lblTips mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(14);
        make.right.mas_equalTo(line.mas_left);
        make.top.mas_equalTo(self.mas_top).offset(18);
        make.height.mas_equalTo(11);
    }];
    lblTips.text = @"+86";

    _textFiled = [[CustomTextFiled alloc]init];
    _textFiled.tintColor = [UIColor whiteColor];
    _textFiled.keyboardType = UIKeyboardTypePhonePad;
    _textFiled.delegate = self;
    _textFiled.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"手机号" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    _textFiled.textColor = [UIColor whiteColor];
    _textFiled.font = [UIFont systemFontOfSize:12.0f];
    [self addSubview:_textFiled];
    [_textFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(line.mas_right).offset(15);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-12);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
}


- (void)pwdView {
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor whiteColor];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(48);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-12);
        make.width.mas_equalTo(0.5);
    }];
    UIView *rightView = [[UIView alloc]init];
//    rightView.backgroundColor = [UIColor redColor];
    [self addSubview:rightView];
    rightView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapSecture)];
    [rightView addGestureRecognizer:tap];
    [rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right);
        make.top.mas_equalTo(self.mas_top);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.width.mas_equalTo(48);
    }];
    _rightImageView = [[UIImageView alloc]init];
    CGFloat offX = (48-self.leftImage.size.width)/2;
    CGFloat offY = (48-self.leftImage.size.height)/2;
    [rightView addSubview:_rightImageView];
    [_rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(offX);
        make.top.mas_equalTo(self.mas_top).offset(offY);
        make.size.mas_equalTo(self.leftImage.size);
    }];
    _rightImageView.image = self.leftImage;

    _textFiled = [[CustomTextFiled alloc]init];
    _textFiled.tintColor = [UIColor whiteColor];
    _textFiled.delegate = self;
    _textFiled.secureTextEntry = NO;
    _textFiled.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"密码" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    _textFiled.textColor = [UIColor whiteColor];
    _textFiled.font = [UIFont systemFontOfSize:12.0f];
    [self addSubview:_textFiled];
    [_textFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(line.mas_right).offset(15);
        make.top.mas_equalTo(self.mas_top).offset(12);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-12);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
}


- (void)tapSecture {
    self.textFiled.secureTextEntry = !self.textFiled.secureTextEntry;
    if (self.textFiled.secureTextEntry) {
        self.rightImageView.image = [UIImage imageNamed:@"loginCloseEye"];
        UIImage *image = [UIImage imageNamed:@"loginCloseEye"];
        CGFloat offX = (48-image.size.width)/2;
        CGFloat offY = (48-image.size.height)/2;
        [_rightImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_right).offset(-offX);
            make.top.mas_equalTo(self.mas_top).offset(offY);
            make.size.mas_equalTo(CGSizeMake(image.size.width, image.size.height));
        }];
    }else {
        self.rightImageView.image = self.rightImage;
        CGFloat offX = (48-self.rightImage.size.width)/2;
        CGFloat offY = (48-self.rightImage.size.height)/2;
        [_rightImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_right).offset(-offX);
            make.top.mas_equalTo(self.mas_top).offset(offY);
            make.size.mas_equalTo(self.rightImage.size);
        }];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(changeTextFiledSecturyStatus:)]) {
        [self.delegate changeTextFiledSecturyStatus:self.textFiled.secureTextEntry];
    }
}

- (void)onLoginButtonClick:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(loginAcction)]) {
        [self.delegate loginAcction];
    }
}

- (void)onCodeButtonClick {
    [self startCode];
    if (self.delegate && [self.delegate respondsToSelector:@selector(fetchCode)]) {
        [self.delegate fetchCode];
    }
}

- (void)setLimitCount:(NSInteger)limitCount {
    _limitCount = limitCount;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.limitCount == 0) {
        return YES;
    }
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;

    NSUInteger newLength = oldLength - rangeLength + replacementLength;

    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;

    return newLength <= self.limitCount || returnKey;
}

- (void)startCode {
    self.codeButton.start = YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
