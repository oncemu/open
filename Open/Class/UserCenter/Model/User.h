//
//  User.h
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject<NSCoding>

@property (nonatomic, strong) NSString *avatar;//头像
@property (nonatomic, assign) NSInteger collectionCount;//收藏数
@property (nonatomic, assign) NSInteger fansCount;//粉丝
@property (nonatomic, strong) NSString *gender;//性别
@property (nonatomic, assign) NSInteger level;//等级
@property (nonatomic, strong) NSString  *levelName;
@property (nonatomic, assign) NSInteger likeCount;//关注数
@property (nonatomic, strong) NSString *signature;//签名
@property (nonatomic, strong) NSString *nickname;//昵称
@property (nonatomic, strong) NSString *userId;//用用户id
@property (nonatomic, assign) NSInteger worksCount;//作品数
@property (nonatomic, assign) NSInteger gold;//金币数量
@property (nonatomic, assign) NSInteger favoriteCount;//点赞数
@property (nonatomic, assign) NSInteger photoCount;//美图数
@property (nonatomic, assign) NSInteger postCount;//发布数
@property (nonatomic, assign) NSInteger canModifyPassword;
@property (nonatomic, strong) NSString *background;//背景图



@end

//background (string, optional): 背景图片 ,
//canModifyPassword (integer, optional): 是否能修改密码 ,
//collectionCount (integer, optional): 收藏数 ,
//fansCount (integer, optional): 粉丝数 ,
//favoriteCount (integer, optional): 点赞数 ,
//gender (string, optional): 性别 ,
//gold (integer, optional): 金币 ,
//level (integer, optional): 等级 ,
//levelName (string, optional): 等级名称 ,
//likeCount (integer, optional): 关注数 ,
//nickname (string, optional): 昵称 ,
//photoCount (integer, optional): 美图数 ,
//postCount (integer, optional): 发布数 ,
//signature (string, optional): 个性签名 ,
//userId (integer, optional): 用户ID


//likeCount = 2;
//photoCount = 9;
//postCount = 12;
