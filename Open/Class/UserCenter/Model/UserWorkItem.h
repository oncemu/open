//
//  UserWorkItem.h
//  Open
//
//  Created by onceMu on 2017/9/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlbumModel.h"

@interface UserWorkItem : NSObject

@property (nonatomic, assign) NSInteger albumId;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, assign) NSInteger canMake;
@property (nonatomic, copy) NSString *category;
@property (nonatomic, copy) NSString *categoryText;
@property (nonatomic, copy) NSString *city;//城市
@property (nonatomic, assign) NSInteger collectionCount;//收藏数
@property (nonatomic, assign) NSInteger count;//总数
@property (nonatomic, assign) NSInteger commentCount;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *createdAt;
@property (nonatomic, copy) NSString *userDescription;
@property (nonatomic, copy) NSString *detailedAddress;
@property (nonatomic, assign) NSInteger favoriteCount;//喜欢数
@property (nonatomic, assign) NSInteger isFavourite;//是否已经点赞
@property (nonatomic, assign) NSInteger isLikedAuthor;//是否关注作者
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, assign) NSInteger level;
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, copy) NSString *weather;
@property (nonatomic, copy) NSString *nickname;



@end

