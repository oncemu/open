//
//  QXPickImageViewModel.h
//  ShownailSeller
//
//  Created by mfp on 16/11/5.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

#define MAX_COUNT  9

typedef void(^PHAssetExceedBoundsBlock)(void);

@interface PickImageViewModel : NSObject

//相册里的图片列表
@property (strong,nonatomic) NSMutableArray *selectMediaDataArray;

@property (strong,nonatomic) id touchColletionViewCell;

@property (copy,nonatomic) PHAssetExceedBoundsBlock exceedBoundsBlock;

@property (nonatomic, assign) NSInteger maxCount;//当前能选择上限

-(BOOL)selectionAnyMediaItem:(PHAsset *)asset;

@end

@interface PickAlbumViewModel : NSObject

@property (strong,nonatomic) NSMutableArray *albumArray;//相册列表

@end
