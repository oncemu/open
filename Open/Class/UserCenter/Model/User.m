//
//  User.m
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "User.h"

@implementation User

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.avatar = [aDecoder decodeObjectForKey:@"avatar"];
        self.collectionCount = [aDecoder decodeIntegerForKey:@"collectionCount"];
        self.fansCount = [aDecoder decodeIntegerForKey:@"fansCount"];
        self.gender = [aDecoder decodeObjectForKey:@"gender"];
        self.level = [aDecoder decodeIntegerForKey:@"level"];
        self.levelName = [aDecoder decodeObjectForKey:@"levelName"];
        self.likeCount = [aDecoder decodeIntegerForKey:@"likeCount"];
        self.signature = [aDecoder decodeObjectForKey:@"signature"];
        self.nickname = [aDecoder decodeObjectForKey:@"nickname"];
        self.userId = [aDecoder decodeObjectForKey:@"userId"];
        self.worksCount = [aDecoder decodeIntegerForKey:@"worksCount"];
        self.gold = [aDecoder decodeIntegerForKey:@"gold"];
        self.favoriteCount = [aDecoder decodeIntegerForKey:@"favoriteCount"];
        self.photoCount = [aDecoder decodeIntegerForKey:@"photoCount"];
        self.postCount = [aDecoder decodeIntegerForKey:@"postCount"];
        self.canModifyPassword = [aDecoder decodeIntegerForKey:@"canModifyPassword"];
        self.background = [aDecoder decodeObjectForKey:@"background"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.avatar forKey:@"avatar"];
    [aCoder encodeInteger:self.collectionCount forKey:@"collectionCount"];
    [aCoder encodeInteger:self.fansCount forKey:@"fansCount"];
    [aCoder encodeObject:self.gender forKey:@"gender"];
    [aCoder encodeInteger:self.level forKey:@"level"];
    [aCoder encodeObject:self.levelName forKey:@"levelName"];
    [aCoder encodeInteger:self.likeCount forKey:@"likeCount"];
    [aCoder encodeObject:self.signature forKey:@"signature"];
    [aCoder encodeObject:self.nickname forKey:@"nickname"];
    [aCoder encodeObject:self.userId forKey:@"userId"];
    [aCoder encodeInteger:self.worksCount forKey:@"worksCount"];
    [aCoder encodeInteger:self.gold forKey:@"gold"];
    [aCoder encodeInteger:self.favoriteCount forKey:@"favoriteCount"];
    [aCoder encodeInteger:self.photoCount forKey:@"photoCount"];
    [aCoder encodeInteger:self.postCount forKey:@"postCount"];
    [aCoder encodeInteger:self.canModifyPassword forKey:@"canModifyPassword"];
    [aCoder encodeObject:self.background forKey:@"background"];
}
@end
