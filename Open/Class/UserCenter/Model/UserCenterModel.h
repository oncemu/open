//
//  UserCenterModel.h
//  Open
//
//  Created by onceMu on 2017/8/1.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "UserNetService.h"

@interface UserCenterModel : NSObject



/**
 更新用户信息

 @param user 用户信息
 @param success 成功
 @param failed 失败
 */
- (void)updateUserInfo:(User *)user
               success:(responseSuccess)success
                failed:(responseFailure)failed;

@end
