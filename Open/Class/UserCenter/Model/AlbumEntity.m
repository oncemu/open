//
//  QXAlbumEntity.m
//  ShownailSeller
//
//  Created by mfp on 16/11/5.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import "AlbumEntity.h"

@implementation AlbumEntity

+(AlbumEntity *)filmCameras{
    
    AlbumEntity *albumEntity;
    PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    for (PHAssetCollection *assetCollection in smartAlbums) {
        PHFetchResult *fetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:nil];
        if (assetCollection.assetCollectionSubtype == PHAssetCollectionSubtypeSmartAlbumUserLibrary && fetchResult.count>0) {
            albumEntity = [[AlbumEntity alloc] init];
            albumEntity.fetchResult = fetchResult;
            albumEntity.assetCollection = assetCollection;
        }
    }
    return albumEntity;
}

@end
