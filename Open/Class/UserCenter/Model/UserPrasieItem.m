//
//  UserPrasieItem.m
//  Open
//
//  Created by onceMu on 2017/9/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserPrasieItem.h"

@implementation UserPrasieItem

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"photos":[PhotoItem class],
             };
}

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper {

    return @{@"description":@"userdDescription"};
}

@end
