//
//  UserPrasieItem.h
//  Open
//
//  Created by onceMu on 2017/9/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlbumModel.h"

@interface UserPrasieItem : NSObject

@property (nonatomic, assign) NSInteger albumId;
@property (nonatomic, assign) NSInteger canMake;
@property (nonatomic, copy) NSString *category;
@property (nonatomic, copy) NSString *categoryText;
@property (nonatomic, assign) NSInteger collectionCount;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *createdAt;
@property (nonatomic, copy) NSString *userdDescription;
@property (nonatomic, assign) NSInteger favoriteCount;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, strong) NSArray *photos;
@end
