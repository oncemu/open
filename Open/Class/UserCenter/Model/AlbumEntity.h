//
//  QXAlbumEntity.h
//  ShownailSeller
//
//  Created by mfp on 16/11/5.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

@interface AlbumEntity : NSObject

@property (strong,nonatomic) PHFetchResult *fetchResult;

@property (strong,nonatomic) PHAssetCollection *assetCollection;

+(AlbumEntity *)filmCameras;

@end
