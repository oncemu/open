//
//  QXPickImageViewModel.m
//  ShownailSeller
//
//  Created by mfp on 16/11/5.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import "PickImageViewModel.h"
#import <Photos/Photos.h>
#import "AlbumEntity.h"


@implementation PickImageViewModel
-(instancetype)init{
    
    if (self = [super init]) {
        
        _selectMediaDataArray = [NSMutableArray array];
        
    }
    
    return self;
}

-(BOOL)selectionAnyMediaItem:(PHAsset *)asset{
    
    if (self.selectMediaDataArray.count>self.maxCount) {
        
        self.exceedBoundsBlock();
        return NO;
        
    }else{
        
        if ([self.selectMediaDataArray containsObject:asset]) {
            
            [self.selectMediaDataArray removeObject:asset];
            return NO;
            
        }else{
            
            if (self.selectMediaDataArray.count==self.maxCount) {
                
                self.exceedBoundsBlock();
                return NO;
                
            }else{
                [self.selectMediaDataArray addObject:asset];
            }
            
        }
        return YES;
    }
    
}


@end


@implementation PickAlbumViewModel

-(NSMutableArray *)albumArray{
    
    if (![_albumArray isKindOfClass:[NSMutableArray class]]) {
        _albumArray = [NSMutableArray array];
    }else{
        [_albumArray removeAllObjects];
    }
    
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:NO]];
    
    PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:options];
    PHFetchOptions *sort = [[PHFetchOptions alloc] init];
    sort.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    for (PHAssetCollection *assetCollection in smartAlbums) {
        if (assetCollection.assetCollectionSubtype == PHAssetCollectionSubtypeSmartAlbumVideos) {
            break;
        }

        PHFetchResult *fetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:sort];
        if (fetchResult.count>0) {
            AlbumEntity *albumEntity = [[AlbumEntity alloc] init];
            albumEntity.fetchResult = fetchResult;
            albumEntity.assetCollection = assetCollection;
            if (assetCollection.assetCollectionSubtype == PHAssetCollectionSubtypeSmartAlbumUserLibrary) {
                [_albumArray insertObject:albumEntity atIndex:0];
            }else{
                [_albumArray addObject:albumEntity];
            }
        }
    }
    //单独获取相机胶卷
    PHAssetCollection *cameraRoll = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary options:nil].lastObject;
    DLog(@"%@",cameraRoll);
    PHFetchResult *cameraResult = [PHAsset fetchAssetsInAssetCollection:cameraRoll options:sort];
    if (cameraResult.count >0) {
        AlbumEntity *albumEntity = [[AlbumEntity alloc]init];
        albumEntity.fetchResult = cameraResult;
        albumEntity.assetCollection = cameraRoll;
        [_albumArray insertObject:albumEntity atIndex:0];
    }
    
    PHFetchResult *albums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    for (PHAssetCollection *assetCollection in albums) {
        PHFetchResult *fetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:sort];
        
        if (fetchResult.count>0) {
            
            AlbumEntity *albumEntity = [[AlbumEntity alloc] init];
            albumEntity.fetchResult = fetchResult;
            albumEntity.assetCollection = assetCollection;
            [_albumArray addObject:albumEntity];
        }
        
    }
    
    return _albumArray;
}

@end
