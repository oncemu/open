//
//  MyRankVC.m
//  Open
//
//  Created by onceMu on 2017/7/28.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MyRankVC.h"
#import "RankReusableView.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "RankCell.h"

static NSString * const kRankCellIdentifier = @"RankCellIdentifier";
static NSString * const kRankHeaderIdentifier = @"RankHeaderIdentifier";

@interface MyRankVC ()<CHTCollectionViewDelegateWaterfallLayout>

@end

@implementation MyRankVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"头衔";
    self.collectionView.backgroundColor = [UIColor whiteColor];

    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc]init];
    layout.columnCount = 1;
    layout.minimumColumnSpacing = 0;
    layout.minimumContentHeight = 0;
    layout.minimumInteritemSpacing = 0;

    self.collectionView.collectionViewLayout = layout;
    [self.collectionView registerClass:[RankCell class] forCellWithReuseIdentifier:kRankCellIdentifier];
    [self.collectionView registerClass:[RankReusableView class] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:kRankHeaderIdentifier];
    [self.collectionView reloadData];
    [self.header endRefreshing];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 5;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RankCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kRankCellIdentifier forIndexPath:indexPath];
    [cell assignObejct:nil index:indexPath.item];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
        RankReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:kRankHeaderIdentifier forIndexPath:indexPath];
        return view;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(SCREEN_WIDTH, 100);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section {
    return 238.0f;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
