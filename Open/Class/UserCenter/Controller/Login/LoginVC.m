//
//  LoginVC.m
//  Open
//
//  Created by onceMu on 2017/7/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "LoginVC.h"


#define kThirdTypeTag      10000

@interface LoginVC ()

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    self.view.backgroundColor = [UIColor redColor];

    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 64, 64)];
    [self.view addSubview:backView];
    backView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goBack)];
    [backView addGestureRecognizer:tap];

    //26*44
    UIImageView *backImage = [[UIImageView alloc]init];
    backImage.frame = CGRectMake(20, 23, 13, 22);
    backImage.image = [UIImage imageNamed:@"base_back"];
    [backView addSubview:backImage];

    //112
    UIImage *log = [UIImage imageNamed:@"loginLog"];
    UIImageView *logImageView = [[UIImageView alloc]init];
    logImageView.frame = CGRectMake((SCREEN_WIDTH-log.size.width)/2, 56, log.size.width, log.size.height);
    logImageView.image = log;
    [self.view addSubview:logImageView];

    _originY = logImageView.frameSizeHeight + logImageView.frameOriginY;

    UIImageView *bkImage = [[UIImageView alloc]initWithFrame:self.view.bounds];
    bkImage.image = [UIImage imageNamed:@"loginBK"];
    [self.view addSubview: bkImage];
    [self.view sendSubviewToBack:bkImage];

    [self createThirdLogin];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}



- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)createThirdLogin {
    NSString *text = @"第三方登录";
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    _lblThird = [[UILabel alloc]init];
    _lblThird.textColor = [UIColor whiteColor];
    _lblThird.font = font;
    _lblThird.textAlignment = NSTextAlignmentCenter;
    _lblThird.text = text;
    _lblThird.hidden = YES;
    [self.view addSubview:_lblThird];

    _leftLine = [[UIView alloc]init];
    _leftLine.hidden = YES;
    _leftLine.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_leftLine];
    _rightLine = [[UIView alloc]init];
    _rightLine.hidden = YES;
    _rightLine.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_rightLine];

    //102 *102
    CGFloat offWidth = (SCREEN_WIDTH-56*3)/4;
    _weiboLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    _weiboLogin.tag = kThirdTypeTag+ThirdTypeWeibo;
    _weiboLogin.hidden = YES;
    [_weiboLogin setBackgroundImage:[UIImage imageNamed:@"weiboLogin"] forState:UIControlStateNormal];
    [_weiboLogin addTarget:self action:@selector(onWeiBoButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_weiboLogin];
    [_weiboLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(offWidth);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-25);
        make.size.mas_equalTo(CGSizeMake(51, 51));
    }];


    _wxLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    _wxLogin.tag = kThirdTypeTag+ThirdTypeWeixin;
    _wxLogin.hidden = YES;
    [_wxLogin setBackgroundImage:[UIImage imageNamed:@"weixinLogin"] forState:UIControlStateNormal];
    [_wxLogin addTarget:self action:@selector(onWeiBoButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_wxLogin];
    [_wxLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.weiboLogin.mas_right).offset(offWidth);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-25);
        make.size.mas_equalTo(CGSizeMake(51, 51));
    }];

    _qqLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    _qqLogin.tag = kThirdTypeTag+ThirdTypeQQ;
    _qqLogin.hidden = YES;
    [_qqLogin setBackgroundImage:[UIImage imageNamed:@"qqLogin"] forState:UIControlStateNormal];
    [_qqLogin addTarget:self action:@selector(onWeiBoButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_qqLogin];
    [_qqLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.wxLogin.mas_right).offset(offWidth);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-25);
        make.size.mas_equalTo(CGSizeMake(51, 51));
    }];
}

- (void)onWeiBoButtonClick:(UIButton *)sender {
    NSInteger tag = sender.tag - kThirdTypeTag;
    switch (tag) {
        case ThirdTypeWeixin:
        {

        }
            break;
        case ThirdTypeWeibo:
        {

        }
            break;
        case ThirdTypeQQ:
        {
            
        }
            break;
        default:
            break;
    }
}

- (BOOL)checkMobile:(NSString *)mobile {
    if (![StringUtil isNONil:mobile]) {
        [KVNProgress showErrorWithStatus:@"请输入手机号"];
        return NO;
    }
    if (![StringUtil checkIsPhoneNumber:mobile]) {
        [KVNProgress showErrorWithStatus:@"请输入正确的手机号"];
        return NO;
    }
    return YES;
}

- (UserNetService *)netService {
    if (_netService == nil) {
        _netService = [[UserNetService alloc]init];
    }
    return _netService;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
