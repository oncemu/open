//
//  LoginVC.h
//  Open
//
//  Created by onceMu on 2017/7/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseVC.h"
#import "UserNetService.h"

typedef NS_ENUM(NSInteger,ThirdType) {
    ThirdTypeWeibo,
    ThirdTypeWeixin,
    ThirdTypeQQ
};

@interface LoginVC : BaseVC

@property (nonatomic, assign) CGFloat originY;
@property (nonatomic, strong) UIView *leftLine;
@property (nonatomic, strong) UIView *rightLine;
@property (nonatomic, strong) UILabel *lblThird;
@property (nonatomic, strong) UIButton *wxLogin;
@property (nonatomic, strong) UIButton *weiboLogin;
@property (nonatomic, strong) UIButton *qqLogin;
@property (nonatomic, strong) UserNetService *netService;


- (BOOL)checkMobile:(NSString *)mobile;


@end
