//
//  ModifyPasswordVC.m
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "ModifyPasswordVC.h"
#import "CustomLabelTextView.h"
#import "CustomCircleView.h"

@interface ModifyPasswordVC ()<CustomCircleViewDelegate>

@property (nonatomic, strong) CustomLabelTextView *oldView;
@property (nonatomic, strong) CustomLabelTextView *pwdView;
@property (nonatomic, strong) CustomLabelTextView *tPwdView;
@property (nonatomic, strong) CustomCircleView *btnView;

@end

@implementation ModifyPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"修改密码";

    _oldView = [[CustomLabelTextView alloc]initWithFrame:CGRectMake(0, 30+64, SCREEN_WIDTH, 50)];
    _oldView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_oldView];
    _oldView.title = @"旧密码";
    _oldView.placeholder = @"输入旧密码";

    _pwdView = [[CustomLabelTextView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.oldView.frame)+30, SCREEN_WIDTH, 50)];
    _pwdView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_pwdView];
    _pwdView.title = @"新密码";
    _pwdView.placeholder = @"输入新密码";

    _tPwdView = [[CustomLabelTextView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.pwdView.frame), SCREEN_WIDTH, 50)];
    _tPwdView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_tPwdView];
    _tPwdView.title = @"输入新密码";
    _tPwdView.placeholder = @"输入新密码";
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.5)];
    line.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [_tPwdView addSubview:line];

    _btnView = [[CustomCircleView alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(self.tPwdView.frame)+30, SCREEN_WIDTH-60, 48) type:CircleTypeButton leftImage:nil rightImage:nil title:@"修改密码"];
    _btnView.delegate = self;
    _btnView.backgroundColor = [UIColor colorWithHex:0xffe000];
    [self.view addSubview:_btnView];

    // Do any additional setup after loading the view.
}

- (void)loginAcction {

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
