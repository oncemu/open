//
//  PwdLoginVC.m
//  Open
//
//  Created by onceMu on 2017/7/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PwdLoginVC.h"
#import "CustomCircleView.h"
#import "FindPwdVC.h"
#import "RegisterVC.h"

@interface PwdLoginVC ()<CustomCircleViewDelegate>

@property (nonatomic, strong) CustomCircleView *phoneView;
@property (nonatomic, strong) CustomCircleView *pwdView;
@property (nonatomic, strong) CustomCircleView *buttomView;
@property (nonatomic, strong) UIButton *findPwdButton;
@property (nonatomic, strong) UIButton *registerButton;

@end

@implementation PwdLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CGFloat offY = self.originY + 30;

    _phoneView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypePhone leftImage:nil rightImage:nil title:nil];
    _phoneView.limitCount = 11;
    [self.view addSubview:_phoneView];
    //48 是高度，28是间距
    offY += 48+28;

    _pwdView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypePwd leftImage:[UIImage imageNamed:@"pwdLock"] rightImage:nil title:nil];
    [self.view addSubview:_pwdView];

    offY += 48 +28;

    _buttomView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypeButton leftImage:nil rightImage:nil title:@"登录"];
    _buttomView.delegate = self;
    [self.view addSubview:_buttomView];


    _findPwdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _findPwdButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    _findPwdButton.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [_findPwdButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_findPwdButton setTitle:@"忘记密码?" forState:UIControlStateNormal];
    [_findPwdButton addTarget:self action:@selector(onFindPwdButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_findPwdButton];
    [_findPwdButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.buttomView.mas_left);
        make.top.mas_equalTo(self.buttomView.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(85, 25));
    }];


    _registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _registerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _registerButton.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [_registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_registerButton setTitle:@"注册" forState:UIControlStateNormal];
    [_registerButton addTarget:self action:@selector(onRegisterButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_registerButton];
    [_registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.buttomView.mas_right);
        make.top.mas_equalTo(self.buttomView.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(60, 25));
    }];

    self.lblThird.hidden = NO;
    CGSize bounds = [@"第三方登录" boundingRectWithSize:CGSizeMake(MAXFLOAT, 14) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil].size;
    CGFloat width = ceil(bounds.width)+4;
    CGFloat height = 75;
    if (iPhone5 || isRetina) {
        height = 35;
    }
    [self.lblThird mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset((SCREEN_WIDTH-width)/2);
        make.top.mas_equalTo(self.registerButton.mas_bottom).offset(height);
        make.size.mas_equalTo(CGSizeMake(width, 14));
    }];

    self.leftLine.hidden = NO;
    [self.leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.top.mas_equalTo(self.lblThird.mas_top).offset(7);
        make.right.mas_equalTo(self.lblThird.mas_left);
        make.height.mas_equalTo(0.5);
    }];

    self.rightLine.hidden = NO;
    [self.rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.lblThird.mas_right);
        make.top.mas_equalTo(self.lblThird.mas_top).offset(7);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(0.5);
    }];
    self.weiboLogin.hidden = NO;
    self.wxLogin.hidden = NO;
    self.qqLogin.hidden = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onFindPwdButtonClick {
    FindPwdVC *findVC = [[FindPwdVC alloc]initWithShowBackButton:NO];
    [self pushVC:findVC animated:YES];
}

- (void)onRegisterButtonClick {
    RegisterVC *regVC = [[RegisterVC alloc]initWithShowBackButton:NO];
    [self pushVC:regVC animated:YES];
}

- (void)loginAcction {
    NSString *mobile = self.phoneView.textFiled.text;
    NSString *pwd = self.pwdView.textFiled.text;
    BOOL phoneSuccess = [self checkMobile:mobile];
    if (!phoneSuccess) {
        return;
    }
    if (![StringUtil isNONil:pwd]) {
        [KVNProgress showErrorWithStatus:@"请输入密码"];
        return;
    }
    NSMutableDictionary *parames = [NSMutableDictionary dictionaryWithCapacity:1];
    [parames setObject:mobile forKey:@"mobile"];
    [parames setObject:pwd forKey:@"password"];
    [KVNProgress showWithStatus:@"正在登录"];
    [self.netService signType:SignTypeNormal parames:parames success:^(NSString *requestInfo, id responseObject) {
        [KVNProgress showSuccessWithStatus:@"登录成功"];
        if (self.delegate && [self.delegate respondsToSelector:@selector(loginSuccess)]) {
            [self.delegate loginSuccess];
        }
        [self.navigationController popViewControllerAnimated:YES];
    } failed:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:[responseObject objectForKey:@"message"]];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
