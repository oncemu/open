//
//  CodeLoginVC.m
//  Open
//
//  Created by onceMu on 2017/7/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CodeLoginVC.h"
#import "CustomCircleView.h"
#import "RegisterVC.h"
#import "PwdLoginVC.h"

@interface CodeLoginVC ()<PwdLoginVCDelegate,RegisterVCDelegate,CustomCircleViewDelegate>

@property (nonatomic, strong) CustomCircleView *phoneView;
@property (nonatomic, strong) CustomCircleView *codeView;
@property (nonatomic, strong) CustomCircleView *loginView;
@property (nonatomic, strong) UIButton *pwdLoingButton;
@property (nonatomic, strong) UIButton *registerButton;


@end

@implementation CodeLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];

    CGFloat offY = self.originY;
    offY += 30;
    _phoneView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypePhone leftImage:nil rightImage:nil title:nil];
    [self.view addSubview:_phoneView];
    //48 是高度，28是间距
    offY += 48+28;
    _codeView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypeVerifyCode leftImage:[UIImage imageNamed:@"pwdLock"] rightImage:nil title:nil];
    [self.view addSubview:_codeView];

    offY +=48+28;

    _loginView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypeButton leftImage:nil rightImage:nil title:@"登录"];
    _loginView.delegate = self;
    [self.view addSubview:_loginView];

    _pwdLoingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _pwdLoingButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    _pwdLoingButton.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [_pwdLoingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_pwdLoingButton setTitle:@"密码登录" forState:UIControlStateNormal];
    [_pwdLoingButton addTarget:self action:@selector(onPwdButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_pwdLoingButton];
    [_pwdLoingButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.loginView.mas_left);
        make.top.mas_equalTo(self.loginView.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(85, 25));
    }];


    _registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _registerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _registerButton.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [_registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_registerButton setTitle:@"注册" forState:UIControlStateNormal];
    [_registerButton addTarget:self action:@selector(onRegisterButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_registerButton];
    [_registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.loginView.mas_right);
        make.top.mas_equalTo(self.loginView.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(60, 25));
    }];

    self.lblThird.hidden = NO;
    CGSize bounds = [@"第三方登录" boundingRectWithSize:CGSizeMake(MAXFLOAT, 14) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil].size;
    CGFloat width = ceil(bounds.width)+4;
    CGFloat height = 75;
    if (iPhone5 || isRetina) {
        height = 35;
    }
    [self.lblThird mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset((SCREEN_WIDTH-width)/2);
        make.top.mas_equalTo(self.registerButton.mas_bottom).offset(height);
        make.size.mas_equalTo(CGSizeMake(width, 14));
    }];

    self.leftLine.hidden = NO;
    [self.leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.top.mas_equalTo(self.lblThird.mas_top).offset(7);
        make.right.mas_equalTo(self.lblThird.mas_left);
        make.height.mas_equalTo(0.5);
    }];

    self.rightLine.hidden = NO;
    [self.rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.lblThird.mas_right);
        make.top.mas_equalTo(self.lblThird.mas_top).offset(7);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(0.5);
    }];
    self.weiboLogin.hidden = NO;
    self.wxLogin.hidden = NO;
    self.qqLogin.hidden = NO;

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//密码登录
- (void)onPwdButtonClick {
    PwdLoginVC *pwdVC = [[PwdLoginVC alloc]initWithShowBackButton:NO];
    pwdVC.delegate = self;
    pwdVC.navigationController.navigationBar.hidden = YES;
//    [self pushVC:pwdVC animated:YES];
    [self.navigationController pushViewController:pwdVC animated:YES];
}

//注册
- (void)onRegisterButtonClick {
    RegisterVC *regVC = [[RegisterVC alloc]initWithShowBackButton:NO];
    regVC.delegate = self;
    regVC.navigationController.navigationBar.hidden = YES;
//    [self pushVC:regVC animated:YES];
    [self.navigationController pushViewController:regVC animated:YES];
}

- (void)goBack {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)loginSuccess {
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserLoginSuccessNotificationName object:nil];
    [self goBack];
}

- (void)registerSuccess {

}

- (void)loginAcction {
    NSString *phone = self.phoneView.textFiled.text;
    NSString *code = self.codeView.textFiled.text;
    BOOL phoneSuccess = [self checkMobile:phone];
    if (!phoneSuccess) {
        return;
    }
    if (![StringUtil checkIsPhoneNumber:code]) {
        [KVNProgress showErrorWithStatus:@"请输入验证码"];
        return;
    }
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
