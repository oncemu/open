//
//  RegisterVC.m
//  Open
//
//  Created by onceMu on 2017/7/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "RegisterVC.h"
#import "CustomCircleView.h"
#import "ProtocalVC.h"

@interface RegisterVC ()<CustomCircleViewDelegate>

@property (nonatomic, strong) CustomCircleView *phoneView;
@property (nonatomic, strong) CustomCircleView *showPwd;
@property (nonatomic, strong) CustomCircleView *ensurePwd;
@property (nonatomic, strong) CustomCircleView *codeView;
@property (nonatomic, strong) CustomCircleView *buttonView;
@property (nonatomic, strong) UIView *protocalView;
@property (nonatomic, strong) UIButton *protocalImage;
@property (nonatomic, strong) UIButton *backLogin;
@property (nonatomic, assign) BOOL protocalSelect;

@end

@implementation RegisterVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGFloat offY = self.originY + 30;
    _phoneView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypePhone leftImage:nil rightImage:nil title:nil];
    _phoneView.limitCount = 11;
    [self.view addSubview:_phoneView];
    //48 是高度，28是间距
    offY += 48+28;

    _showPwd = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypeShowPwd leftImage:[UIImage imageNamed:@"pwdLock"] rightImage:[UIImage imageNamed:@"loginEye"] title:nil];
    _showPwd.delegate = self;
    [self.view addSubview:_showPwd];

    offY += 48+28;

    _ensurePwd = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypeEnsurePwd leftImage:[UIImage imageNamed:@"pwdCopy"] rightImage:nil title:nil];
    _ensurePwd.limitCount = 16;
    [self.view addSubview:_ensurePwd];

    offY +=48 +28;

    _codeView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypeVerifyCode leftImage:[UIImage imageNamed:@"pwdPhone"] rightImage:nil title:nil];
    _codeView.limitCount = 6;
    _codeView.delegate = self;
    [self.view addSubview:_codeView];

    offY +=48 +28;

    _buttonView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypeButton leftImage:nil rightImage:nil title:@"注册"];
    _buttonView.delegate = self;
    [self.view addSubview:_buttonView];

    _protocalView = [[UIView alloc]init];
    [self.view addSubview:_protocalView];
    [_protocalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(45);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-25);
        make.size.mas_equalTo(CGSizeMake(120, 40));
    }];

    UILabel *lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 16, 60, 14)];
    lblName.font = [UIFont systemFontOfSize:14.0f];
    lblName.textColor = [UIColor whiteColor];
    lblName.text = @"用户协议";
    [_protocalView addSubview:lblName];
    //protocalSelected 28*24 protocalUnSelected 24*24
    // Do any additional setup after loading the view.

    _protocalImage = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image = nil;
    if (_protocalSelect) {
        image = [UIImage imageNamed:@"protocalSelected"];
    }else {
        image = [UIImage imageNamed:@"protocalUnSelected"];
    }
    [_protocalImage setBackgroundImage:image forState:UIControlStateNormal];
    _protocalImage.frame = CGRectMake(60, (40-image.size.height)/2+4, image.size.width, image.size.height);
    [_protocalImage addTarget:self action:@selector(onProtocalClick) forControlEvents:UIControlEventTouchUpInside];
    [_protocalView addSubview:_protocalImage];

    UIView *back = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 60, 40)];
    [_protocalView addSubview:back];

    back.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onProtocalButtonClick)];
    [back addGestureRecognizer:tap];

    _backLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    _backLogin.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [_backLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _backLogin.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _backLogin.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    [_backLogin setTitle:@"返回登录" forState:UIControlStateNormal];
    [_backLogin addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_backLogin];
    [_backLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.buttonView.mas_right).offset(-40);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-25);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(40);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onProtocalClick {
    _protocalSelect = !_protocalSelect;
    UIImage *image = nil;
    if (_protocalSelect) {
        image = [UIImage imageNamed:@"protocalSelected"];
    }else {
        image = [UIImage imageNamed:@"protocalUnSelected"];
    }
    _protocalImage.frame = CGRectMake(60, (40-image.size.height)/2+4, image.size.width, image.size.height);
//    _protocalImage.image = image;
    [_protocalImage setBackgroundImage:image forState:UIControlStateNormal];
}

- (void)onProtocalButtonClick {
    ProtocalVC *protocal = [[ProtocalVC alloc]initWithShowBackButton:YES];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:protocal];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)backButtonClick {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
- (void)changeTextFiledSecturyStatus:(BOOL)status {
    self.ensurePwd.textFiled.secureTextEntry = status;
}

- (void)loginAcction {
//注册
    NSString *mobile = self.phoneView.textFiled.text;
    NSString *pwd = self.showPwd.textFiled.text;
    NSString *twoPwd = self.ensurePwd.textFiled.text;
    NSString *code = self.codeView.textFiled.text;
    BOOL phoneSuccess = [self checkMobile:mobile];
    if (!phoneSuccess) {
        return;
    }
    if (![StringUtil isNONil:pwd]) {
        [KVNProgress showErrorWithStatus:@"请输入密码"];
        return;
    }
    if (![StringUtil isNONil:twoPwd]) {
        [KVNProgress showErrorWithStatus:@"请再次输入密码"];
        return;
    }
    if (![pwd isEqualToString:twoPwd]) {
        [KVNProgress showErrorWithStatus:@"两次密码不一样"];
        return;
    }
    if (![StringUtil isNONil:code]) {
        [KVNProgress showErrorWithStatus:@"请输入验证码"];
        return;
    }
    if (!self.protocalSelect) {
        [KVNProgress showErrorWithStatus:@"请选择用户协议"];
        return;
    }
    NSMutableDictionary *parames = [NSMutableDictionary dictionaryWithCapacity:1];
    [parames setObject:mobile forKey:@"mobile"];
    [parames setObject:pwd forKey:@"password"];
    [parames setObject:twoPwd forKey:@"confirmPassword"];
    [parames setObject:code forKey:@"captcha"];
    [KVNProgress showWithStatus:@"正在注册"];
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    [self.netService signUp:parames success:^(NSString *requestInfo, id responseObject) {
        [KVNProgress showSuccessWithStatus:@"注册成功"];
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(registerSuccess)]) {
            [strongSelf.delegate registerSuccess];
        }
        [strongSelf.navigationController popViewControllerAnimated:YES];
    } failed:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:[responseObject objectForKey:@"message"]];
    }];
}

#pragma mark --todo 彭彬，获取注册码没有写
- (void)fetchCode {

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
