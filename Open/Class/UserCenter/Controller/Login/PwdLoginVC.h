//
//  PwdLoginVC.h
//  Open
//
//  Created by onceMu on 2017/7/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//  密码登录

#import "LoginVC.h"

@protocol PwdLoginVCDelegate <NSObject>

- (void)loginSuccess;

@end

@interface PwdLoginVC : LoginVC

@property (nonatomic, weak) id<PwdLoginVCDelegate>delegate;

@end
