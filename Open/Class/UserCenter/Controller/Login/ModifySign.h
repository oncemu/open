//
//  ModifySign.h
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseVC.h"
#import "User.h"

typedef void(^ModifyBlock)(BOOL isModifyNick,NSString *name);

@interface ModifySign : BaseVC

@property (nonatomic, assign) BOOL isModifyNick;
@property (nonatomic, copy) ModifyBlock callBackBlock;
@property (nonatomic, strong) User *user;
@end
