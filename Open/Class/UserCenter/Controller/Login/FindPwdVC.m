//
//  FindPwdVC.m
//  Open
//
//  Created by onceMu on 2017/7/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "FindPwdVC.h"
#import "CustomCircleView.h"

@interface FindPwdVC ()<CustomCircleViewDelegate>

@property (nonatomic, strong) CustomCircleView *phoneView;
@property (nonatomic, strong) CustomCircleView *showPwdView;
@property (nonatomic, strong) CustomCircleView *ensureView;
@property (nonatomic, strong) CustomCircleView *codeView;
@property (nonatomic, strong) CustomCircleView *buttonView;

@end

@implementation FindPwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    CGFloat offY = self.originY + 30;

    _phoneView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypePhone leftImage:nil rightImage:nil title:nil];
    [self.view addSubview:_phoneView];
    //48 是高度，28是间距
    offY += 48+28;

    _showPwdView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypeShowPwd leftImage:[UIImage imageNamed:@"pwdLock"] rightImage:[UIImage imageNamed:@"loginEye"] title:nil];
    [self.view addSubview:_showPwdView];

    offY += 48 +28;

    _ensureView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypeEnsurePwd leftImage:[UIImage imageNamed:@"pwdCopy"] rightImage:nil title:nil];
    [self.view addSubview:_ensureView];

    offY += 48 +28;

    _codeView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypeVerifyCode leftImage:[UIImage imageNamed:@"pwdPhone"] rightImage:nil title:nil];
    [self.view addSubview:_codeView];

    offY += 48 +28;

    _buttonView = [[CustomCircleView alloc]initWithFrame:CGRectMake(45, offY, SCREEN_WIDTH-90, 48) type:CircleTypeButton leftImage:nil rightImage:nil title:@"确定重置密码"];
    [self.view addSubview:_buttonView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeTextFiledSecturyStatus:(BOOL)status {
    self.ensureView.textFiled.secureTextEntry = status;
}

- (void)loginAcction {
    NSString *mobile = self.phoneView.textFiled.text;
    NSString *pwd = self.showPwdView.textFiled.text;
    NSString *tpwd = self.ensureView.textFiled.text;
    NSString *code = self.codeView.textFiled.text;
    BOOL phoneSuccess = [self checkMobile:mobile];
    if (!phoneSuccess) {
        return;
    }
    if (![StringUtil isNONil:pwd]) {
        [KVNProgress showErrorWithStatus:@"请输入新密码"];
        return;
    }
    if (![StringUtil isNONil:tpwd]) {
        [KVNProgress showErrorWithStatus:@"请再次输入新密码"];
        return;
    }
    if (![pwd isEqualToString:tpwd]) {
        [KVNProgress showErrorWithStatus:@"两次密码不一致"];
        return;
    }
    if (![StringUtil isNONil:code]) {
        [KVNProgress showErrorWithStatus:@"请输入验证码"];
        return;
    }
}

- (void)fetchCode {
    NSString *mobile = self.phoneView.textFiled.text;
    BOOL phoneSuccess = [self checkMobile:mobile];
    if (!phoneSuccess) {
        return;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
