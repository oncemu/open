//
//  ModifySign.m
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "ModifySign.h"
#import <IQTextView.h>

@interface ModifySign ()<UITextViewDelegate>

@property (nonatomic, assign) NSInteger limitNumber;
@property (nonatomic, strong) IQTextView *textView;
@property (nonatomic, strong) UILabel *lblCount;

@end

@implementation ModifySign

- (void)viewDidLoad {
    [super viewDidLoad];


    _textView = [[IQTextView alloc]initWithFrame:CGRectMake(0, 80, SCREEN_WIDTH, 150)];
    _textView.placeholder = @"请输入内容";
    _textView.delegate = self;
    self.automaticallyAdjustsScrollViewInsets = NO;
    _textView.font = [UIFont systemFontOfSize:15.0f];
    [self.view addSubview:_textView];
    if (_isModifyNick) {
        self.navigationItem.title = @"昵称";
        _limitNumber = 24;
        _textView.text = self.user.nickname;
    }else {
        self.navigationItem.title = @"个性签名";
        _limitNumber = 100;
        _textView.text = self.user.signature;
    }

    _lblCount = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-100, 83+50, 80, 12)];
    _lblCount.font = [UIFont systemFontOfSize:12.0f];
    _lblCount.textAlignment = NSTextAlignmentRight;
    _lblCount.textColor = [UIColor colorWithHex:0x646464];
    [_textView addSubview:_lblCount];
    _lblCount.text = [NSString stringWithFormat:@"%ld/0",_limitNumber];

    // Do any additional setup after loading the view.
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    NSString *comcatstr = [textView.text stringByReplacingCharactersInRange:range withString:text];
    NSInteger caninputlen = _limitNumber - comcatstr.length;
    if (caninputlen >= 0) {
        return YES;
    }
    else {
        NSInteger len = text.length + caninputlen;
        //防止当text.length + caninputlen < 0时，使得rg.length为一个非法最大正数出错
        NSRange rg = {0,MAX(len,0)};
        if (rg.length > 0) {
            NSString *s = [text substringWithRange:rg];
            [textView setText:[textView.text stringByReplacingCharactersInRange:range withString:s]];
        }
        return NO;
    }

}

- (void)textViewDidChange:(UITextView *)textView {
    NSString *nsTextContent = textView.text;
    NSInteger existTextNum = nsTextContent.length;
    if (existTextNum > _limitNumber) {
        //截取到最大位置的字符
        NSString *s = [nsTextContent substringToIndex:_limitNumber];
        [textView setText:s];
    }

    //不让显示负数
    self.lblCount.text = [NSString stringWithFormat:@"%ld/%ld",_limitNumber,MAX(0,existTextNum)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.callBackBlock) {
        self.callBackBlock(_isModifyNick, self.textView.text);
    }
}

- (void)goBack {
    if (self.callBackBlock) {
        self.callBackBlock(_isModifyNick, self.textView.text);
    }
    [super goBack];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
