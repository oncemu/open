//
//  RegisterVC.h
//  Open
//
//  Created by onceMu on 2017/7/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//  注册

#import "LoginVC.h"

@protocol RegisterVCDelegate <NSObject>

- (void)registerSuccess;

@end

@interface RegisterVC : LoginVC

@property (nonatomic, weak) id<RegisterVCDelegate>delegate;

@end
