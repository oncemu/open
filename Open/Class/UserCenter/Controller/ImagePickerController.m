//
//  QXImagePickerController.m
//  ShownailSeller
//
//  Created by mfp on 16/11/4.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import "ImagePickerController.h"
#import "UIBarButtonItem+QXBarbutton.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "PickImageViewModel.h"
#import "AlbumListView.h"
#import "PickImageCell.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "ViewFactory.h"
#import "SRAlertView.h"


#define TITLE_LEN   (SCREEN_WIDTH-210)
static  NSString    *cellIdentify = @"imageCell";


@interface ImagePickerController ()<AlbumListViewDelegate,CHTCollectionViewDelegateWaterfallLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate,SRAlertViewDelegate>

@property (nonatomic, strong) UIButton              *titleBtn;
@property (nonatomic, strong) UIButton              *rightBtn;
@property (nonatomic, strong) NSMutableArray        *albumList;
@property (nonatomic, assign) AlbumEntity         *albumEntity;
@property (nonatomic, strong) PickAlbumViewModel  *albumViewModel;
@property (nonatomic, strong) PickImageViewModel  *imageViewModel;
@property (nonatomic, strong) AlbumListView       *albumListView;
@property (nonatomic, strong) NSMutableArray        *selectImageList;

@end

@implementation ImagePickerController

- (void)createModel {
    self.albumList = [NSMutableArray array];
    self.selectImageList = [NSMutableArray array];
    self.albumViewModel = [[PickAlbumViewModel alloc] init];
    self.imageViewModel = [[PickImageViewModel alloc] init];
    self.imageViewModel.maxCount = MAX_COUNT - self.imageCount;
    WS(weakSelf);
    self.imageViewModel.exceedBoundsBlock = ^(){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"最多只能选%d张图",MAX_COUNT] message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [weakSelf presentViewController:alertController animated:YES completion:nil];
    };
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createModel];
    [self setNavigationBar];
    [self setUpCollectonView];
    [self getAlbums];
}

- (void)setNavigationBar {
    UIBarButtonItem *leftBar = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    [leftBar setTintColor: ColorWithHex(0xff4770)];
    [leftBar setTitleTextAttributes:@{Font(15):NSFontAttributeName} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftBar;

//    _rightBtn = [ViewFactory getButton:CM(0, 0, 60, 26) font:Font(15) selectTitle:nil norTitle:@"下一步" sColor:ColorWithHex(0xff2673) norColor:ColorWithHex(0xff2673) target:self action:@selector(next)];
//    UIBarButtonItem *_rightItem = [[UIBarButtonItem alloc] initWithCustomView:_rightBtn];
//    _rightBtn.enabled = NO;
//    self.navigationItem.rightBarButtonItem = _rightItem;
    [self setNavBarbuttonState];
    
//    _titleBtn = [ViewFactory getButton:CM(0, 0, TITLE_LEN, 20) font:Font(18) selectImage: norImage: target:self action:@selector(titleClick:)];
    _titleBtn = [ViewFactory getButton:CGRectMake(0, 0, TITLE_LEN, 20) selectImage:[UIImage imageNamed:@"pullUp"] norImage:[UIImage imageNamed:@"pullDown"] target:self action:@selector(titleClick:)];
    
    [_titleBtn setTitleColor:ColorWithHex(0x434348) forState:UIControlStateNormal];
    self.navigationItem.titleView = _titleBtn;
}

- (void)setUpCollectonView {
    self.view.backgroundColor = ColorWithHex(0xf8f8f8);
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc]init];
    layout.sectionInset = UIEdgeInsetsMake(4, 4, 4, 4);
    layout.columnCount = 4;
    layout.headerHeight = 0;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 4;
    layout.minimumInteritemSpacing = 4;

    self.collectionView.collectionViewLayout = layout;
    self.collectionView.backgroundColor = ColorWithHex(0xf8f8f8);
    self.collectionView.frameSizeHeight = SCREEN_HEIGHT;
    [self.collectionView registerClass:[PickImageCell class] forCellWithReuseIdentifier:cellIdentify];
//    self.collectionView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
}

- (void)titleClick:(UIButton *)sender {
    if (sender.selected == NO) {
        [self.view addSubview:self.albumListView];
    }else{
        [self.albumListView removeFromSuperview];
    }
    sender.selected = !sender.selected;
}

- (void)next {
    [self.selectImageList removeAllObjects];
    __block NSInteger count = 0;
    for (PHAsset *asset in self.imageViewModel.selectMediaDataArray) {
        @autoreleasepool {
            PHImageRequestOptions *opt = [[PHImageRequestOptions alloc]init];
            opt.synchronous = NO;
            opt.resizeMode = PHImageRequestOptionsResizeModeFast;
            opt.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
            
            PHImageManager *imageManager = [PHImageManager defaultManager];
            [imageManager requestImageForAsset:asset targetSize:CGSizeMake(MAXFLOAT, MAXFLOAT) contentMode:PHImageContentModeAspectFill options:opt resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                count ++;
                if (result) {
                    [self.selectImageList addObject:result];
                }
            }];
        }
    }
    while (count<self.imageViewModel.selectMediaDataArray.count) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    if (self.isAdd) {

    }else{

    }
}

- (void)goBack {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (void)getAlbums {
    WS(weakSelf);
    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized) {
        [self.albumList addObjectsFromArray:_albumViewModel.albumArray];
        self.albumEntity = [weakSelf.albumList firstObject];
        [self loadTitleAndList];
    }else if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                [weakSelf.albumList addObjectsFromArray:_albumViewModel.albumArray];
                weakSelf.albumEntity = [weakSelf.albumList firstObject];
                [weakSelf performSelectorOnMainThread:@selector(loadTitleAndList) withObject:nil waitUntilDone:YES];
            }else{
//                [weakSelf performSelectorOnMainThread:@selector(goBack) withObject:nil waitUntilDone:YES];
            }
        }];
    }else{
        [self showAccessDenied];
    }
}

#pragma mark - 提示允许访问相册功能
- (void)showAccessDenied {
    NSString *message = @"访问相册需要授权。请在系统-设置-相机中打开Open";
    SRAlertView *alert = [[SRAlertView alloc]initWithTitle:@"授权请求" message:message  leftActionTitle:@"取消" rightActionTitle:@"确定" animationStyle:AlertViewAnimationNone delegate:self];
    [alert show];
}

- (void)openCamera {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [KVNProgress showErrorWithStatus:@"当前设备没有摄像头"];
        return;
    }
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)setNavBarbuttonState {
//    BOOL buttonEnabled = (self.imageViewModel.selectMediaDataArray.count + self.imageCount)>0?YES:NO;
//    self.rightBtn.enabled = buttonEnabled;
//    if (buttonEnabled) {
//        [self.rightBtn setTitle:[NSString stringWithFormat:@"下一步 %ld",(long)self.imageViewModel.selectMediaDataArray.count+(long)self.imageCount] forState:UIControlStateNormal];
//    }else{
//        [self.rightBtn setTitle:@"下一步" forState:UIControlStateNormal];
//    }
}

- (void)loadTitleAndList {
    NSString *title = self.albumEntity.assetCollection.localizedTitle;
    [self.titleBtn setTitle:title forState:UIControlStateNormal];
    CGFloat len = [StringUtil getLabelLength:title font:[UIFont boldSystemFontOfSize:18] height:40]+15;
    if (len>TITLE_LEN) {
        len = TITLE_LEN;
    }
    self.titleBtn.imageEdgeInsets = UIEdgeInsetsMake(0, len-10, 0, -len+10);
    self.titleBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [self.collectionView reloadData];
}

#pragma mark - QXAlbumListViewDelegate

- (void)didSelectAlbumIndex:(NSInteger)index {
    self.titleBtn.selected = NO;
    self.albumEntity = [self.albumList objectAtIndex:index];
    [self loadTitleAndList];
    [self.albumListView removeFromSuperview];
}

- (void)hideListView {
    [self titleClick:self.titleBtn];
}

#pragma mark - UICollectionViewDelegate && UICollectionViewDataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    [collectionView.collectionViewLayout invalidateLayout];
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.albumEntity.fetchResult.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PickImageCell *cell = (PickImageCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentify forIndexPath:indexPath];
    cell.imageViewModel = self.imageViewModel;
    cell.isCamera = (indexPath.item == 0);
    if (indexPath.item>0) {
        PHAsset *asset = self.albumEntity.fetchResult[indexPath.item - 1];
        cell.asset = asset;
        WS(weakSelf);
        cell.clickSelectedButtonBlock = ^(PHAsset *asset){
            [weakSelf setNavBarbuttonState];
        };
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item == 0) { //拍照
        [self openCamera];
    }else {
        PickImageCell *cell = (PickImageCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cell.selectBtn.selected = !cell.selectBtn.selected;
    }
}


#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return Size((SCREEN_WIDTH-20)/4, (SCREEN_WIDTH-20)/4);
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
        UIImage *photo=nil;
        photo=[info objectForKey:UIImagePickerControllerEditedImage];
        [picker dismissViewControllerAnimated:YES completion:^{
          
        }];
}

- (AlbumListView *)albumListView {
    if (_albumListView == nil) {
        _albumListView = [[AlbumListView alloc] initWithFrame:CM(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64)];
        _albumListView.delegate = self;
    }
    _albumListView.albumList = self.albumList;
    return _albumListView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
