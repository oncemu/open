//
//  WaterFlowVC.m
//  Open
//
//  Created by onceMu on 2017/9/20.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "WaterFlowVC.h"
#import "CHTCollectionViewWaterfallLayout.h"

@interface WaterFlowVC ()<CHTCollectionViewDelegateWaterfallLayout>

@end

@implementation WaterFlowVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"选择水印";

    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc]init];
    layout.columnCount = 3;

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((SCREEN_WIDTH-70)/3, 106);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
