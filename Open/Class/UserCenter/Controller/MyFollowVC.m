//
//  MyFollowVC.m
//  Open
//
//  Created by onceMu on 2017/7/28.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MyFollowVC.h"
#import "UserFansCell.h"
#import "CHTCollectionViewWaterfallLayout.h"

@interface MyFollowVC ()<CHTCollectionViewDelegateWaterfallLayout>

@end

@implementation MyFollowVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"关注";
    self.view.backgroundColor = [UIColor colorWithHex:0xffffff];
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc]init];
    layout.columnCount = 1;
    layout.minimumColumnSpacing = 0;
    layout.minimumContentHeight = 0;
    layout.minimumInteritemSpacing = 0;

    self.collectionView.collectionViewLayout = layout;
    [self.collectionView registerClass:[UserFansCell class] forCellWithReuseIdentifier:kUserFansIdentifier];

    for (NSInteger i =0; i<10; i++) {
        [self.dataList addObject:@(i)];
    }
    [self changeRefreshStatus:YES data:self.dataList];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UserFansCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUserFansIdentifier forIndexPath:indexPath];
    [cell assignFans:nil hiddenLine:(indexPath.item == self.dataList.count-1)];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(SCREEN_WIDTH, 60);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
