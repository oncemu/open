//
//  MyPhotoVC.m
//  Open
//
//  Created by onceMu on 2017/7/28.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MyPhotoVC.h"
#import "UserPhotoCell1.h"
#import "UserPhotoCell2.h"
#import "GreedoCollectionViewLayout.h"
#import "UserNetService.h"
#import "PictureDetailVC.h"


static NSString * const kUserPhotoCell1Identifier = @"UserPhotoCell1Identifier";
static NSString * const kUserPhotoCell2Identifier = @"UserPhotoCell2Identifier";
static NSString * const kUserPhotoCell1ReIdentifier = @"UserPhotoCell1ReIdentifier";
static NSString * const kUserPhotoCell2ReIdentifier = @"UserPhotoCell2ReIdentifier";

@interface MyPhotoVC ()<GreedoCollectionViewLayoutDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) GreedoCollectionViewLayout *collectionViewSizeCalculator;
@property (nonatomic, strong) UserNetService *netService;
@property (nonatomic, assign) NSInteger salePageIndex;
@property (nonatomic, assign) NSInteger commonPageIndex;
@property (nonatomic, strong) NSMutableArray *saleList;

@end

@implementation MyPhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的美图";
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    layout.headerReferenceSize = CGSizeMake(SCREEN_WIDTH, 44);
//    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.collectionView.collectionViewLayout = layout;

    self.view.backgroundColor = [UIColor whiteColor];
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 570/2+44, 0);
    [self.collectionView registerClass:[UserPhotoCell1 class] forCellWithReuseIdentifier:kUserPhotoCell1Identifier];
    [self.collectionView registerClass:[UserPhotoCell2 class] forCellWithReuseIdentifier:kUserPhotoCell2Identifier];
    [self.collectionView registerClass:[UserPhotoCell1ReuseableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kUserPhotoCell2ReIdentifier];

    
    
    self.collectionViewSizeCalculator.rowMaximumHeight = CGRectGetHeight(self.collectionView.bounds) / 3;
    self.collectionViewSizeCalculator.fixedHeight = NO;

//    [self loadData];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)loadData {
    if (![self checkNetWork]) {
        [self showNoNetWorkStauts];
        return;
    }
    _salePageIndex = 1;
    _commonPageIndex = 1;
    NSMutableDictionary *parames = [NSMutableDictionary dictionaryWithCapacity:1];
    if ([self.gdm isLogin]) {
        [parames setObject:self.gdm.accessToken forKey:@"accessToken"];
    }
    [parames setObject:@(_salePageIndex) forKey:@"page"];
    [parames setObject:@(20) forKey:@"size"];
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    [self.netService getUserPhotoList:parames success:^(NSString *requestInfo, id responseObject) {
        [strongSelf.saleList removeAllObjects];
        [strongSelf.saleList addObjectsFromArray:responseObject];
        [strongSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } failed:^(NSError *error, id responseObject) {
         [KVNProgress showErrorWithStatus:[responseObject objectForKey:@"message"]];
    }];
    [self loadCommonPhoto];
}

- (void)loadCommonPhoto {
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    [self.netService getUserCommonPhotoList:[self getParames] success:^(NSString *requestInfo, id responseObject) {
        [strongSelf.dataList removeAllObjects];
        [strongSelf.dataList addObjectsFromArray:responseObject];
        [strongSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:1]];
        [strongSelf changeRefreshStatus:YES data:responseObject];
    } failed:^(NSError *error, id responseObject) {
        [strongSelf changeRefreshStatus:YES data:nil];
        [KVNProgress showErrorWithStatus:[responseObject objectForKey:@"message"]];
    }];
}

- (void)loadMoreData {
    if (![self checkNetWork]) {
        [self showNoNetWorkStauts];
        return;
    }
    _commonPageIndex ++;
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    [self.netService getUserCommonPhotoList:[self getParames] success:^(NSString *requestInfo, id responseObject) {
        [strongSelf.dataList addObjectsFromArray:responseObject];
        [strongSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:1]];
        [strongSelf changeRefreshStatus:NO data:responseObject];
    } failed:^(NSError *error, id responseObject) {
        [strongSelf changeRefreshStatus:NO data:nil];
        [KVNProgress showErrorWithStatus:[responseObject objectForKey:@"message"]];
    }];
}

- (NSMutableDictionary *)getParames {
    NSMutableDictionary *parames = [NSMutableDictionary dictionaryWithCapacity:1];
    [parames setObject:self.gdm.accessToken forKey:@"accessToken"];
    [parames setObject:@(_commonPageIndex) forKey:@"page"];
    [parames setObject:@(10) forKey:@"size"];
    return parames;
}

#pragma mark --
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return self.dataList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        UserPhotoCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUserPhotoCell1Identifier forIndexPath:indexPath];
        [cell assignData:self.saleList];
        return cell;
    }
    UserPhotoCell2 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUserPhotoCell2Identifier forIndexPath:indexPath];
    cell.item = self.dataList[indexPath.item];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        UserPhotoCell2 *cell = (UserPhotoCell2 *)[collectionView cellForItemAtIndexPath:indexPath];
        PictureDetailVC *detailVC = [[PictureDetailVC alloc]initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
        detailVC.object = cell.item;
        [self pushVC:detailVC animated:YES];
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UserPhotoCell1ReuseableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:kUserPhotoCell2ReIdentifier forIndexPath:indexPath];
        if (indexPath.section == 0) {
            header.lblName.text = @"上架美图";
        }else {
            header.lblName.text = @"我的美图";
        }
        return header;
    }
    return nil;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.01f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.01f;
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return CGSizeMake(SCREEN_WIDTH, 130);
    }
    return CGSizeMake((SCREEN_WIDTH-50)/2, (SCREEN_WIDTH-50)/2+30);
}

- (CGSize)greedoCollectionViewLayout:(GreedoCollectionViewLayout *)layout originalImageSizeAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return Size(SCREEN_WIDTH, 130);
    }else{
        return CGSizeMake((SCREEN_WIDTH-50)/2, (SCREEN_WIDTH-50)/2+30);
    }
    return CGSizeMake(0.1, 0.1);
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (section == 0) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return UIEdgeInsetsMake(0, 20, 0, 20);
}




- (GreedoCollectionViewLayout *)collectionViewSizeCalculator {
    if (!_collectionViewSizeCalculator) {
        _collectionViewSizeCalculator = [[GreedoCollectionViewLayout alloc] initWithCollectionView:self.collectionView];
        _collectionViewSizeCalculator.dataSource = self;
    }
    return _collectionViewSizeCalculator;
}

- (UserNetService *)netService {
    if (_netService == nil) {
        _netService = [[UserNetService alloc]init];
    }
    return _netService;
}

- (NSMutableArray *)saleList {
    if (_saleList == nil) {
        _saleList = [NSMutableArray arrayWithCapacity:1];
    }
    return _saleList;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
