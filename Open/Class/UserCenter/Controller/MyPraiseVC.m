//
//  MyPraiseVC.m
//  Open
//
//  Created by onceMu on 2017/7/28.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MyPraiseVC.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "UserPrasieCell.h"
#import "UserNetService.h"
#import "PictureDetailVC.h"

static NSString * const kUserPrasieCellIdentifier = @"UserPrasieCellIdentifier";

@interface MyPraiseVC ()<CHTCollectionViewDelegateWaterfallLayout>
@property (nonatomic, strong) CHTCollectionViewWaterfallLayout *layout;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) UserNetService *netService;

@end

@implementation MyPraiseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的赞";
    self.collectionView.collectionViewLayout = self.layout;
    [self.collectionView registerClass:[UserPrasieCell class] forCellWithReuseIdentifier:kUserPrasieCellIdentifier];
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 570/2+44, 0);
//    [self loadData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData {
    if (![self checkNetWork]) {
        [self showNoNetWorkStauts];
        return;
    }
    if (![self checkLogin:YES]) {
        return;
    }
    _pageIndex = 1;
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    [self.netService getUserPrasieList:[self getParames] success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
        [strongSelf.dataList removeAllObjects];
        [strongSelf.dataList addObjectsFromArray:responseObject];
        [strongSelf.collectionView reloadData];
        [strongSelf changeRefreshStatus:YES data:responseObject];
    } failed:^(NSError *error, id responseObject) {
        [strongSelf changeRefreshStatus:YES data:nil];
        [KVNProgress showErrorWithStatus:[responseObject objectForKey:@"message"]];
    }];
}

- (void)loadMoreData {
    if (![self checkNetWork]) {
        [self showNoNetWorkStauts];
        return;
    }
    _pageIndex ++;
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    [self.netService getUserPrasieList:[self getParames] success:^(NSString *requestInfo, id responseObject) {
        [strongSelf.dataList addObjectsFromArray:responseObject];
        [strongSelf changeRefreshStatus:NO data:responseObject];
        [strongSelf.collectionView reloadData];
    } failed:^(NSError *error, id responseObject) {
        [strongSelf changeRefreshStatus:NO data:nil];
        [KVNProgress showErrorWithStatus:[responseObject objectForKey:@"message"]];
    }];
}

- (NSMutableDictionary *)getParames {
    NSMutableDictionary *parames = [NSMutableDictionary dictionaryWithCapacity:1];
    [parames setObject:@(_pageIndex) forKey:@"page"];
    [parames setObject:@(10) forKey:@"size"];
    if ([StringUtil isNONil:self.gdm.accessToken]) {
        [parames setObject:self.gdm.accessToken forKey:@"accessToken"];
    }
    return parames;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UserPrasieCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUserPrasieCellIdentifier forIndexPath:indexPath];
    cell.item = self.dataList[indexPath.item];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(SCREEN_WIDTH, 174);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UserPrasieCell *cell = (UserPrasieCell *)[collectionView cellForItemAtIndexPath:indexPath];
    PictureDetailVC *detailVC = [[PictureDetailVC alloc]initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
    detailVC.object = cell.item;
    [self pushVC:detailVC animated:YES];
}


- (CHTCollectionViewWaterfallLayout *)layout {
    if (_layout == nil) {
        _layout = [[CHTCollectionViewWaterfallLayout alloc]init];
        _layout.columnCount = 1;
        _layout.minimumColumnSpacing = 0;
        _layout.minimumContentHeight = 0;
        _layout.minimumInteritemSpacing = 0;
    }
    return _layout;
}

- (UserNetService *)netService {
    if (_netService == nil) {
        _netService = [[UserNetService alloc]init];
    }
    return _netService;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
