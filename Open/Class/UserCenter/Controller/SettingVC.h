//
//  SettingVC.h
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseVC.h"
#import "User.h"

@interface SettingVC : BaseVC


@property (nonatomic, strong) User *user;

@end
