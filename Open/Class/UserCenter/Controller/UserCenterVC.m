//
//  UserCenterVC.m
//  Open
//
//  Created by onceMu on 2017/7/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "UserCenterVC.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "UserCenterHeader.h"
#import "UserCenterViewCell.h"
#import "UserNetService.h"
#import "User.h"
#import "SettingVC.h"
#import "CustomSheetView.h"
#import "MyWorkVC.h"
#import "MyPhotoVC.h"
#import "MyPraiseVC.h"
#import "MyFollowVC.h"
#import "MyFansVC.h"
#import "MyRankVC.h"
#import "MyEarningsVC.h"
#import "ImagePickerController.h"
#import "WRNavigationBar.h"
#import <SwipeView.h>
#import "UserCenterSelectHeader.h"
#import "PhotoAlbumVC.h"
#import "UserCartVC.h"


static NSString * const kUserCenterCellIdentifier = @"userCenterCellIdentifier";
static NSString * const kUserCenterCellReuseIdentifier = @"userCenterCellReuseIdentifier";

@interface UserCenterVC ()<SwipeViewDelegate,SwipeViewDataSource,UserCenterHeaderDelegate,PhotoAlubumVCDelegate>
@property (nonatomic, strong) SwipeView *swipeView;
@property (nonatomic, strong) UserNetService *userNetService;
@property (nonatomic, strong) User *user;
@property (nonatomic, strong) MyPhotoVC *photoVC;
@property (nonatomic, strong) MyPraiseVC *praiseVC;
@property (nonatomic, strong) MyWorkVC *workVC;
@property (nonatomic, strong) UserCenterHeader *centerHeader;
@property (nonatomic, strong) UserCenterSelectHeader *selectedHeader;


@end

@implementation UserCenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHex:0xffffff];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSucceed) name:kUserUpdateSuccessNotificationName object:nil];
    [self.view addSubview:self.centerHeader];
    [self createSwipeTableView];
//    [self showLoginVC];
    [self loginSucceed];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessLoadUserInfo) name:@"loginsuccess" object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
//    [self wr_setNavBarBackgroundAlpha:0];
    [self checkLogin:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    [self wr_setNavBarBackgroundAlpha:1];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)createSwipeTableView {
    _swipeView = [[SwipeView alloc]initWithFrame:CGRectMake(0, 570/2+34, SCREEN_WIDTH, SCREEN_HEIGHT-570/2-44)];
    _swipeView.backgroundColor = WhiteColor;
    _swipeView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _swipeView.delegate = self;
    _swipeView.dataSource = self;
    _swipeView.bounces = NO;
    [self.view addSubview:_swipeView];
}

//登录成功之后需要获取用户信息
- (void)loginSucceed {
    if (self.gdm.isLogin) {
        __unsafe_unretained typeof(self)weakSelf = self;
        __block __strong typeof(weakSelf)strongSelf = weakSelf;
        NSMutableDictionary *parames = [NSMutableDictionary dictionaryWithCapacity:1];
        [parames setObject:self.gdm.accessToken forKey:@"accessToken"];
        [self.userNetService getUserInfo:parames success:^(NSString *requestInfo, id    responseObject) {
            strongSelf.user = responseObject;
            strongSelf.gdm.user = responseObject;
            strongSelf.centerHeader.user = responseObject;
        } failed:^(NSError *error, id responseObject) {

        }];
    }
}

#pragma mark --SwipeTableViewDelegate && SwipeTableViewDataSource
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView {
    return 3;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    if (index == 0) {
        if (_workVC == nil) {
            _workVC = [[MyWorkVC alloc]initWithShowBackButton:NO showHeaderRefresh:YES showFooterRefresh:YES];
            _workVC.view.frame = CGRectMake(0, 0, swipeView.frameSizeWidth, swipeView.frameSizeHeight);
            _workVC.parentVC = self;
            //            self.picView = [[QXWaterFallView alloc] initWithFrame:self.swipeTableView.bounds type:QXSearchTypePost searchKey:nil];
            //            self.picView.parentVC = self;
        }
        return _workVC.view;
    }
    if (index == 1) {
        if (_praiseVC == nil) {
            _praiseVC = [[MyPraiseVC alloc]initWithShowBackButton:NO showHeaderRefresh:YES showFooterRefresh:YES];
            _praiseVC.view.frame = CGRectMake(0, 0, swipeView.frameSizeWidth, swipeView.frameSizeHeight);
            _praiseVC.parentVC = self;
        }
        return _praiseVC.view;
    }
    if (index == 2) {
        if (_photoVC == nil) {
            _photoVC = [[MyPhotoVC alloc]initWithShowBackButton:NO showHeaderRefresh:YES showFooterRefresh:YES];
            _photoVC.view.frame = CGRectMake(0, 0, swipeView.frameSizeWidth, swipeView.frameSizeHeight);
            _photoVC.parentVC = self;
        }
        return _photoVC.view;
    }
    return nil;
}

- (void)swipeViewDidEndDecelerating:(SwipeView *)swipeView {
    [_centerHeader didSelctedIndex:swipeView.currentPage];
}




//#pragma mark --
//- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//    return 1;
//}
//
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return 6;
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    return CGSizeMake(SCREEN_WIDTH/4, SCREEN_WIDTH/4);
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section {
//    return 570/2;
//}
//
//- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    UserCenterViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUserCenterCellIdentifier forIndexPath:indexPath];
//    [cell assignChannelIndex:indexPath.item];
//    return cell;
//}
//
//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
//    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
//        UserCenterHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:kUserCenterCellReuseIdentifier forIndexPath:indexPath];
//        if (header == nil) {
//            header = [[UserCenterHeader alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 570/2)];
//        }
//        header.delegate = self;
//        header.user = self.user;
//        return header;
//    }
//    return nil;
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath: (NSIndexPath *)indexPath {
//    if (![self.gdm isLogin]) {
//        [self showLoginVC];
//    }
//    if (indexPath.item == 0) {
//        [self workVc];
//    }
//    if (indexPath.item == 1) {
//        [self pariseVc];
//    }
//    if (indexPath.item == 2) {
//        [self followVc];
//    }
//    if (indexPath.item == 3) {
//        [self fansVc];
//    }
//    if (indexPath.item == 4) {
//        [self photoVc];
//    }
//    if (indexPath.item == 5) {
//        [self rankVc];
//    }
//}
//
//- (void)workVc {
//    MyWorkVC *work = [[MyWorkVC alloc]initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
//    [self pushVC:work animated:YES];
//}
//
//- (void)pariseVc {
//    MyPraiseVC *work = [[MyPraiseVC alloc]initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
//    [self pushVC:work animated:YES];
//}
//
//- (void)followVc {
//    MyFollowVC *work = [[MyFollowVC alloc]initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
//    [self pushVC:work animated:YES];
//}
//
//- (void)fansVc {
//    MyFansVC *work = [[MyFansVC alloc]initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
//    [self pushVC:work animated:YES];
//}
//
//- (void)photoVc {
//    MyPhotoVC *work = [[MyPhotoVC alloc]initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
//    [self pushVC:work animated:YES];
//}
//
//- (void)rankVc {
//    MyRankVC *work = [[MyRankVC alloc]initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:NO];
//    [self pushVC:work animated:YES];
//}

#pragma mark --headerDelegate
- (void)settingBtnClick {
    SettingVC *setting = [[SettingVC alloc]initWithShowBackButton:YES];
    setting.user = self.user;
    [self pushVC:setting animated:YES];
}

- (void)signBtnClick {
    if (!self.gdm.isLogin) {
        [self showLoginViewControllerWithCompletion];
        return;
    }
}

- (void)earningBtnClick {
    if (!self.gdm.isLogin) {
        [self showLoginViewControllerWithCompletion];
        return;
    }
    MyEarningsVC *earnVC = [[MyEarningsVC alloc]initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
    [self pushVC:earnVC animated:YES];
}

- (void)followBtnClick {
    if (!self.gdm.isLogin) {
        [self showLoginViewControllerWithCompletion];
        return;
    }
}

- (void)shopCartBtnClick {
    UserCartVC *cart = [[UserCartVC alloc]initWithShowBackButton:YES];
    [self pushVC:cart animated:YES];
}

- (void)clickHeaderWithIndex:(NSInteger)index {
    [self.swipeView scrollToPage:index duration:0];
}


- (void)changeBackgroundImage {
    if (!self.gdm.isLogin) {
        [self showLoginViewControllerWithCompletion];
        return;
    }
    CustomSheetView *view = [[CustomSheetView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) title:@"更换封面" buttons:@[@"手机相册",@"从作品选择",@"取消"]];
    view.block = ^(NSInteger index, NSString *titles) {
        DLog(@"%@",titles);
        if ([titles isEqualToString:@"手机相册"]) {
            [self openImagePicker];
        }
        if ([titles isEqualToString:@"从作品选择"]) {
            [self selectedPublishImage];
        }
    };
    [view show];
}

- (void)openImagePicker {
    PhotoAlbumVC *vc = [[PhotoAlbumVC alloc] initWithShowBackButton:YES];
    vc.type = PublishTypeNormal;
    vc.delegate = self;
//    ImagePickerController *picker = [[ImagePickerController alloc]initWithShowBackButton:YES];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)selectedPhotoImage:(UIImage *)image {
    if (![self checkNetWork]) {
        [self showNoNetWorkStauts];
        return;
    }
    NSMutableDictionary *parames = [NSMutableDictionary dictionaryWithCapacity:1];
    [parames setObject:self.gdm.accessToken forKey:@"accessToken"];
    [parames setObject:image forKey:@"background"];
    [KVNProgress showWithStatus:@"正在更新"];
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    [self.userNetService updateUserBackGround:parames success:^(NSString *requestInfo, id responseObject) {
        [KVNProgress dismiss];
        [KVNProgress showSuccess];
        [strongSelf loginSucceed];
    } failed:^(NSError *error, id responseObject) {
        [KVNProgress dismiss];
        [KVNProgress showErrorWithStatus:[responseObject objectForKey:@"message"]];
    }];
}

- (void)selectedPublishImage {

}


- (UserCenterHeader *)centerHeader {
    if (_centerHeader == nil) {
        _centerHeader = [[UserCenterHeader alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 570/2+34)];
        _centerHeader.delegate = self;
    }
    return _centerHeader;
}


- (UserNetService *)userNetService {
    if (_userNetService == nil) {
        _userNetService = [[UserNetService alloc]init];
    }
    return _userNetService;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
