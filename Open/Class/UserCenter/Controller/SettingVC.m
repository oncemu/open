//
//  SettingVC.m
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "SettingVC.h"
#import "SettingCell.h"
#import "CustomSheetView.h"
#import "ModifyPasswordVC.h"
#import "ProtocalVC.h"
#import "ModifySign.h"
#import "ActivityView.h"
#import "WXApi.h"
#import "SRAlertView.h"
#import "UserNetService.h"
#import "PhotoAlbumVC.h"
#import "WaterFlowVC.h"

static NSString * const kSettingCellIdentifier = @"SettingCellIdentifier";

@interface SettingVC ()<UITableViewDelegate,UITableViewDataSource,SettingCellDelegate,PhotoAlubumVCDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *rightBtn;
@property (nonatomic, strong) UserNetService *userNetService;
@property (nonatomic, strong) UIImage *headerImage;

@end

@implementation SettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"设置";

    [self.navigationController.navigationBar addSubview:self.rightBtn];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(12, 0, SCREEN_WIDTH-24, SCREEN_HEIGHT) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    [_tableView reloadData];

    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.rightBtn.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.rightBtn.hidden = YES;
}

- (void)modifyUserInfo {
    NSMutableDictionary *parames = [NSMutableDictionary dictionaryWithCapacity:1];
    NSString *gender = nil;
    if ([self.user.gender isEqualToString:@"男"]) {
        gender = @"MALE";
    }else if ([self.user.gender isEqualToString:@"女"]) {
        gender = @"FEMALE";
    }else {
        gender = @"UNKNOWN";
    }
    [parames setObject:gender forKey:@"gender"];
    if ([StringUtil isNONil:self.user.nickname]) {
        [parames setObject:self.user.nickname forKey:@"nickname"];
    }
    if ([StringUtil isNONil:self.user.signature]) {
        [parames setObject:self.user.signature forKey:@"signature"];
    }
    if (self.headerImage) {
        [parames setObject:self.headerImage forKey:@"avatar"];
    }
    [parames setObject:self.gdm.accessToken forKey:@"accessToken"];
    if (![self checkNetWork]) {
        [self showNoNetWorkStauts];
        return;
    }
    [KVNProgress showWithStatus:@"正在更新"];
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    [self.userNetService updateUserInfo:parames success:^(NSString *requestInfo, id responseObject) {
        [KVNProgress dismiss];
        [KVNProgress showSuccessWithStatus:@"资料更新成功"];
        [[NSNotificationCenter defaultCenter]postNotificationName:kUserUpdateSuccessNotificationName object:nil];
        [strongSelf goBack];
    } failed:^(NSError *error, id responseObject) {
        [KVNProgress dismiss];
        [KVNProgress showErrorWithStatus:[responseObject objectForKey:@"message"]];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:kSettingCellIdentifier];
    if (cell == nil) {
        cell = [[SettingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kSettingCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.delegate = self;
    [cell assignIndex:indexPath user:self.user];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 110 + 48 *2;
    }
    if (indexPath.section == 2) {
        return 48 *3;
    }
    return 48*2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10.0f;
}


#pragma mark --
- (void)modifyHeader {
    CustomSheetView *view = [[CustomSheetView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) title:@"修改头像" buttons:@[@"相册",@"取消"]];
    view.block = ^(NSInteger index, NSString *titles) {
        if (index == 1) {
            [self showAlbum];
        }
    };
    [view show];
}

- (void)showAlbum {
    PhotoAlbumVC *album = [[PhotoAlbumVC alloc]initWithShowBackButton:YES];
    album.type = PublishTypeNormal;
    album.delegate = self;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:album];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)selectedPhotoImage:(UIImage *)image {
    SettingCell *cell = (SettingCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell updateHeader:image];
    self.headerImage = image;
}

- (void)clickSection:(NSInteger)section index:(NSInteger)index {
    if (section == 0) {
        if (index == 0) {
            [self modifyHeader];
        }else if (index == 1) {
            [self modifyNick];
        }else if(index == 2) {
            [self modifyGender];
        }else {
            [self modifySign];
        }
    }
    if (section == 1) {
        if (index == 0) {
            [self modifyPwd];
        }else {
            [self clearMemory];
        }
    }
    if (section == 2) {
        if (index == 0) {
            [self protocal];
        }else if (index == 1) {
            [self aboutUs];
        }else {
            [self openTelephone];
        }
    }
    if (section == 3) {
        if (index == 0) {
            [self shareApp];
        }else {
            [self loginOut];
        }
    }
}

- (void)modifyNick {
    ModifySign *sign = [[ModifySign alloc]initWithShowBackButton:YES];
    sign.user = self.user;
    sign.isModifyNick = YES;
    sign.callBackBlock = ^(BOOL isModifyNick, NSString *name) {
        if (isModifyNick) {
            SettingCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            [cell updateIndex:1 string:name];
            self.user.nickname = name;
        }
    };
    [self pushVC:sign animated:YES];
}

- (void)modifySign {
    ModifySign *sign = [[ModifySign alloc]initWithShowBackButton:YES];
    sign.user = self.user;
    sign.isModifyNick = NO;
    sign.callBackBlock = ^(BOOL isModifyNick, NSString *name) {
        if (!isModifyNick) {
            SettingCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            [cell updateIndex:3 string:name];
            self.user.signature = name;
        }
    };
    [self pushVC:sign animated:YES];
}

- (void)modifyGender {
    CustomSheetView *view = [[CustomSheetView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) title:@"修改性别" buttons:@[@"男",@"女",@"取消"]];
    view.block = ^(NSInteger index, NSString *titles) {
        SettingCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [cell updateIndex:2 string:titles];
        self.user.gender = titles;
    };
    [view show];
}

- (void)modifyPwd {
    ModifyPasswordVC *pwd = [[ModifyPasswordVC alloc]initWithShowBackButton:YES];
    [self pushVC:pwd animated:YES];
}

- (void)clearMemory {
    [KVNProgress showWithStatus:@"正在清除缓存"];
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:cachePath];

    for (NSString *p in files) {
        NSError *error;
        NSString *path = [cachePath stringByAppendingString:[NSString stringWithFormat:@"/%@",p]];
        if([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        }
    }
    [KVNProgress showSuccessWithStatus:@"缓存清理成功"];
    [self.tableView reloadData];
}

- (void)protocal {
    WaterFlowVC *protocal = [[WaterFlowVC alloc]initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
    [self pushVC:protocal animated:YES];
}

- (void)aboutUs {

}

- (void)openTelephone {

}

- (void)shareApp {
    ActivityView * activityView = [[ActivityView alloc]initWithTitle:@"选择分享方式" referView:[[UIApplication sharedApplication] keyWindow]];
    activityView.bgColor = [UIColor whiteColor];
    activityView.numberOfButtonPerLine = 4;
    ButtonView *bv = nil;
    bv = [[ButtonView alloc]initWithText:@"朋友圈" image:[UIImage imageNamed:@"wxLine"] handler:^(ButtonView *buttonView){
        if (![WXApi isWXAppInstalled]) {
            return ;
        }
    }];
    [activityView addButtonView:bv];
    bv = [[ButtonView alloc]initWithText:@"微信" image:[UIImage imageNamed:@"wxfriend"] handler:^(ButtonView *buttonView){
        if (![WXApi isWXAppInstalled]) {
            return ;
        }
    }];
    [activityView addButtonView:bv];
    bv = [[ButtonView alloc]initWithText:@"微博" image:[UIImage imageNamed:@"weibo"] handler:^(ButtonView *buttonView){
        if (![WXApi isWXAppInstalled]) {
            return ;
        }
    }];
    [activityView addButtonView:bv];
    bv = [[ButtonView alloc]initWithText:@"QQ" image:[UIImage imageNamed:@"qq-oc"] handler:^(ButtonView *buttonView){
        if (![WXApi isWXAppInstalled]) {
            return ;
        }
    }];
    [activityView addButtonView:bv];
    bv = [[ButtonView alloc]initWithText:@"复制链接" image:[UIImage imageNamed:@"link"] handler:^(ButtonView *buttonView){
        if (![WXApi isWXAppInstalled]) {
            return ;
        }
    }];
    [activityView addButtonView:bv];
    bv = [[ButtonView alloc]initWithText:@"QQ空间" image:[UIImage imageNamed:@"qqZone"] handler:^(ButtonView *buttonView){
        if (![WXApi isWXAppInstalled]) {
            return ;
        }
    }];
    [activityView addButtonView:bv];
    [activityView show];
}

- (void)loginOut {
    SRAlertView *alertView = [[SRAlertView alloc] initWithTitle:@"退出登录"
                                                        message:@"您确定要退出登录吗?"
                                                leftActionTitle:@"确定"
                                               rightActionTitle:@"取消"
                                                 animationStyle:AlertViewAnimationZoom
                                                   selectAction:^(AlertViewActionType actionType) {
                                                       if (actionType == AlertViewActionTypeLeft) {
                                                           [self loginOutAction];
                                                       } else {

                                                       }
                                                   }];
    alertView.blurCurrentBackgroundView = NO;
    [alertView show];
}

- (void)loginOutAction {
    __unsafe_unretained typeof(self)weakSelf = self;
    __block __strong typeof(weakSelf)strongSelf = weakSelf;
    NSMutableDictionary *parames = [NSMutableDictionary dictionaryWithCapacity:1];
    [parames setObject:self.gdm.accessToken forKey:@"accessToken"];
    [self.userNetService userLoginOut:parames success:^(NSString *requestInfo, id responseObject) {
        [KVNProgress showSuccessWithStatus:@"退出成功"];
        [strongSelf.gdm loginOut];
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserLogOutSuccessNotificationName object:nil];
        [strongSelf goBack];
    } failed:^(NSError *error, id responseObject) {
        [KVNProgress showErrorWithStatus:[responseObject objectForKey:@"message"]];
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIButton *)rightBtn {
    if (_rightBtn == nil) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightBtn.frame = CGRectMake(self.view.frameSizeWidth-70,5 , 60, 34);
        [_rightBtn setTitle:@"修改" forState:UIControlStateNormal];
        [_rightBtn setBackgroundColor:[UIColor colorWithHex:0xffde00]];
        [_rightBtn setTitleColor:[UIColor colorWithHex:0x666666] forState:UIControlStateNormal];
        _rightBtn.layer.cornerRadius = 6.0f;
        _rightBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [_rightBtn addTarget:self action:@selector(modifyUserInfo) forControlEvents:UIControlEventTouchUpInside];

    }
    return _rightBtn;
}

- (UserNetService *)userNetService {
    if (_userNetService == nil) {
        _userNetService = [[UserNetService alloc]init];
    }
    return _userNetService;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
