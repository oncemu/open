//
//  QXImagePickerController.h
//  ShownailSeller
//
//  Created by mfp on 16/11/4.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import "CollectionBaseVC.h"

@interface ImagePickerController : CollectionBaseVC

@property (nonatomic, assign) BOOL      isAdd;
@property (nonatomic, assign) NSInteger imageCount;
@property (nonatomic, assign) BOOL      isTopical;//是否为话题 NO为正常发帖
@property (nonatomic, strong) NSMutableArray    *topicalFlags;//话题标签

@end
