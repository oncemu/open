//
//  MakeCategoryItem.h
//  Open
//
//  Created by mfp on 17/10/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MakeCategoryItem : NSObject

@property(nonatomic, assign) NSInteger categoryId;
@property(nonatomic, copy) NSString *categoryName;
@property(nonatomic, copy) NSString *makeDesc;
@property(nonatomic, copy) NSString *makeTitle;
@end
