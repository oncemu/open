//
//  PicOnShelfVC.m
//  Open
//
//  Created by mfp on 17/7/13.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PicOnShelfVC.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "PhotoPicView.h"

static NSString *const piconshelfcellId = @"piconshelfcellId";
@interface PicOnShelfVC ()<CHTCollectionViewDelegateWaterfallLayout>

@end

@implementation PicOnShelfVC

- (void)testCode {
    [self.dataList addObjectsFromArray:@[@"a",@"b",@"cd",@"d",@"ff",@"ss",@"aa"]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"上架作品";
    [self testCode];
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.minimumColumnSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(0, 17, 0, 17);
    self.collectionView.collectionViewLayout = layout;
    [self.collectionView registerClass:[PicOnShelfCell class] forCellWithReuseIdentifier:piconshelfcellId];
    self.collectionView.backgroundColor = WhiteColor;
}

#pragma mark loaddata
- (void)loadData {
    [self.header endRefreshing];
}

- (void)loadMoreData {
    
}

#pragma mark UICollectionViewDataSource,UICollectionViewDelegate>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PicOnShelfCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:piconshelfcellId forIndexPath:indexPath];
    cell.object = nil;
    return cell;
}

#pragma mark CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return Size((SCREEN_WIDTH - 34 - 5)/2.0f, (SCREEN_WIDTH - 34 - 5)/2.0f + 10 + 20 + 15);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
