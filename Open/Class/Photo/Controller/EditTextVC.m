//
//  EditTextVC.m
//  Open
//
//  Created by mfp on 17/10/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "EditTextVC.h"
#import "AlbumMakeView.h"

@interface EditTextVC ()<UITextViewDelegate>

@property(nonatomic, strong) EditToolBar *bottomBar;
@property(nonatomic, strong) ColorListView *colorListView;
@property(nonatomic, strong) UITextView *inputView;
@property(nonatomic, assign) CGFloat fontSize;
@property(nonatomic, assign) BOOL bold;

@end

@implementation EditTextVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"编辑文字";
    self.view.backgroundColor = ColorWithHex(0xefefef);
    self.fontSize = 15;
    [self setBarButtonItemWithImage:nil title:@"完成" isLeft:NO];
    [self.view addSubview:self.inputView];
    [self.view addSubview:self.bottomBar];
    [self reloadTextView];
    [self.inputView becomeFirstResponder];
}

- (void)rightButtonItemAction {
    if (self.inputView.text.length == 0) {
        return;
    }
    self.object.text = self.inputView.text;
    CGSize constraintSize = CGSizeMake(SCREEN_WIDTH, MAXFLOAT);
    CGSize size = [self.inputView sizeThatFits:constraintSize];
    self.object.frameValue = [NSValue valueWithCGRect:CM(30, 30, size.width + 20, size.height + 20)];
    [self.inputView resignFirstResponder];
    if (self.editText) {
        self.editText(self.object);
        [self goBack];
    }
}

- (void)reloadTextView {
    self.inputView.textColor = self.object.textColor;
    self.inputView.font = self.object.font;
}

- (UITextView *)inputView {
    if (_inputView == nil) {
        _inputView = [[UITextView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _inputView.backgroundColor = ColorWithHex(0xefefef);
        _inputView.text = self.object.text;
        _inputView.textColor = self.object.textColor;
        _inputView.font = self.object.font;
        _inputView.delegate = self;
    }
    return _inputView;
}

- (EditToolBar *)bottomBar {
    if (_bottomBar == nil) {
        _bottomBar = [[EditToolBar alloc] initWithFrame:CM(0, SCREEN_HEIGHT - 44, 74*4, 44) imageName:@"make_texttool"];
        WS(weakSelf);
        _bottomBar.actionBlock = ^(NSInteger index) {
            switch (index) {
                case 0:
                {
                    if (weakSelf.fontSize > 5) {
                        weakSelf.fontSize -= 2;
                    }
                    weakSelf.object.font = Font(weakSelf.fontSize);
                }
                    break;
                case 1:
                {
                    if (weakSelf.fontSize < 60) {
                        weakSelf.fontSize += 2;
                    }
                    weakSelf.object.font = Font(weakSelf.fontSize);
                }
                    break;
                case 2:
                    [weakSelf.colorListView show];
                    break;
                case 3:
                {
                    weakSelf.bold = !weakSelf.bold;
                    weakSelf.object.font = weakSelf.bold?BoldFont(weakSelf.fontSize):Font(weakSelf.fontSize);
                }
                    break;
            }
            [weakSelf reloadTextView];
        };
    }
    return _bottomBar;
}

- (ColorListView *)colorListView {
    if (!_colorListView) {
        _colorListView = [[ColorListView alloc] initWithFrame:CM(self.view.frameSizeWidth, 0, 140, self.view.frameSizeHeight)];
        WS(weakSelf);
        _colorListView.colorSelect = ^(UIColor *color) {
            weakSelf.object.textColor = color;
            [weakSelf reloadTextView];
        };
    }
    return _colorListView;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    if ([textView.text length] > 60) {
        textView.text = [textView.text substringWithRange:NSMakeRange(0, 60)];
        [textView.undoManager removeAllActions];
        [textView becomeFirstResponder];
        return;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
