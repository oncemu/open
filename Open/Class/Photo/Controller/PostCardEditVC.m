//
//  PostCardEditVC.m
//  Open
//
//  Created by mfp on 17/10/12.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PostCardEditVC.h"
#import "ConfirmOrderVC.h"
#import "UIImage+Resize.h"

@interface PostCardEditVC ()
{
    UIView * _imageViewScale;
    CGPoint contentOffset;
}
@property(nonatomic, strong) UIView *overView;//覆盖层
@property(nonatomic, assign) CGRect circularFrame;//裁剪框的frame
@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, assign) CGRect OriginalFrame;
@property(nonatomic, assign) CGRect currentFrame;
@property(nonatomic, assign) CGFloat scaleRation;//图片缩放的最大倍数
@property(nonatomic, strong) UILabel *tipLabel;
@property(nonatomic, strong) UIScrollView *modeScrollView;
@property(nonatomic, strong) UIImageView *modeView;
@property(nonatomic, strong) UIImage *image;

@end

@implementation PostCardEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    self.navigationItem.title = @"明信片预览，编辑";
    [self setBarButtonItemWithImage:nil title:@"完成" isLeft:NO];
    self.scaleRation =  10;
    BaseAlbumObject *imgObj = [self.object.contentList firstObject];
    self.image = imgObj.image;
    [self createCropZone];
    [self addAllGesture];
    [self.view addSubview:self.tipLabel];
    [self.view addSubview:self.modeView];
    [self.view addSubview:self.modeScrollView];
}

- (void)createCropZone {
    CGFloat height = SCREEN_HEIGHT - 75 - 82;
    CGFloat width = (height * 676)/1017;
    CGPoint center = self.view.center;

    _imageView = [[UIImageView alloc]init];
    [_imageView setImage:self.image];
    _imageView.backgroundColor = WhiteColor;
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    [_imageView setFrame:CGRectMake(center.x - width/2, 75, width, height)];
    self.OriginalFrame = _imageView.frame;
    [self.view addSubview:_imageView];
    
    _imageViewScale = _imageView;
    if (_image.size.width/_image.size.height > width/height) {
        CGFloat scal = width / _image.size.width;
        CGFloat scalH = _image.size.height * scal;
        contentOffset = Point(0, (height - scalH)/2.0f);
        _image = [UIImage resetSize:_image rect:CM(0, contentOffset.y, width, scalH) size:Size(width, height)];
    }else{
        CGFloat scal = height / _image.size.height;
        CGFloat scalW = _image.size.width * scal;
        contentOffset = Point((width - scalW)/2.0f, 0);
        _image = [UIImage resetSize:_image rect:CM(contentOffset.x, 0, scalW, height) size:Size(width, height)];
    }
    
//    _overView = [[UIView alloc]init];
//    [_overView setBackgroundColor:[UIColor clearColor]];
//    _overView.opaque = NO;
//    [_overView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height )];
//    [self.view addSubview:_overView];
    
    self.circularFrame = CGRectMake(center.x - width/2, NavigationBar_HEIGHT + StatusBar_HEIGHT + 30, width, height);
//    UIBezierPath * path= [UIBezierPath bezierPathWithRect:CGRectMake(0, NavigationBar_HEIGHT + StatusBar_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - NavigationBar_HEIGHT - StatusBar_HEIGHT)];
//    CAShapeLayer *layer = [CAShapeLayer layer];
//    [path appendPath:[UIBezierPath bezierPathWithRoundedRect:CGRectMake(center.x - width/2, NavigationBar_HEIGHT + StatusBar_HEIGHT + 30, width, height) cornerRadius:10]];
//    [path setUsesEvenOddFillRule:YES];
//    layer.path = path.CGPath;
//    layer.fillRule = kCAFillRuleEvenOdd;
//    layer.fillColor = [[UIColor whiteColor] CGColor];
//    layer.opacity = 1;
//    [_overView.layer addSublayer:layer];
}

//让图片自己适应裁剪框的大小
-(void)MakeImageViewFrameAdaptClipFrame
{
    CGFloat width = _imageView.frame.size.width ;
    CGFloat height = _imageView.frame.size.height;
    if(height < self.circularFrame.size.height)
    {
        width = (width / height) * self.circularFrame.size.height;
        height = self.circularFrame.size.height;
        CGRect frame = CGRectMake(0, 0, width, height);
        [_imageView setFrame:frame];
        [_imageView setCenter:self.view.center];
    }
}

-(void)addAllGesture
{
    //捏合手势
    UIPinchGestureRecognizer * pinGesture = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(handlePinGesture:)];
    [self.view addGestureRecognizer:pinGesture];
    //拖动手势
    UIPanGestureRecognizer * panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePanGesture:)];
    [self.view addGestureRecognizer:panGesture];
}

-(void)handlePinGesture:(UIPinchGestureRecognizer *)pinGesture
{
    UIView * view = _imageView;
    if(pinGesture.state == UIGestureRecognizerStateBegan || pinGesture.state == UIGestureRecognizerStateChanged) {
        view.transform = CGAffineTransformScale(_imageViewScale.transform, pinGesture.scale,pinGesture.scale);
        pinGesture.scale = 1.0;
    }
    else if(pinGesture.state == UIGestureRecognizerStateEnded) {
        CGFloat ration =  view.frame.size.width /self.OriginalFrame.size.width;
        
        if(ration>_scaleRation) {// 缩放倍数 > 自定义的最大倍数
            CGRect newFrame =CGRectMake(0, 0, self.OriginalFrame.size.width * _scaleRation, self.OriginalFrame.size.height * _scaleRation);
            view.frame = newFrame;
        }else if (view.frame.size.width < self.circularFrame.size.width && self.OriginalFrame.size.width <= self.OriginalFrame.size.height)
        {
            view.frame = [self handelWidthLessHeight:view];
            view.frame = [self handleScale:view];
        }
        else if(view.frame.size.height< self.circularFrame.size.height && self.OriginalFrame.size.height <= self.OriginalFrame.size.width)
        {
            view.frame =[self handleHeightLessWidth:view];
            view.frame = [self handleScale:view];
        }
        else
        {
            view.frame = [self handleScale:view];
        }
        self.currentFrame = view.frame;
    }
}

-(void)handlePanGesture:(UIPanGestureRecognizer *)panGesture
{
    UIView * view = _imageView;
    
    if(panGesture.state == UIGestureRecognizerStateBegan || panGesture.state == UIGestureRecognizerStateChanged)
    {
        CGPoint translation = [panGesture translationInView:view.superview];
        [view setCenter:CGPointMake(view.center.x + translation.x, view.center.y + translation.y)];
        
        [panGesture setTranslation:CGPointZero inView:view.superview];
    }
    else if ( panGesture.state == UIGestureRecognizerStateEnded)
    {
        CGRect currentFrame = view.frame;
        //向右滑动 并且超出裁剪范围后
        if(currentFrame.origin.x >= self.circularFrame.origin.x)
        {
            currentFrame.origin.x =self.circularFrame.origin.x;
            
        }
        //向下滑动 并且超出裁剪范围后
        if(currentFrame.origin.y >= self.circularFrame.origin.y)
        {
            currentFrame.origin.y = self.circularFrame.origin.y;
        }
        //向左滑动 并且超出裁剪范围后
        if(currentFrame.size.width + currentFrame.origin.x < self.circularFrame.origin.x + self.circularFrame.size.width)
        {
            CGFloat movedLeftX =fabs(currentFrame.size.width + currentFrame.origin.x -(self.circularFrame.origin.x + self.circularFrame.size.width));
            currentFrame.origin.x += movedLeftX;
        }
        //向上滑动 并且超出裁剪范围后
        if(currentFrame.size.height+currentFrame.origin.y < self.circularFrame.origin.y + self.circularFrame.size.height)
        {
            CGFloat moveUpY =fabs(currentFrame.size.height + currentFrame.origin.y -(self.circularFrame.origin.y + self.circularFrame.size.height));
            currentFrame.origin.y += moveUpY;
        }
        [UIView animateWithDuration:0.05 animations:^{
            [view setFrame:currentFrame];
        }];
    }
}

//缩放结束后 确保图片在裁剪框内
-(CGRect )handleScale:(UIView *)view
{
    // 图片.right < 裁剪框.right
    if(view.frame.origin.x + view.frame.size.width< self.circularFrame.origin.x+self.circularFrame.size.width)
    {
        CGFloat right =view.frame.origin.x + view.frame.size.width;
        CGRect viewFrame = view.frame;
        CGFloat space = self.circularFrame.origin.x+self.circularFrame.size.width - right;
        viewFrame.origin.x+=space;
        view.frame = viewFrame;
    }
    // 图片.top < 裁剪框.top
    if(view.frame.origin.y > self.circularFrame.origin.y)
    {
        CGRect viewFrame = view.frame;
        viewFrame.origin.y=self.circularFrame.origin.y;
        view.frame = viewFrame;
    }
    // 图片.left < 裁剪框.left
    if(view.frame.origin.x > self.circularFrame.origin.x)
    {
        CGRect viewFrame = view.frame;
        viewFrame.origin.x=self.circularFrame.origin.x;
        view.frame = viewFrame;
    }
    // 图片.bottom < 裁剪框.bottom
    if((view.frame.size.height +view.frame.origin.y) < (self.circularFrame.origin.y + self.circularFrame.size.height))
    {
        CGRect viewFrame = view.frame;
        CGFloat space = self.circularFrame.origin.y + self.circularFrame.size.height - (view.frame.size.height +view.frame.origin.y);
        viewFrame.origin.y +=space;
        view.frame = viewFrame;
    }
    
    return view.frame;
}

// 图片的高<宽 并且缩放后的图片高小于裁剪框的高
-(CGRect )handleHeightLessWidth:(UIView *)view
{
    CGRect tempFrame = view.frame;
    CGFloat rat = self.OriginalFrame.size.width / self.OriginalFrame.size.height;
    CGFloat width = self.circularFrame.size.width * rat;
    CGFloat height = self.circularFrame.size.height ;
    CGFloat  x  = view.frame.origin.x ;
    CGFloat y = self.circularFrame.origin.y;
    
    if(view.frame.origin.x > self.circularFrame.origin.x)
    {
        x = self.circularFrame.origin.x;
    }
    else if ((view.frame.origin.x+view.frame.size.width) < self.circularFrame.origin.x + self.circularFrame.size.width)
    {
        x = self.circularFrame.origin.x + self.circularFrame.size.width - width ;
    }
    
    CGRect newFrame =CGRectMake(x, y, width,height);
    view.frame = newFrame;
    
    if((tempFrame.origin.x > self.circularFrame.origin.x &&(tempFrame.origin.x+tempFrame.size.width) < self.circularFrame.origin.x + self.circularFrame.size.width))
    {
        [view setCenter:self.view.center];
    }
    
    if((tempFrame.origin.y > self.circularFrame.origin.y &&(tempFrame.origin.y+tempFrame.size.height) < self.circularFrame.origin.y + self.circularFrame.size.height))
    {
        [view setCenter:CGPointMake(tempFrame.size.width/2 + tempFrame.origin.x, view.frame.size.height /2)];
    }
    return  view.frame;
}

//图片的宽<高 并且缩放后的图片宽小于裁剪框的宽
-(CGRect)handelWidthLessHeight:(UIView *)view
{
    CGFloat rat = self.OriginalFrame.size.height / self.OriginalFrame.size.width;
    CGRect tempFrame = view.frame;
    
    CGFloat width = self.circularFrame.size.width;
    CGFloat height = self.circularFrame.size.height * rat ;
    
    CGFloat  x  = self.circularFrame.origin.x ;
    CGFloat y = view.frame.origin.y;
    
    if(view.frame.origin.y > self.circularFrame.origin.y)
    {
        y = self.circularFrame.origin.y;
    }
    else if ((view.frame.origin.y+view.frame.size.height) < self.circularFrame.origin.y + self.circularFrame.size.height)
    {
        y = self.circularFrame.origin.y + self.circularFrame.size.height - height ;
    }
    CGRect newFrame =CGRectMake(x, y, width,height);
    view.frame = newFrame;
    
    if((tempFrame.origin.y > self.circularFrame.origin.y &&(tempFrame.origin.y+tempFrame.size.height) < self.circularFrame.origin.y + self.circularFrame.size.height))
    {
        [view setCenter:self.view.center];
        
    }
    if((tempFrame.origin.x > self.circularFrame.origin.x &&(tempFrame.origin.x+tempFrame.size.width) < self.circularFrame.origin.x + self.circularFrame.size.width))
    {
        [view setCenter:CGPointMake(view.frame.size.width/2, tempFrame.size.height /2 + tempFrame.origin.y)];
    }
    return  view.frame;
}

//修复图片显示方向问题
-(UIImage *)fixOrientation:(UIImage *)image
{
    if (image.imageOrientation == UIImageOrientationUp)
        return image;
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

//方形裁剪
-(UIImage *)getSmallImage
{
    CGFloat width= _imageView.frame.size.width;
    CGFloat rationScale = (width /_image.size.width);
    
    CGFloat origX = (self.circularFrame.origin.x - _imageView.frame.origin.x) / rationScale;
    CGFloat origY = (self.circularFrame.origin.y - _imageView.frame.origin.y) / rationScale;
    CGFloat oriWidth = self.circularFrame.size.width / rationScale;
    CGFloat oriHeight = self.circularFrame.size.height / rationScale;
    
    CGRect myRect = CGRectMake(origX, origY, oriWidth, oriHeight);
    CGImageRef  imageRef = CGImageCreateWithImageInRect(_image.CGImage, myRect);
    UIGraphicsBeginImageContext(myRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, myRect, imageRef);
    UIImage * clipImage = [UIImage imageWithCGImage:imageRef];
    UIGraphicsEndImageContext();
    return clipImage;
}

- (UIImage *)getEditImage {
    CGFloat scale = [UIScreen mainScreen].scale;
    UIGraphicsBeginImageContextWithOptions(self.view.frameSize, YES, scale);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGImageRef sourceImageRef = [image CGImage];
    CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, CM(self.OriginalFrame.origin.x*scale, self.OriginalFrame.origin.y*scale, self.OriginalFrame.size.width*scale, self.OriginalFrame.size.height*scale));
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef scale:scale orientation:UIImageOrientationUp];
    return newImage;
}

- (void)rightButtonItemAction {
    self.modeScrollView.hidden = YES;
    UIImage *image = [self getEditImage];
    self.object.image = image;
    if (self.completeEditBlock) {
        self.completeEditBlock(self.object);
    }
    [self goBack];
}

- (void)selectMode:(UIButton *)sender {
    NSString *imageName = [NSString stringWithFormat:@"postcart_back%ld",sender.tag];
    self.modeView.image = getImage(imageName);
}

- (UILabel *)tipLabel {
    if (!_tipLabel) {
        _tipLabel = [ViewFactory getLabel:CM(0, SCREEN_HEIGHT - 30, SCREEN_WIDTH, 20) font:Font(13) textColor:ColorWithHex(0x666666) text:@"拖动，缩放控制范围"];
        _tipLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _tipLabel;
}

- (UIScrollView *)modeScrollView {
    CGFloat space = (SCREEN_WIDTH/4 - 53)/2.0f;
    if (_modeScrollView == nil) {
        _modeScrollView = [[UIScrollView alloc] initWithFrame:CM(0, self.view.frameSizeHeight - 82, SCREEN_WIDTH, 82)];
        _modeScrollView.backgroundColor = WhiteColor;
        for (NSInteger i = 0; i<4; i++) {
            NSString *imageName = [NSString stringWithFormat:@"postcart_mode%ld",i];
            UIButton *btn = [ViewFactory getButton:CM(space + i * (53 + space * 2), 14.5f, 37, 53) selectImage:getImage(imageName) norImage:getImage(imageName) target:self action:@selector(selectMode:)];
            btn.tag = i;
            btn.showsTouchWhenHighlighted = NO;
            [_modeScrollView addSubview:btn];
        }
    }
    return _modeScrollView;
}

- (UIImageView *)modeView {
    if (_modeView == nil) {
        _modeView = [ViewFactory getImageView:self.view.bounds image:getImage(@"postcart_back0")];
    }
    return _modeView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
