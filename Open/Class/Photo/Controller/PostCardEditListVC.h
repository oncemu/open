//
//  PostCardEditListVC.h
//  Open
//
//  Created by mfp on 17/10/14.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CollectionBaseVC.h"
#import "MallGoodsModel.h"
#import "AlbumMakeObject.h"
#import "MallCategoryModel.h"

@interface PostCardEditListVC : CollectionBaseVC

@property(nonatomic, assign) AlbumFormatType type;//图片种类
@property(nonatomic, strong) MallCategoryModel *categoryModel;
@property(nonatomic, strong) MallGoodsModel *model;

@end
