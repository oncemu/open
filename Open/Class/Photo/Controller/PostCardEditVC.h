//
//  PostCardEditVC.h
//  Open
//
//  Created by mfp on 17/10/12.
//  Copyright © 2017年 彭彬. All rights reserved.
//  明信片编辑预览

#import "BaseVC.h"
#import "AlbumMakeObject.h"

@interface PostCardEditVC : BaseVC
@property(nonatomic, strong) AlbumMakeObject *object;
@property(nonatomic, copy) void (^completeEditBlock)(AlbumMakeObject *obj);

@end
