//
//  AlbumMakeVC.h
//  Open
//
//  Created by mfp on 17/10/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseLandscapeVC.h"
#import "FormView.h"

@interface AlbumMakeVC : BaseLandscapeVC

@property(nonatomic, weak) AlbumMakeObject *object;

@end
