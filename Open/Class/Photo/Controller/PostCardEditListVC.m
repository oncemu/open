//
//  PostCardEditListVC.m
//  Open
//
//  Created by mfp on 17/10/14.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PostCardEditListVC.h"
#import "PostCardEditListView.h"
#import "PostCardEditVC.h"
#import "PhotoAlbumVC.h"
#import "ConfirmOrderVC.h"

@interface PostCardEditListVC ()
@property(nonatomic, strong) PostCartBottomBar *bottomBar;
@end

@implementation PostCardEditListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"商品预览，编辑";

    [self.collectionView registerClass:[PostCartCell class] forCellWithReuseIdentifier:NSStringFromClass([PostCartCell class])];
    [self.collectionView registerClass:[PostCartHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([PostCartHeader class])];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 24;
    layout.minimumLineSpacing = 24;
    layout.headerReferenceSize = Size(SCREEN_WIDTH, 60);
    layout.sectionInset = UIEdgeInsetsMake(0, 20, 10, 20);
    self.collectionView.collectionViewLayout = layout;
    self.collectionView.frame = CM(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kTabBarHeight);
    self.bottomBar = [[PostCartBottomBar alloc] initWithFrame:CM(0, SCREEN_HEIGHT - kTabBarHeight, SCREEN_WIDTH, kTabBarHeight)];
    WS(weakSelf);
    self.bottomBar.addBlock = ^() {
        PhotoAlbumVC *vc = [[PhotoAlbumVC alloc] initWithShowBackButton:YES];
        vc.type = PublishTypePicture;
        vc.addPhotoBlock = ^(NSArray *images) {
            if (images.count > 0) {
                [weakSelf.dataList addObjectsFromArray:images];
            }
            [weakSelf.collectionView reloadData];
            weakSelf.bottomBar.count = weakSelf.dataList.count;
        };
        CustomNavigationController *nav = [[CustomNavigationController alloc]initWithRootViewController:vc];
        [weakSelf presentViewController:nav animated:YES completion:nil];
    };
    self.bottomBar.nextBlock = ^() {
        if (weakSelf.dataList.count == 0) {
            [KVNProgress showErrorWithStatus:@"请选择图片!"];
            return ;
        }
        [weakSelf pushToConfirmVC];
    };
    [self.view addSubview:self.bottomBar];
    self.bottomBar.count = weakSelf.dataList.count;
}

- (void)deleteImage:(NSIndexPath *)indexPath {
    [self.dataList removeObjectAtIndex:indexPath.item];
    [self.collectionView performBatchUpdates:^{
        [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        if (finished) {
            [self.collectionView reloadData];
            self.bottomBar.count = self.dataList.count;
        }
    }];
}

//进入订单
- (void)pushToConfirmVC {
    if (self.dataList.count == 0) {
        [KVNProgress showErrorWithStatus:@"还没有制作图片！" onView:self.view];
        return;
    }
    NSMutableArray *photos = [NSMutableArray array];
    for (AlbumMakeObject *object in self.dataList) {
        if (object.image) {
            [photos addObject:object.image];
        }
    }
    self.model.itemPhotos = photos;
    self.model.itemName = self.categoryModel.categoryName;
    self.model.price = self.type/10.0f;
    self.model.expressFee = 12;
    self.model.num = 1;
    ConfirmOrderVC *vc = [[ConfirmOrderVC alloc] initWithShowBackButton:YES];
    vc.goodsList = @[self.model];
    vc.type = AlbumConfirmTypePicture;
    CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark UICollectionViewDataSource UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PostCartCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PostCartCell class]) forIndexPath:indexPath];
    AlbumMakeObject *obj = self.dataList[indexPath.row];
    cell.cardView.image = obj.image;
    WS(weakSelf);
    cell.deleteBlock = ^() {
        [weakSelf deleteImage:indexPath];
    };
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    PostCartHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([PostCartHeader class]) forIndexPath:indexPath];
    return header;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    PostCardEditVC *vc = [[PostCardEditVC alloc] initWithShowBackButton:YES];
    vc.object = self.dataList[indexPath.row];;
    WS(weakSelf);
    vc.completeEditBlock = ^(AlbumMakeObject *obj) {
        [weakSelf.dataList replaceObjectAtIndex:indexPath.item withObject:obj];
        [weakSelf.collectionView reloadItemsAtIndexPaths:@[indexPath]];
    };
    [self pushVC:vc animated:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = SCREEN_HEIGHT - NavigationBar_HEIGHT - StatusBar_HEIGHT - kTabBarHeight - 20;
    CGFloat width = (height * 676)/1017;
    CGFloat cellW = (SCREEN_WIDTH - 64)/2;
    CGFloat cellH = cellW * height / width;
    return Size((SCREEN_WIDTH - 64)/2, cellH);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

