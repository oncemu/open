//
//  PhotoVC.m
//  Open
//
//  Created by onceMu on 2017/7/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PhotoVC.h"
#import "SegmentBar.h"
#import "SwipeView.h"
#import "PhotoPicVC.h"

@interface PhotoVC ()<SegmentBarDelegate,SwipeViewDelegate,SwipeViewDataSource>

@property(nonatomic, strong) SegmentBar *segmentBar;
@property(nonatomic, strong) SwipeView  *swipeView;
@property(nonatomic, strong) PhotoPicVC *picVC;
@property(nonatomic, strong) PhotoPicVC *followedVC;

@end

@implementation PhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.segmentBar = [SegmentBar segmentBarWithFrame:CM(100, 0, SCREEN_WIDTH - 200, 30)];
    self.segmentBar.items = @[@"美图",@"已上架"];
    self.segmentBar.delegate = self;
    self.segmentBar.selectIndex = 0;
    self.navigationItem.titleView = self.segmentBar;

    self.swipeView = [[SwipeView alloc] initWithFrame:CM(0, NavigationBar_HEIGHT + StatusBar_HEIGHT , SCREEN_WIDTH, SCREEN_HEIGHT - NavigationBar_HEIGHT - StatusBar_HEIGHT - kTabBarHeight)];
    self.swipeView.delegate = self;
    self.swipeView.dataSource = self;
    self.swipeView.alignment = SwipeViewAlignmentCenter;
    self.swipeView.pagingEnabled = YES;
    self.swipeView.bounces = NO;
    [self.swipeView scrollToItemAtIndex:0 duration:0];
    [self.view addSubview:self.swipeView];
}

#pragma mark - SegmentBarDelegate

- (void)segmentBar:(SegmentBar *)segmentBar didSelectIndex:(NSInteger)toIndex fromIndex:(NSInteger)fromIndex {
    if (toIndex == fromIndex) {
        switch (toIndex) {
            case 0:
                [self.picVC startRefresh];
                break;
            case 1:
                [self.followedVC startRefresh];
                break;
            default:
                break;
        }
    }else {
        [self.swipeView scrollToPage:toIndex duration:0.3];
    }
}

#pragma mark - SwipeViewDelegate,SwipeViewDataSource
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView {
    return 2;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    switch (index) {
        case 0:
            return self.picVC.view;
            break;
        case 1:
            return self.followedVC.view;
            break;
        default:
            break;
    }
    return [UIView new];
}

- (void)swipeViewDidEndDecelerating:(SwipeView *)swipeView {
    if (self.segmentBar.selectIndex != swipeView.currentPage) {
        self.segmentBar.selectIndex = swipeView.currentItemIndex;
    }
}


- (PhotoPicVC *)picVC {
    if (_picVC == nil) {
        _picVC = [self getTableVC];
    }
    return _picVC;
}

- (PhotoPicVC *)followedVC {
    if (_followedVC == nil) {
        _followedVC = [self getTableVC];
    }
    return _followedVC;
}

- (PhotoPicVC *)getTableVC {
    PhotoPicVC *vc = [[PhotoPicVC alloc] initWithShowBackButton:NO showHeaderRefresh:YES showFooterRefresh:YES];
    vc.view.frame = CM(0, 0 , SCREEN_WIDTH, SCREEN_HEIGHT - NavigationBar_HEIGHT - StatusBar_HEIGHT - kTabBarHeight);
    vc.parentVC = self;
    return vc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
