//
//  EditTextVC.h
//  Open
//
//  Created by mfp on 17/10/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseLandscapeVC.h"
#import "AlbumMakeObject.h"

@interface EditTextVC : BaseLandscapeVC

@property(nonatomic, strong) BaseAlbumObject *object;

@property(nonatomic, copy) void (^editText)(BaseAlbumObject *text);

@end
