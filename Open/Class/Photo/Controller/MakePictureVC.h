//
//  MakePictureVC.h
//  Open
//
//  Created by mfp on 17/10/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//  美图制作

#import "TableBaseVC.h"
#import "MakeCategoryItem.h"

@interface MakePictureVC : TableBaseVC

@end


@interface MakePictureCell : UITableViewCell

@property(nonatomic, strong) UIButton *makeBtn;
@property(nonatomic, copy) void (^makeBlock)(MakeCategoryItem *object);
@property(nonatomic, strong) MakeCategoryItem *item;

@end
