//
//  MyAlbumVC.m
//  Open
//
//  Created by mfp on 17/10/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MyAlbumVC.h"
#import "PreviewPicAlbumVC.h"
#import "MyAlbumView.h"
#import "AlbumMakeVC.h"

@interface MyAlbumVC ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) AlbumMakeData *makeData;
@end

@implementation MyAlbumVC

//testCode
- (void)testCode {
    self.makeData = [AlbumMakeData shareData];
    self.makeData.type = self.type;
    if ([self.makeData hasExistAlbum:self.type]) {
        [self.makeData getAlbumData:self.type];
    }else{
        [self.makeData.dataList removeAllObjects];
        for (int i = 0; i<11; i++) {
            AlbumMakeObject *obj = [[AlbumMakeObject alloc] init];
            obj.type = self.type;
            obj.mode = AlbumFormatMode_1;
            obj.pageIndex = i;
            obj.backColor = WhiteColor;
            [self.makeData.dataList addObject:obj];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationController.fd_fullscreenPopGestureRecognizer.enabled = YES;
    [self.collectionView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self testCode];
    self.navigationItem.title = @"我的画册";
    [self setBarButtonItemWithImage:getImage(@"make_eye") title:nil isLeft:NO];
    CGFloat height = self.view.frameSizeHeight - NavigationBar_HEIGHT - 20 - 45;
    CGFloat width = (height * 935) / 558.0f;

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 2;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    CGFloat itemWidth = (SCREEN_WIDTH - 50)/4;
    CGFloat itemHeight = itemWidth * height / width;
    layout.itemSize = Size(itemWidth, itemHeight);
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) collectionViewLayout:layout];
    _collectionView.backgroundColor = ColorWithHex(0xeeeeee);
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [self.view addSubview:_collectionView];
    [_collectionView registerClass:[MyAlbumCell class] forCellWithReuseIdentifier:NSStringFromClass([MyAlbumCell class])];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(back) name:kAlbumMakeSuccess object:nil];
}

- (void)rightButtonItemAction {
    if (!self.makeData.hasImage) {
        [KVNProgress showErrorWithStatus:@"还没有制作相册" onView:self.view];
        return;
    }
    PreviewPicAlbumVC *vc = [[PreviewPicAlbumVC alloc] initWithShowBackButton:YES];
    vc.type = self.type;
    vc.model = self.model;
    [self pushVC:vc animated:YES];
}

- (void)goBack {
    if (self.makeData.hasImage && !self.makeData.hasMake) {
        [self.makeData saveAlbumData:self.type];
    }
    [super goBack];
}

- (void)back {
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.makeData.dataList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MyAlbumCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MyAlbumCell class]) forIndexPath:indexPath];
    cell.pageIndex = indexPath.item;
    AlbumMakeObject *obj = self.makeData.dataList[indexPath.item];
    cell.image = obj.image;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    AlbumMakeVC *vc = [[AlbumMakeVC alloc] initWithShowBackButton:YES];
    vc.object = self.makeData.dataList[indexPath.item];
    [self pushVC:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
