//
//  PhotoPicVC.m
//  Open
//
//  Created by mfp on 17/7/12.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PhotoPicVC.h"
#import "PhotoPicView.h"
#import "GreedoCollectionViewLayout.h"
#import "AlbumNetService.h"
#import "PictureDetailVC.h"

static NSString *const hotcellId = @"hotcellId";
static NSString *const newsummitcellId = @"newsummitcellId";
static NSString *const photopicviewheaderId = @"photopicviewheaderId";

#define SizeValue(w,h)   [NSValue valueWithCGSize:Size(w, h)]


@interface PhotoPicVC ()<GreedoCollectionViewLayoutDataSource>
{
    NSArray *sectionList;
}
@property (strong, nonatomic) GreedoCollectionViewLayout *collectionViewSizeCalculator;
@property(nonatomic, strong) NSMutableArray *layoutData;
@property(nonatomic, strong) NSMutableArray *hotList;//热点数据
@property(nonatomic, strong) NSMutableArray *lastList;//最新上传数据
@property(nonatomic, strong) AlbumNetService *albumService;

@end

@implementation PhotoPicVC

- (void)testCode {
    self.hotList = [NSMutableArray array];
    self.lastList = [NSMutableArray array];
    sectionList = @[@"热点",@"最新上传"];
    self.layoutData = @[[NSValue valueWithCGSize:Size(SCREEN_WIDTH , 100)],
                  [NSValue valueWithCGSize:Size((SCREEN_WIDTH - 17 * 2 - 5)/2.0f, 140)],
                  [NSValue valueWithCGSize:Size((SCREEN_WIDTH - 17 * 2 - 5)/2.0f, 130)],
                  [NSValue valueWithCGSize:Size((SCREEN_WIDTH - 17 * 2 - 5)/2.0f, 50)],
                  [NSValue valueWithCGSize:Size((SCREEN_WIDTH - 17 * 2 - 10)/3.0f, 80)],
                  [NSValue valueWithCGSize:Size((SCREEN_WIDTH - 17 * 2 - 10)/3.0f, 80)],
                  [NSValue valueWithCGSize:Size((SCREEN_WIDTH - 17 * 2 - 10)/3.0f, 80)]].mutableCopy;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self testCode];
    self.collectionView.backgroundColor = WhiteColor;
    [self.collectionView registerClass:[HotCell class] forCellWithReuseIdentifier:hotcellId];
    [self.collectionView registerClass:[PicCell class] forCellWithReuseIdentifier:newsummitcellId];
    [self.collectionView registerClass:[PhotoPicViewHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:photopicviewheaderId];
    self.collectionViewSizeCalculator.rowMaximumHeight = CGRectGetHeight(self.collectionView.bounds) / 3;
    self.collectionViewSizeCalculator.fixedHeight = NO;
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 2.0f;
    layout.minimumLineSpacing = 2.0f;
    layout.headerReferenceSize = Size(SCREEN_WIDTH, 40);
    layout.sectionInset = UIEdgeInsetsMake(0, 2, 0, 2);
    self.collectionView.collectionViewLayout = layout;
    self.collectionView.frame = CM(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64 - kTabBarHeight);
}

#pragma mark loadData 
- (void)loadData {
    if (self.isShelf) {
        [self loadShelfHot];
        [self loadShelfLast:NO];
    }else{
        [self loadHotAlbum];
        [self loadLastAlbum:NO];
    }
}

- (void)loadMoreData {
    if (self.isShelf) {
        [self loadShelfLast:YES];
    }else{
        [self loadLastAlbum:YES];
    }
}

//美图-最热
- (void)loadHotAlbum {
    WS(weakSelf);
    [self.albumService getAlbumsHotPhotos:^(NSString *requestInfo, id responseObject) {
        [weakSelf.hotList addObjectsFromArray:responseObject];
        [weakSelf.collectionView reloadData];
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

//美图-最新
- (void)loadLastAlbum:(BOOL)more {
    WS(weakSelf);
    [self.albumService getMoreHotPhotos:more success:^(NSString *requestInfo, id responseObject) {
        if (!more) {
            [weakSelf.lastList removeAllObjects];
        }
        [weakSelf.lastList addObjectsFromArray:responseObject];
        [weakSelf changeRefreshStatus:!more data:responseObject];
        self.noDataButton.hidden = YES;
    } failure:^(NSError *error, id responseObject) {
        [weakSelf changeRefreshStatus:!more data:nil];
        self.noDataButton.hidden = YES;
    }];
}

//上架-最热
- (void)loadShelfHot {
    WS(weakSelf);
    [self.albumService getShelfPhotos:^(NSString *requestInfo, id responseObject) {
        [weakSelf.hotList addObjectsFromArray:responseObject];
        [weakSelf.collectionView reloadData];
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

//上架-最新
- (void)loadShelfLast:(BOOL)more {
    WS(weakSelf);
    [self.albumService getshelfMoreHotPhotos:more success:^(NSString *requestInfo, id responseObject) {
        if (!more) {
            [weakSelf.lastList removeAllObjects];
        }
        [weakSelf.lastList addObjectsFromArray:responseObject];
        [weakSelf changeRefreshStatus:!more data:responseObject];
    } failure:^(NSError *error, id responseObject) {
        [weakSelf changeRefreshStatus:!more data:nil];
    }];
}

- (void)pushToDetail:(id)mode {
    PictureDetailVC *vc = [[PictureDetailVC alloc] initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
    vc.object = mode;
    [self pushVC:vc animated:YES];
}

#pragma mark UICollectionViewDataSource,UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return (section == 0)?1:self.lastList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            HotCell *cell = (HotCell *)[collectionView dequeueReusableCellWithReuseIdentifier:hotcellId forIndexPath:indexPath];
            cell.list = self.hotList;
            WS(weakSelf);
            cell.selectBlock = ^(NSInteger index) {
                [weakSelf pushToDetail:weakSelf.hotList[index]];
            };
            return cell;
        }
            break;
        case 1:
        {
            PicCell *cell = (PicCell *)[collectionView dequeueReusableCellWithReuseIdentifier:newsummitcellId forIndexPath:indexPath];
            cell.object = self.lastList[indexPath.row];
            return cell;
        }
            break;
        default:
            break;
    }
    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    PhotoPicViewHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:photopicviewheaderId forIndexPath:indexPath];
    header.title = sectionList[indexPath.section];
    return header;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        [self pushToDetail:self.lastList[indexPath.item]];
    }
}

#pragma mark - <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.collectionViewSizeCalculator sizeForPhotoAtIndexPath:indexPath];
}

#pragma mark - <GreedoCollectionViewLayoutDataSource>

- (CGSize)greedoCollectionViewLayout:(GreedoCollectionViewLayout *)layout originalImageSizeAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return Size(SCREEN_WIDTH, 165);
    }else{
        if (indexPath.item < self.layoutData.count) {
            NSValue *v = self.layoutData[indexPath.item];
            return v.CGSizeValue;
        }
        return CGSizeMake(0.1, 0.1);
    }
    return CGSizeMake(0.1, 0.1);
}

- (GreedoCollectionViewLayout *)collectionViewSizeCalculator {
    if (!_collectionViewSizeCalculator) {
        _collectionViewSizeCalculator = [[GreedoCollectionViewLayout alloc] initWithCollectionView:self.collectionView];
        _collectionViewSizeCalculator.dataSource = self;
    }
    return _collectionViewSizeCalculator;
}

- (AlbumNetService *)albumService {
    if (!_albumService) {
        _albumService = [[AlbumNetService alloc] init];
    }
    return _albumService;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
