//
//  AlbumMakeVC.m
//  Open
//
//  Created by mfp on 17/10/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "AlbumMakeVC.h"
#import "UIBarButtonItem+QXBarbutton.h"
#import "UIImage+UIColor.h"
#import "AlbumMakeView.h"
#import "EditTextVC.h"
#import "PhotoAlbumVC.h"
#import "MUndoManager.h"
#import "UIImage-Extensions.h"
#import "MakeNetService.h"

//最多添图数量
#define MAX_IMAGE_COUNT 12

@interface AlbumMakeVC ()

@property(nonatomic, strong) MakeNetService *service;
@property(nonatomic, strong) FormView       *formView;
@property(nonatomic, strong) UIButton       *prePageBtn;
@property(nonatomic, strong) UIButton       *nextPageBtn;
@property(nonatomic, strong) UILabel        *titleLabel;
@property(nonatomic, strong) EditToolBar    *topBar;
@property(nonatomic, strong) LeftToolBar    *leftBar;
@property(nonatomic, strong) FormListView   *formListView;
@property(nonatomic, strong) ColorListView  *colorListView;
@property(nonatomic, strong) ElementView    *elementView;
@property(nonatomic, weak)   BaseEditView   *editView;
@property(nonatomic, strong) AlbumMakeData  *makeData;
@property(nonatomic, strong) AlbumMakeObject *originObject;

@end

@implementation AlbumMakeVC

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_colorListView hide];
    [_elementView hide];
    [_formListView hide];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.fd_fullscreenPopGestureRecognizer.enabled = NO;
    [_colorListView hide];
    [_elementView hide];
    [_formListView hide];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavgationBar];
    [self setupFormwork];
    [self setupBottom];
    [self leftBar];
    [self addUndoManagerChangeObserver];
    [self loadStickers];
}

- (void)goBack {
    [self saveAlbum:^{
        [super goBack];
    }];
}

//保存作品
- (void)saveAlbum:(void(^)())completeHandle {
    //如果有更改，则保存
    if (![self.object isEqual:self.originObject]) {
        [KVNProgress showWithStatus:@"正在保存作品"];
        dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(globalQueue, ^{
            self.editView.selected = NO;
            self.object.image = [self.formView getAlubmImage];
            [[MUndoManager sharedInstance].manager removeAllActions];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateBarButtonItemStatus:nil];
                [KVNProgress dismiss];
                completeHandle();
            });
        });
    }else{
        completeHandle();
    }
}

- (void)initData {
    self.makeData = [AlbumMakeData shareData];
    self.originObject = self.object.mutableCopy;
    if (_formView) {
        [_formView removeFromSuperview];
        _formView = nil;
    }
    self.topBar.hidden = YES;
}

//加载模板
- (void)setupFormwork {
    [self initData];
    
    _formView = [[FormView alloc] initWithFrame:CM((self.view.frameSizeWidth - self.object.formSize.width)/2, (SCREEN_HEIGHT - self.object.formSize.height + NavigationBar_HEIGHT)/2, self.object.formSize.width, self.object.formSize.height)];
    _formView.object = self.object;
    [self.view addSubview:_formView];
    
    WS(weakSelf);
    _formView.editBlock = ^(BaseEditView *editView) {
        weakSelf.editView = editView;
        weakSelf.topBar.hidden = !editView.selected;
        if (editView.object.type == AlbumObjectTypeText || editView.object.type == AlbumObjectTypeElement) {
            [weakSelf.topBar hideButton:YES index:3];
        }else{
            [weakSelf.topBar hideButton:NO index:3];
        }
    };
    _formView.doubleTap = ^(BaseEditView *view) {
        [weakSelf pushToTextVC:view.object isAdd:NO];
    };
}

- (void)setNavgationBar {
    UIBarButtonItem *item1 = [UIBarButtonItem itemWithImageName:@"make_undo" target:self action:@selector(undo)];
    UIBarButtonItem *item2 = [UIBarButtonItem itemWithImageName:@"make_redo" target:self action:@selector(redo)];
    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spaceItem.width = 15;
    self.navigationItem.rightBarButtonItems = @[item2,spaceItem,item1];
    [self updateBarButtonItemStatus:nil];
    self.topBar = [[EditToolBar alloc] initWithFrame:CM(0, 0, 74*4, 44) imageName:@"make_toptool"];
    _topBar.hidden = YES;
    self.navigationItem.titleView = self.topBar;
    WS(weakSelf);
    _topBar.actionBlock = ^(NSInteger index) {
        switch (index) {
            case 0://旋转
                weakSelf.editView.canRotate = YES;
                break;
            case 1://缩放
                weakSelf.editView.canScale = YES;
                break;
            case 2://删除
                [weakSelf deleteView];
                break;
            case 3://换图
                [weakSelf addImage];
                break;
        }
    };
}

//下载贴纸
- (void)loadStickers {
    WS(weakSelf);
    [self.service getStickers:^(NSString *requestInfo, id responseObject) {
        weakSelf.elementView.elements = responseObject;
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

//撤销一次操作
- (void)undo {
    [[MUndoManager sharedInstance] undo];
    [self updateBarButtonItemStatus:nil];
}

//恢复一次撤销
- (void)redo {
    [[MUndoManager sharedInstance] redo];
    [self updateBarButtonItemStatus:nil];
}

//上一页
- (void)prePageAction {
    NSInteger pageIndex = self.object.pageIndex;
    if (pageIndex > 0) {
        [self updatePage:pageIndex - 1];
    }
}

//下一页
- (void)nextPageAction {
    NSInteger pageIndex = self.object.pageIndex;
    if (pageIndex < self.makeData.dataList.count - 1) {
        [self updatePage:pageIndex + 1];
    }
}

- (void)updatePage:(NSInteger)page {
    [self saveAlbum:^{
        self.object = self.makeData.dataList[page];
        self.originObject = self.object.mutableCopy;
        [self refreshPageIndex];
        [self changeForm];
    }];
}

//左侧按钮  版式
- (void)showFormView {
    [self.formListView show];
}
//左侧按钮  底色
- (void)showColorListView {
    [self.colorListView show];
}

//左侧按钮  文字
- (void)editText {
    if (self.object.textCount > 5) {
        [KVNProgress showErrorWithStatus:@"最多添加6段文字！"];
        return;
    }
    BaseAlbumObject *obj = [BaseAlbumObject getAlbumText:@"" frame:CM(30, 40, 100, 40) color:BlackColor font:Font(15)];
    [self pushToTextVC:obj isAdd:YES];
}

//添图 | 换图 
- (void)addImage {
    PhotoAlbumVC *vc = [[PhotoAlbumVC alloc] initWithShowBackButton:YES];
    if ([self.editView isKindOfClass:[ImageEditView class]] && self.editView.selected) {
        vc.maxCount = 1;//如果当前有选中的editView，并且属于ImageEditView，则为换图操作，否则为添图
    }else{
        if (self.object.imageCount == MAX_IMAGE_COUNT) {
            [KVNProgress showErrorWithStatus:@"最多添图12张！"];
            return;
        }
        vc.maxCount = MAX_IMAGE_COUNT - self.object.imageCount;
    }
    WS(weakSelf);
    vc.addPhotoBlock = ^(NSArray *images) {//在非自由模板下有选中的图片类view
        if ([weakSelf.editView isKindOfClass:[ImageEditView class]] && weakSelf.editView.selected && weakSelf.object.mode != AlbumFormatMode_blank) {
            UIImage *image = [images firstObject];
            NSData *data = UIImagePNGRepresentation(image);
            image = [UIImage imageWithData:data];
            ImageEditView *editV = (ImageEditView *)weakSelf.editView;
            if (editV.object.type == AlbumObjectTypeForm) {//选中的是模板
                [editV changeImage:image];
            }else if (editV.object.type == AlbumObjectTypeElement){//选中的是贴图
                [weakSelf addNewImageView:@[image]];
            }else{//其他情况替换图片的做法是删除旧图，加新图
                BaseAlbumObject *obj = editV.object;
                obj.image = image;
                obj.frameValue = [NSValue valueWithCGRect:CM(30, 10, ((weakSelf.formView.frameSizeHeight - 30) * image.size.width)/image.size.height, weakSelf.formView.frameSizeHeight - 30)];
                obj.center = obj.center;
                [editV removeSelf];
                [weakSelf.formView addView:obj];
            }
        }else{
            [weakSelf addNewImageView:images];
        }
    };
    CustomNavigationController *nav = [[CustomNavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

//添加图片类view
- (void)addNewImageView:(NSArray *)images {
    for (NSInteger i = 0; i<images.count; i++) {
        UIImage *image = images[i];
        BaseAlbumObject *ai = [BaseAlbumObject getAlbumImage:image frame:CM(30 + i*10, 10 + i*5, ((self.formView.frameSizeHeight - 30) * image.size.width)/image.size.height, self.formView.frameSizeHeight - 30)];
        ai.type = AlbumObjectTypeImage;
        [self.formView addView:ai];
    }
}

//添加/修改文字 add: YES：添加   NO:修改
- (void)pushToTextVC:(BaseAlbumObject *)object isAdd:(BOOL)add {
    EditTextVC *vc = [[EditTextVC alloc] initWithShowBackButton:YES];
    vc.object = object;
    WS(weakSelf);
    vc.editText = ^(BaseAlbumObject *text) {
        if (!add) {
            [weakSelf.editView removeSelf];
        }
        [weakSelf.formView addView:text];
    };
    [self pushVC:vc animated:YES];
}

//删除
- (void)deleteView {
    [self.editView removeSelf];
    self.topBar.hidden = (self.editView.object.type != AlbumObjectTypeForm);
}

//左侧按钮  元素
- (void)showElementView {
    [self.elementView show];
}

//切换模板
- (void)changeForm {
    [self setupFormwork];
}

//更新页数
- (void)refreshPageIndex {
    NSString *title = @"封面";
    if (self.object.pageIndex > 0) {
        title = [NSString stringWithFormat:@"%ld",(long)self.object.pageIndex];
    }
    self.titleLabel.text =title;
}

- (void)setupBottom {
    _prePageBtn = [ViewFactory getButton:CM(self.formView.frameOriginX + 100, SCREEN_HEIGHT - 9 - 28, 28, 28) selectImage:getImage(@"") norImage:getImage(@"make_arrowleft") target:self action:@selector(prePageAction)];
    [self.view addSubview:_prePageBtn];

    _nextPageBtn = [ViewFactory getButton:CM(CGRectGetMaxX(self.formView.frame) - 128, self.prePageBtn.frameOriginY, 28, 28) selectImage:getImage(@"") norImage:getImage(@"make_arrowright") target:self action:@selector(nextPageAction)];
    [self.view addSubview:_nextPageBtn];
    
    _titleLabel = [ViewFactory getLabel:CM(SCREEN_WIDTH/2 - 50, self.prePageBtn.frameOriginY, 100, 28) font:Font(14) textColor:ColorWithHex(0x666666) text:@""];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_titleLabel];
    [self refreshPageIndex];
}

- (LeftToolBar *)leftBar {
    if (_leftBar == nil) {
        CGFloat x = CGRectGetMaxX(self.formView.frame) + (self.view.frameSizeWidth - CGRectGetMaxX(self.formView.frame) - 45 )/2;
        _leftBar = [[LeftToolBar alloc] initWithFrame:CM(x, NavigationBar_HEIGHT + 20 + 22, 45, 55 * 5) titles:@[@"版式",@"底色",@"文字",@"添图",@"元素"]];
        WS(weakSelf);
        [self.view addSubview:_leftBar];
        _leftBar.clickBlock = ^(NSInteger index) {
            switch (index) {
                case 0:
                    [weakSelf showFormView];
                    break;
                case 1:
                    [weakSelf showColorListView];
                    break;
                case 2:
                    [weakSelf editText];
                    break;
                case 3:
                    [weakSelf addImage];
                    break;
                case 4:
                    [weakSelf showElementView];
                    break;
            }
        };
    }
    return _leftBar;
}

- (FormListView *)formListView {
    if (!_formListView) {
        _formListView = [[FormListView alloc] initWithFrame:CM(self.view.frameSizeWidth, 0, 140, self.view.frameSizeHeight) type:self.object.type];
        WS(weakSelf);
        _formListView.formSelect = ^(AlbumFormatMode mode) {
            weakSelf.object.mode = mode;
            [weakSelf changeForm];
        };
    }
    return _formListView;
}

- (ColorListView *)colorListView {
    if (!_colorListView) {
        _colorListView = [[ColorListView alloc] initWithFrame:CM(self.view.frameSizeWidth, 0, 140, self.view.frameSizeHeight)];
        WS(weakSelf);
        _colorListView.colorSelect = ^(UIColor *color) {
            weakSelf.formView.baseColor = color;
        };
    }
    return _colorListView;
}

- (ElementView *)elementView {
    if (!_elementView) {
        _elementView = [[ElementView alloc] initWithFrame:CM(self.view.frameSizeWidth, 0, 140, self.view.frameSizeHeight)];
        WS(weakSelf);
        _elementView.elementSelect = ^(UIImage *image) {
            if (weakSelf.object.elementCount > 5) {
                [KVNProgress showErrorWithStatus:@"最多添加6个贴图！"];
                return;
            }
            BaseAlbumObject *ai = [BaseAlbumObject getAlbumImage:image frame:CM(30, 15, ((weakSelf.formView.frameSizeHeight - 30) * image.size.width)/image.size.height, weakSelf.formView.frameSizeHeight - 30)];
            ai.type = AlbumObjectTypeElement;
            [weakSelf.formView addView:ai];
        };
    }
    return _elementView;
}

- (void)updateBarButtonItemStatus:(NSNotification *)noti {
    NSUndoManager *manager = self.formView.mundoManager.manager;
    UIBarButtonItem *itemleft = [self.navigationItem.rightBarButtonItems lastObject];
    UIButton *btnleft = itemleft.customView;
    UIBarButtonItem *itemright = [self.navigationItem.rightBarButtonItems firstObject];
    UIButton *btnright = itemright.customView;
    btnleft.enabled = manager.canUndo;
    btnright.enabled = manager.canRedo;
}

//监听撤回和取消撤回
- (void)addUndoManagerChangeObserver {
    NSUndoManager *manager = self.formView.mundoManager.manager;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBarButtonItemStatus:) name:NSUndoManagerCheckpointNotification object:manager];
}

- (void)removeUndoManagerChangeObserver {
    NSUndoManager *manager = self.formView.mundoManager.manager;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSUndoManagerCheckpointNotification object:manager];
}

- (MakeNetService *)service {
    if (_service == nil) {
        _service = [[MakeNetService alloc] init];
    }
    return _service;
}

- (void)dealloc {
    [self removeUndoManagerChangeObserver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
