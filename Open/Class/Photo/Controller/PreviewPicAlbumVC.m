//
//  PreviewPicAlbumVC.m
//  Open
//
//  Created by mfp on 17/7/13.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PreviewPicAlbumVC.h"
#import "AlbumMakeObject.h"
#import "MakeNetService.h"
#import "SDCycleScrollView.h"
#import "ConfirmOrderVC.h"

@interface PreviewPicAlbumVC ()
{
    CGSize formSize;
}
@property(nonatomic, strong) MakeNetService *makeService;
@property(nonatomic, strong) NSMutableArray *dataList;
@property(nonatomic, strong) SDCycleScrollView *cycleScrollView;

@end

@implementation PreviewPicAlbumVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"预览";
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.dataList = [NSMutableArray array];
    for (AlbumMakeObject *obj in [AlbumMakeData shareData].dataList) {
        if (obj.image) {
            formSize = obj.formSize;
            [self.dataList addObject:obj.image];
        }
    }
    [self setBarButtonItemWithImage:getImage(@"mallCart") title:nil isLeft:NO];

    self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CM((self.view.frameSizeWidth - formSize.width)/2, (SCREEN_HEIGHT - formSize.height + NavigationBar_HEIGHT)/2, formSize.width, formSize.height) imageNamesGroup:self.dataList];
    self.cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
    self.cycleScrollView.infiniteLoop = NO;
    [self.view addSubview:self.cycleScrollView];
}

//制作相册
- (void)rightButtonItemAction {
    self.model.itemPhotos = self.dataList;
    self.model.itemName = @"相册";
    self.model.price = self.type/10.0f;
    self.model.expressFee = 0;
    self.model.num = 1;
    ConfirmOrderVC *vc = [[ConfirmOrderVC alloc] initWithShowBackButton:YES];
    vc.goodsList = @[self.model];
    vc.type = AlbumConfirmTypeAlbum;
    CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
