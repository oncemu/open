//
//  PicBuyVC.m
//  Open
//
//  Created by mfp on 17/7/13.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PicBuyVC.h"
#import "PicBuyView.h"

@interface PicBuyVC ()

@property(nonatomic, strong) PayToolBar *payBar;

@end

@implementation PicBuyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"相框";
    [self setBarButtonItemWithImage:getImage(@"pic_shopcart") title:nil isLeft:NO];
    self.payBar = [[PayToolBar alloc] initWithFrame:CM(0, SCREEN_HEIGHT - kTabBarHeight, SCREEN_WIDTH, kTabBarHeight)];
    self.payBar.object = nil;
    [self.view addSubview:self.payBar];
}

- (void)rightButtonItemAction {
    
}

#pragma mark UITableViewDataSource,UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
