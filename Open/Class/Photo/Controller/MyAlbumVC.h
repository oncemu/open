//
//  MyAlbumVC.h
//  Open
//
//  Created by mfp on 17/10/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseLandscapeVC.h"
#import "MallGoodsModel.h"
#import "AlbumMakeObject.h"

@interface MyAlbumVC : BaseLandscapeVC

@property(nonatomic, assign) AlbumFormatType type;//相册种类
@property(nonatomic, strong) MallGoodsModel *model;

@end
