//
//  PreviewPicAlbumVC.h
//  Open
//
//  Created by mfp on 17/7/13.
//  Copyright © 2017年 彭彬. All rights reserved.
//  画册预览

#import "BaseLandscapeVC.h"
#import "MallGoodsModel.h"
#import "AlbumMakeObject.h"

@interface PreviewPicAlbumVC : BaseLandscapeVC

@property(nonatomic, assign) AlbumFormatType type;//相册种类
@property(nonatomic, strong) MallGoodsModel *model;

@end
