//
//  MakePictureVC.m
//  Open
//
//  Created by mfp on 17/10/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MakePictureVC.h"
#import "MakeNetService.h"
#import "PicBuyView.h"
#import "PreviewPicAlbumVC.h"
#import "PostCardEditVC.h"
#import "MallNetService.h"

@interface MakePictureVC ()
@property(nonatomic, strong) MakeNetService *service;
@property(nonatomic, strong) PicBuyHeader *headerView;
@end

@implementation MakePictureVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"美图制作";
    _service = [[MakeNetService alloc] init];
    self.tableView.tableHeaderView = self.headerView;
    [self.tableView registerClass:[MakePictureCell class] forCellReuseIdentifier:NSStringFromClass([MakePictureCell class])];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.rowHeight = 240;
    [self loadBanner];
    [self loadData];
    [self getItemCategories];
}

//商品制作分类
- (void)getItemCategories {
    WS(weakSelf);
    [self.service getItemCategories:^(NSString *requestInfo, id responseObject) {
        [weakSelf.dataList addObjectsFromArray:responseObject];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

//美图制作分类
- (void)loadData {
    WS(weakSelf);
    [self.service getAlbumsMakeCategories:^(NSString *requestInfo, id responseObject) {
        [weakSelf.dataList addObjectsFromArray:responseObject];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

- (void)loadBanner {
    WS(weakSelf);
    [self.service getMakeAlbumsAds:^(NSString *requestInfo, id responseObject) {
        NSArray *list = responseObject[@"content"];
        NSMutableArray *picUrls = [NSMutableArray array];
        for (NSDictionary *dic in list) {
            if ([dic objectForKey:@"photo"]) {
                [picUrls addObject:dic[@"photo"]];
            }
        }
        weakSelf.headerView.listArray = picUrls;
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

//进入制作页面
- (void)pushToMakeVC:(MakeCategoryItem *)item {
    if (item.categoryId == 4) {//制作相框
        
        
    }else if (item.categoryId == 6) {//制作明信片
        
        
    }
}


#pragma mark - table delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MakePictureCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MakePictureCell class])];
    cell.item = self.dataList[indexPath.row];
    WS(weakSelf);
    cell.makeBlock = ^(MakeCategoryItem *object) {
        [weakSelf pushToMakeVC:object];
    };
    return cell;
}

- (PicBuyHeader *)headerView {
    if (!_headerView) {
        _headerView = [[PicBuyHeader alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, HEIGHT_FIT(190))];
    }
    return _headerView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

@implementation MakePictureCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = WhiteColor;
        self.textLabel.font = Font(16);
        self.textLabel.textColor = ColorWithHex(0x333333);
        self.detailTextLabel.font = Font(13);
        self.detailTextLabel.textColor = ColorWithHex(0x999999);
        self.textLabel.textAlignment = self.detailTextLabel.textAlignment = NSTextAlignmentCenter;
        self.makeBtn = [ViewFactory getButton:CGRectZero font:Font(15) selectTitle:@"" norTitle:@"立即制作" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(make)];
        self.makeBtn.layer.cornerRadius  = 15;
        self.makeBtn.layer.borderWidth = 0.5;
        self.makeBtn.layer.borderColor = BtnLemonYellow.CGColor;
        [self.contentView addSubview:self.makeBtn];
    }
    return self;
}

- (void)setItem:(MakeCategoryItem *)item {
    _item = item;
    self.textLabel.text = item.makeTitle;
    self.detailTextLabel.text = item.makeDesc;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = CM(0, 5, self.frameSizeWidth, 20);
    self.detailTextLabel.frame = CM(0, CGRectGetMaxY(self.textLabel.frame) + 5, self.frameSizeWidth, 17);
    self.makeBtn.frame = CM(self.frameSizeWidth/2 - 95, self.frameSizeHeight - 50, 190, 30);
}

- (void)make {
    if (self.makeBlock) {
        self.makeBlock(self.item);
    }
}

@end
