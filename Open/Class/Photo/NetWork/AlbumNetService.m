//
//  AlbumNetService.m
//  Open
//
//  Created by mfp on 17/9/5.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "AlbumNetService.h"
#import "User.h"

@implementation AlbumNetService
//GET /albums/hot_photos 美图 - 热门美图列表 - 热点
- (void)getAlbumsHotPhotos:(responseSuccess)success
                   failure:(responseFailure)failure {
    [self requestGETwithUrl:kHotPhotos parameters:nil success:^(NSString *requestInfo, id responseObject) {
        NSArray *albums = [AlbumModel albumList:responseObject];
        success(requestInfo,albums);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//GET /albums/more_hot_photos 美图 - 热门美图列表 - 最新上传 page
- (void)getMoreHotPhotos:(BOOL)more
                 success:(responseSuccess)success
                 failure:(responseFailure)failure {
    if (!more) {
        MoreHotPhotosIndex = 1;
    }
    [self requestGETwithUrl:kMoreHotPhotos parameters:@{@"page":@(MoreHotPhotosIndex),@"size":@(20)}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        NSArray *albums = [AlbumModel albumList:responseObject];
        success(requestInfo,albums);
        MoreHotPhotosIndex += 1;
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//GET /albums/more_shelf_photos 美图 - 上架美图列表 - 最新上传page
- (void)getshelfMoreHotPhotos:(BOOL)more
                       success:(responseSuccess)success
                      failure:(responseFailure)failure {
    if (!more) {
        MoreShelfPhotosIndex = 1;
    }
    [self requestGETwithUrl:kMoreShelfPhotos parameters:@{@"size":@(20),@"page":@(MoreShelfPhotosIndex)}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        NSArray *albums = [AlbumModel albumList:responseObject];
        success(requestInfo,albums);
        MoreShelfPhotosIndex += 1;
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//GET /albums/shelf_photos 美图 - 上架美图列表 - 热点
- (void)getShelfPhotos:(responseSuccess)success
               failure:(responseFailure)failure {
    [self requestGETwithUrl:kShelfPhotos parameters:nil success:^(NSString *requestInfo, id responseObject) {
        NSArray *albums = [AlbumModel albumList:responseObject];
        success(requestInfo,albums);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//GET /albums/{album_id} 作品详情
- (void)getAlbumDetail:(NSDictionary *)params
               success:(responseSuccess)success
               failure:(responseFailure)failure {
    NSString *url = [NSString stringWithFormat:@"albums/%@",params[@"albumId"]];
    [self requestGETwithUrl:url parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        AlbumModel *album = [AlbumModel albumModel:responseObject];
        success(requestInfo,album);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//POST /albums/{album_id}/collections 作品详情 - 收藏 or 取消收藏
- (void)albumsCollections:(NSDictionary *)params
                  success:(responseSuccess)success
                  failure:(responseFailure)failure {
    NSString *url = [NSString stringWithFormat:@"albums/%@/collections",params[@"albumId"]];
    [self requestPOSTwithUrl:url parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//GET /albums/{album_id}/comments 作品详情 - 评论列表
- (void)getAlbumsComments:(NSDictionary *)params
                     more:(BOOL)more
                  success:(responseSuccess)success
                  failure:(responseFailure)failure {
    NSString *url = [NSString stringWithFormat:@"albums/%@/comments",params[@"albumId"]];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:params];
    if (!more) {
        MoreCommentIndex = 1;
    }
    [dic setObject:@(MoreCommentIndex) forKey:@"page"];
    [dic setObject:@(20) forKey:@"size"];
    [self requestGETwithUrl:url parameters:dic success:^(NSString *requestInfo, id responseObject) {
//        NSArray *comments = [CommentModel commentList:responseObject];
        CommentData *comment = [CommentData parseBaseModel:responseObject];
        MoreCommentIndex += 1;
        success(requestInfo,comment);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//POST /albums/{album_id}/comments 作品详情 - 评论 album_id content
- (void)sendAlbumsComments:(NSDictionary *)params
                   success:(responseSuccess)success
                   failure:(responseFailure)failure {
    NSString *url = [NSString stringWithFormat:@"albums/%@/comments",params[@"album_id"]];
    [self requestPOSTwithUrl:url parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//POST /albums/{album_id}/favorites 作品详情 - 点赞 or 取消点赞
- (void)albumsFavorites:(NSDictionary *)params
                success:(responseSuccess)success
                failure:(responseFailure)failure {
    NSString *url = [NSString stringWithFormat:@"albums/%@/favorites",params[@"album_id"]];
    [self requestPOSTwithUrl:url parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//GET /albums/{album_id}/favourites 作品点赞列表
- (void)getAlbumFavorites:(NSDictionary *)params
                     more:(BOOL)more
                  success:(responseSuccess)success
                  failure:(responseFailure)failure{
    NSString *url = [NSString stringWithFormat:@"albums/%@/favourites",params[@"album_id"]];
    if (!more) {
        MoreFavoriteIndex = 1;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:params];
    [dict setObject:@(MoreFavoriteIndex) forKey:@"page"];
    [self requestGETwithUrl:url parameters:dict success:^(NSString *requestInfo, id responseObject) {
        MoreFavoriteIndex += 1;
        success(requestInfo,responseObject[@"content"][@"content"]);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}
@end
