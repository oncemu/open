//
//  MakeNetService.h
//  Open
//
//  Created by mfp on 17/10/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseNetService.h"
#import "MakeCategoryItem.h"
//美图
//GET /albums/make/ads 上架美图制作 - 广告
#define kMakeAlbumsAds @"albums/make/ads"

//GET /albums/make/categories 上架美图制作 - 制作分类
#define kMakeAlbums  @"albums/make/categories"

//POST /albums/{album_id}/make 上架美图制作（制作成功直接生成订单，所以需要地址参数)

//商品
//POST /items/{item_id}/make 商品 - 制作（返回相册ID）

//POST /items/album_make 相册制作（制作成功直接生成订单，所以需要地址参数）
#define kItemAlbumMake @"items/album_make"

//GET /items/make/categories 商品制作 - 制作分类
#define kItemMakeCategories @"items/make/categories"

//GET /commons/stickers 获取贴纸
#define kGetStickers @"commons/stickers"

@interface MakeNetService : BaseNetService

//上架美图制作 - 广告
- (void)getMakeAlbumsAds:(responseSuccess)success
                 failure:(responseFailure)failure;

//上架美图制作 - 制作分类
- (void)getAlbumsMakeCategories:(responseSuccess)success
                        failure:(responseFailure)failure;

//上架美图制作
- (void)postAlbumsMake:(NSDictionary *)params
               success:(responseSuccess)success
               failure:(responseFailure)failure;


//POST /items/album_make 相册制作（制作成功直接生成订单，所以需要地址参数）
- (void)albumMake:(NSArray *)images
           params:(NSDictionary *)params
         progress:(void (^)(NSProgress *progress))progress
          success:(responseSuccess)success
          failure:(responseFailure)failure;

//GET /items/make/categories 商品制作 - 制作分类
- (void)getItemCategories:(responseSuccess)success
                  failure:(responseFailure)failure;

//POST /items/{item_id}/make 商品 - 制作（返回相册ID）
- (void)itemsMake:(NSDictionary *)params
          success:(responseSuccess)success
          failure:(responseFailure)failure;

//GET /commons/stickers 获取贴纸
- (void)getStickers:(responseSuccess)success
            failure:(responseFailure)failure;

@end
