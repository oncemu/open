//
//  MakeNetService.m
//  Open
//
//  Created by mfp on 17/10/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MakeNetService.h"

@implementation MakeNetService

//上架美图制作 - 广告
- (void)getMakeAlbumsAds:(responseSuccess)success
                 failure:(responseFailure)failure {
    [self requestGETwithUrl:kMakeAlbumsAds parameters:nil success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

//上架美图制作 - 制作分类
- (void)getAlbumsMakeCategories:(responseSuccess)success
                        failure:(responseFailure)failure {
    [self requestGETwithUrl:kMakeAlbums parameters:nil success:^(NSString *requestInfo, id responseObject) {
        NSArray *list = [NSArray yy_modelArrayWithClass:[MakeCategoryItem class] json:responseObject[@"content"]];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

//上架美图制作
- (void)postAlbumsMake:(NSDictionary *)params
               success:(responseSuccess)success
               failure:(responseFailure)failure {
    
}


//POST /items/album_make 相册制作（制作成功直接生成订单，所以需要地址参数）
- (void)albumMake:(NSArray *)images
             params:(NSDictionary *)params
           progress:(void (^)(NSProgress *progress))progress
            success:(responseSuccess)success
            failure:(responseFailure)failure {
    [self uploadImage:images params:params url:kItemAlbumMake progress:progress success:success failure:failure];
}

//GET /items/make/categories 商品制作 - 制作分类
- (void)getItemCategories:(responseSuccess)success
                  failure:(responseFailure)failure {
    [self requestGETwithUrl:kItemMakeCategories parameters:nil success:^(NSString *requestInfo, id responseObject) {
        NSArray *list = [NSArray yy_modelArrayWithClass:[MakeCategoryItem class] json:responseObject[@"content"]];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//POST /items/{item_id}/make 商品 - 制作（返回相册ID）
- (void)itemsMake:(NSDictionary *)params
          success:(responseSuccess)success
          failure:(responseFailure)failure {
    NSString *url = [NSString stringWithFormat:@"items/%@/make",params[@"item_id"]];
    [self requestPOSTwithUrl:url parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//GET /commons/stickers 获取贴纸
- (void)getStickers:(responseSuccess)success
            failure:(responseFailure)failure {
    [self requestGETwithUrl:kGetStickers parameters:nil success:^(NSString *requestInfo, id responseObject) {
        NSArray *list = responseObject[@"content"][@"stickers"];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

@end
