//
//  AlbumNetService.h
//  Open
//
//  Created by mfp on 17/9/5.
//  Copyright © 2017年 彭彬. All rights reserved.
//  作品控制器

#import "BaseNetService.h"
#import "AlbumModel.h"
#import "CommentModel.h"

//GET /albums/hot_photos 美图 - 热门美图列表 - 热点
#define kHotPhotos  @"albums/hot_photos"

//GET /albums/more_hot_photos 美图 - 热门美图列表 - 最新上传 page
#define kMoreHotPhotos  @"/albums/more_hot_photos"

//GET /albums/more_shelf_photos 美图 - 上架美图列表 - 最新上传page
#define kMoreShelfPhotos @"albums/more_shelf_photos"

//GET /albums/shelf_photos 美图 - 上架美图列表 - 热点
#define kShelfPhotos @"albums/shelf_photos"

//GET /albums/{album_id} 作品详情
#define kAlbumsDetail @"albums"

//POST /albums/{album_id}/collections 作品详情 - 收藏 or 取消收藏
#define kAlbumsCollections  @"/collections"

//GET /albums/{album_id}/comments 作品详情 - 评论列表
//POST /albums/{album_id}/comments 作品详情 - 评论 album_id content
#define kAlbumsComments  @"/comments"

//POST /albums/{album_id}/favorites 作品详情 - 点赞 or 取消点赞
#define kAlbumsFavorites   @"/favorites"

@interface AlbumNetService : BaseNetService
{
    NSInteger MoreHotPhotosIndex;//热门美图列表分页
    NSInteger MoreShelfPhotosIndex;//上架美图列表分页
    NSInteger MoreCommentIndex;//美图详情评论列表分页
    NSInteger MoreFavoriteIndex;//点赞列表分页
}

//GET /albums/hot_photos 美图 - 热门美图列表 - 热点
- (void)getAlbumsHotPhotos:(responseSuccess)success
                   failure:(responseFailure)failure;

//GET /albums/more_hot_photos 美图 - 热门美图列表 - 最新上传 page
- (void)getMoreHotPhotos:(BOOL)more
                 success:(responseSuccess)success
                 failure:(responseFailure)failure;

//GET /albums/more_shelf_photos 美图 - 上架美图列表 - 最新上传page
- (void)getshelfMoreHotPhotos:(BOOL)more
                      success:(responseSuccess)success
                      failure:(responseFailure)failure;

//GET /albums/shelf_photos 美图 - 上架美图列表 - 热点
- (void)getShelfPhotos:(responseSuccess)success
               failure:(responseFailure)failure;

//GET /albums/{album_id} 作品详情
- (void)getAlbumDetail:(NSDictionary *)params
               success:(responseSuccess)success
               failure:(responseFailure)failure;

//POST /albums/{album_id}/collections 作品详情 - 收藏 or 取消收藏
- (void)albumsCollections:(NSDictionary *)params
                  success:(responseSuccess)success
                  failure:(responseFailure)failure;

//GET /albums/{album_id}/comments 作品详情 - 评论列表
- (void)getAlbumsComments:(NSDictionary *)params
                     more:(BOOL)more
                  success:(responseSuccess)success
                  failure:(responseFailure)failure;

//POST /albums/{album_id}/comments 作品详情 - 评论 album_id content
- (void)sendAlbumsComments:(NSDictionary *)params
                   success:(responseSuccess)success
                   failure:(responseFailure)failure;

//POST /albums/{album_id}/favorites 作品详情 - 点赞 or 取消点赞
- (void)albumsFavorites:(NSDictionary *)params
                success:(responseSuccess)success
                failure:(responseFailure)failure;

//GET /albums/{album_id}/favourites 作品点赞列表
- (void)getAlbumFavorites:(NSDictionary *)params
                     more:(BOOL)more
                  success:(responseSuccess)success
                  failure:(responseFailure)failure;

@end
