//
//  PhotoPicView.m
//  Open
//
//  Created by mfp on 17/7/12.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PhotoPicView.h"
#import "PicToolBar.h"

static NSString *const hotpiccellId = @"hotpiccellId";

@implementation PhotoPicView


@end


@interface HotCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic, strong) UICollectionView *collectionV;

@end

@implementation HotCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.itemSize = Size(220, 165);
        layout.minimumLineSpacing = 2;
        self.collectionV = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        self.collectionV.showsHorizontalScrollIndicator = NO;
        self.collectionV.backgroundColor = WhiteColor;
        self.collectionV.delegate = self;
        self.collectionV.dataSource = self;
        [self.collectionV registerClass:[HotPicCell class] forCellWithReuseIdentifier:hotpiccellId];
        [self.contentView addSubview:self.collectionV];
    }
    return self;
}

- (void)setList:(NSMutableArray *)list {
    if (_list != list) {
        _list = list;
    }
    [self.collectionV reloadData];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.collectionV.frame = self.bounds;
}

#pragma mark --UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.list.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HotPicCell *cell = (HotPicCell *)[collectionView dequeueReusableCellWithReuseIdentifier:hotpiccellId forIndexPath:indexPath];
    cell.object = self.list[indexPath.item];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectBlock) {
        self.selectBlock(indexPath.item);
    }
}

@end

@interface PicCell ()

@property(nonatomic, strong) UIImageView *picImageV;
@property(nonatomic, strong) PicToolBar *toolbar;

@end

@implementation PicCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.picImageV = [ViewFactory getImageView:CGRectZero image:nil];
        _picImageV.contentMode = UIViewContentModeScaleAspectFill;
        self.picImageV.backgroundColor = grayLineColor;
        [self.contentView addSubview:self.picImageV];
        self.toolbar = [[PicToolBar alloc] initWithFrame:CM(self.frameSizeWidth - 10, self.frameSizeHeight - 20, 0, 20)];
        self.toolbar.backgroundColor = ClearColor;
        self.toolbar.textColor = ColorWithHex(0xffffff);
        [self.contentView addSubview:self.toolbar];
        [self.toolbar addItem:getImage(@"pic_heart") handle:^(UIButton *btn, NSInteger index) {
            
        }];
        
        [self.toolbar addItem:getImage(@"home_comment") handle:^(UIButton *btn, NSInteger index) {
            
        }];
    }
    return self;
}

- (void)setObject:(AlbumModel *)object {
    if (_object != object) {
        _object = object;
    }
    PhotoItem *item = [object.photos firstObject];
    if (item) {
        [self.picImageV sd_setImageWithURL:[NSURL URLWithString:item.url]];
    }else{
        self.picImageV.image = nil;
    }
    [self.toolbar setCount:object.favoriteCount atIndex:0];
    [self.toolbar setCount:object.commentCount atIndex:1];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.picImageV.image = nil;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.contentView.frame = self.bounds;
    self.picImageV.frame = self.bounds;
    self.toolbar.frame = CM(self.frameSizeWidth - 10, self.frameSizeHeight - 20, 0, 20);
}

@end

@interface HotPicCell ()

@property(nonatomic, strong) UIImageView *picImageV;
@property(nonatomic, strong) UILabel *contentLabel;
@property(nonatomic, strong) UILabel *writerLabel;
@property(nonatomic, strong) UILabel *localLabel;

@end

@implementation HotPicCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.picImageV = [ViewFactory getImageView:CGRectZero image:nil];
        _picImageV.contentMode = UIViewContentModeScaleAspectFill;
        self.contentLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:WhiteColor text:nil];
//        self.writerLabel = [ViewFactory getLabel:CGRectZero font:Font(10) textColor:ColorWithHex(0x999999) text:nil];
        self.localLabel = [ViewFactory getLabel:CGRectZero font:Font(12) textColor:ColorWithHex(0x999999) text:nil];
        _contentLabel.backgroundColor = ClearColor;
        _localLabel.backgroundColor = ClearColor;
        [self.contentView addSubview:self.picImageV];
        [self.contentView addSubview:self.contentLabel];
//        [self.contentView addSubview:self.writerLabel];
        [self.contentView addSubview:self.localLabel];
    }
    return self;
}

- (void)setObject:(AlbumModel *)object {
    if (_object != object) {
        _object = object;
    }
    PhotoItem *item = [object.photos firstObject];
    if (item) {
        [self.picImageV sd_setImageWithURL:[NSURL URLWithString:item.url]];
    }else{
        self.picImageV.image = nil;
    }
    self.contentLabel.text = object.title;
    NSMutableString *str = [NSMutableString string];
    if ([StringUtil isNONil:object.country]) {
        [str appendString:object.country];
        if ([StringUtil isNONil:object.city]) {
            [str appendString:@"·"];
            [str appendString:object.city];
        }
    }else{
        if ([StringUtil isNONil:object.city]) {
            [str appendString:object.city];
        }
    }

    self.localLabel.text = str;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.picImageV.frame = self.bounds;
    CGFloat len = [StringUtil getLabelLength:self.contentLabel.text font:self.contentLabel.font height:18];
    self.contentLabel.frame = CM(5, self.frameSizeHeight - 30, len, 18);
//    self.writerLabel.frame = CM(0, CGRectGetMaxY(self.contentLabel.frame), self.frameSizeWidth, 10);
    self.localLabel.frame = CM(CGRectGetMaxX(self.contentLabel.frame) + 12,self.contentLabel.frameOriginY, self.frameSizeWidth - CGRectGetMaxX(self.contentLabel.frame) - 20 , self.contentLabel.frameSizeHeight);
}

@end

@interface PhotoPicViewHeader ()

@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) UILabel *titleLabel;

@end

@implementation PhotoPicViewHeader

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.imageView = [ViewFactory getImageView:CGRectZero image:getImage(@"pic_line_vertical")];
        [self addSubview:self.imageView];
        self.titleLabel = [ViewFactory getLabel:CGRectZero font:Font(15) textColor:ColorWithHex(0x666666) text:nil];
        [self addSubview:self.titleLabel];
    }
    return self;
}

- (void)setTitle:(NSString *)title {
    if (_title != title) {
        _title = title;
    }
    self.titleLabel.text = _title;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CM(17, (self.frameSizeHeight - 12)/2.0f, 2, 12);
    self.titleLabel.frame = CM(CGRectGetMaxX(self.imageView.frame) + 5, 0, self.frameSizeWidth - self.imageView.frameSizeWidth - 17 - 5 - 10, self.frameSizeHeight);
}

@end


@interface PicOnShelfCell ()
@property(nonatomic, strong) UIImageView *picImageV;
@property(nonatomic, strong) UILabel     *titleLabel;
@property(nonatomic, strong) UIImageView *rmbSymbol;
@property(nonatomic, strong) UILabel     *priceLabel;

@end


@implementation PicOnShelfCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.picImageV = [ViewFactory getImageView:CGRectZero image:nil];
        self.picImageV.backgroundColor = grayLineColor;
        self.titleLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x000000) text:nil];
        self.rmbSymbol = [ViewFactory getImageView:CGRectZero image:getImage(@"pic_line_vertical")];
        self.priceLabel = [ViewFactory getLabel:CGRectZero font:Font(12) textColor:ColorWithHex(0x959595) text:nil];
        [self addSubview:self.picImageV];
        [self addSubview:self.titleLabel];
        [self addSubview:self.rmbSymbol];
        [self addSubview:self.priceLabel];
        
    }
    return self;
}

- (void)setObject:(id)object {
    if (_object != object) {
        _object = object;
    }
    self.titleLabel.text = @"一个人的旅行";
    self.priceLabel.text = @"223";
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.picImageV.frame = CM(0, 0, self.frameSizeWidth, self.frameSizeWidth + 10);
    self.titleLabel.frame = CM(0, CGRectGetMaxY(self.picImageV.frame), self.frameSizeWidth, 20);
    self.rmbSymbol.frame = CM(0, CGRectGetMaxY(self.titleLabel.frame), 10, 15);
    self.priceLabel.frame = CM(CGRectGetMaxX(self.rmbSymbol.frame), self.rmbSymbol.frameOriginY, self.frameSizeWidth - CGRectGetMaxX(self.rmbSymbol.frame) - 10, self.rmbSymbol.frameSizeHeight);
}
@end
