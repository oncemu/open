//
//  AlbumMakeView.h
//  Open
//
//  Created by mfp on 17/10/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumMakeObject.h"

@interface AlbumMakeView : UIView

@end

@interface LeftToolBar : UIView

@property(nonatomic, copy) void (^clickBlock)(NSInteger index);

- (instancetype)initWithFrame:(CGRect)frame titles:(NSArray *)titles;

@end

@interface EditToolBar : UIView

@property(nonatomic, copy) void (^actionBlock)(NSInteger index);

- (instancetype)initWithFrame:(CGRect)frame imageName:(NSString *)name;

- (void)hideButton:(BOOL)show index:(NSInteger)index;

@end

@interface BaseToolView : UIView
- (void)show;
- (void)hide;
@end


//模板列表
@interface FormListView : BaseToolView
@property(nonatomic, copy) void (^formSelect)(AlbumFormatMode mode);

- (instancetype)initWithFrame:(CGRect)frame type:(AlbumFormatType)type;

@end

//底色列表
@interface ColorListView : BaseToolView
@property(nonatomic, copy) void (^colorSelect)(UIColor *color);

@end

//元素列表  估计是贴图
@interface ElementView : BaseToolView
@property(nonatomic, strong) NSMutableArray *elements;
@property(nonatomic, copy) void (^elementSelect)(UIImage *image);

@end

