//
//  MyAlbumView.m
//  Open
//
//  Created by mfp on 17/10/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MyAlbumView.h"

@implementation MyAlbumView


@end


@interface MyAlbumCell ()
@property(nonatomic, strong) UIImageView *albumImage;
@property(nonatomic, strong) UILabel *pageLabel1;
@property(nonatomic, strong) UILabel *pageLabel2;

@end

@implementation MyAlbumCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = ColorWithHex(0xeeeeee);
        _albumImage = [[UIImageView alloc] init];
        _albumImage.backgroundColor = WhiteColor;
        _albumImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_albumImage];
        
        _pageLabel1 =  [ViewFactory getLabel:CGRectZero font:Font(12) textColor:ColorWithHex(0x666666) text:nil];
        _pageLabel1.textAlignment = NSTextAlignmentCenter;
        _pageLabel1.backgroundColor = ColorWithHex(0xeeeeee);
        [self.contentView addSubview:_pageLabel1];
        
        _pageLabel2 =  [ViewFactory getLabel:CGRectZero font:Font(12) textColor:ColorWithHex(0x666666) text:nil];
        _pageLabel2.backgroundColor = ColorWithHex(0xeeeeee);
        _pageLabel2.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_pageLabel2];
    }
    return self;
}

- (void)setPageIndex:(NSInteger)pageIndex {
    _pageIndex = pageIndex;
    if (pageIndex > 0) {
        self.pageLabel1.text = [NSString stringWithFormat:@"%ld页",pageIndex * 2 - 1];
        self.pageLabel2.text = [NSString stringWithFormat:@"%ld页",pageIndex * 2];
    }else{
        self.pageLabel1.text = @"封面";
        self.pageLabel2.text = @"封面";
    }
}

- (void)setImage:(UIImage *)image {
    _image = image;
    if (image) {
        self.albumImage.image = image;
    }else{
        self.albumImage.image = getImage(@"make_defaultcell");
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.albumImage.frame = CM(0, 0, self.frameSizeWidth, self.frameSizeHeight - 20);
    self.pageLabel1.frame = CM(0, self.frameSizeHeight - 20, self.frameSizeWidth/2, 20);
    self.pageLabel2.frame = CM(self.frameSizeWidth/2, self.frameSizeHeight - 20, self.frameSizeWidth/2, 20);
}

@end
