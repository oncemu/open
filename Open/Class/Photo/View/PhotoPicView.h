//
//  PhotoPicView.h
//  Open
//
//  Created by mfp on 17/7/12.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumModel.h"

@interface PhotoPicView : UIView

@end

@interface HotCell : UICollectionViewCell

@property(nonatomic, strong) NSMutableArray *list;
@property(nonatomic, copy) void (^selectBlock)(NSInteger index);

@end

@interface PicCell : UICollectionViewCell

@property(nonatomic, strong) AlbumModel *object;

@end

@interface HotPicCell : UICollectionViewCell

@property(nonatomic, strong) AlbumModel *object;

@end

@interface PhotoPicViewHeader : UICollectionReusableView

@property(nonatomic, copy) NSString *title;

@end

@interface PicOnShelfCell : UICollectionViewCell

@property(nonatomic, strong) id object;

@end
