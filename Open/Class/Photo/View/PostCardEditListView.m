//
//  PostCardEditListView.m
//  Open
//
//  Created by mfp on 17/10/14.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PostCardEditListView.h"

@implementation PostCardEditListView


@end

@interface PostCartCell ()
@property(nonatomic, strong) UIButton *deleteBtn;

@end

@implementation PostCartCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = ColorWithHex(0xeeeeee);
        self.cardView = [[UIImageView alloc] init];
        self.cardView.layer.cornerRadius = 5;
        self.cardView.layer.masksToBounds = YES;
        self.cardView.backgroundColor = WhiteColor;
        self.cardView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.cardView];
        [self.cardView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
        
        self.deleteBtn = [ViewFactory getButton:CGRectZero selectImage:getImage(@"edit_delete") norImage:getImage(@"edit_delete") target:self action:@selector(deleteImage:)];
        self.deleteBtn.layer.cornerRadius = 15;
        self.deleteBtn.layer.masksToBounds = YES;
        self.deleteBtn.backgroundColor = [UIColor colorWithHex:0xffffff alpha:0.7];
        [self.contentView addSubview:self.deleteBtn];
        [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.equalTo(self.contentView).offset(-10);
            make.size.mas_equalTo(Size(30, 30));
        }];
    }
    return self;
}

- (void)deleteImage:(UIButton *)sender {
    if (self.deleteBlock) {
        self.deleteBlock();
    }
}

@end

@implementation PostCartHeader

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = ColorWithHex(0xeeeeee);
        self.tipLabel = [ViewFactory getLabel:CGRectZero font:Font(13) textColor:ColorWithHex(0x999999) text:@"#点击照片进行编辑#"];
        self.tipLabel.backgroundColor = ColorWithHex(0xeeeeee);
        [self addSubview:self.tipLabel];
        self.tipLabel.textAlignment = NSTextAlignmentCenter;
        [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}

@end

@interface PostCartBottomBar ()
@property(nonatomic, strong) UIButton *addBtn;
@property(nonatomic, strong) UILabel *countLabel;
@property(nonatomic, strong) UIButton *nextBtn;

@end

@implementation PostCartBottomBar

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.addBtn = [ViewFactory getButton:CGRectZero font:Font(15) selectTitle:@"添加照片" norTitle:@"添加照片" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(addImage)];
        [self addSubview:self.addBtn];
        
        self.countLabel = [ViewFactory getLabel:CGRectZero font:Font(13) textColor:ColorWithHex(0x333333) text:nil];
        self.countLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:self.countLabel];
        
        self.nextBtn = [ViewFactory getButton:CGRectZero font:Font(15) selectTitle:@"下一步" norTitle:@"下一步" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(next)];
        self.nextBtn.backgroundColor = BtnLemonYellow;
        [self addSubview:self.nextBtn];
        
        [self.addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(20);
            make.centerY.equalTo(self.mas_centerY);
            make.size.mas_equalTo(Size(65, 25));
        }];
        
        [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.equalTo(self);
            make.width.mas_equalTo(@100);
        }];
        
        [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.nextBtn);
            make.right.equalTo(self.nextBtn.mas_left).offset(-10);
            make.size.mas_equalTo(Size(120, 20));
        }];
    }
    return self;
}

- (void)setCount:(NSInteger)count {
    _count = count;
    if (count > 0) {
        self.countLabel.text = [NSString stringWithFormat:@"已选%ld张",(long)count];
    }else{
        self.countLabel.text = @"";
    }
}

- (void)addImage {
    if (self.addBlock) {
        self.addBlock();
    }
}

- (void)next {
    if (self.nextBlock) {
        self.nextBlock();
    }
}
@end
