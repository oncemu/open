//
//  AlbumMakeView.m
//  Open
//
//  Created by mfp on 17/10/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "AlbumMakeView.h"


@implementation AlbumMakeView


@end

@interface LeftToolBar ()

@end

@implementation LeftToolBar

- (instancetype)initWithFrame:(CGRect)frame titles:(NSArray *)titles
{
    self = [super initWithFrame:frame];
    if (self) {
        for (NSInteger i = 0; i<titles.count; i++) {
            UIImage *image = getImage(titles[i]);
            UIButton *btn = [ViewFactory getButton:CM(0, i * (frame.size.width + 10), frame.size.width, frame.size.width) selectImage:image norImage:image target:self action:@selector(buttonClicked:)];
            [btn setTitleColor:ColorWithHex(0x999999) forState:UIControlStateNormal];
            btn.titleLabel.font = Font(12);
            [btn setTitle:titles[i] forState:UIControlStateNormal];
            btn.imageEdgeInsets = UIEdgeInsetsMake(-11, 10, 11, -10);
            btn.titleEdgeInsets = UIEdgeInsetsMake(11, -20, -11, 0);
            btn.tag = i;
            [self addSubview:btn];
        }
    
    }
    return self;
}

- (void)buttonClicked:(UIButton *)button {
    if (self.clickBlock) {
        self.clickBlock(button.tag);
    }
}

@end


@implementation EditToolBar

- (instancetype)initWithFrame:(CGRect)frame imageName:(NSString *)name
{
    self = [super initWithFrame:frame];
    if (self) {
        for (NSInteger i = 0; i<4; i++) {
            NSString *imgName = [NSString stringWithFormat:@"%@%ld",name,i];
            UIButton *btn  = [ViewFactory getButton:CM(i*(44 + 30), 0, 44, 44) selectImage:getImage(imgName) norImage:getImage(imgName) target:self action:@selector(toolAction:)];
            btn.tag = i;
            [self addSubview:btn];
        }
    }
    return self;
}

- (void)toolAction:(UIButton *)sender {
    if (self.actionBlock) {
        self.actionBlock(sender.tag);
    }
}

- (void)hideButton:(BOOL)hide index:(NSInteger)index {
    UIButton *btn = [self viewWithTag:index];
    btn.hidden = hide;
}

- (CGSize)intrinsicContentSize {
    return UILayoutFittingExpandedSize;
}

@end

@interface BaseToolView()<UIGestureRecognizerDelegate>
{
    UITapGestureRecognizer *hideTap;
}
@end
@implementation BaseToolView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.userInteractionEnabled = YES;
        hideTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)];
        hideTap.delegate = self;
    }
    return self;
}


- (void)show {
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    [window addGestureRecognizer:hideTap];
    [window addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        self.frameOriginX = SCREEN_WIDTH - self.frameSizeWidth;
    }];
}

- (void)hide {
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    [window removeGestureRecognizer:hideTap];
    [UIView animateWithDuration:0.3 animations:^{
        self.frameOriginX = SCREEN_WIDTH;
    }];
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    NSString *supClass = NSStringFromClass([touch.view.superview.superview class]);
    if ([supClass isEqualToString:@"UICollectionView"]) {
        return NO;
    }
    return YES;
}
@end

//模板列表
@interface FormListView ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, assign) AlbumFormatType type;
@end

@implementation FormListView

- (instancetype)initWithFrame:(CGRect)frame type:(AlbumFormatType)type {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.type = type;
        self.tableView = [[UITableView alloc] initWithFrame:CM(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.contentInset = UIEdgeInsetsMake(NavigationBar_HEIGHT, 0, 0, 0);
        [self addSubview:_tableView];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = 60;
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
        _tableView.tableFooterView = [UIView new];
    }
    return self;
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (self.type == AlbumFormatType_10_Asset)?6:AlbumFormatMode_blank + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIImageView *formImageV = [cell.contentView viewWithTag:33];
    if (formImageV == nil) {
        formImageV = [[UIImageView alloc] initWithFrame:CM((self.frameSizeWidth - 97)/2, 6, 97, 47)];
        formImageV.tag = 33;
        [cell.contentView addSubview:formImageV];
    }
    NSString *formName = [NSString stringWithFormat:@"make_formwork%ld%ld",self.type,(long)indexPath.row];
    formImageV.image = getImage(formName);
    formImageV.backgroundColor = grayLineColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.formSelect) {
        self.formSelect(indexPath.row);
    }
}

@end

//底色列表
@interface ColorListView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) NSArray *colorList;
@end

@implementation ColorListView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.colorList = @[
ColorWithHex(0xf3c0c7),  ColorWithHex(0xb34c5d),   ColorWithHex(0xbe6b67),    ColorWithHex(0x64373c),
ColorWithHex(0x964d47),  ColorWithHex(0xb4aea0),   ColorWithHex(0xdcd3b2),    ColorWithHex(0xb2aca0),
ColorWithHex(0xf5d389),  ColorWithHex(0x88ced6 ),  ColorWithHex(0xf4be44),    ColorWithHex(0xf8bf0d),
ColorWithHex(0xfaae1d),  ColorWithHex(0xde943f),   ColorWithHex(0xa2d9dc),    ColorWithHex(0x298884),
ColorWithHex(0x33a9db),  ColorWithHex(0x388cb1),   ColorWithHex(0x258aa8),    ColorWithHex(0x098ba5),
ColorWithHex(0x978fa4),  ColorWithHex(0x715b67),   ColorWithHex(0x5f414b),   ColorWithHex(0xd3ddd5),
ColorWithHex(0xa89e94),  ColorWithHex(0x8e7aaf),   ColorWithHex(0xffffff)];
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 7;
        layout.minimumInteritemSpacing = 7;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.sectionInset = UIEdgeInsetsMake(NavigationBar_HEIGHT, 10, 10, 10);
        layout.itemSize = Size(34, 34);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
        _collectionView.backgroundColor = WhiteColor;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [self addSubview:_collectionView];
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
    }
    return self;
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.colorList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class]) forIndexPath:indexPath];
    UIColor *color = self.colorList[indexPath.item];
    cell.contentView.backgroundColor = color;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.colorSelect) {
        UIColor *color = self.colorList[indexPath.item];
        self.colorSelect(color);
    }
}


@end

//元素贴图列表
@interface ElementView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic, strong) UICollectionView *collectionView;

@end

@implementation ElementView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.sectionInset = UIEdgeInsetsMake(NavigationBar_HEIGHT, 20, 10, 20);
        layout.itemSize = Size(50, 50);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CM(0, 0, frame.size.width, frame.size.height) collectionViewLayout:layout];
        _collectionView.backgroundColor = WhiteColor;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [self addSubview:_collectionView];
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
    }
    return self;
}

- (void)setElements:(NSMutableArray *)elements {
    if (_elements == nil) {
        _elements = [NSMutableArray array];
    }
    [_elements removeAllObjects];
    [_elements addObjectsFromArray:elements];
    [self.collectionView reloadData];
}
#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.elements.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class]) forIndexPath:indexPath];
    UIImageView *imageView = [cell.contentView viewWithTag:34];
    NSString *imageName = [NSString stringWithFormat:@"make_element%ld",indexPath.item];
    UIImage *image = getImage(imageName);
    if (imageView == nil) {
        cell.contentView.backgroundColor = grayLineColor;
        imageView = [[UIImageView alloc] initWithFrame: CM(1, 1, 48, 48)];
        imageView.backgroundColor = WhiteColor;
        imageView.tag = 34;
        [cell.contentView addSubview:imageView];
    }
    NSString *imgURLStr = self.elements[indexPath.item];
    [imageView sd_setImageWithURL:[NSURL URLWithString:imgURLStr] placeholderImage:image];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *imageName = [NSString stringWithFormat:@"make_element%ld",indexPath.item];
    UIImage *image = getImage(imageName);
    NSData *data = UIImagePNGRepresentation(image);
    image = [UIImage imageWithData:data];
    if (self.elementSelect) {
        self.elementSelect(image);
    }
}

@end

