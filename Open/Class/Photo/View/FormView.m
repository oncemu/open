//
//  FormView.m
//  Open
//
//  Created by mfp on 17/10/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "FormView.h"

@interface FormView ()
@property(nonatomic, strong) UIView *line1;
@property(nonatomic, strong) UIView *line2;
@end

@implementation FormView

@synthesize undoManager = _undoManager;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = ColorWithHex(0xffd900).CGColor;
        self.layer.borderWidth = 2;
        self.layer.masksToBounds = YES;
        self.mundoManager = [MUndoManager sharedInstance];
        [self.mundoManager.manager removeAllActions];
        
        _contentView = [[UIView alloc] initWithFrame:CM(0, 0, frame.size.width, frame.size.height)];
        _contentView.layer.masksToBounds = YES;
        [self addSubview:_contentView];
        self.line1 = [self getLine:CM(frame.size.width/2 - 24, 0, 2, frame.size.height)];
        self.line2 = [self getLine:CM(frame.size.width/2 + 24, 0, 2, frame.size.height)];
    }
    return self;
}

- (void)setObject:(AlbumMakeObject *)object {
    _object = object;
    self.contentView.backgroundColor = object.backColor;
    [object.contentList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BaseAlbumObject *object = (BaseAlbumObject *)obj;
        [self getNewView:object];
    }];
    self.line1.hidden = self.line2.hidden = ((self.object.pageIndex != 0) || object.type != AlbumFormatType_10_Asset);
}

- (void)setBaseColor:(UIColor *)baseColor {
    [[self.mundoManager.manager prepareWithInvocationTarget:self] setBaseColor:self.contentView.backgroundColor];
    self.contentView.backgroundColor = baseColor;
    self.object.backColor = baseColor;
}

- (void)addView:(BaseAlbumObject *)obj {
    if (obj.image) {
        [self addImageEditView:obj];
    }else {
        [self addTextView:obj];
    }
}

- (UIImage *)getAlubmImage {
    UIGraphicsBeginImageContextWithOptions(self.frameSize, YES, [UIScreen mainScreen].scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

//新增选图
- (void)addImageEditView:(BaseAlbumObject *)obj {
    [self.object addObject:obj];
    ImageEditView *editView = (ImageEditView *)[self getNewView:obj];
    editView.selected = YES;
    [self imageTap:editView];
    [[self.mundoManager.manager prepareWithInvocationTarget:self] undoAddView:editView];
}

//新增文字
- (void)addTextView:(BaseAlbumObject *)object {
    [self.object addObject:object];
    TextEditView *editView = (TextEditView *)[self getNewView:object];
    editView.selected = YES;
    [self imageTap:editView];
    [[self.mundoManager.manager prepareWithInvocationTarget:self] undoAddView:editView];
}

//撤销新增视图
- (void)undoAddView:(BaseEditView *)editView {
    [[self.mundoManager.manager prepareWithInvocationTarget:self] redoAddView:editView];
    if ([self.object.contentList containsObject:editView.object]) {
        [self.object removeObject:editView.object];
    }
    editView.selected = NO;
    [editView removeFromSuperview];
}

//恢复撤销新增视图
- (void)redoAddView:(BaseEditView *)editView {
    [[self.mundoManager.manager prepareWithInvocationTarget:self] undoAddView:editView];
    [self.contentView addSubview:editView];
    editView.selected = editView.selected;
    [self.object addObject:editView.object];
}

- (BaseEditView *)getNewView:(BaseAlbumObject *)object {
    WS(weakSelf);
    BaseEditView *editView = nil;
    if (object.text) {
        editView = [[TextEditView alloc] initWithObject:object];
    }else {
        editView = [[ImageEditView alloc] initWithObject:object];
    }
    editView.removeBlock = ^(BaseEditView *editView) {
        [[weakSelf.mundoManager.manager prepareWithInvocationTarget:weakSelf] redoAddView:editView];
        if ([weakSelf.object.contentList containsObject:object]) {
            [weakSelf.object removeObject:object];
        }
    };
    editView.tapBlock = ^(BaseEditView *view) {
        [weakSelf imageTap:view];
    };
    editView.doubleTapBlock = ^(BaseEditView *view) {
        if (weakSelf.doubleTap) {
            weakSelf.doubleTap(view);
        }
    };
    [self.contentView addSubview:editView];
    return editView;
}

- (void)imageTap:(BaseEditView *)view {
    if (![self.editView isEqual:view]) {
        self.editView.selected = NO;
        self.editView = view;
    }
    if (self.editBlock) {
        self.editBlock(self.editView);
    }
}

- (UIView *)getLine:(CGRect)rect {
    UIView *line = [[UIView alloc] initWithFrame:rect];
    line.backgroundColor = ColorWithHex(0xffd900);
    [_contentView addSubview:line];
    return line;
}

@end


@implementation BaseEditView

@synthesize canScale = _canScale,canRotate = _canRotate;

- (instancetype)initWithObject:(BaseAlbumObject *)object {
    self = [super initWithFrame:object.frameValue.CGRectValue];
    if (self) {
        self.userInteractionEnabled = YES;
        self.layer.borderWidth = 2;
        self.layer.borderColor = ClearColor.CGColor;
        self.layer.masksToBounds = YES;
        self.backgroundColor = ClearColor;
        self.mundoManager = [MUndoManager sharedInstance];
        _object = object;
        if (object.type == AlbumObjectTypeText) {
            [self textLabel];
        }else {
            [self imageView];
        }
        
        //单击手势
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeEditStatus)];
        singleTap.numberOfTapsRequired = 1;

        // 旋转手势
        UIRotationGestureRecognizer *rotationGestureRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotateView:)];
        
        // 缩放手势
        UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchView:)];
        
        // 拖动手势
        UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panView:)];
        
        if (object.type == AlbumObjectTypeForm) {
            [self.imageView addGestureRecognizer:singleTap];
            [self.imageView addGestureRecognizer:rotationGestureRecognizer];
            [self.imageView addGestureRecognizer:pinchGestureRecognizer];
            [self.imageView addGestureRecognizer:panGestureRecognizer];
        }else{
            [self addGestureRecognizer:singleTap];
            [self addGestureRecognizer:rotationGestureRecognizer];
            [self addGestureRecognizer:pinchGestureRecognizer];
            [self addGestureRecognizer:panGestureRecognizer];
        }
        if (object.type == AlbumObjectTypeText) {
            //双击手势
            UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapGesture:)];
            doubleTap.numberOfTapsRequired = 2;
            [self addGestureRecognizer:doubleTap];
        }
        
    }
    return self;
}

- (void)changeEditStatus {
    self.selected = !self.selected;
}

- (void)setCanRotate:(BOOL)canRotate {
    _canRotate = canRotate;
    _canScale = NO;
}

- (void)setCanScale:(BOOL)canScale {
    _canScale = canScale;
    _canRotate = NO;
}

//删除自身
- (void)removeSelf {
    if (self.removeBlock) {
        self.removeBlock(self);
        [self removeFromSuperview];
    }
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
    self.layer.borderColor = selected ? ColorWithHex(0x333333).CGColor : ClearColor.CGColor;
    if (self.tapBlock) {
        self.tapBlock(self);
    }
}

#pragma mark - 手势 与 撤销恢复操作
//双击
- (void)doubleTapGesture:(UITapGestureRecognizer *)gesutre {
    if (self.doubleTapBlock) {
        self.doubleTapBlock(self);
    }
}

// 处理旋转手势
- (void)rotateView:(UIRotationGestureRecognizer *)rotationGestureRecognizer {
    if (!self.canRotate) {
        return;
    }
    UIView *view = rotationGestureRecognizer.view;
    if (rotationGestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [[self.mundoManager.manager prepareWithInvocationTarget:self] undoGestureAction:view.transform center:view.center];
    }
    if (rotationGestureRecognizer.state == UIGestureRecognizerStateBegan || rotationGestureRecognizer.state ==UIGestureRecognizerStateChanged) {
        view.transform = CGAffineTransformRotate(view.transform, rotationGestureRecognizer.rotation);
        [rotationGestureRecognizer setRotation:0];
    }
    if (rotationGestureRecognizer.state == UIGestureRecognizerStateEnded || rotationGestureRecognizer.state == UIGestureRecognizerStateCancelled) {
        self.object.transform = view.transform;
    }
}

//处理缩放手势
- (void)pinchView:(UIPinchGestureRecognizer *)pinchGestureRecognizer {
    if (!self.canScale) {
        return;
    }
    UIView *view = pinchGestureRecognizer.view;
    if (pinchGestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [[self.mundoManager.manager prepareWithInvocationTarget:self] undoGestureAction:view.transform center:view.center];
    }
    if (pinchGestureRecognizer.state == UIGestureRecognizerStateBegan || pinchGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        view.transform = CGAffineTransformScale(view.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
        pinchGestureRecognizer.scale = 1;
    }
    if (pinchGestureRecognizer.state == UIGestureRecognizerStateEnded || pinchGestureRecognizer.state == UIGestureRecognizerStateCancelled) {
        self.object.transform = view.transform;
    }
}

//处理拖拉手势
- (void)panView:(UIPanGestureRecognizer *)panGestureRecognizer {
    if (!self.selected || (!_imageView.image && self.object.type == AlbumObjectTypeForm)) {
        return;
    }
    UIView *view = panGestureRecognizer.view;
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [[self.mundoManager.manager prepareWithInvocationTarget:self] undoGestureAction:view.transform center:view.center];
    }
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan || panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [panGestureRecognizer translationInView:view.superview];
        view.center = Point(view.center.x + translation.x, view.center.y + translation.y);
        [panGestureRecognizer setTranslation:CGPointZero inView:view.superview];
    }
    if (panGestureRecognizer.state == UIGestureRecognizerStateCancelled || panGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        self.object.center = view.center;
    }
}

//处理缩放，旋转，拖拽的撤销
- (void)undoGestureAction:(CGAffineTransform)transform center:(CGPoint)center {
    if (self.object.type == AlbumObjectTypeForm) {
        [[self.mundoManager.manager prepareWithInvocationTarget:self] redoGestureAction:self.imageView.transform center:self.imageView.center];
    }else{
        [[self.mundoManager.manager prepareWithInvocationTarget:self] redoGestureAction:self.transform center:self.center];
    }
    [self setViewStatus:transform center:center];
}

//恢复缩放，旋转，拖拽的撤销
- (void)redoGestureAction:(CGAffineTransform)transform center:(CGPoint)center {
    if (self.object.type == AlbumObjectTypeForm) {
        [[self.mundoManager.manager prepareWithInvocationTarget:self] undoGestureAction:self.imageView.transform center:self.imageView.center];
    }else{
        [[self.mundoManager.manager prepareWithInvocationTarget:self] undoGestureAction:self.transform center:self.center];
    }
    [self setViewStatus:transform center:center];
}

- (void)setViewStatus:(CGAffineTransform)transform center:(CGPoint)center {
    if (self.object.type == AlbumObjectTypeForm) {
        self.imageView.transform = transform;
        self.imageView.center = center;
    }else{
        self.transform = transform;
        self.center = center;
    }
    self.object.transform = self.transform;
    self.object.center = center;
}

//get method
- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CM(2, 2, self.frameSizeWidth - 4, self.frameSizeHeight - 4)];
        _imageView.backgroundColor = ClearColor;
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        _imageView.userInteractionEnabled = YES;
        [self addSubview:_imageView];
    }
    return _imageView;
}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] initWithFrame:CM(2, 2, self.frameSizeWidth - 4, self.frameSizeHeight - 4)];
        _textLabel.backgroundColor = ClearColor;
        _textLabel.numberOfLines = 0;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self addSubview:_textLabel];
    }
    return _textLabel;
}
@end

@implementation ImageEditView

- (instancetype)initWithObject:(BaseAlbumObject *)object {
    self = [super initWithObject:object];
    if (self) {
        self.image = object.image;
        if (object.type == AlbumObjectTypeForm) {
            self.imageView.transform = object.transform;
            self.imageView.center = object.center;
        }else{
            self.transform = object.transform;
            self.center = object.center;
        }
    }
    return self;
}

- (void)setImage:(UIImage *)image {
    _image = image;
    self.imageView.image = image;
    self.object.image = image;
    self.imageView.backgroundColor = image ? ClearColor : ColorWithHex(0x999999);
    if (!image) {
        self.imageView.transform = CGAffineTransformIdentity;
        self.imageView.frame = CM(2, 2, self.frameSizeWidth - 4, self.frameSizeHeight - 4);
        self.object.center = Point(self.frameSizeWidth/2, self.frameSizeHeight/2);
    }
}

- (void)removeSelf {
    if (!self.image) {//已经没图了就不用删了
        return;
    }
    if (self.object.type == AlbumObjectTypeForm) {
        [self deleteImage];
    }else {
        [super removeSelf];
    }
}

//删除图片
- (void)deleteImage {
    [[self.mundoManager.manager prepareWithInvocationTarget:self] undoDeleteImage:self.image];
    self.image = nil;
}

//撤销删除图片
- (void)undoDeleteImage:(UIImage *)img {
    [[self.mundoManager.manager prepareWithInvocationTarget:self] deleteImage];
    self.image = img;
}

//换图
- (void)changeImage:(UIImage *)img {
    [[self.mundoManager.manager prepareWithInvocationTarget:self] undoChangeImage:self.image];
    self.image = img;
}

//取消更换图片
- (void)undoChangeImage:(UIImage *)img {
    [[self.mundoManager.manager prepareWithInvocationTarget:self] changeImage:self.image];
    self.image = img;
}

@end

@implementation TextEditView

- (instancetype)initWithObject:(BaseAlbumObject *)object {
    self = [super initWithObject:object];
    if (self) {
        self.textLabel.text = object.text;
        self.textLabel.textColor = object.textColor;
        self.textLabel.font = object.font;
        self.transform = object.transform;
        self.center = object.center;
    }
    return self;
}

@end

