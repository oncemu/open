//
//  PostCardEditListView.h
//  Open
//
//  Created by mfp on 17/10/14.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostCardEditListView : UIView

@end

@interface PostCartCell : UICollectionViewCell
@property(nonatomic, strong) UIImageView *cardView;
@property(nonatomic, copy) void (^deleteBlock)();
@end

@interface PostCartHeader : UICollectionReusableView
@property(nonatomic, strong) UILabel *tipLabel;

@end

@interface PostCartBottomBar : UIView
@property(nonatomic, copy) void (^addBlock)();
@property(nonatomic, copy) void (^nextBlock)();
@property(nonatomic, assign) NSInteger count;
@end
