//
//  MyAlbumView.h
//  Open
//
//  Created by mfp on 17/10/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAlbumView : UIView

@end

@interface MyAlbumCell : UICollectionViewCell
@property(nonatomic, assign) NSInteger pageIndex;
@property(nonatomic, strong) UIImage *image;
@end
