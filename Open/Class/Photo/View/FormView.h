//
//  FormView.h
//  Open
//
//  Created by mfp on 17/10/16.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumMakeObject.h"
#import "MUndoManager.h"

@class ImageEditView;
@class BaseEditView;

@interface FormView : UIView

@property(nonatomic, weak) AlbumMakeObject *object;
@property(nonatomic, strong) UIView *contentView;
@property(nonatomic, weak) BaseEditView *editView;//当前编辑的view
@property(nonatomic, strong) UIColor *baseColor;
@property(nonatomic, strong) MUndoManager *mundoManager;

@property(nonatomic, copy) void (^editBlock)(BaseEditView *editView);
@property(nonatomic, copy) void (^doubleTap)(BaseEditView *editView);

- (void)addView:(BaseAlbumObject *)obj;

- (UIImage *)getAlubmImage;

@end

@interface BaseEditView : UIView
{
    BOOL _canRotate;
    BOOL _canScale;
}
@property(nonatomic, weak) BaseAlbumObject *object;
@property(nonatomic, assign) BOOL selected;//是否选中
@property(nonatomic, assign) BOOL canRotate;//是否可以旋转
@property(nonatomic, assign) BOOL canScale;//是否可以缩放
@property(nonatomic, strong) MUndoManager *mundoManager;

@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) UILabel *textLabel;

@property(nonatomic, copy) void (^tapBlock)(BaseEditView *view);
@property(nonatomic, copy) void (^doubleTapBlock)(BaseEditView *view);
@property(nonatomic, copy) void (^removeBlock)(BaseEditView *view);

- (instancetype)initWithObject:(BaseAlbumObject *)object;

- (void)removeSelf;

@end

//图片编辑模块
@interface ImageEditView : BaseEditView

@property(nonatomic, strong) UIImage *image;

//删除图片
- (void)deleteImage;

//换图
- (void)changeImage:(UIImage *)img;

@end

//文字编辑模块

@interface TextEditView : BaseEditView

@end
