//
//  OtherCenterView.m
//  Open
//
//  Created by mfp on 17/9/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "OtherCenterView.h"

@implementation OtherCenterView

@end

@interface OtherCenterViewHeader ()
@property(nonatomic, strong) UIImageView *bgView;
@end

@implementation OtherCenterViewHeader

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.bgView = [ViewFactory getImageView:CM(0, 0, frame.size.width, frame.size.height) image:getImage(@"rank3")];
        [self addSubview:_bgView];
    }
    return self;
}

@end

@interface OtherSegmentControl ()

@end

@implementation OtherSegmentControl

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = RedColor;
        
    }
    return self;
}

@end
