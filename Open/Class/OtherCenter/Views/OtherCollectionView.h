//
//  OtherCollectionView.h
//  Open
//
//  Created by mfp on 17/9/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "STCollectionView.h"

typedef NS_ENUM(NSUInteger, CollectionType) {
    CollectionTypeProduct = 0,
    CollectionTypeFavorite,
    CollectionTypePicture
};

@interface OtherCollectionView : STCollectionView

@property(nonatomic, assign) CollectionType type;
@property(nonatomic, weak) UIViewController *parentVC;

- (instancetype)initWithFrame:(CGRect)frame type:(CollectionType)type;

@end
