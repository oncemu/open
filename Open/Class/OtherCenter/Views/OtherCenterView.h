//
//  OtherCenterView.h
//  Open
//
//  Created by mfp on 17/9/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeTableView.h"
#import "User.h"


@interface OtherCenterView : UIView

@end

@interface OtherCenterViewHeader : STHeaderView

@property(nonatomic, strong) User *userMode;

@end

@interface OtherSegmentControl : UIView

@end


