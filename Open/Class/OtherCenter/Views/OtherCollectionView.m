//
//  OtherCollectionView.m
//  Open
//
//  Created by mfp on 17/9/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "OtherCollectionView.h"
#import "MJRefresh.h"
#import "SwipeTableView.h"

static NSString *othercellid = @"othercellid";

@interface OtherCollectionView ()<STCollectionViewDelegate,STCollectionViewDataSource>
@property(nonatomic, strong) NSMutableArray *dataList;

@end

@implementation OtherCollectionView
- (instancetype)initWithFrame:(CGRect)frame type:(CollectionType)type {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.type = type;
        [self commonInit];
    }
    return self;
}
- (void)commonInit {
    self.dataList = [NSMutableArray arrayWithObjects:@"",@"",@"",@"",@"",@"", nil];
    STCollectionViewFlowLayout * layout = self.st_collectionViewLayout;
    layout.minimumInteritemSpacing = 10;
    layout.minimumLineSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(50, 19, 0, 19);
    layout.itemSize = Size((SCREEN_WIDTH - 20 - 38)/3, ceil(((SCREEN_WIDTH - 20 - 38)/3 * 245) / 211.0));
    self.stDelegate = self;
    self.stDataSource = self;
    self.showsVerticalScrollIndicator = NO;
    [self registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:othercellid];

    WS(weakSelf);
    self.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadData:NO];
    }];
    self.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadData:YES];
    }];
    [self.mj_header beginRefreshing];
}

- (void)loadData:(BOOL)more {
    
}

#pragma mark - STCollectionViewDelegate,STCollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(STCollectionViewFlowLayout *)layout numberOfColumnsInSection:(NSInteger)section {
    return 3;
}

- (NSInteger)numberOfSectionsInStCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)stCollectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UICollectionViewCell *)stCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:othercellid forIndexPath:indexPath];
    cell.backgroundColor = YellowColor;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

}

@end
