//
//  OtherCenterController.h
//  Open
//
//  Created by mfp on 17/9/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseVC.h"

@interface OtherCenterVC : BaseVC

@property(nonatomic, assign) NSInteger userId;

@end
