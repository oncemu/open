//
//  OtherCenterController.m
//  Open
//
//  Created by mfp on 17/9/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "OtherCenterVC.h"
#import "SwipeTableView.h"
#import "OtherCenterView.h"
#import "OtherCollectionView.h"
#import "WRNavigationBar.h"

@interface OtherCenterVC ()<SwipeTableViewDelegate,SwipeTableViewDataSource>

@property(nonatomic, strong) SwipeTableView *swipeTableView;
@property(nonatomic, strong) OtherCenterViewHeader *headerView;
@property(nonatomic, strong) OtherSegmentControl   *segment;
@property(nonatomic, strong) OtherCollectionView *productView;
@property(nonatomic, strong) OtherCollectionView *favoriteView;
@property(nonatomic, strong) OtherCollectionView *pictureView;

@end

@implementation OtherCenterVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self wr_setNavBarBackgroundAlpha:0];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self wr_setNavBarBackgroundAlpha:1];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createSwipeTableView];
    [self setBarButtonItemWithImage:nil title:@"关注" isLeft:NO];
    [self setNavigationBarUI];
}

#pragma mark - SwipeTableViewDelegate,SwipeTableViewDataSource

- (NSInteger)numberOfItemsInSwipeTableView:(SwipeTableView *)swipeView {
    return 3;
}
- (UIScrollView *)swipeTableView:(SwipeTableView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIScrollView *)view {
    OtherCollectionView *wview = [self getWaterFallView:index];
    [self configRefreshHeaderForItem:wview];
    return wview;
}

- (void)swipeTableViewCurrentItemIndexDidChange:(SwipeTableView *)swipeView {
//    self.headerView.selectIndex = swipeView.currentItemIndex;
}

- (BOOL)swipeTableView:(SwipeTableView *)swipeTableView shouldPullToRefreshAtIndex:(NSInteger)index {
    return NO;
}

- (CGFloat)swipeTableView:(SwipeTableView *)swipeTableView heightForRefreshHeaderAtIndex:(NSInteger)index {
//    return MJRefreshHeaderHeight;
    return 0;
}

- (void)swipeHeaderBarFrameChanged:(SwipeTableView *)swipeView {
    CGFloat top = swipeView.swipeHeaderBar.frameOriginY;
    CGFloat alpha = 84.0 / top;
    if (top >= 320) {
        alpha = 0;
    }
    [self wr_setNavBarBackgroundAlpha:alpha];
}

#pragma mark - 刷新控件
- (void)configRefreshHeaderForItem:(UIScrollView *)itemView {
    itemView.mj_header = nil;
    itemView.mj_footer = nil;
//    MJRefreshHeader * header = itemView.mj_header;
//    header.frameOriginY = -header.frameSizeHeight;
}

#pragma mark - get method
- (OtherCollectionView *)getWaterFallView:(NSInteger)index {
    switch (index) {
        case 0:
            if (self.productView == nil) {
                self.productView = [[OtherCollectionView alloc] initWithFrame:self.swipeTableView.bounds type:CollectionTypeProduct];
                self.productView.parentVC = self;
            }
            return self.productView;
            break;
        case 1:
            if (self.favoriteView == nil) {
                self.favoriteView = [[OtherCollectionView alloc] initWithFrame:self.swipeTableView.bounds type:CollectionTypeFavorite];
                self.favoriteView.parentVC = self;
            }
            return self.favoriteView;
            break;
        case 2:
            if (self.pictureView == nil) {
                self.pictureView = [[OtherCollectionView alloc] initWithFrame:self.swipeTableView.bounds type:CollectionTypePicture];
                self.pictureView.parentVC = self;
            }
            return self.pictureView;
            break;
            
        default:
            break;
    }
    return nil;
}


- (void)createSwipeTableView {
    self.swipeTableView = [[SwipeTableView alloc]initWithFrame:self.view.bounds];
    _swipeTableView.backgroundColor = WhiteColor;
    _swipeTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _swipeTableView.delegate = self;
    _swipeTableView.dataSource = self;
    _swipeTableView.shouldAdjustContentSize = YES;
    _swipeTableView.swipeHeaderView = self.headerView;
    _swipeTableView.swipeHeaderBar = self.segment;
    _swipeTableView.swipeHeaderBarScrollDisabled = YES;
    _swipeTableView.swipeHeaderTopInset = 0;
    _swipeTableView.swipeHeaderBarTopInset = 64;
    [self.view addSubview:_swipeTableView];
}

- (void)setNavigationBarUI {
//    self.navBar = [[OtherNavBar alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, 64)];
//    [self.view addSubview:_navBar];
}

- (OtherCenterViewHeader *)headerView {
    if (!_headerView) {
        _headerView = [[OtherCenterViewHeader alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, 320)];
    }
    return _headerView;
}

- (OtherSegmentControl *)segment {
    if (!_segment) {
        _segment = [[OtherSegmentControl alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH, 40)];
    }
    return _segment;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
