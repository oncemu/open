//
//  FavoriteVC.h
//  Open
//
//  Created by mfp on 17/9/12.
//  Copyright © 2017年 彭彬. All rights reserved.
//  作品点赞列表

#import "TableBaseVC.h"
#import "AlbumModel.h"

@interface FavoriteVC : TableBaseVC

@property(nonatomic, strong) AlbumModel *model;

@end
