//
//  SelectUsersVC.m
//  Open
//
//  Created by mfp on 17/8/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "SelectUsersVC.h"
#import "SelectUsersView.h"

static NSString *selectuserscellid = @"selectuserscellid";
@interface SelectUsersVC ()
@property(nonatomic, strong) NSMutableArray *userList;
@end

@implementation SelectUsersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"精选用户";
    [self setBarButtonItemWithImage:nil title:@"刷新" isLeft:NO];
    [self.tableView registerClass:[SelectUsersView class] forCellReuseIdentifier:selectuserscellid];
    self.tableView.rowHeight = 55;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    self.tableView.separatorColor = ColorWithHex(0xdddddd);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.tableFooterView = [UIView new];
}

- (void)loadData {
    
}

- (void)loadMoreData {
    
}

#pragma mark - table delegate datasource 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
    return self.userList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SelectUsersView *cell = (SelectUsersView *)[tableView dequeueReusableCellWithIdentifier:selectuserscellid];
    cell.followBlock = ^(id userModel){
        
    };
    return cell;
}

- (void)rightButtonItemAction {
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
