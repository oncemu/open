//
//  CommentListVC.m
//  Open
//
//  Created by mfp on 17/8/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CommentListVC.h"
#import "AlbumNetService.h"
#import "PictureDetailView.h"

static NSString *commentcellId = @"commentcellId";

@interface CommentListVC ()
@property(nonatomic, strong) AlbumNetService   *albumService;

@end

@implementation CommentListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"评论";
    self.tableView.rowHeight = 99;
    [self.tableView registerClass:[CommentCell class] forCellReuseIdentifier:commentcellId];
    self.tableView.backgroundColor = WhiteColor;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 17, 0, 0)];
}

- (void)loadData {
    [self loadComment:NO];
}

- (void)loadMoreData {
    [self loadComment:YES];
}

- (void)loadComment:(BOOL)more {
    WS(weakSelf);
    [self.albumService getAlbumsComments:@{@"albumId":@(self.mode.albumId)} more:more success:^(NSString *requestInfo, id responseObject) {
        CommentData *data = (CommentData *)responseObject;
        if (!more) {
            [weakSelf.dataList removeAllObjects];
        }
        [weakSelf.dataList addObjectsFromArray:data.content];
        [weakSelf loadDataFinishWithHeader:!more withData:data.content];
    } failure:^(NSError *error, id responseObject) {
        
    }];
}

#pragma mark UITableViewDataSource,UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:commentcellId];
    cell.object = self.dataList[indexPath.row];
    return cell;
}

- (AlbumNetService *)albumService {
    if (!_albumService) {
        _albumService = [[AlbumNetService alloc] init];
    }
    return _albumService;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
