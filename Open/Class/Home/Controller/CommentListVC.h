//
//  CommentListVC.h
//  Open
//
//  Created by mfp on 17/8/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//  作品的评论列表页

#import "TableBaseVC.h"
#import "AlbumModel.h"

@interface CommentListVC : TableBaseVC
@property(nonatomic, strong) AlbumModel *mode;

@end
