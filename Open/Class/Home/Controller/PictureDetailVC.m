//
//  PictureDetailVC.m
//  Open
//
//  Created by mfp on 17/7/9.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PictureDetailVC.h"
#import "PictureCell.h"
#import "AlbumModel.h"
#import "PictureDetailView.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "PicBuyVC.h"
#import "AlbumNetService.h"
#import "CommentVC.h"
#import "CommentListVC.h"
#import "OtherCenterVC.h"
#import "FavoriteVC.h"
#import "MActionSheet.h"
#import "HZPhotoBrowser.h"
#import "MakePictureVC.h"

static NSString *commentcellId = @"commentcellId";

@interface PictureDetailVC ()<PictureCellDelegate,ActionSheetDelegate,HZPhotoBrowserDelegate>
@property(nonatomic, strong) PictureDetailView *topView;
@property(nonatomic, strong) DetailBottomBar   *bottomView;
@property(nonatomic, strong) UIAlertController *alertController;
@property(nonatomic, strong) AlbumNetService   *albumService;
@property(nonatomic, strong) CommentData *commentData;
@property(nonatomic, weak) NSIndexPath *clickIndexPath;

@end

@implementation PictureDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"图片详情";
    [self setBarButtonItemWithImage:getImage(@"pic_share") title:nil isLeft:NO];
    self.tableView.frame = CM(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kTabBarHeight);
    self.tableView.rowHeight = 99;
    [self.tableView registerClass:[CommentCell class] forCellReuseIdentifier:commentcellId];
    self.topView = [[PictureDetailView alloc] initWithFrame:CM(0, 0, SCREEN_WIDTH,0)];
    self.topView.delegate = self;
    self.topView.object = self.object;
    WS(weakSelf);
    _topView.scanFavorite = ^() {
        [weakSelf scanFavorite];
    };
    _topView.selectImageBlock = ^(UIImageView *view) {
        HZPhotoBrowser *browser = [[HZPhotoBrowser alloc] init];
        browser.sourceImagesContainerView = view.superview; // 原图的父控件
        browser.imageCount = weakSelf.object.photos.count; // 图片总数
        browser.currentImageIndex = view.tag;
        browser.delegate = weakSelf;
        [browser show];
    };

    self.tableView.tableHeaderView = self.topView;
    self.tableView.backgroundColor = WhiteColor;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 17, 0, 0)];
    self.bottomView = [[DetailBottomBar alloc] initWithFrame:CM(0, SCREEN_HEIGHT - kTabBarHeight, SCREEN_WIDTH, kTabBarHeight) items:@[@"评论",@"点赞"]];
    [self.bottomView setValue:self.object.favoriteCount atIndex:1];
    self.bottomView.clickBlock = ^(NSInteger index) {
        if (index == 0) {
            [weakSelf  comment:NO];
        }else if (index == 1) {
            [weakSelf favorite];
        }else{
            if (weakSelf.object.canMake) {
                [weakSelf makePicture];
            }
        }
    };
    [self.view addSubview:self.bottomView];
}

- (void)makePicture {
    MakePictureVC *vc = [[MakePictureVC alloc] initWithShowBackButton:YES showHeaderRefresh:NO showFooterRefresh:NO];
    [self pushVC:vc animated:YES];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.tableView reloadData];
}

//分享
- (void)rightButtonItemAction {
    
}

//去评论 
- (void)comment:(BOOL)isReplyComment {
    CommentVC *vc = [[CommentVC alloc] initWithShowBackButton:YES];
    vc.isReplyComment = isReplyComment;
    vc.albumModel = self.object;
    if (isReplyComment) {
        vc.commentModel = self.dataList[self.clickIndexPath.row];
    }
    [self pushVC:vc animated:YES];
}

//查看点赞
- (void)scanFavorite {
    FavoriteVC *vc = [[FavoriteVC alloc] initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
    vc.model = self.object;
    [self pushVC:vc animated:YES];
}

//点赞
- (void)favorite {
    WS(weakSelf);
    [self.albumService albumsFavorites:@{@"album_id":@(self.object.albumId)} success:^(NSString *requestInfo, id responseObject) {
        NSString *msg = responseObject[@"message"];
        [KVNProgress showSuccessWithStatus:[NSString formatNilString:msg] onView:self.view completion:^{
            weakSelf.object.isFavourite = !weakSelf.object.isFavourite;
            weakSelf.object.favoriteCount += weakSelf.object.isFavourite ? 1 : (-1);
            if (weakSelf.object.favoriteCount < 0) {
                weakSelf.object.favoriteCount = 0;
            }
            [weakSelf.bottomView setValue:weakSelf.object.favoriteCount atIndex:1];
        }];
    } failure:^(NSError *error, id responseObject) {
        NSString *msg = responseObject[@"message"];
        [KVNProgress showErrorWithStatus:[NSString formatNilString:msg]];
    }];
}

#pragma mark loaddata
- (void)loadData {
    [self loadAlbumDetail];
    [self loadComment:NO];
}

- (void)loadInfo {

}

- (void)loadMoreData {
    [self loadComment:YES];
}

- (void)loadAlbumDetail {
    WS(weakSelf);
    NSMutableDictionary *parames = [NSMutableDictionary dictionaryWithCapacity:1];
    if (self.object) {
        [parames setObject:@(self.object.albumId) forKey:@"albumId"];
    }else {
        [parames setObject:@(self.albumId) forKey:@"albumId"];
    }
    [self.albumService getAlbumDetail:parames success:^(NSString *requestInfo, id responseObject) {
        weakSelf.object = responseObject;
        weakSelf.topView.object = responseObject;
        if (weakSelf.object.canMake && weakSelf.bottomView.buttons.count < 3) {
            [weakSelf.bottomView addItem:@"立即制作"];
        }
        [weakSelf.header endRefreshing];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error, id responseObject) {
        [weakSelf.header endRefreshing];
    }];
}

- (void)loadComment:(BOOL)more {
    WS(weakSelf);
    [self.albumService getAlbumsComments:@{@"albumId":@(self.object.albumId)} more:more success:^(NSString *requestInfo, id responseObject) {
        CommentData *data = (CommentData *)responseObject;
        if (!more) {
            [weakSelf.dataList removeAllObjects];
        }
        [weakSelf.dataList addObjectsFromArray:data.content];
        [weakSelf.bottomView setValue:data.totalElements atIndex:0];
        weakSelf.noDataButton.hidden = YES;
    } failure:^(NSError *error, id responseObject) {
        [weakSelf loadDataFinishWithHeader:!more withData:@[]];
    }];
}

#pragma mark - PictureCellDelegate
- (void)tapUserInfo:(id)object {
    OtherCenterVC *vc = [[OtherCenterVC alloc] initWithShowBackButton:YES];
    [self pushVC:vc animated:YES];
}

- (void)toolBarAction:(NSInteger)index {
    
}

- (void)followButtonClick {
    [KVNProgress showProgress:0 status:@"关注中..."];
    WS(weakSelf);
    
    [self followUser:self.object.userId like:!self.topView.followBtn.selected success:^{
        weakSelf.topView.followBtn.selected = !weakSelf.topView.followBtn.selected;
        [KVNProgress showSuccessWithStatus:@"关注成功！"];
    } failed:^{
        [KVNProgress showErrorWithStatus:@"关注失败！"];
    }];
}
- (void)typeButtonClick {
    
}

#pragma mark UITableViewDataSource,UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:commentcellId];
    cell.object = self.dataList[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self clickCommentIndexPath:indexPath];
}

- (void)clickCommentIndexPath:(NSIndexPath *)indexPath {
    self.clickIndexPath = indexPath;
    MActionSheet *sheet = [[MActionSheet alloc] initWithTitle:nil buttonTitles:@[@"回复",@"拷贝"] cancelButtonTitle:@"取消" delegate:self];
    [sheet show];
}

#pragma mark - ActionSheetDelegate
- (void)actionSheet:(MActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self comment:YES];
    }else{
        UIPasteboard*pasteboard = [UIPasteboard generalPasteboard];
        CommentModel *model = self.dataList[self.clickIndexPath.row];
        pasteboard.string=model.content;
    }
}

#pragma mark -  HZPhotoBrowserDelegate

- (UIImage *)photoBrowser:(HZPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index {
    return nil;
}

- (NSURL *)photoBrowser:(HZPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index {
    if (index<self.object.photos.count) {
        PhotoItem *item = self.object.photos[index];
        return [NSURL URLWithString:item.url];
    }
    return nil;
}

- (AlbumNetService *)albumService {
    if (!_albumService) {
        _albumService = [[AlbumNetService alloc] init];
    }
    return _albumService;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
