//
//  PictureCollectionVC.m
//  Open
//
//  Created by mfp on 17/7/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PictureCollectionVC.h"
#import "PictureCell.h"
#import "AlbumModel.h"
#import "DateTools.h"
#import "PictureDetailVC.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "SelectUsersVC.h"
#import "HomeNetService.h"
#import "OtherCenterVC.h"

 NSString *const pictureCellId   = @"pictureCellId";
static NSString *const pictureCell1Id  = @"pictureCell1Id";
static NSString *const pictureheaderId = @"pictureheaderId";
static NSString *const picturefooterId = @"picturefooterId";

@interface PictureCollectionVC ()<CHTCollectionViewDelegateWaterfallLayout,PictureCellDelegate>
@property(nonatomic, strong) HomeNetService *homeService;
@end

@implementation PictureCollectionVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.collectionView.scrollsToTop = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.collectionView.scrollsToTop = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.homeService = [[HomeNetService alloc] init];
    self.collectionView.backgroundColor = WhiteColor;
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.minimumInteritemSpacing = 5;
    self.collectionView.collectionViewLayout = layout;
    [self.collectionView registerClass:[PictureCell class] forCellWithReuseIdentifier:pictureCellId];
    [self.collectionView registerClass:[Picture1Cell class] forCellWithReuseIdentifier:pictureCell1Id];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter withReuseIdentifier:picturefooterId];
    [self.collectionView registerClass:[SelectUserView class] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:pictureheaderId];
    self.collectionView.scrollsToTop = YES;
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 64+49, 0);
    WS(weakSelf);
    self.noDataAction = ^() {
        [weakSelf startRefresh];
    };
}

#pragma mark - LoadData

- (void)loadData {
    [self requestData:NO];
}

- (void)loadMoreData {
    [self requestData:YES];
}

- (void)requestData:(BOOL)more {
    switch (self.type) {
        case PictureTypeNewest:
            [self loadLastAlbums:more];
            break;
        case PictureTypeFollowed:
            [self loadUserLike:more];
            break;
        case PictureTypeSelected:
            [self loadSelectedAlbums:more];
            break;
    }
}

- (void)loginSucceed {
    [self loadData];
}

//关注
- (void)loadUserLike:(BOOL)more {
    WS(weakSelf);
    [self.homeService getHomeUserLikeAlbumsMore:more success:^(NSString *requestInfo, id responseObject) {
        if (!more) {
            [weakSelf.dataList removeAllObjects];
        }
        [weakSelf.dataList addObjectsFromArray:responseObject];
        [weakSelf changeRefreshStatus:!more data:responseObject];
    } failed:^(NSError *error, id responseObject) {
        [weakSelf changeRefreshStatus:!more data:nil];
    }];
}

//最新
- (void)loadLastAlbums:(BOOL)more {
    WS(weakSelf);
    [self.homeService getHomeLastPhotos:more success:^(NSString *requestInfo, id responseObject) {
        if (!more) {
            [weakSelf.dataList removeAllObjects];
        }
        [weakSelf.dataList addObjectsFromArray:responseObject];
        [weakSelf changeRefreshStatus:!more data:responseObject];
    } failed:^(NSError *error, id responseObject) {
        [weakSelf changeRefreshStatus:!more data:nil];
    }];
}

//精选
- (void)loadSelectedAlbums:(BOOL)more {
    WS(weakSelf);
    [self.homeService getHomeSelectedAlbums:more success:^(NSString *requestInfo, id responseObject) {
        if (!more) {
            [weakSelf.dataList removeAllObjects];
        }
        [weakSelf.dataList addObjectsFromArray:responseObject];
        [weakSelf changeRefreshStatus:!more data:responseObject];
    } failure:^(NSError *error, id responseObject) {
        [weakSelf changeRefreshStatus:!more data:nil];
    }];
    
    //精选用户
    
}

#pragma mark UICollectionViewDataSource UICollectionViewDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.dataList.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PictureCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:pictureCellId forIndexPath:indexPath];
    cell.object = self.dataList[indexPath.section];
    cell.delegate = self;
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
        SelectUserView *header = (SelectUserView *)[collectionView dequeueReusableSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:pictureheaderId forIndexPath:indexPath];
        header.users = @[@"d",@"dd",@"ddd",@"dddd",@"ddddd",@"dddc",@"cc"].mutableCopy;
        WS(weakSelf);
        header.moreBlock = ^() {
            SelectUsersVC *vc = [[SelectUsersVC alloc] initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
            [weakSelf pushVC:vc animated:YES];
        };
        header.userBlock = ^(id userModel) {
            DLog(@"%@",userModel);
        };
        return header;
    }
    if ([kind isEqualToString:CHTCollectionElementKindSectionFooter]) {
        UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter withReuseIdentifier:picturefooterId forIndexPath:indexPath];
        view.backgroundColor = ColorWithHex(0xdddddd);
        return view;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    PictureDetailVC *vc = [[PictureDetailVC alloc] initWithShowBackButton:YES showHeaderRefresh:YES showFooterRefresh:YES];
    vc.object = self.dataList[indexPath.section];
    [self pushVC:vc animated:YES];
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    AlbumModel *mode = self.dataList[indexPath.section];
    CGFloat height = [mode cellHeight];
    return Size(SCREEN_WIDTH, height);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout columnCountForSection:(NSInteger)section {
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section {
//    if (self.type == PictureTypeSelected && section == 0) {
//        return 69;
//    }
    return 0.01f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForFooterInSection:(NSInteger)section {
    return 0.5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 17, 0, 17);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumColumnSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

#pragma mark - PictureCellDelegate
- (void)tapUserInfo:(id)object {
    OtherCenterVC *vc = [[OtherCenterVC alloc] initWithShowBackButton:YES];
    [self.parentVC pushVC:vc animated:YES];
}

- (void)toolBarAction:(NSInteger)index {
    
}
- (void)followButtonClick {
    
}
- (void)typeButtonClick {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
