//
//  FavoriteVC.m
//  Open
//
//  Created by mfp on 17/9/12.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "FavoriteVC.h"
#import "AlbumNetService.h"
#import "User.h"
#import "MsgUserCell.h"

@interface FavoriteVC ()
@property(nonatomic, strong) AlbumNetService *albumService;
@end

@implementation FavoriteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    self.navigationItem.title = @"赞了";
    self.albumService = [[AlbumNetService alloc] init];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView registerClass:[MsgUserCell class] forCellReuseIdentifier:NSStringFromClass([MsgUserCell class])];
    self.tableView.rowHeight = 56;
    self.tableView.tableFooterView = [UIView new];
}

- (void)loadData {
    [self loadFavorites:NO];
}

- (void)loadMoreData {
    [self loadFavorites:YES];
}

- (void)loadFavorites:(BOOL)more {
    WS(weakSelf);
    [self.albumService getAlbumFavorites:@{@"album_id":@(self.model.albumId)} more:more success:^(NSString *requestInfo, id responseObject) {
        if (!more) {
            [weakSelf.dataList removeAllObjects];
        }
        [weakSelf.dataList addObjectsFromArray:responseObject];
        [weakSelf loadDataFinishWithHeader:!more withData:responseObject];
    } failure:^(NSError *error, id responseObject) {
        [weakSelf loadDataFinishWithHeader:!more withData:nil];
    }];
}

#pragma mark - tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MsgUserCell *cell = (MsgUserCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MsgUserCell class])];
    cell.type = MsgUserTypeFavorite;
    cell.model = self.dataList[indexPath.row];
    WS(weakSelf);
    cell.followBlock = ^(NSDictionary *user) {
        [weakSelf followUser:[user[@"userId"] integerValue] like:NO success:^{
            
        } failed:^{
            
        }];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
