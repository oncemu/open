//
//  PictureCollectionVC.h
//  Open
//
//  Created by mfp on 17/7/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//  首页显示的内容

#import "CollectionBaseVC.h"

typedef NS_ENUM(NSUInteger,PictureType){
    PictureTypeFollowed = 0, //关注
    PictureTypeNewest = 1,  //最新
    PictureTypeSelected = 2 //精选
};

extern  NSString * const pictureCellId;

@interface PictureCollectionVC : CollectionBaseVC

@property(nonatomic, assign) PictureType type;

@end
