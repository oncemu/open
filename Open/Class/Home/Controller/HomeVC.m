//
//  HomeVC.m
//  Open
//
//  Created by onceMu on 2017/7/6.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "HomeVC.h"
#import "SegmentBar.h"
#import "SwipeView.h"
#import "PictureCollectionVC.h"
#import "SearchVC.h"
#import "HomeNetService.h"
#import "UserMessageVC.h"

@interface HomeVC ()<SegmentBarDelegate,SwipeViewDelegate,SwipeViewDataSource>

@property (nonatomic, strong) HomeNetService        *homeService;
@property(nonatomic, strong) SegmentBar             *segmentBar;
@property(nonatomic, strong) SwipeView              *swipeView;
@property(nonatomic, strong) PictureCollectionVC    *followVC;
@property(nonatomic, strong) PictureCollectionVC    *newestVC;
@property(nonatomic, strong) PictureCollectionVC    *selectVC;

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBarButtonItemWithImage:getImage(@"home_search") title:nil isLeft:YES];
    [self setBarButtonItemWithImage:getImage(@"home_msg") title:nil isLeft:NO];
    self.homeService = [[HomeNetService alloc] init];
    self.segmentBar = [SegmentBar segmentBarWithFrame:CM(100, 0, SCREEN_WIDTH-200 , 30)];
    self.segmentBar.items = @[@"关注",@"最新",@"精选"];
    self.segmentBar.delegate = self;
    self.segmentBar.selectIndex = 1;
    self.navigationItem.titleView = self.segmentBar;

    self.swipeView = [[SwipeView alloc] initWithFrame:CM(0, NavigationBar_HEIGHT + StatusBar_HEIGHT , SCREEN_WIDTH, SCREEN_HEIGHT - NavigationBar_HEIGHT - StatusBar_HEIGHT - kTabBarHeight)];
    self.swipeView.delegate = self;
    self.swipeView.dataSource = self;
    self.swipeView.alignment = SwipeViewAlignmentCenter;
    self.swipeView.pagingEnabled = YES;
    self.swipeView.bounces = NO;
    [self.swipeView scrollToItemAtIndex:1 duration:0];
    [self.view addSubview:self.swipeView];
}

- (void)loadBanner {
    [self.homeService getHomeBannerSuccess:^(NSString *requestInfo, id responseObject) {
        
    } failed:^(NSError *error, id responseObject) {
        
    }];
}

#pragma mark - super Method
- (void)leftButtonItemAction {    
    SearchVC *vc = [[SearchVC alloc] initWithShowBackButton:YES];
    [self pushVC:vc animated:YES];
}

- (void)rightButtonItemAction {
    UserMessageVC *vc = [[UserMessageVC alloc] initWithShowBackButton:YES];
    [self pushVC:vc animated:YES];
}

#pragma mark - SegmentBarDelegate

- (void)segmentBar:(SegmentBar *)segmentBar didSelectIndex:(NSInteger)toIndex fromIndex:(NSInteger)fromIndex {
    if (toIndex == fromIndex) {
        switch (toIndex) {
            case 0:
                [self.followVC startRefresh];
                break;
            case 1:
                [self.newestVC startRefresh];
                break;
            case 2:
                [self.selectVC startRefresh];
                break;
            default:
                break;
        }
    }else {
        [self.swipeView scrollToPage:toIndex duration:0.3];
    }
}

#pragma mark - SwipeViewDelegate,SwipeViewDataSource
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView {
    return 3;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    switch (index) {
        case 0:
            return self.followVC.view;
            break;
        case 1:
            return self.newestVC.view;
            break;
        case 2:
            return self.selectVC.view;
            break;
        default:
            break;
    }
    return [UIView new];
}

- (void)swipeViewDidEndDecelerating:(SwipeView *)swipeView {
    if (self.segmentBar.selectIndex != swipeView.currentPage) {
        self.segmentBar.selectIndex = swipeView.currentItemIndex;
    }
}

#pragma mark - get method
- (PictureCollectionVC *)followVC {
    if (_followVC == nil) {
        _followVC = [self getTableVC];
        _followVC.type = PictureTypeFollowed;
    }
    return _followVC;
}

- (PictureCollectionVC *)newestVC {
    if (_newestVC == nil) {
        _newestVC = [self getTableVC];
        _newestVC.type = PictureTypeNewest;
    }
    return _newestVC;
}

- (PictureCollectionVC *)selectVC {
    if (_selectVC == nil) {
        _selectVC = [self getTableVC];
        _selectVC.type = PictureTypeSelected;
    }
    return _selectVC;
}

- (PictureCollectionVC *)getTableVC {
    PictureCollectionVC *vc = [[PictureCollectionVC alloc] initWithShowBackButton:NO showHeaderRefresh:YES showFooterRefresh:YES];
    vc.view.frame = CM(0, 0 , SCREEN_WIDTH, SCREEN_HEIGHT - NavigationBar_HEIGHT - StatusBar_HEIGHT - kTabBarHeight);
    vc.parentVC = self;
    return vc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
