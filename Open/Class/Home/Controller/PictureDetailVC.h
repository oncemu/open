//
//  PictureDetailVC.h
//  Open
//
//  Created by mfp on 17/7/9.
//  Copyright © 2017年 彭彬. All rights reserved.
//  详情页

#import "TableBaseVC.h"
#import "AlbumModel.h"

@interface PictureDetailVC : TableBaseVC

@property(nonatomic, strong) AlbumModel *object;
@property(nonatomic, assign) NSInteger albumId;

@end
