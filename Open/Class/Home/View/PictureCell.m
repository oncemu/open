//
//  PictureCell.m
//  Open
//
//  Created by mfp on 17/7/9.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PictureCell.h"
#import "UIImage-Extensions.h"
#import "UIButton+WebCache.h"

static NSString *picviewcellId = @"picviewcellId";

@interface PictureCell()
@property(nonatomic, strong) UIButton           *userIcon;
@property(nonatomic, strong) UILabel            *level;
@property(nonatomic, strong) UILabel            *userNameL;
@property(nonatomic, strong) CellPicView        *picView;
@property(nonatomic, strong) UILabel            *dateLabel;
@property(nonatomic, strong) UILabel            *weatherLabel;
@property(nonatomic, strong) UIButton           *typeBtn;
@property(nonatomic, strong) PicToolBar         *toolBar;
@property(nonatomic, strong) UILabel            *contentLabel;

@end

@implementation PictureCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}
- (void)setupUI {
    self.userIcon = [ViewFactory getButton:CM(0, 7, 27, 27) selectImage:nil norImage:nil target:self action:@selector(tapUserHead)];
    self.userIcon.layer.cornerRadius = 13.5f;
    self.userIcon.layer.masksToBounds = YES;
    
    self.userNameL = [ViewFactory getLabel:CM(CGRectGetMaxX(self.userIcon.frame)+11, self.userIcon.frameOriginY, 100, 27) font:Font(16) textColor:ColorWithHex(0x666666) text:nil];
    self.userNameL.textColor = ColorWithHex(0x333333);
    
    self.level = [ViewFactory getLabel:CGRectZero font:Font(16) textColor:LightLemonYellow text:@"LV"];

    self.picView = [[CellPicView alloc] initWithFrame:CM(0, CGRectGetMaxY(self.userIcon.frame) + 9, SCREEN_WIDTH - 38, 153)];
    self.picView.userInteractionEnabled = NO;
    self.dateLabel = [ViewFactory getLabel:CM(SCREEN_WIDTH - 34 - 100, 12, 100, 14) font:Font(13) textColor:ColorWithHex(0x333333) text:nil];
    self.dateLabel.textAlignment = NSTextAlignmentRight;

    self.weatherLabel = [ViewFactory getLabel:CM(self.dateLabel.frameOriginX, CGRectGetMaxY(self.dateLabel.frame) + 2, self.dateLabel.frameSizeWidth, 12) font:Font(10) textColor:ColorWithHex(0x333333) text:nil];
    self.weatherLabel.textAlignment = NSTextAlignmentRight;

    self.typeBtn = [ViewFactory getButton:CM(10, CGRectGetMaxY(self.picView.frame) - 5 - 20, 40, 20) selectImage:nil norImage:nil target:self action:@selector(buttonClicked:)];
    self.typeBtn.titleLabel.font = Font(12);
    [self.typeBtn setTitleColor:ColorWithHex(0x333333) forState:UIControlStateNormal];
    
    self.toolBar = [[PicToolBar alloc] initWithFrame:CM(SCREEN_WIDTH - 34, 0, 0, 30)];
    WS(weakSelf);
    [self.toolBar addItem:getImage(@"home_heart") handle:^(UIButton *btn, NSInteger index) {
        if ([weakSelf.delegate respondsToSelector:@selector(toolBarAction:)]) {
            [weakSelf.delegate toolBarAction:index];
        }
    }];
    [self.toolBar addItem:getImage(@"home_comment") handle:^(UIButton *btn, NSInteger index) {
        if ([weakSelf.delegate respondsToSelector:@selector(toolBarAction:)]) {
            [weakSelf.delegate toolBarAction:index];
        }
    }];
    
    self.contentLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:nil];
    _contentLabel.numberOfLines = 0;
    _contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    [self addSubview:self.userIcon];
    [self addSubview:self.userNameL];
    [self addSubview:self.level];
    [self addSubview:self.picView];
    [self addSubview:self.dateLabel];
    [self addSubview:self.weatherLabel];
    [self addSubview:self.typeBtn];
    [self addSubview:self.contentLabel];
    [self addSubview:self.toolBar];
}

- (void)setObject:(id)object {
    if (![object isKindOfClass:[AlbumModel class]]) {
        return;
    }
    if (_object != object) {
        _object = object;
    }
    AlbumModel *model = (AlbumModel *)object;
    self.userNameL.text = model.nickname;
    self.dateLabel.text = [StringUtil timeWithTimeIntervalString:model.createdAt];
    [self.typeBtn setTitle:model.categoryText forState:UIControlStateNormal];
    self.typeBtn.backgroundColor = BtnLemonYellow;
    self.picView.picList = model.photos.mutableCopy;
    self.weatherLabel.text = [NSString stringWithFormat:@"%@ %@",model.city,model.weather];
    [self.toolBar setCount:model.favoriteCount atIndex:0];
    [self.toolBar setCount:model.commentCount atIndex:1];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:model.avatar] forState:UIControlStateNormal placeholderImage:getImage(@"headerDefault")];
    self.contentLabel.text = model.description;
    self.level.text = [NSString stringWithFormat:@"LV%ld",(long)model.level];
}

- (void)buttonClicked:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(typeButtonClick)]) {
        [self.delegate typeButtonClick];
    }
}

- (void)tapUserHead {
    if ([self.delegate respondsToSelector:@selector(tapUserInfo:)]) {
        [self.delegate tapUserInfo:_object];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat nameLen = [StringUtil getLabelLength:self.userNameL.text font:self.userNameL.font height:27];
    self.userNameL.frame = CM(CGRectGetMaxX(self.userIcon.frame)+11, self.userIcon.frameOriginY, nameLen, 27);
    self.level.frame = CM(CGRectGetMaxX(self.userNameL.frame) + 5, self.userNameL.frameOriginY, 50, 27);
    CGFloat contentH = [StringUtil getLabelHeight:self.contentLabel.text font:self.contentLabel.font width:self.frameSizeWidth];
    self.contentLabel.frame = CM(0, CGRectGetMaxY(self.picView.frame) + 9, SCREEN_WIDTH - 34, contentH);
    self.toolBar.frameOriginY = CGRectGetMaxY(self.contentLabel.frame) + 5;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.userIcon setImage:nil forState:UIControlStateNormal];
    self.userNameL.text = @"";
    self.dateLabel.text = @"";
    self.weatherLabel.text = @"";
    self.level.text = @"";
    [self.picView prepareForReuse];
    [self.typeBtn setTitle:@"" forState:UIControlStateNormal];

}

- (void)awakeFromNib {
    [super awakeFromNib];
}

@end


@interface Picture1Cell ()
@property(nonatomic, strong) UIImageView *userImageV;
@property(nonatomic, strong) UILabel *userNameL;
@property(nonatomic, strong) UIImageView *picView;
@property(nonatomic, strong) UIButton *typeBtn;
@property(nonatomic, strong) UILabel *contentLabel;
@property(nonatomic, strong) UIButton *followBtn;

@end

@implementation Picture1Cell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    self.userImageV = [ViewFactory getImageView:CGRectZero image:nil];
    self.userImageV.backgroundColor = grayLineColor;
    self.userNameL = [ViewFactory getLabel:CGRectZero font:Font(16) textColor:ColorWithHex(0x666666) text:nil];
    self.userNameL.textColor = ColorWithHex(0x333333);
    self.userImageV.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapUserHead)];
    [self.userImageV addGestureRecognizer:tap];
    [self.userNameL addGestureRecognizer:tap];
    self.picView = [ViewFactory getImageView:CGRectZero image:nil];
    self.typeBtn = [ViewFactory getButton:CGRectZero selectImage:nil norImage:nil target:self action:@selector(buttonClicked:)];
    self.typeBtn.titleLabel.font = Font(12);
    [self.typeBtn setTitleColor:ColorWithHex(0x333333) forState:UIControlStateNormal];
    [self.typeBtn setTitle:@"" forState:UIControlStateNormal];
    self.contentLabel = [ViewFactory getLabel:CGRectZero font:Font(13) textColor:ColorWithHex(0x333333) text:nil];
    self.followBtn = [ViewFactory getButton:CGRectZero font:Font(13) selectTitle:@"关注" norTitle:@"关注" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(buttonClicked:)];
    [self.followBtn setBackgroundImage:getImage(@"pic_send_comment") forState:UIControlStateNormal];
    self.followBtn.layer.cornerRadius = 4;
    self.followBtn.layer.masksToBounds = YES;
    
    [self.contentView addSubview:self.userImageV];
    [self.contentView addSubview:self.userNameL];
    [self.contentView addSubview:self.picView];
    [self.contentView addSubview:self.typeBtn];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.followBtn];
}

- (void)setObject:(id)object {
    if (_object != object) {
        _object = object;
    }
    self.userNameL.text = @"作者";
    self.contentLabel.text = @"dadfadfadfadf";
    [self.typeBtn setTitle:@"邮寄" forState:UIControlStateNormal];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.userImageV.frame = CM(0, 0, 27, 27);
    self.userNameL.frame = CM(CGRectGetMaxX(self.userImageV.frame) + 5, self.userImageV.frameOriginY, self.frameSizeWidth - 27 - 5 - 53 - 10, 27 );
    self.followBtn.frame = CM(self.frameSizeWidth - 53, self.userImageV.frameOriginY + 2, 53, 23.5f);
    self.picView.frame = CM(0, CGRectGetMaxY(self.userImageV.frame) + 15, self.frameSizeWidth, self.frameSizeWidth);
    self.contentLabel.frame = CM(0, CGRectGetMaxY(self.picView.frame) + 10, self.frameSizeWidth - 53 - 10, 18);
    CGFloat typeLen = ceil([StringUtil getLabelLength:self.typeBtn.currentTitle font:self.typeBtn.titleLabel.font height:15]) + 20;
    self.typeBtn.frame = CM(self.frameSizeWidth - typeLen , self.contentLabel.frameOriginY, typeLen, 18);
    UIImage *img = [getImage(@"home_pic_type") imageByScalingToSize:self.typeBtn.frameSize];
    [self.typeBtn setBackgroundImage:img forState:UIControlStateNormal];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.userImageV.image = nil;
    self.userNameL.text = nil;
    self.contentLabel.text = @"";
    self.picView.image = nil;
    [self.followBtn setTitle:@"" forState:UIControlStateNormal];
    [self.typeBtn setTitle:@"" forState:UIControlStateNormal];
}

@end

@interface CellPicView ()

@property (nonatomic, strong) NSArray *imageVlist;

@end

@implementation CellPicView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.layer.masksToBounds = YES;
        self.showPicCount = YES;
        _imageVlist = @[[self createImageViewTag:0],
                        [self createImageViewTag:1],
                        [self createImageViewTag:2],
                        [self createImageViewTag:3],
                        [self createImageViewTag:4],
                        [self createImageViewTag:5],
                        [self createImageViewTag:6],
                        [self createImageViewTag:7],
                        [self createImageViewTag:8],];
        self.picCountL = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:WhiteColor text:nil];
        self.picCountL.textAlignment = NSTextAlignmentRight;
        self.picCountL.backgroundColor = ClearColor;
        [self addSubview:self.picCountL];
    }
    return self;
}

- (void)setPicList:(NSMutableArray *)picList {
    _picList = picList;
    NSString *text = [NSString stringWithFormat:@"1/%ld",(long)picList.count];
    if (picList.count <= 3) {
        text = @"";
    }
    self.picCountL.text = text;
    for (NSInteger i = 0; i<picList.count; i++) {
        PhotoItem *item = (PhotoItem *)picList[i];
        if ([item isKindOfClass:[PhotoItem class]]) {
            if (i<self.imageVlist.count) {
                UIImageView *v = self.imageVlist[i];
                [v sd_setImageWithURL:[NSURL URLWithString:item.url]];
            }
        }
    }
    [self setNeedsLayout];
}

- (void)selectPicture:(UITapGestureRecognizer *)tap {
    if (self.selectBlock) {
        self.selectBlock((UIImageView *)tap.view);
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.picCountL.hidden = !self.showPicCount;
    self.picCountL.frame = CM(self.frameSizeWidth - 80, self.frameSizeHeight - 25, 80, 20);
    [_imageVlist enumerateObjectsUsingBlock:^(UIImageView *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.frame = CM(0, 0, 0, 0);
    }];
    UIImageView *v1 = self.imageVlist[0];
    UIImageView *v2 = self.imageVlist[1];
    UIImageView *v3 = self.imageVlist[2];
    NSInteger showCount = self.showPicCount?((self.picList.count>3)?3:self.picList.count):self.picList.count;
    switch (showCount) {
        case 0:
            break;
        case 1:
            v1.frame = self.bounds;
            break;
        case 2:
            v1.frame = CM(0, 0, (self.frameSizeWidth - 10)/2, self.frameSizeHeight);
            v2.frame = CM(CGRectGetMaxX(v1.frame) + 10, v1.frameOriginY, v1.frameSizeWidth, v1.frameSizeHeight);
            break;
        case 3:
            v1.frame = CM(0, 0, (self.frameSizeWidth - 10) * 0.688f, self.frameSizeHeight);
            v2.frame = CM(CGRectGetMaxX(v1.frame) + 10, v1.frameOriginY, (self.frameSizeWidth - 10) * 0.312f, (self.frameSizeHeight - 7)/2);
            v3.frame = CM(v2.frameOriginX, CGRectGetMaxY(v2.frame) + 7, v2.frameSizeWidth, v2.frameSizeHeight);
            break;
        case 4:
            for (NSInteger i = 0; i<2; i++) {
                for (NSInteger j = 0; j<2; j++) {
                    UIImageView *v = self.imageVlist[i*2 + j];
                    v.frame = CM(j * ((self.frameSizeWidth - 5)/2 + 5), i * ((self.frameSizeHeight - 5)/2 + 5), (self.frameSizeWidth - 5)/2, (self.frameSizeHeight - 5)/2);
                }
            }
            break;
        case 5:
            for (NSInteger i = 0; i<5; i++) {
                UIImageView *v = self.imageVlist[i];
                if (i<1) {
                    v.frame = CM(0, 0, self.frameSizeWidth, SCALE(99));
                }else{
                    UIImageView *pv = self.imageVlist[0];
                    v.frame = CM((i-1)*((self.frameSizeWidth - 15)/4 + 5), CGRectGetMaxY(pv.frame) + 5, (self.frameSizeWidth - 15)/4, SCALE(74));
                }
            }
            break;
        case 6:
            for (NSInteger i = 0; i<6; i++) {
                UIImageView *v = self.imageVlist[i];
                if (i<2) {
                    v.frame = CM(i*((self.frameSizeWidth - 5)/2 + 5), 0, (self.frameSizeWidth - 5)/2, SCALE(99));
                }else{
                    UIImageView *pv = self.imageVlist[0];
                    v.frame = CM((i-2)*((self.frameSizeWidth - 15)/4 + 5), CGRectGetMaxY(pv.frame) + 5, (self.frameSizeWidth - 15)/4, SCALE(74));
                }
            }
            break;
        case 7:
            for (NSInteger i = 0; i<7; i++) {
                UIImageView *v = self.imageVlist[i];
                if (i < 3) {
                    v.frame = CM(i*((self.frameSizeWidth - 10)/3 + 5), 0, (self.frameSizeWidth - 10)/3, SCALE(99));
                }else{
                    UIImageView *pv = self.imageVlist[0];
                    v.frame = CM((i-3)*((self.frameSizeWidth - 15)/4 + 5), CGRectGetMaxY(pv.frame) + 5, (self.frameSizeWidth - 15)/4, SCALE(74));
                }
            }
            break;
        case 8:
            for (NSInteger i = 0; i<2; i++) {
                for (NSInteger j = 0; j<4; j++) {
                    UIImageView *v = self.imageVlist[i*4 + j];
                    v.frame = CM(j * ((self.frameSizeWidth - 5 * 3)/4 + 5), i * ((self.frameSizeHeight - 5)/2 + 5), (self.frameSizeWidth - 5*3)/4, (self.frameSizeHeight - 5)/2);
                }
            }
            break;
        case 9:
            for (NSInteger i = 0; i<3; i++) {
                for (NSInteger j = 0; j<3; j++) {
                    UIImageView *v = self.imageVlist[i*3 + j];
                    v.frame = CM(j * ((self.frameSizeWidth - 5 * 2)/3 + 5), i * ((self.frameSizeHeight - 5 * 2)/3 + 5), (self.frameSizeWidth - 5 * 2)/3, (self.frameSizeHeight - 5 * 2)/3);
                }
            }
            break;
        default:

            break;
    }
}

- (void)prepareForReuse {
    for(UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView *)view;
            imageView.image = nil;
            [imageView sd_cancelCurrentImageLoad];
        }
    }
}

- (UIImageView *)createImageViewTag:(NSInteger)tag {
    UIImageView *imageView = [ViewFactory getImageView:CGRectZero image:nil];
    imageView.tag = tag;
    imageView.userInteractionEnabled = YES;
    imageView.backgroundColor = ColorWithHex(0xf1f1f1);
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:imageView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectPicture:)];
    [imageView addGestureRecognizer:tap];
    return imageView;
}

@end

@interface SelectUserView ()

@property(nonatomic, strong) NSMutableArray *buttons;
@property(nonatomic, strong) UIButton *moreBtn;
@property(nonatomic, strong) UIView *line;

@end

static CGFloat buttonSpace = 16;
static CGFloat leftPadding = 19;

@implementation SelectUserView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.layer.masksToBounds = YES;
        self.buttons = [NSMutableArray arrayWithCapacity:6];
        self.moreBtn = [ViewFactory getButton:CGRectZero font:Font(14) selectTitle:@"更多精选\n用户" norTitle:@"更多精选\n用户" sColor:ColorWithHex(0x999999) norColor:ColorWithHex(0x999999) target:self action:@selector(more)];
        _moreBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _moreBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        _moreBtn.layer.borderColor = ColorWithHex(0x999999).CGColor;
        _moreBtn.layer.borderWidth = 0.5f;
        _moreBtn.layer.cornerRadius = 5.0f;
        _moreBtn.layer.masksToBounds = YES;
        [self addSubview:self.moreBtn];
        
        _line = [[UIView alloc] initWithFrame:CGRectZero];
        _line.backgroundColor = ColorWithHex(0xdddddd);
        [self addSubview:_line];
    }
    return self;
}

- (void)setUsers:(NSMutableArray *)users {
    _users = users;
    [self setUpButtons];
}

- (void)setUpButtons {
    [self.buttons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.buttons removeAllObjects];
    NSInteger count = (self.users.count > 5) ? (iPhone6Plus?6:5) : self.users.count;
    for (NSInteger i = 0; i<count; i++) {
        UIButton *btn = [ViewFactory getButton:CGRectZero selectImage:nil norImage:nil target:self action:@selector(userIconClicked:)];
        btn.layer.masksToBounds = YES;
        btn.backgroundColor = grayLineColor;
        [self addSubview:btn];
        [self.buttons addObject:btn];
    }
    [self setNeedsLayout];
}

- (void)userIconClicked:(UIButton *)button {
    if (self.userBlock) {
        NSInteger index = [self.buttons indexOfObject:button];
        self.userBlock(self.users[index]);
    }
}

- (void)more {
    if (self.moreBlock) {
        self.moreBlock();
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.moreBtn.frame = CM(self.frameSizeWidth - 61 - leftPadding , (self.frameSizeHeight - 40)/2, 61, 40);
    NSInteger btn_w = (CGRectGetMinX(self.moreBtn.frame) - leftPadding * 2 - 5 * buttonSpace)/6;
    for (NSInteger i = 0; i<self.buttons.count; i++) {
        UIButton *btn = (UIButton *)self.buttons[i];
        btn.frame = CM(leftPadding + i * (btn_w + buttonSpace), (self.frameSizeHeight - btn_w)/2, btn_w, btn_w);
        btn.layer.cornerRadius = btn_w/2;
    }
    self.line.frame = CM(0, self.frameSizeHeight - 0.5f, self.frameSizeWidth, 0.5f);
}

- (void)prepareForReuse {
    [super prepareForReuse];
}

@end
