//
//  SelectUsersView.h
//  Open
//
//  Created by mfp on 17/9/4.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectUsersView : UITableViewCell
@property(nonatomic, strong) id userModel;
@property(nonatomic, copy)void (^followBlock)(id userModel);

@end
