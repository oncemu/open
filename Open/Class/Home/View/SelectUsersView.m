//
//  SelectUsersView.m
//  Open
//
//  Created by mfp on 17/9/4.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "SelectUsersView.h"

@interface SelectUsersView ()

@property(nonatomic, strong) UIImageView *userIcon;
@property(nonatomic, strong) UILabel *userName;
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) UIButton *followBtn;

@end

@implementation SelectUsersView

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.userIcon = [ViewFactory getImageView:CGRectZero image:nil];
        _userIcon.layer.masksToBounds = YES;
        [self.contentView addSubview:_userIcon];
        
        self.userName = [ViewFactory getLabel:CGRectZero font:Font(15) textColor:ColorWithHex(0x666666) text:@"我的名字"];
        [self.contentView addSubview:_userName];
        
        self.dateLabel = [ViewFactory getLabel:CGRectZero font:Font(12) textColor:ColorWithHex(0x999999) text:@"2017-3-15"];
        [self.contentView addSubview:_dateLabel];
        
        self.followBtn = [ViewFactory getButton:CGRectZero font:Font(15) selectTitle:@"关注" norTitle:@"关注" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(follow)];
        _followBtn.backgroundColor = LightLemonYellow;
        _followBtn.layer.cornerRadius = 5;
        _followBtn.layer.masksToBounds = YES;
        [self.contentView addSubview:_followBtn];

    }
    return self;
}

- (void)follow {
    if (self.followBlock) {
        self.followBlock(self.userModel);
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.userIcon.frame = CM(19, self.frameSizeHeight/2 - 15, 30, 30);
    self.userName.frame = CM(CGRectGetMaxX(self.userIcon.frame) + 10, self.userIcon.frameOriginY, self.frameSizeWidth - 60 - 88, 18);
    self.dateLabel.frame = CM(self.userName.frameOriginX, CGRectGetMaxY(self.userName.frame)+4, self.userName.frameSizeWidth, 15);
    self.followBtn.frame = CM(self.frameSizeWidth - 60 - 19 , self.frameSizeHeight/2 - 13, 60, 25);
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
