//
//  PictureCell.h
//  Open
//
//  Created by mfp on 17/7/9.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PicToolBar.h"
#import "AlbumModel.h"

@class CellPicView;
@protocol PictureCellDelegate <NSObject>

@optional
- (void)tapUserInfo:(id)object;//点击用户头像或者用户名
- (void)toolBarAction:(NSInteger)index;//点击关注或者评论bar
- (void)followButtonClick;//点击关注
- (void)typeButtonClick;//点击分类图标

@end

@interface PictureCell : UICollectionViewCell

@property(nonatomic, strong)id object;
@property(nonatomic, assign)BOOL showFollow;//是否显示关注按钮
@property(nonatomic, weak) id<PictureCellDelegate> delegate;
@property(nonatomic, copy) void (^selectImageBlock)(UIImageView *view);

@end

@interface Picture1Cell : UICollectionViewCell

@property(nonatomic, strong) id object;

@end

@interface CellPicView : UIView

@property(nonatomic, strong) NSMutableArray  *picList;
@property(nonatomic, strong) UILabel         *picCountL;
@property(nonatomic, assign) BOOL            showPicCount;
@property(nonatomic, copy) void (^selectBlock)(UIImageView *view);

- (void)prepareForReuse;

@end

//更多精选用户
@interface SelectUserView : UICollectionReusableView

@property(nonatomic, strong) NSMutableArray *users;

@property(nonatomic, copy) void(^moreBlock)();

@property(nonatomic, copy) void(^userBlock)(id userModel);

@end
