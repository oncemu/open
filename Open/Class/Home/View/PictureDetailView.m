//
//  PictureDetailView.m
//  Open
//
//  Created by mfp on 17/7/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PictureDetailView.h"
#import "PicToolBar.h"
#import "UIImageView+WebCache.h"

@implementation PictureDetailView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    self.userImageV = [ViewFactory getImageView: CM(17, 7, 27, 27) image:getImage(@"headerDefault")];
    self.userImageV.backgroundColor = grayLineColor;
    self.userImageV.userInteractionEnabled = YES;
    self.userImageV.layer.cornerRadius = 13.5f;
    self.userImageV.layer.masksToBounds = YES;
    
    self.userNameL = [ViewFactory getLabel:CGRectZero font:Font(16) textColor:ColorWithHex(0x000000) text:nil];
    
    self.level = [ViewFactory getLabel:CGRectZero font:Font(16) textColor:LightLemonYellow text:@"LV"];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapUserHead)];
    [self.userImageV addGestureRecognizer:tap];
    [self.userNameL addGestureRecognizer:tap];
    
    self.dateLabel = [ViewFactory getLabel:CGRectZero font:Font(13) textColor:ColorWithHex(0x666666) text:nil];
    
    self.picView = [[CellPicView alloc] initWithFrame:CGRectZero];
    self.picView.showPicCount = NO;
    WS(weakSelf);
    self.picView.selectBlock = ^(UIImageView *view) {
        if (weakSelf.selectImageBlock) {
            weakSelf.selectImageBlock(view);
        }
    };
    self.toolBar = [[PicToolBar alloc] initWithFrame:CM(17, 0, 0, 30)];
    _toolBar.barAlignment = ToolBarAlignmentLeft;
    
    [self.toolBar addItem:getImage(@"pic_yellowheart") handle:^(UIButton *btn, NSInteger index) {
        if ([weakSelf.delegate respondsToSelector:@selector(toolBarAction:)]) {
            [weakSelf.delegate toolBarAction:index];
        }
    }];
    
    self.followBtn = [ViewFactory getButton:CGRectZero font:Font(13) selectTitle:@"已关注" norTitle:@"关注" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(buttonClicked:)];
    _followBtn.adjustsImageWhenHighlighted = NO;
    [self.followBtn setBackgroundImage:getImage(@"pic_send_comment") forState:UIControlStateNormal];
    self.followBtn.layer.cornerRadius = 4;
    self.followBtn.layer.masksToBounds = YES;
    
    self.contentLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:nil];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    self.arrowBtn = [ViewFactory getButton:CGRectZero selectImage:getImage(@"pic_scanzan") norImage:getImage(@"pic_scanzan") target:self action:@selector(chakandianzan)];
    
    [self addSubview:self.userImageV];
    [self addSubview:self.userNameL];
    [self addSubview:self.level];
    [self addSubview:self.dateLabel];
    [self addSubview:self.picView];
    [self addSubview:self.toolBar];
    [self addSubview:self.followBtn];
    [self addSubview:self.contentLabel];
    [self addSubview:self.arrowBtn];
}

- (void)tapUserHead {
    if ([self.delegate respondsToSelector:@selector(tapUserInfo:)]) {
        [self.delegate tapUserInfo:self.object];
    }
}

- (void)chakandianzan {
    if (self.scanFavorite) {
        self.scanFavorite();
    }
}

- (void)buttonClicked:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(followButtonClick)]) {
        [self.delegate followButtonClick];
    }
}

- (void)setObject:(id)object {
    _object = object;
    if (!object || ![object isKindOfClass:[AlbumModel class]]) {
        return;
    }
    AlbumModel *model = (AlbumModel *)object;
    self.picView.picList = model.photos.mutableCopy;
    [self.toolBar setCount:model.favoriteCount atIndex:0];
    self.userNameL.text = model.nickname;
    [self.userImageV sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:getImage(@"headerDefault")];
    self.contentLabel.text = model.description;
    self.dateLabel.text = [StringUtil timeWithTimeIntervalString:model.createdAt];
    self.level.text = [NSString stringWithFormat:@"LV%ld",(long)model.level];
    GlobalDataModel *gdm = [GlobalDataModel sharedInstance];
    self.followBtn.hidden = gdm.user.userId.integerValue == model.userId;
    self.followBtn.selected = model.isLikedAuthor;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat nameLen = [StringUtil getLabelLength:self.userNameL.text font:self.userNameL.font height:16];
    self.userNameL.frame = CM(CGRectGetMaxX(self.userImageV.frame)+11, self.userImageV.frameOriginY, nameLen, 16);
    self.level.frame = CM(CGRectGetMaxX(self.userNameL.frame) + 5, self.userNameL.frameOriginY, 50, 16);
    self.dateLabel.frame = CM(self.userNameL.frameOriginX, CGRectGetMaxY(self.userNameL.frame), 200, 14);
    self.followBtn.frame = CM(self.frameSizeWidth - 53 - 17, self.userImageV.frameOriginY + 2, 53, 23.5f);
    self.picView.frame = CM(17, CGRectGetMaxY(self.userImageV.frame) + 9, self.frameSizeWidth - 34, SCALE(356/2));
    CGFloat contentH = [StringUtil getLabelHeight:self.contentLabel.text font:self.contentLabel.font width:self.frameSizeWidth - 34];
    self.contentLabel.frame = CM(17, CGRectGetMaxY(self.picView.frame) + 10, self.frameSizeWidth - 34, contentH);
    self.toolBar.frame = CM(17, CGRectGetMaxY(self.contentLabel.frame) + 10, 0, 30);
    self.arrowBtn.frame = CM(self.frameSizeWidth - 17 - 31, self.toolBar.frameOriginY, 11 + 40, 18 + 12);
    self.frameSizeHeight = CGRectGetMaxY(self.toolBar.frame) + 10;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, grayLineColor.CGColor);
    CGContextMoveToPoint(context, 0, self.frameSizeHeight - 0.5f);
    CGContextAddLineToPoint(context, self.frameSizeWidth, self.frameSizeHeight - 0.5f);
    CGContextStrokePath(context);
}

@end


@interface CommentCell ()
@property(nonatomic, strong) UILabel    *commentLabel;
@property(nonatomic, strong) UILabel    *dateLabel;
//@property(nonatomic, strong) UIButton   *replyBtn;
//@property(nonatomic, strong) PicToolBar *toolBar;

@end

@implementation CommentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleDefault;
        
        self.backgroundColor = WhiteColor;
        self.textLabel.textColor = ColorWithHex(0x999999);
        self.imageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapUserIcon)];
        [self.imageView addGestureRecognizer:tap];
        [self.textLabel addGestureRecognizer:tap];
        self.commentLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:nil];
        self.commentLabel.numberOfLines = 0;
        self.commentLabel.lineBreakMode = NSLineBreakByWordWrapping;
//        self.toolBar = [[PicToolBar alloc] initWithFrame:CGRectZero];
//        [self.toolBar addItem:getImage(@"home_heart") handle:^(UIButton *btn, NSInteger index) {
//            
//        }];
        self.dateLabel = [ViewFactory getLabel:CGRectZero font:Font(12) textColor:ColorWithHex(0x999999) text:nil];
//        self.replyBtn = [ViewFactory getButton:CGRectZero font:Font(12) selectTitle:@"回复" norTitle:@"回复" sColor:ColorWithHex(0x999999) norColor:ColorWithHex(0x999999) target:self action:@selector(reply)];
        [self.contentView addSubview:self.commentLabel];
        [self.contentView addSubview:self.dateLabel];
//        [self.contentView addSubview:self.replyBtn];
//        [self.contentView addSubview:self.toolBar];
    }
    return self;
}

- (void)setObject:(CommentModel *)object {
    if (_object != object) {
        _object = object;
    }
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:object.avatar] placeholderImage:getImage(@"headerDefault")];
    self.textLabel.text = object.nickname;
    self.dateLabel.text = [NSString stringWithFormat:@"时间：%@",[StringUtil timeWithTimeIntervalString:object.createdAt]];
    self.commentLabel.text = object.content;
}

- (void)tapUserIcon {
    if (self.tapUserBlock) {
        self.tapUserBlock(self.object);
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CM(17, 17, 32, 32);
    self.textLabel.frame = CM(CGRectGetMaxX(self.imageView.frame)+11, self.imageView.frameOriginY + 2, self.frameSizeWidth - (CGRectGetMaxX(self.imageView.frame)+11) - 17, 20);
    self.dateLabel.frame = CM(self.textLabel.frameOriginX, CGRectGetMaxY(self.textLabel.frame) + 3, self.textLabel.frameSizeWidth, 15);
    CGFloat commentH = [StringUtil getLabelHeight:self.commentLabel.text font:self.commentLabel.font width:self.frameSizeWidth - self.textLabel.frameOriginX - 17 - 70];
    self.commentLabel.frame = CM(self.textLabel.frameOriginX, CGRectGetMaxY(self.dateLabel.frame) + 10, self.frameSizeWidth - self.textLabel.frameOriginX - 17 - 70, commentH);
}

@end


@interface CommentBar ()
@property(nonatomic, strong) UITextField *commentField;
@property(nonatomic, strong) UIButton    *commentBtn;

@end

@implementation CommentBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.commentField = [ViewFactory getTextfield:CGRectZero font:Font(13) textColor:ColorWithHex(0x666666)];
        self.commentField.borderStyle = UITextBorderStyleRoundedRect;
        self.commentField.placeholder = @"评论一下吧";
        self.commentBtn = [ViewFactory getButton:CGRectZero font:Font(13) selectTitle:@"发送" norTitle:@"发送" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(send)];
        [self.commentBtn setBackgroundImage:getImage(@"pic_send_comment") forState:UIControlStateNormal];
        self.commentBtn.layer.cornerRadius = 4;
        self.commentBtn.layer.masksToBounds = YES;
        [self addSubview:self.commentField];
        [self addSubview:self.commentBtn];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.commentBtn.frame = CM(self.frameSizeWidth - 17 - 53, (self.frameSizeHeight - 23.5f)/2, 53, 23.5f);
    self.commentField.frame = CM(17, 5, self.frameSizeWidth - 17 * 2 - 10 - self.commentBtn.frameSizeWidth , self.frameSizeHeight - 10);
}

- (void)send {
    if (self.sendAction) {
        self.sendAction(_commentField.text);
    }
}

@end


@implementation DetailBottomBar

- (instancetype)initWithFrame:(CGRect)frame items:(NSArray *)items {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.buttons = [NSMutableArray array];
        for (NSString *item in items) {
            [self addItem:item];
        }
    }
    return self;
}

- (void)clickAction:(UIButton *)sender {
    if (self.clickBlock) {
        NSInteger index = [self.buttons indexOfObject:sender];
        self.clickBlock(index);
    }
}

- (void)addItem:(NSString *)title {
    UIButton *btn = [ViewFactory getButton:CGRectZero font:Font(16) selectTitle:title norTitle:title sColor:ColorWithHex(0xffffff) norColor:ColorWithHex(0xffffff) target:self action:@selector(clickAction:)];
    btn.backgroundColor = BtnLemonYellow;
    [self addSubview:btn];
    [self.buttons addObject:btn];
    [self setNeedsLayout];
}

- (void)setValue:(NSInteger)value atIndex:(NSInteger)index {
    if (index < self.buttons.count) {
        UIButton *btn = self.buttons[index];
        NSString *title = btn.currentTitle;
        NSArray *titles = [title componentsSeparatedByString:@" "];
        [btn setTitle:[NSString stringWithFormat:@"%@ %ld",[titles firstObject],(long)value] forState:UIControlStateNormal];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat btn_w = (self.frameSizeWidth - (self.buttons.count - 1))/self.buttons.count;
    for (NSInteger i = 0; i<self.buttons.count; i++) {
        UIButton *btn = self.buttons[i];
        btn.frame = CM(i * (btn_w + 1), 0, btn_w, self.frameSizeHeight);
    }
}

@end
