//
//  PictureDetailView.h
//  Open
//
//  Created by mfp on 17/7/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureCell.h"
#import "CommentModel.h"

@interface PictureDetailView : UIView<PictureCellDelegate>
@property(nonatomic, strong) UIImageView        *userImageV;
@property(nonatomic, strong) UILabel            *userNameL;
@property(nonatomic, strong) UILabel            *level;
@property(nonatomic, strong) UILabel            *userLevel;
@property(nonatomic, strong) UILabel            *dateLabel;
@property(nonatomic, strong) UILabel            *contentLabel;
@property(nonatomic, strong) PicToolBar         *toolBar;
@property(nonatomic, strong) UIButton           *followBtn;
@property(nonatomic, strong) UIButton           *arrowBtn;
@property(nonatomic, strong) CellPicView        *picView;
@property(nonatomic,   weak) id<PictureCellDelegate> delegate;
@property(nonatomic, strong) id                 object;
@property(nonatomic, copy) void (^scanFavorite)();//查看点赞
@property(nonatomic, copy) void (^selectImageBlock)(UIImageView *view);

@end

@interface CommentCell : UITableViewCell

@property(nonatomic, strong) CommentModel *object;

@property(nonatomic, copy) void (^tapUserBlock)(CommentModel *model);

@end

//评论条
@interface CommentBar : UIView

@property(nonatomic, copy) void (^sendAction)(NSString *comment);

@end

@interface DetailBottomBar : UIView

@property(nonatomic, copy) void (^clickBlock)(NSInteger index);

@property(nonatomic, strong) NSMutableArray *buttons;

- (instancetype)initWithFrame:(CGRect)frame items:(NSArray *)items;

- (void)addItem:(NSString *)title;

- (void)setValue:(NSInteger)value atIndex:(NSInteger)index;

@end


