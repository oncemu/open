//
//  PictureModel.m
//  Open
//
//  Created by mfp on 17/7/9.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "AlbumModel.h"

@implementation AlbumModel
@synthesize description = _description;

- (CGFloat)cellHeight {
    if (_cellHeight > 0) {
        return _cellHeight;
    }
    CGFloat Height = 0;
    Height += (7 + 27 + 9 + 153 + 9);
    CGFloat contentH = [StringUtil getLabelHeight:self.description font:Font(14) width:SCREEN_WIDTH - 34];
    Height +=  (contentH + 45 + 20);
    _cellHeight = Height;
    return _cellHeight;
}

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{ @"photos" : [PhotoItem class]};
}

+ (NSArray *)albumList:(NSDictionary *)dict {
    if (![dict objectForKey:@"content"]) {
        return @[];
    }
    id contentDic = dict[@"content"];
    if ([contentDic isKindOfClass:[NSArray class]]) {
        NSArray *list = [NSArray yy_modelArrayWithClass:[AlbumModel class] json:contentDic];
        return list;
    }else if ([contentDic isKindOfClass:[NSDictionary class]]) {
        if (![contentDic objectForKey:@"content"]) {
            return @[];
        }
        NSArray *list = [NSArray yy_modelArrayWithClass:[AlbumModel class] json:contentDic[@"content"]];
        return list;
    }
    return @[];
}

+ (AlbumModel *)albumModel:(NSDictionary *)dict {
    if (![dict objectForKey:@"content"]) {
        return nil;
    }
    AlbumModel *mode = [AlbumModel yy_modelWithDictionary:dict[@"content"]];
    return mode;
}

@end

@implementation PhotoItem

@end
