//
//  CommentModel.m
//  Open
//
//  Created by mfp on 17/9/5.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CommentModel.h"

@implementation CommentData

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{ @"content" : [CommentModel class]};
}

@end

@implementation CommentModel

+ (NSArray *)commentList:(NSDictionary *)dict {
    if (![dict objectForKey:@"content"]) {
        return @[];
    }
    NSDictionary *contentDic = dict[@"content"];
    if (![contentDic objectForKey:@"content"]) {
        return @[];
    }
    NSArray *list = [NSArray yy_modelArrayWithClass:[CommentModel class] json:contentDic[@"content"]];
    return list;
}

@end
