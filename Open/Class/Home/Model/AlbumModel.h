//
//  PictureModel.h
//  Open
//
//  Created by mfp on 17/7/9.
//  Copyright © 2017年 彭彬. All rights reserved.
//  图片、banner通用

#import <Foundation/Foundation.h>
@class PhotoItem;

@interface AlbumModel : NSObject

@property(nonatomic, assign) NSInteger albumId;
@property(nonatomic, copy) NSString *avatar;//用户头像
@property(nonatomic, copy) NSString *nickname;
@property(nonatomic, assign) CGFloat latitude;
@property(nonatomic, assign) CGFloat longitude;
@property(nonatomic, assign) NSInteger canMake;//是否可制作：1 - 是；0 - 否 ,
@property(nonatomic, copy) NSString *category;//美图：PHOTO ,游记：JOURNEYJUN
@property(nonatomic, copy) NSString *categoryText;
@property(nonatomic, copy) NSString *city;
@property(nonatomic, copy) NSString *country;
@property(nonatomic, assign) NSInteger collectionCount;//收藏数
@property(nonatomic, assign) NSInteger count;//作品图片数量
@property(nonatomic, assign) double createdAt;
@property(nonatomic, copy) NSString *description;
@property(nonatomic, assign) NSInteger favoriteCount;//点赞数
@property(nonatomic, strong) NSArray *photos;// PhotoItem
@property(nonatomic, assign) double price;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, assign) NSInteger userId;
@property(nonatomic, copy) NSString *weather;//天气
@property(nonatomic, copy) NSString *detailedAddress;

@property(nonatomic, assign) NSInteger level;
@property(nonatomic, assign) BOOL isFavourite;//是否点赞
@property(nonatomic, assign) BOOL isLikedAuthor;//是否已关注
@property(nonatomic, assign) NSInteger commentCount;

@property(nonatomic, assign) CGFloat cellHeight;

+ (NSArray *)albumList:(NSDictionary *)dict;
+ (AlbumModel *)albumModel:(NSDictionary *)dict;
@end

@interface PhotoItem : NSObject
@property(nonatomic, strong) NSNumber *photoId;
@property(nonatomic, copy) NSString *url;

@end
