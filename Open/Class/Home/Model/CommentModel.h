//
//  CommentModel.h
//  Open
//
//  Created by mfp on 17/9/5.
//  Copyright © 2017年 彭彬. All rights reserved.
//  消息评论

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface CommentData : BaseModel;

@end

@interface CommentModel : NSObject

@property(nonatomic, copy) NSString *content;
@property(nonatomic, assign) double createdAt;
@property(nonatomic, assign) NSInteger commentId;
@property(nonatomic, assign) NSInteger userId;
@property(nonatomic, assign) NSInteger albumId;
@property(nonatomic, copy) NSString *avatar;//用户头像
@property(nonatomic, copy) NSString *nickname;


@property(nonatomic, copy) NSString *fromNickname;
@property(nonatomic, strong) NSNumber *fromUserId;
@property(nonatomic, strong) NSNumber *messageId;
@property(nonatomic, copy) NSString *toNickname;
@property(nonatomic, strong) NSNumber *toUserId;
@property(nonatomic, copy) NSString *type;
@property(nonatomic, copy) NSString *typeText;
@property(nonatomic, strong) NSNumber *itemId;

+ (NSArray *)commentList:(NSDictionary *)dict;

@end
