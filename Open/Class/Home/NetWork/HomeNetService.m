//
//  HomeNetService.m
//  Open
//
//  Created by mfp on 17/7/31.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "HomeNetService.h"

@implementation HomeNetService

//首页banner
- (void)getHomeBannerSuccess:(responseSuccess)success
                      failed:(responseFailure)failure {
    [self requestGETwithUrl:kHomeBanner parameters:nil success:^(NSString *requestInfo, id responseObject) {
        DLog(@"%@",responseObject);
        NSArray *list = [AlbumModel albumList:responseObject];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//首页最新列表
- (void)getHomeLastPhotos:(BOOL)more
                  success:(responseSuccess)success
                   failed:(responseFailure)failure {
    if (!more) {
        HomeLastAlbumsPageIndex = 1;
    }
    
    [self requestGETwithUrl:kHomeLastPhotos parameters:@{@"page":@(HomeLastAlbumsPageIndex)}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        HomeLastAlbumsPageIndex += 1;
        DLog(@"%@",responseObject);
        NSArray *list = [AlbumModel albumList:responseObject];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//首页关注 accessToken page
- (void)getHomeUserLikeAlbumsMore:(BOOL)more
                          success:(responseSuccess)success
                           failed:(responseFailure)failure {
    if (!more) {
        UserLikeAlbumsPageIndex = 1;
    }
    [self requestGETwithUrl:kHomeUserLikeAlbums parameters:@{@"page":@(UserLikeAlbumsPageIndex)}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        UserLikeAlbumsPageIndex += 1;
        DLog(@"%@",responseObject);
        NSArray *list = [AlbumModel albumList:responseObject];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//首页精选列表 page
- (void)getHomeSelectedAlbums:(BOOL)more
                      success:(responseSuccess)success
                      failure:(responseFailure)failure {
    if (!more) {
        SelectedAlbumsPageIndex = 1;
    }
    [self requestGETwithUrl:kHomeSelectedAlbums parameters:@{@"page":@(SelectedAlbumsPageIndex)}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        SelectedAlbumsPageIndex += 1;
        NSArray *list = [AlbumModel albumList:responseObject];
        success(requestInfo,list);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//首页消息列表 评论 accessToken
- (void)getHomeMsgMore:(BOOL)more
               success:(responseSuccess)success
               failure:(responseFailure)failure {
    if (!more) {
        HomeMsgCommentPageIndex = 1;
    }
    [self requestGETwithUrl:kHomeUserCommentMsg parameters:@{@"page":@(HomeMsgCommentPageIndex)}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        NSArray *comments = [CommentModel commentList:responseObject];
        HomeMsgCommentPageIndex += 1;
        success(requestInfo,comments);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//首页消息列表 关注 accessToken
- (void)getUserLikeMsgMore:(BOOL)more
                   success:(responseSuccess)success
                   failure:(responseFailure)failure {
    if (!more) {
        HomeMsgFollowPageIndex = 1;
    }
    [self requestGETwithUrl:kHomeUserLikeMsg parameters:@{@"page":@(HomeMsgFollowPageIndex)}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        NSArray *comments = [CommentModel commentList:responseObject];
        HomeMsgFollowPageIndex += 1;
        success(requestInfo,comments);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

// 首页 - 获取消息推送设置信息 accessToken
- (void)getUserMsgGet:(responseSuccess)success
              failure:(responseFailure)failure {
    [self requestGETwithUrl:kHomeUserMsgGet parameters:nil success:^(NSString *requestInfo, id responseObject) {
        PushModel *mode = [PushModel yy_modelWithDictionary:responseObject[@"content"]];
        success(requestInfo,mode);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

// 设置消息推送 accessToken commentPush starPush likePush activityPush systemPush
- (void)setUserMsgGet:(NSDictionary *)params
              success:(responseSuccess)success
              failure:(responseFailure)failure {
    [self requestPOSTwithUrl:kHomeUserMsgSet parameters:params.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//首页消息列表 点赞 accessToken
- (void)userStarMsgMore:(BOOL)more
                success:(responseSuccess)success
                failure:(responseFailure)failure {
    if (!more) {
        HomeMsgStarPageIndex = 1;
    }
    [self requestGETwithUrl:kHomeUserStarMsg parameters:@{@"page":@(HomeMsgStarPageIndex)}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        NSArray *comments = [CommentModel commentList:responseObject];
        success(requestInfo,comments);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//首页 系统消息列表accessToken
- (void)homeSystemMsg:(responseSuccess)success
              failure:(responseFailure)failure {
    GlobalDataModel *gdm = [GlobalDataModel sharedInstance];
    [self requestGETwithUrl:kHomeUserSystemMsg parameters:@{@"accessToken":(gdm.accessToken?gdm.accessToken:@"")}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        NSArray *comments = [CommentModel commentList:responseObject];
        success(requestInfo,comments);
    } failure:^(NSError *error, id responseObject) {
        failure(error,responseObject);
    }];
}

//关注用户
- (void)usersLikes:(NSInteger)userid
              like:(BOOL)like
           success:(responseSuccess)success
           failure:(responseFailure)failure {
    NSMutableString *url = [NSMutableString stringWithFormat:@"users/%ld/likes",(long)userid];
    if (!like) {
        [url appendString:@"/delete"];
    }
    
    [self requestPOSTwithUrl:url parameters:@{@"user_id":@(userid)}.mutableCopy success:^(NSString *requestInfo, id responseObject) {
        success(requestInfo,responseObject);
    } failure:^(NSError *error, id responseObject) {
        DLog(@"%@",responseObject);
        failure(error,responseObject);
    }];

}

@end
