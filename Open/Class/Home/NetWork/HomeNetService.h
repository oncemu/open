//
//  HomeNetService.h
//  Open
//
//  Created by mfp on 17/7/31.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseNetService.h"
#import "AlbumModel.h"
#import "CommentModel.h"
#import "PushModel.h"

#define kHomeBanner            @"home/banner"//首页banner get
#define kHomeLastPhotos        @"home/last_albums"//首页 - 最新列表 get
#define kHomeUserLikeAlbums    @"home/liked_albums"//首页关注 post

#define kHomeSelectedAlbums @"home/selected_albums"// 首页 - 精选 - 列表
#define kHomeUserCommentMsg @"home/user/comment_messages"// 首页 - 消息列表: 评论
#define kHomeUserLikeMsg @"home/user/like_messages"// 首页 - 消息列表: 关注
#define kHomeUserMsgGet @"home/user/message_settings"// 首页 - 获取消息推送设置信息
#define kHomeUserMsgSet @"home/user/message_settings"// 首页 - 设置消息推送 post
#define kHomeUserStarMsg @"home/user/star_messages"// 首页 - 消息列表: 点赞
#define kHomeUserSystemMsg @"home/user/system_messages"// 首页 - 消息列表: 系统消息

@interface HomeNetService : BaseNetService
{
    NSInteger HomeLastAlbumsPageIndex;//首页最新分页
    NSInteger UserLikeAlbumsPageIndex;//首页关注分页
    NSInteger SelectedAlbumsPageIndex;//首页精选分页
    NSInteger HomeMsgStarPageIndex;//首页消息点赞分页
    NSInteger HomeMsgFollowPageIndex;//首页消息关注我分页
    NSInteger HomeMsgCommentPageIndex;//首页消息评论分页
}
//首页banner
- (void)getHomeBannerSuccess:(responseSuccess)success
                      failed:(responseFailure)failure;
//首页最新 page
- (void)getHomeLastPhotos:(BOOL)more
                  success:(responseSuccess)success
                   failed:(responseFailure)failure;

//首页关注 accessToken page
- (void)getHomeUserLikeAlbumsMore:(BOOL)more
                          success:(responseSuccess)success
                           failed:(responseFailure)failure;

//首页精选列表 page
- (void)getHomeSelectedAlbums:(BOOL)more
                      success:(responseSuccess)success
                      failure:(responseFailure)failure;

//首页消息列表 评论 accessToken
- (void)getHomeMsgMore:(BOOL)more
               success:(responseSuccess)success
               failure:(responseFailure)failure;

//首页消息列表 关注 accessToken
- (void)getUserLikeMsgMore:(BOOL)more
                   success:(responseSuccess)success
                   failure:(responseFailure)failure;
// 首页 - 获取消息推送设置信息 accessToken
- (void)getUserMsgGet:(responseSuccess)success
              failure:(responseFailure)failure;

// 设置消息推送 accessToken commentPush starPush likePush activityPush systemPush
- (void)setUserMsgGet:(NSDictionary *)params
              success:(responseSuccess)success
              failure:(responseFailure)failure;

//首页消息列表 点赞 accessToken
- (void)userStarMsgMore:(BOOL)more
                success:(responseSuccess)success
            failure:(responseFailure)failure;

//首页 系统消息列表accessToken
- (void)homeSystemMsg:(responseSuccess)success
              failure:(responseFailure)failure;

//关注用户 like, YES: 关注  NO：取消关注
- (void)usersLikes:(NSInteger)userid
              like:(BOOL)like
           success:(responseSuccess)success
           failure:(responseFailure)failure;
@end
