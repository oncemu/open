//
//  AddressModel.h
//  Open
//
//  Created by mfp on 17/9/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressModel : NSObject

@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSArray *sub;
@property(nonatomic, assign) NSInteger type;

@end


