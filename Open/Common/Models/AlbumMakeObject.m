//
//  AlbumMakeObject.m
//  Open
//
//  Created by mfp on 17/10/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "AlbumMakeObject.h"

#define RectValue(r) [NSValue valueWithCGRect:r]

@implementation AlbumMakeData

+ (instancetype)shareData {
    static dispatch_once_t onceToken;
    static AlbumMakeData *instance;
    dispatch_once(&onceToken, ^{
        instance = [[AlbumMakeData alloc] init];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.dataList = [NSMutableArray array];
    }
    return self;
}

- (BOOL)hasImage {
    BOOL hasImage = NO;
    for (AlbumMakeObject *obj in self.dataList) {
        if (obj.image) {
            hasImage = YES;
            break;
        }
    }
    return hasImage;
}

- (BOOL)hasExistAlbum:(AlbumFormatType)type {
    self.hasMake = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir;
    if ([fileManager fileExistsAtPath:[self checkFilePath:type] isDirectory:&isDir]) {
        return YES;
    }
    return NO;
}

- (void)saveAlbumData:(AlbumFormatType)type {
    NSMutableData *data = [NSMutableData data];
    NSKeyedArchiver *keyedArchiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [keyedArchiver encodeObject:self.dataList forKey:@"album_data"];
    [keyedArchiver finishEncoding];
    [data writeToFile:[self checkFilePath:type] atomically:YES];
}

- (void)getAlbumData:(AlbumFormatType)type {
    NSMutableData *data = [NSMutableData dataWithContentsOfFile:[self checkFilePath:type]];
    NSKeyedUnarchiver *keyedUnarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    NSArray *list = [keyedUnarchiver decodeObjectForKey:@"album_data"];
    [self.dataList removeAllObjects];
    if (list.count > 0) {
        [self.dataList addObjectsFromArray:list];
    }
    [keyedUnarchiver finishDecoding];
}

- (void)deleteAlbumData {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:[self checkFilePath:self.type] error:nil];
    [self.dataList removeAllObjects];
    self.hasMake = YES;
}

- (NSString *)checkFilePath:(AlbumFormatType)type {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDir = [paths firstObject];
    NSString *filePath = [NSString stringWithFormat:@"%@/%ld",cachesDir,(long)type];
    return filePath;
}

@end

@implementation AlbumMakeObject

- (instancetype)mutableCopyWithZone:(NSZone *)zone {
    AlbumMakeObject *object = [[self class] allocWithZone:zone];
    object.type = _type;
    object.mode = _mode;
    object.contentList = [_contentList mutableCopy];
    object.imageCount = _imageCount;
    object.textCount = _textCount;
    object.elementCount = _elementCount;
    NSMutableArray *contentList = [NSMutableArray array];
    for (int i = 0; i<_contentList.count; i++) {
        BaseAlbumObject *obj = [_contentList[i] copy];
        [contentList addObject:obj];
    }
    object.contentList = contentList;
    object.backColor = [_backColor copy];
    object.image = [_image copy];
    object.pageIndex = _pageIndex;
    object.rectValues = [_rectValues mutableCopy];
    return object;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.contentList = [NSMutableArray array];
        self.backColor = WhiteColor;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.type = [aDecoder decodeIntegerForKey:@"album_type"];
        self.mode = [aDecoder decodeIntegerForKey:@"album_mode"];
        self.contentList = [aDecoder decodeObjectForKey:@"album_contentList"];
        self.formSize = [aDecoder decodeCGSizeForKey:@"album_formSize"];
        self.backColor = [aDecoder decodeObjectForKey:@"album_backColor"];
        self.image = [aDecoder decodeObjectForKey:@"album_image"];
        self.pageIndex = [aDecoder decodeIntegerForKey:@"album_pageIndex"];
        self.rectValues = [aDecoder decodeObjectForKey:@"album_rectValues"];
        self.imageCount = [aDecoder decodeIntegerForKey:@"album_imageCount"];
        self.textCount = [aDecoder decodeIntegerForKey:@"album_textCount"];
        self.elementCount = [aDecoder decodeIntegerForKey:@"album_elementCount"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:_type forKey:@"album_type"];
    [aCoder encodeInteger:_mode forKey:@"album_mode"];
    [aCoder encodeObject:_contentList forKey:@"album_contentList"];
    [aCoder encodeCGSize:_formSize forKey:@"album_formSize"];
    [aCoder encodeObject:_backColor forKey:@"album_backColor"];
    [aCoder encodeObject:_image forKey:@"album_image"];
    [aCoder encodeInteger:_pageIndex forKey:@"album_pageIndex"];
    [aCoder encodeObject:_rectValues forKey:@"album_rectValues"];
    [aCoder encodeInteger:_imageCount forKey:@"album_imageCount"];
    [aCoder encodeInteger:_textCount forKey:@"album_textCount"];
    [aCoder encodeInteger:_elementCount forKey:@"album_elementCount"];
}

- (void)setType:(AlbumFormatType)type {
    _type = type;
    CGFloat width = (SCREEN_WIDTH - 140);
    CGFloat height = 0;
    switch (type) {
        case AlbumFormatType_10_Asset:
            height = ((SCREEN_WIDTH - 140) * 468.0f)/1068.0f;
            if (height > (SCREEN_HEIGHT - (NavigationBar_HEIGHT + StatusBar_HEIGHT + kTabBarHeight + 20))) {
                height = (SCREEN_HEIGHT - (NavigationBar_HEIGHT + StatusBar_HEIGHT + kTabBarHeight + 20));
                width = (height * 1068.0f) / 468.0f;
            }
            break;
        case AlbumFormatType_12_Portrait:
            height = ((SCREEN_WIDTH - 140) * 556.0f)/936.0f;
            if (height > (SCREEN_HEIGHT - (NavigationBar_HEIGHT + StatusBar_HEIGHT + kTabBarHeight + 20))) {
                height = (SCREEN_HEIGHT - (NavigationBar_HEIGHT + StatusBar_HEIGHT + kTabBarHeight + 20));
                width = (height * 936.0f) / 556.0f;
            }
            break;
        case AlbumFormatType_7X10_Landscape:
            height = ((SCREEN_WIDTH - 140) * 340.f)/1066.0f;
            if (height > (SCREEN_HEIGHT - (NavigationBar_HEIGHT + StatusBar_HEIGHT + kTabBarHeight + 20))) {
                height = (SCREEN_HEIGHT - (NavigationBar_HEIGHT + StatusBar_HEIGHT + kTabBarHeight + 20));
                width = (height * 1066.0f) / 340.0f;
            }
            break;
        default:
            break;
    }
    self.formSize = Size(width, height);
}

- (void)setMode:(AlbumFormatMode)mode {
    _mode = mode;
    _imageCount = _textCount = _elementCount = 0;
    [self.contentList removeAllObjects];
    [self.rectValues removeAllObjects];
    NSArray *frameList = @[];
    switch (mode) {
        case AlbumFormatMode_1:
            switch (self.type) {
                case AlbumFormatType_10_Asset:
                case AlbumFormatType_12_Portrait:
                    frameList = @[RectValue(CM(5, 5, self.formSize.width/2 - 24 - 10, self.formSize.height - 10)),
                                  RectValue(CM(self.formSize.width/2 + 24 + 5, 5, self.formSize.width/2 - 24 - 10, self.formSize.height - 10))];
                    break;
                case AlbumFormatType_7X10_Landscape:
                {
                    CGFloat first_width = self.formSize.width/2-5-18.5;
                    frameList = @[RectValue(CM(5, 5, first_width, self.formSize.height-10)),
                                  RectValue(CM(self.formSize.width/2+18.5, 5, first_width, self.formSize.height-10))];
                }
                    break;
                default:
                    break;
            }
            break;
        case AlbumFormatMode_2:
            switch (self.type) {
                case AlbumFormatType_10_Asset:
                    frameList = @[RectValue(CM(5, 5, self.formSize.width/2 - 24 - 10, (self.formSize.height - 15)/2)),
                                  RectValue(CM(5, (self.formSize.height - 15)/2 + 10, self.formSize.width/2 - 24 - 10, (self.formSize.height - 15)/2)),
                                  RectValue(CM(self.formSize.width/2 + 24 + 5, 5, self.formSize.width/2 - 24 - 10, self.formSize.height - 10))];
                    break;
                case AlbumFormatType_12_Portrait:
                    frameList = @[RectValue(CM(15, 20, self.formSize.width/2 - 15 - 16, (self.formSize.height - 55)/2.0)),RectValue(CM(15, self.formSize.height/2 + 7.5, self.formSize.width/2 - 15 - 16, (self.formSize.height - 55)/2.0)),RectValue(CM(self.formSize.width/2+16, 7, self.formSize.width/2 - 7 - 16, self.formSize.height - 14))];
                    break;
                case AlbumFormatType_7X10_Landscape:
                {
                    CGFloat first_width = self.formSize.width/2-5-18.5;
                    CGFloat first_heith = (self.formSize.height-10-7.5)/2;
                    frameList = @[RectValue(CM(5, 5, first_width, first_heith)),
                                  RectValue(CM(5, self.formSize.height/2+3.5, first_width, first_heith)),
                                  RectValue(CM(self.formSize.width/2+18.5, 5, first_width, self.formSize.height-10))];
                }
                    break;
                default:
                    break;
            }
            break;
        case AlbumFormatMode_3:
            switch (self.type) {
                case AlbumFormatType_10_Asset:
                    frameList = @[RectValue(CM(5, 5, (self.formSize.width/2 - 24 - 15)/2, (self.formSize.height - 15)/2)),
                                  RectValue(CM((self.formSize.width/2 - 24 - 15)/2 + 10, 5, (self.formSize.width/2 - 24 - 15)/2, (self.formSize.height - 15)/2)),
                                  RectValue(CM(5, (self.formSize.height - 15)/2 + 10, self.formSize.width/2 - 24 - 10, (self.formSize.height - 15)/2)),
                                  RectValue(CM(self.formSize.width/2 + 24 + 5, 5, self.formSize.width/2 - 24 - 10, self.formSize.height - 10))];
                    break;
                case AlbumFormatType_12_Portrait:
                {
                    frameList = @[RectValue(CM(5, 5, (self.formSize.width/2-5-16-16)/2 , (self.formSize.width/2-5-16-16)/2)),
                                  RectValue(CM(5+(self.formSize.width/2-5-16-16)/2+16, 5, (self.formSize.width/2-5-16-16)/2, (self.formSize.width/2-5-16-16)/2)),
                                  RectValue(CM(5, 5+(self.formSize.width/2-5-16-16)/2+18, self.formSize.width/2-5-16, self.formSize.height - (5+(self.formSize.width/2-5-16-16)/2+18)-5)),
                                  RectValue(CM(self.formSize.width/2+16, 5, self.formSize.width/2-16-5, self.formSize.height-10))];
                }
                    break;
                case AlbumFormatType_7X10_Landscape:
                {
                    CGFloat first_width = (self.formSize.width/2-10-18.5)/2;
                    CGFloat first_height = (self.formSize.height - 15)/2;
                    frameList = @[RectValue(CM(5, 5, first_width, first_height)),
                                  RectValue(CM(first_width+10, 5, first_width, first_height)),
                                  RectValue(CM(5, 10+first_height, first_width*2+5, first_height)),
                                  RectValue(CM(self.formSize.width/2+18.5, 5, first_width*2+5, first_height*2+5))];
                }
                    break;
                default:
                    break;
            }
            break;
        case AlbumFormatMode_4:
            switch (self.type) {
                case AlbumFormatType_10_Asset:
                    frameList = @[RectValue(CM(5, 5, (self.formSize.width/2 - 24 - 15)/2, (self.formSize.height - 15)/2)),RectValue(CM((self.formSize.width/2 - 24 - 15)/2 + 10, 5, (self.formSize.width/2 - 24 - 15)/2, self.formSize.height - 10)),RectValue(CM(5, (self.formSize.height - 15)/2 + 10, (self.formSize.width/2 - 24 - 15)/2, (self.formSize.height - 15)/2)),RectValue(CM(self.formSize.width/2 + 24 + 5, 5, self.formSize.width/2 - 24 - 10, self.formSize.height - 10))];

                    break;
                case AlbumFormatType_12_Portrait:
                {
                    CGFloat height_2 = (self.formSize.height-10-9)/2;
                    CGFloat first_width = (self.formSize.width/2-5-17-16)/2+4;
                    frameList = @[RectValue(CM(5, 5, first_width, height_2)),RectValue(CM(first_width+5+17, 5, first_width-4, self.formSize.height-10)),RectValue(CM(5, height_2+5+9, first_width, height_2)),RectValue(CM(self.formSize.width/2+16, 5, self.formSize.width/2-16-5, self.formSize.height-10))];
                }
                    break;
                case AlbumFormatType_7X10_Landscape:
                {
                    CGFloat first_width = (self.formSize.width/2-10-18.5)/2;
                    CGFloat first_height = (self.formSize.height - 15)/2;
                    frameList = @[RectValue(CM(5, 5, first_width, first_height)),
                                  RectValue(CM(10+first_width, 5, first_width, first_height*2+5)),
                                  RectValue(CM(5, first_height+10, first_width, first_height)),
                                  RectValue(CM(self.formSize.width/2+18.5, 5, first_width*2+5, first_height*2+5))];
                }
                    break;
                default:
                    break;
            }
            break;
        case AlbumFormatMode_5:
            switch (self.type) {
                case AlbumFormatType_10_Asset:
                {
                    CGFloat imageSize = (self.formSize.width/2 - 24 - 15 - 20)/4;
                    CGFloat top = (self.formSize.height - imageSize * 2 - 20)/2;
                    frameList = @[RectValue(CM(15, top, imageSize, imageSize)),
                                  RectValue(CM(15+imageSize + 10, top, imageSize, imageSize)),
                                  RectValue(CM(imageSize*2+15 + 20, top, imageSize, imageSize)),
                                  RectValue(CM(15 + imageSize/2, top + imageSize + 20, imageSize, imageSize)),
                                  RectValue(CM(15 + imageSize/2 + imageSize + 10, top + imageSize + 20, imageSize, imageSize)),
                                  RectValue(CM(15 + imageSize/2 + imageSize*2 + 20, top + imageSize + 20, imageSize, imageSize)),
                                  RectValue(CM(self.formSize.width/2 + 24 + 5, 5, self.formSize.width/2 - 24 - 10, self.formSize.height - 10))];
                }
                    break;
                case AlbumFormatType_12_Portrait:
                    frameList = @[RectValue(CM(5, 36, 47, 47)),
                                  RectValue(CM(5+(47+20), 36, 47, 47)),
                                  RectValue(CM(5+(47+20)*2, 36, 47, 47)),
                                  RectValue(CM(27, 36+40+47, 47, 47)),
                                  RectValue(CM(27+(47+20), 36+40+47, 47, 47)),
                                  RectValue(CM(27+(47+20)*2, 36+40+47, 47, 47)),
                                  RectValue(CM(self.formSize.width/2+16, 5, self.formSize.width/2-16-5, self.formSize.height-10))];
                    break;
                case AlbumFormatType_7X10_Landscape:
                {
                    CGFloat first_size = (self.formSize.width/2-25)/4;
                    frameList = @[RectValue(CM(5, 16, first_size, first_size)),
                                  RectValue(CM(5+(first_size+5), 16, first_size, first_size)),
                                  RectValue(CM(5+(first_size+5)*2, 16, first_size, first_size)),
                                  RectValue(CM(5+first_size, self.formSize.height-16-first_size, first_size, first_size)),
                                  RectValue(CM(5+first_size+(first_size+5), self.formSize.height-16-first_size, first_size, first_size)),
                                  RectValue(CM(5+first_size+(first_size+5)*2, self.formSize.height-16-first_size, first_size, first_size)),
                                  RectValue(CM(self.formSize.width/2+18.5, 5, self.formSize.width/2-18.5-5, self.formSize.height-10))];
                }
                    break;
                default:
                    break;
            }
            break;
        case AlbumFormatMode_6:
            switch (self.type) {
                case AlbumFormatType_10_Asset:

                    break;
                case AlbumFormatType_12_Portrait:
                {
                    CGFloat first_width = (self.formSize.width*258.0)/644.0;
                    CGFloat first_height = (self.formSize.height-15-10)/3;
                    frameList = @[RectValue(CM(5, 5, first_width, first_height)),
                                  RectValue(CM(5, 5+7.5+first_height, first_width, first_height)),
                                  RectValue(CM(5, 5+(7.5+first_height)*2, first_width, first_height)),
                                  RectValue(CM(5+first_width+5, 5, self.formSize.width-first_width-15, self.formSize.height-10))];
                }
                    break;
                case AlbumFormatType_7X10_Landscape:
                {
                    CGFloat first_width = ((self.formSize.width-14)*338.0)/698.0;
                    CGFloat first_height = (self.formSize.height-20)/3;
                    frameList = @[RectValue(CM(5, 5, first_width, first_height)),
                                  RectValue(CM(5, 5+first_height+5, first_width, first_height)),
                                  RectValue(CM(5, 5+(first_height+5)*2, first_width, first_height)),
                                  RectValue(CM(10+first_width, 5, self.formSize.width-15-first_width, first_height*3+10))];
                }
                    break;
                default:
                    break;
            }
            break;
        case AlbumFormatMode_7:
            switch (self.type) {
                case AlbumFormatType_10_Asset:

                    break;
                case AlbumFormatType_12_Portrait:
                {
                    CGFloat first_width = (self.formSize.width/2-5-16-7)/2;
                    CGFloat first_height = ((self.formSize.height-17)*204.0)/308.0;
                    frameList = @[RectValue(CM(5, 5, first_width, first_height)),
                                  RectValue(CM(5+first_width+7, 5, first_width, first_height)),
                                  RectValue(CM(5, 5+first_height+7, first_width*2+7, self.formSize.height-first_height-17)),
                                  RectValue(CM(self.formSize.width/2+16, 5, self.formSize.width/2-16-5, (self.formSize.height-17.5)/2)),
                                  RectValue(CM(self.formSize.width/2+16, self.formSize.height/2+3.5, self.formSize.width/2-16-5, (self.formSize.height-17.5)/2))];
                }
                    break;
                case AlbumFormatType_7X10_Landscape:
                {
                    CGFloat first_width = (self.formSize.width/2-16)/2;
                    CGFloat first_height = ((self.formSize.height-16)*122.0)/178.0;
                    frameList = @[RectValue(CM(5, 5, first_width, first_height)),
                                  RectValue(CM(5+first_width+6, 5, first_width, first_height)),
                                  RectValue(CM(5, 5+first_height+6, first_width*2+6, self.formSize.height-10-first_height-6)),
                                  RectValue(CM(self.formSize.width/2+5, 5, first_width*2+6, (self.formSize.height-16)/2)),
                                  RectValue(CM(self.formSize.width/2+5, self.formSize.height/2+3, first_width*2+6, (self.formSize.height-16)/2))];
                }
                    break;
                default:
                    break;
            }

            break;
            default:
            break;
    }
    [self.rectValues addObjectsFromArray:frameList];
    [self createAlbumImage];
}

- (void)createAlbumImage {
    for (NSInteger i = 0; i<self.rectValues.count; i++) {
        NSValue *rectV = self.rectValues[i];
        BaseAlbumObject *imageobj = [BaseAlbumObject getAlbumImage:nil frame:rectV.CGRectValue];
        imageobj.type = AlbumObjectTypeForm;
        imageobj.center = Point(rectV.CGRectValue.size.width/2.0f, rectV.CGRectValue.size.height/2.0f);
        [self addObject:imageobj];
    }
}

- (NSMutableArray *)rectValues {
    if (_rectValues == nil) {
        _rectValues = [NSMutableArray array];
    }
    return _rectValues;
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    if (![object isKindOfClass:[self class]]) {
        return NO;
    }
    return [self isEqualToAlbumObject:(AlbumMakeObject *)object];
}

- (BOOL)isEqualToAlbumObject:(AlbumMakeObject *)object {
    if (!object) {
        return NO;
    }
    BOOL typeEqual = self.type == object.type;
    BOOL modeEqual = self.mode == object.mode;
    BOOL contentEqual = [self.contentList isEqualToArray:object.contentList];
    BOOL backColorEqual = CGColorEqualToColor(self.backColor.CGColor, object.backColor.CGColor);
    return typeEqual && modeEqual && contentEqual && backColorEqual;
}

- (void)addObject:(BaseAlbumObject *)object {
    [self.contentList addObject:object];
    if (object.type == AlbumObjectTypeImage || object.type == AlbumObjectTypeForm) {
        self.imageCount += 1;
    }
    if (object.type == AlbumObjectTypeElement) {
        self.elementCount += 1;
    }
    if (object.type == AlbumObjectTypeText) {
        self.textCount += 1;
    }
}

- (void)removeObject:(BaseAlbumObject *)object {
    [self.contentList removeObject:object];
    if (object.type == AlbumObjectTypeImage || object.type == AlbumObjectTypeForm) {
        self.imageCount -= 1;
    }
    if (object.type == AlbumObjectTypeElement) {
        self.elementCount -= 1;
    }
    if (object.type == AlbumObjectTypeText) {
        self.textCount -= 1;
    }
}

@end

@implementation BaseAlbumObject

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.type = [aDecoder decodeIntegerForKey:@"base_type"];
        self.frameValue = [aDecoder decodeObjectForKey:@"base_frameValue"];
        self.transform = [aDecoder decodeCGAffineTransformForKey:@"base_transform"];
        self.center = [aDecoder decodeCGPointForKey:@"base_center"];
        self.image = [aDecoder decodeObjectForKey:@"base_image"];
        self.text = [aDecoder decodeObjectForKey:@"base_text"];
        self.font = [aDecoder decodeObjectForKey:@"base_font"];
        self.textColor = [aDecoder decodeObjectForKey:@"base_textColor"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:_type forKey:@"base_type"];
    [aCoder encodeObject:_frameValue forKey:@"base_frameValue"];
    [aCoder encodeCGAffineTransform:_transform forKey:@"base_transform"];
    [aCoder encodeCGPoint:_center forKey:@"base_center"];
    [aCoder encodeObject:_image forKey:@"base_image"];
    [aCoder encodeObject:_text forKey:@"base_text"];
    [aCoder encodeObject:_font forKey:@"base_font"];
    [aCoder encodeObject:_textColor forKey:@"base_textColor"];
}

- (instancetype)mutableCopyWithZone:(NSZone *)zone {
    BaseAlbumObject *object = [[self class] allocWithZone:zone];
    object.frameValue = [_frameValue mutableCopy];
    object.transform = _transform;
    object.center = _center;
    object.type = _type;
    object.image = [_image copy];
    object.font = [_font copy];
    object.textColor = [_textColor copy];
    return object;
}

+ (BaseAlbumObject *)getAlbumImage:(UIImage *)image frame:(CGRect)frame {
    BaseAlbumObject *obj = [[BaseAlbumObject alloc] init];
    obj.image = image;
    obj.frameValue = [NSValue valueWithCGRect:frame];
    obj.center = Point(CGRectGetMidX(frame), CGRectGetMidY(frame));
    obj.transform = CGAffineTransformIdentity;
    return obj;
}

+ (BaseAlbumObject *)getAlbumText:(NSString *)text frame:(CGRect)frame color:(UIColor *)color font:(UIFont *)font {
    BaseAlbumObject *obj = [[BaseAlbumObject alloc] init];
    obj.type = AlbumObjectTypeText;
    obj.text = text;
    obj.font = font;
    obj.textColor = color;
    obj.frameValue = [NSValue valueWithCGRect:frame];
    obj.transform = CGAffineTransformIdentity;
    obj.center = Point(CGRectGetMidX(frame), CGRectGetMidY(frame));
    return obj;
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    if (![object isKindOfClass:[self class]]) {
        return NO;
    }
    return [self isEqualToAlbumObject:(BaseAlbumObject *)object];
}

- (BOOL)isEqualToAlbumObject:(BaseAlbumObject *)object {
    if (!object) {
        return NO;
    }
    BOOL transformEqual = CGAffineTransformEqualToTransform(self.transform, object.transform);
    BOOL centerEqual = CGPointEqualToPoint(self.center, object.center);
    BOOL imageEqual = (!self.image && !object.image) || self.image == object.image;
    
    return transformEqual && centerEqual && imageEqual;
}

@end
