//
//  PaymentUtil.m
//  Open
//
//  Created by mfp on 17/9/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PaymentUtil.h"

@implementation PaymentUtil

+ (id)sharedUtil {
    static dispatch_once_t onceToken;
    static PaymentUtil *instance;
    dispatch_once(&onceToken, ^{
        instance = [[PaymentUtil alloc] init];
    });
    return instance;
}

- (void)payOrder:(NSDictionary *)info {
    NSDictionary *dict = info[@"content"];
    NSString *pay_party = dict[@"channel"];
    if ([pay_party isEqualToString:@"wx"]) {
        [self payOrderByWX:dict];
    }else if([pay_party isEqualToString:@"alipay"]){
        [self payOrderByAlipay:dict];
    }
}

- (void)payOrderByWX:(NSDictionary *)info {

}

- (void)payOrderByAlipay:(NSDictionary *)info {
    [Pingpp createPayment:info appURLScheme:@"open" withCompletion:^(NSString *result, PingppError *error) {
        DLog(@"%@ %@",result,error);
    }];
}


@end
