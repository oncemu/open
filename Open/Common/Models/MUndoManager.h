//
//  MUndoManager.h
//  Open
//
//  Created by mfp on 17/10/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MUndoManager : NSObject
@property(nonatomic, strong) NSUndoManager *manager;

+ (instancetype)sharedInstance;

//撤销
- (void)undo;

//恢复
- (void)redo;

@end
