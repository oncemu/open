//
//  BaseModel.h
//  Open
//
//  Created by mfp on 17/9/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject

@property(nonatomic, assign) BOOL first;
@property(nonatomic, assign) BOOL last;
@property(nonatomic, assign) NSInteger number;
@property(nonatomic, assign) NSInteger numberOfElements;
@property(nonatomic, assign) NSInteger size;
@property(nonatomic, strong) NSArray *sort;
@property(nonatomic, assign) NSInteger totalElements;
@property(nonatomic, assign) NSInteger totalPages;

@property(nonatomic, strong) NSArray *content;

+ (id)parseBaseModel:(NSDictionary *)content;

+ (NSArray *)parseListModel:(NSDictionary *)content;
@end
