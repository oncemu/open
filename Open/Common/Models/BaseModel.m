//
//  BaseModel.m
//  Open
//
//  Created by mfp on 17/9/11.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

+ (id)parseBaseModel:(NSDictionary *)content {
    if (![content objectForKey:@"content"]) {
        return nil;
    }
    id mode = [[self class] yy_modelWithDictionary:content[@"content"]];
    return mode;
}

+ (NSArray *)parseListModel:(NSDictionary *)content {
    if (![content objectForKey:@"content"]) {
        return nil;
    }
    NSArray *list = [NSArray yy_modelArrayWithClass:[self class] json:content[@"content"]];
    return list;
}

@end
