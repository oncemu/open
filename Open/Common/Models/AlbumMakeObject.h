//
//  AlbumMakeObject.h
//  Open
//
//  Created by mfp on 17/10/23.
//  Copyright © 2017年 彭彬. All rights reserved.
//  存储编辑内容

#import <Foundation/Foundation.h>
#import <Foundation/NSObject.h>

@class BaseAlbumObject;

//制作种类
typedef NS_ENUM(NSInteger,AlbumFormatType) {
    //相册
    AlbumFormatType_10_Asset = 3200,//10寸方册
    AlbumFormatType_12_Portrait = 3500,//12寸竖册
    AlbumFormatType_7X10_Landscape = 3000,//7x10寸横册
    //宝丽来照片
    AlbumFormatType_BLL_6 = 8, // 6寸
    AlbumFormatType_BLL_7 = 10, // 7寸
    AlbumFormatType_BLL_8 = 30, // 8寸
    AlbumFormatType_BLL_10 = 100, //10寸
    //摆台
    AlbumFormatType_Talbe = 300,
    //画框
    AlbumFormatType_Frame = 600,
    //台历
    AlbumFormatType_DestCalendar = 680,
    //明信片
    AlbumFormatType_PostCart = 260,
    //公仔
    AlbumFormatType_Doll = 400
};

//模板类型 （每种相册模板数量相同，所以同一个枚举）
typedef NS_ENUM(NSInteger,AlbumFormatMode) {
    AlbumFormatMode_1 = 0,
    AlbumFormatMode_2 = 1,
    AlbumFormatMode_3 = 2,
    AlbumFormatMode_4 = 3,
    AlbumFormatMode_5 = 4,
    AlbumFormatMode_6 = 5,
    AlbumFormatMode_7 = 6,
    AlbumFormatMode_blank = 7 //空白模板
};

//页面元素的类型
typedef NS_ENUM(NSInteger, AlbumObjectType) {
    AlbumObjectTypeForm = 0,//模板图
    AlbumObjectTypeImage = 1,//添图
    AlbumObjectTypeText = 2,//文字
    AlbumObjectTypeElement = 4 //元素
};

//保存某个相册的数据
@interface AlbumMakeData : NSObject

@property(nonatomic, strong) NSMutableArray *dataList;///<AlbumMakeObject>
@property(nonatomic, assign) AlbumFormatType type;//相册种类
@property(nonatomic, assign) BOOL hasImage;//是否已经有制作的图片
@property(nonatomic, assign) BOOL hasMake;//是否已经提交订单

+ (instancetype)shareData;

- (BOOL)hasExistAlbum:(AlbumFormatType)type;

- (void)saveAlbumData:(AlbumFormatType)type;

- (void)getAlbumData:(AlbumFormatType)type;

- (void)deleteAlbumData;

@end

//模板object，最终生成内容都由此结构保存
@interface AlbumMakeObject : NSObject<NSMutableCopying,NSCoding>
@property(nonatomic, assign) AlbumFormatType type;//相册种类
@property(nonatomic, assign) AlbumFormatMode mode;//版式
@property(nonatomic, strong) NSMutableArray *contentList;//所有内容数组
@property(nonatomic, assign) CGSize formSize;//模板尺寸
@property(nonatomic, strong) UIColor *backColor;//底色
@property(nonatomic, strong) UIImage *image;//制作完成的图
@property(nonatomic, assign) NSInteger pageIndex;//张数坐标 
@property(nonatomic, strong) NSMutableArray *rectValues;//固定模板的坐标

@property(nonatomic, assign) NSInteger imageCount;//已经添图数量(包括规定模板图)
@property(nonatomic, assign) NSInteger textCount;//已经添文字
@property(nonatomic, assign) NSInteger elementCount;//已经元素数量

- (void)addObject:(BaseAlbumObject *)object;

- (void)removeObject:(BaseAlbumObject *)object;

@end

@interface BaseAlbumObject : NSObject<NSMutableCopying,NSCoding>
@property(nonatomic, assign) AlbumObjectType type;
@property(nonatomic, strong) NSValue *frameValue;//坐标
@property(nonatomic, assign) CGAffineTransform transform;
@property(nonatomic, assign) CGPoint center;
//根据添加内容赋值，
@property(nonatomic, strong) UIImage *image;//图片
@property(nonatomic, strong) NSString *text;
@property(nonatomic, strong) UIFont *font;
@property(nonatomic, strong) UIColor *textColor;

//获取图片object
+ (BaseAlbumObject *)getAlbumImage:(UIImage *)image frame:(CGRect)frame;

//获取文字object
+ (BaseAlbumObject *)getAlbumText:(NSString *)text frame:(CGRect)frame color:(UIColor *)color font:(UIFont *)font;

@end
