//
//  GlobalDataModel.m
//
//  Created by jiang

#import "GlobalDataModel.h"
#import "NSUserDefaults+RMSaveCustomObject.h"
#import "RMMapper.h"

#define kUserInfoItem @"UserInfoItem"
#define kBabyInfoItem @"BabyInfoItem"

@interface GlobalDataModel (){

}

- (void)initData;
//userdefault存储通用方法
- (id)getObjectFromUserDefaults:(NSString *)key;
- (void)setObjectToUserDefaults:(id)object forKey:(NSString*)key;
@end

@implementation GlobalDataModel

#pragma mark -
#pragma mark Singleton Methods
+ (id)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init]; // or some other init method
        [_sharedObject initData];
    });
    return _sharedObject;
}


#pragma mark -
#pragma mark UserDefault Manage Methods

- (void)setAccessToken:(NSString *)accessToken {
    _accessToken = accessToken;
     [[self userDefault]setObject:accessToken forKey:@"accessToken"];
}

- (void)setUser:(User *)user {
    _user = user;
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
    [self setObjectToUserDefaults:data forKey:@"user"];
}

//- (void)setMarket:(Market *)market {
//    _market = market;
//    [[self userDefault] rm_setCustomObject:market forKey:kMarketItem];
//}

- (void)initData {
    _accessToken = [self getObjectFromUserDefaults:@"accessToken"];
    _networkModel = [[NetworkModel alloc] init];
    NSData *data = [self getObjectFromUserDefaults:@"user"];
    _user = (User *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
}


- (NSUserDefaults *)userDefault {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    return ud;
}

- (id)getObjectFromUserDefaults:(NSString *)key {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    return [ud objectForKey:key];
}

- (void)setObjectToUserDefaults:(id)object forKey:(NSString*)key {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:object forKey:key];
    [ud synchronize];
}

#pragma mark -
#pragma mark Property Manage Methods

- (void)archive
{
    [self setObjectToUserDefaults:self.accessToken forKey:@"accessToken"];
}

- (BOOL)isLogin {
    if (self.accessToken) {
        return YES;
    }
    return NO;
}

- (BOOL)isNetworkReachable {
    return (self.networkModel.networkStatus != NotReachable);
}

- (void)loginOut {
    _accessToken = nil;
    [self archive];
}

//根据屏幕原始大小返回scale系数
- (CGSize)getCurrentScale {
    //处理缩放比例
    //    CGFloat imageScale = self.scale / 100.0;
    
    CGSize baseSize = CGSizeMake(750.0 / 2.0, 1334.0 / 2.0); //切图原始大小
    CGSize size = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
    ;
    //计算绽放比例
    CGFloat widthScale = (size.width) / baseSize.width;
    CGFloat heightScale = (size.height) / baseSize.height;
    
    return CGSizeMake(widthScale, heightScale);
    
}

@end
