//
//  GlobalDataModel.h
//
//  Created by jiang on 12-5-2.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.

#import <Foundation/Foundation.h>
#import "NetworkModel.h"
#import "User.h"


@interface GlobalDataModel : NSObject

@property(nonatomic, strong) User *user;

/**
 设备信息
 */
@property (nonatomic, strong) NSString *accessToken;

/**
 *  渠道名称
 */
@property (nonatomic, copy) NSString *channel;



@property(nonatomic,retain) NetworkModel *networkModel;

+ (id)sharedInstance;
- (void)archive;
- (BOOL)isLogin;
- (BOOL)isNetworkReachable;
- (void)loginOut;
- (CGSize)getCurrentScale;
@end
