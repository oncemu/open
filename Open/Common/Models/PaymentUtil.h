//
//  PaymentUtil.h
//  Open
//
//  Created by mfp on 17/9/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pingpp.h"

typedef void (^paySuccess)();
typedef void (^payFailure)(NSString *msg);

@interface PaymentUtil : NSObject
@property(nonatomic, copy) paySuccess successBlock;
@property(nonatomic, copy) payFailure failureBlock;

+ (id)sharedUtil;

- (void)payOrder:(NSDictionary *)info;

@end
