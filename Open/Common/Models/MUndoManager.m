//
//  MUndoManager.m
//  Open
//
//  Created by mfp on 17/10/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "MUndoManager.h"

@implementation MUndoManager

+ (instancetype)sharedInstance {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.manager = [[NSUndoManager alloc] init];
        [self.manager setLevelsOfUndo:20];  //设置最大极限，当达到极限时扔掉旧的撤销
    }
    return self;
}

- (void)undo {
    if ([self.manager canUndo]) {
        [self.manager undo];
    }
}

- (void)redo {
    if ([self.manager canRedo]) {
        [self.manager redo];
    }
}

@end
