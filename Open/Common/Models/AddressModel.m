//
//  AddressModel.m
//  Open
//
//  Created by mfp on 17/9/24.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "AddressModel.h"

@implementation AddressModel

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{ @"sub" : [AddressModel class]};
}

@end
