//
//  TSYStringUtil.h
//  Core
//
//  Created by Once on 14-5-16.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface StringUtil : NSObject

+ (instancetype)shareInstance;

+ (BOOL)isNONil:(NSString*)content;//检测字符串是否为空

+ (BOOL)checkIsPhoneNumber:(NSString *)content;//检测手机号码

+(BOOL)checkIsEmail:(NSString *)content;//检测email

+ (BOOL)validateIdentityCard: (NSString *)identityCard;


//得到一个字符串的前多少位 index 从0计算 例如Str = @"123" ， 想要取12 ， index 设为 2
+ (NSString *)getPerfixNumberWithIndex:(NSUInteger )index WithResourceStr:(NSString *)resourceStr;

+ (NSDate *)getUserLocaleDateFromAnyDate:(NSDate *)anyData;//标准时间转换成当前时间

+ (NSString *)getUserLocaStringDateWith:(NSString *)UTCString;

+ (NSDate *)stringToDate:(NSString *)time;

+ (NSString *)chectWeek:(NSInteger)day;//检查当前时间是星期几

/*
oldStr 是要改变的字符串，法则：如果odlStr.legth >= 5 ，返回的Str 就是对应的*.*万；
 例如：oldStr = @"12345";  返回的是  newStr = @"1.2万";
*/
+ (NSString *)backNewStrWithOldStr:(NSString *)oldStr;


/*
  去除掉 oldStr 中的前后的空格（中间的空格不移除），返回新的NewStr 中前后不含空格（中间可能会含有空格）
  oldStr 不能为空
*/
+ (NSString *)backBothEndsRemoveOfSpaceNewStrWithOldStr:(NSString *)oldStr;

/*
 去除掉 oldStr 中的所有的空格（中间的空格也会移除），返回新的 NewStr 中不含空格（中间也不含）
 oldStr 不能为空
 */
+ (NSString *)backRemoveOfSpaceNewStrWithOldStr:(NSString *)oldStr;

//+(NSString *)stringDisposeWithFloat:(float)floatValue;


+(CGSize)getSizeWith:(NSString*)string font:(UIFont *)font size:(CGSize)size;

+(NSString *)convertToUserDefaultTime:(NSString *)time;

+(CGFloat)getLabelLength:(NSString *)strString font:(UIFont*)font height:(CGFloat)height;

+(CGFloat)getLabelHeight:(NSString *)string font:(UIFont *)font width:(CGFloat)width;

//获取评论的九宫格图片行数
//+ (NSInteger )getHangNumberWith:(NSInteger )number;
//
////获取英文月份
//+ (NSString *)getEnglishMonthWithNumberMonth:(NSInteger )month;
////获取中文星期
//+ (NSString *)getWeekWithNumber:(NSInteger)day;
////
//+ (NSString *)getWeekDayWithDay:(NSInteger)day;
//
//+ (NSString *)getCityNameWithOutSuffix:(NSString *)string;
//+ (NSString *)manageGroupConvertTime:(NSString *)time;
//+ (BOOL)isBlankString:(NSString *)string;
//+(void)checkNull:(id)clas;


//获取设备的platform
//+ (NSString *)fetchDevicePlatform;
//获取用户的useragent
+ (NSString *)fetchUserAgent;

/**
 *  去除字符串中的横杠   例如：234-234-32   ->  23423432
 *
 *  @param string 目标字符串
 *
 *  @return 没有横杠的字符串
 */
//+ (NSString *)clearBridgeWithStr:(NSString *)string;
//
////nsdictionary 转 jsonstring
//+(NSString*)DataTOjsonString:(id)object;

/**
 *  判断是否包含汉字
 *
 *  @param str str
 *
 *  @return YES:有汉字；NO:没有汉字
 */
+(BOOL)isContainChinese:( NSString *)str;

/**
 *  判断是否是用户名格式（只含有汉字、数字、字母、下划线不能以下划线开头和结尾）
 *
 *  @param str <#str description#>
 *
 *  @return <#return value description#>
 */
+(BOOL)isUserName:(NSString *)str;

+(NSString *)checkMonthLogo:(NSInteger)logo;

//获取字体
+ (UIFont *)customFont:(CGFloat)fontSize;
+ (UIFont *)customBoldFont:(CGFloat)fontSize;
+ (NSString *)font;

+ (NSString *)timeWithTimeIntervalString:(double)timeInterval;

@end
