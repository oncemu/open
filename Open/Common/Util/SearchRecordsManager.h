//
//  SearchRecordsManager.h
//  Open
//
//  Created by mfp on 17/9/12.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MAX_RECORD_COUNT 20 //最多记录数量

typedef NS_ENUM(NSInteger, SearchRecordsType) {
    SearchRecordsTypeAlbum = 0,//搜索美图
    SearchRecordsTypeDefalult  //待定
};

@interface SearchRecordsManager : NSObject

@property(nonatomic,assign) SearchRecordsType searchType;

+ (instancetype)defaultManager;

- (void)saveHistoryArray:(NSArray *)array;//存储记录 ：删除单条历史记录重新存储即可
- (NSArray *)getHistory;//获取记录

- (void)deleteAllHistory;//删除某类全部历史记录

- (void)saveSearchHistory:(NSString*)string;

@end
