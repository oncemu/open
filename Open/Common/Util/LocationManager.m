//
//  LocationManager.m
//  Open
//
//  Created by mfp on 17/9/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "LocationManager.h"
#import <MapKit/MapKit.h>

#define DEFAULTSPAN 50

@interface LocationManager ()<CLLocationManagerDelegate>

@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) NSMutableArray *localInfoArray;
@end

@implementation LocationManager

- (instancetype)init {
    self = [super init];
    if (self) {
        self.localInfoArray = [NSMutableArray array];
        [self initializeLocationManager];
    }
    return self;
}

- (void)initializeLocationManager {
    if ([CLLocationManager locationServicesEnabled]) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = 100;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
}

- (void)startLocation {
    [self.locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    NSString *errorMsg = nil;
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        {
            [self.locationManager requestWhenInUseAuthorization];
            break;
        }
        case kCLAuthorizationStatusRestricted:
        {
            errorMsg = @"访问受限";
            break;
        }
        case kCLAuthorizationStatusDenied:
        {
            if ([CLLocationManager locationServicesEnabled]) {
                errorMsg = @"定位服务开启，被拒绝";
            } else {
                errorMsg = @"定位服务关闭，不可用";
            }
            break;
        }
        default:
            break;
    }
    if (errorMsg) {
        if (self.localFailureBlock) {
            self.localFailureBlock(errorMsg);
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *location = [locations firstObject];
    self.location = location;
    WS(weakSelf);
    [self parse:location searchKey:nil success:^(NSArray *data) {
        if (weakSelf.localSuccessBlock && data.count > 0) {
            weakSelf.localSuccessBlock([data firstObject]);
        }
    } failure:^(NSString *msg) {
        if (weakSelf.localFailureBlock) {
            weakSelf.localFailureBlock(msg);
        }
    }];
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if (self.localFailureBlock) {
        self.localFailureBlock(@"定位失败");
    }
}

- (void)parse:(CLLocation *)location
    searchKey:(NSString *)key
      success:(void (^)(NSArray *data))success
      failure:(void (^)(NSString *msg))failure {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    //位置解析
    WS(weakSelf);
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *array, NSError *error){
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf parseUserLocation:array];
                [weakSelf getAroundInfoMationWithCoordinate:location.coordinate searchKey:key success:success];
            });
        } else {
            failure(@"位置解析失败");
        }
    }];
}

- (void)parseUserLocation:(NSArray *)placemarks {
    [self.localInfoArray removeAllObjects];
    for (CLPlacemark *placemark in placemarks) {
        LocationInfo *info = [[LocationInfo alloc] init];
        info.location = placemark.location;
        info.countryCode = placemark.ISOcountryCode;
        info.country = placemark.country;
        info.province = placemark.administrativeArea;
        if (!placemark.locality) {
            info.city = placemark.administrativeArea;
        }else{
            info.city = placemark.locality;
            
        }
        info.subLocality = placemark.subLocality;
        info.thoroughfare = placemark.thoroughfare;
        info.subThoroughfare = placemark.subThoroughfare;
        info.name = placemark.name;
        [self.localInfoArray addObject:info];
    }
}

- (void)parseLocation:(CLLocation *)location
            searchKey:(NSString *)key
              success:(void (^)(NSArray *data))success
              failure:(void (^)(NSString *msg))failure {
    [self parse:location searchKey:key success:success failure:failure];
}

-(void)getAroundInfoMationWithCoordinate:(CLLocationCoordinate2D)coordinate searchKey:(NSString *)key success:(void (^)(NSArray *data))success {
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, DEFAULTSPAN, DEFAULTSPAN);
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc]init];
    request.region = region;
    if (!key) {
        key = @"Street";
    }
    request.naturalLanguageQuery = key;
    MKLocalSearch *localSearch = [[MKLocalSearch alloc]initWithRequest:request];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        if (!error) {
            [self getAroundInfomation:response.mapItems success:success];
        }else{
            NSLog(@"Quest around Error:%@",error.localizedDescription);
        }
    }];
}

-(void)getAroundInfomation:(NSArray *)array success:(void (^)(NSArray *data))success {
    for (MKMapItem *item in array) {
        MKPlacemark * placemark = item.placemark;
        LocationInfo *model = [[LocationInfo alloc]init];
        model.name = placemark.name;
        model.thoroughfare = placemark.thoroughfare;
        model.subThoroughfare = placemark.subThoroughfare;
        model.city = placemark.locality;
        model.location = placemark.location;
        [self.localInfoArray addObject:model];
    }
    success(self.localInfoArray);
}

/* 搜索关键字
 cafe, supermarket,village,Community，Shop,Restaurant，School，hospital，Company，Street，Convenience store，Shopping Centre，Place names，Hotel，Grocery store
 */

@end

@implementation LocationInfo

@end
