//
//  SearchRecordsManager.m
//  Open
//
//  Created by mfp on 17/9/12.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "SearchRecordsManager.h"

@implementation SearchRecordsManager

static SearchRecordsManager *_shareDBManager;

+ (instancetype)defaultManager {
    static dispatch_once_t onece;
    dispatch_once(&onece, ^{
        _shareDBManager=[[SearchRecordsManager alloc] init];
    });
    return _shareDBManager;
}

- (NSString *)path {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString *hpath=[docDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld.plist",(long)self.searchType]];
    return hpath;
}

- (NSArray *)getHistory {
    NSDictionary *dic = [[NSDictionary alloc] initWithContentsOfFile:[self path]];
    NSArray *array=[dic objectForKey:[NSString stringWithFormat:@"%ld",(long)self.searchType]];
    return array;
}

- (void)saveHistoryArray:(NSArray *)array {
    NSString *key=[NSString stringWithFormat:@"%ld",(long)self.searchType];
    NSDictionary *dic = @{key: array};
    [dic writeToFile:[self path] atomically:YES];
}

- (void)deleteAllHistory {
    [[NSFileManager defaultManager] removeItemAtPath:[self path] error:nil];
}

- (void)saveSearchHistory:(NSString*)string {
    NSMutableArray *historyArray = [NSMutableArray arrayWithArray:[self getHistory]];
    if (![historyArray containsObject:string]) {
        [historyArray insertObject:string atIndex:0];
    }else{
        NSInteger index = [historyArray indexOfObject:string];
        [historyArray exchangeObjectAtIndex:0 withObjectAtIndex:index];
    }
    if (historyArray.count > MAX_RECORD_COUNT) {
        NSInteger count = historyArray.count;
        [historyArray removeObjectsInRange:NSMakeRange(MAX_RECORD_COUNT, count - MAX_RECORD_COUNT)];
    }
    [self saveHistoryArray:historyArray];
}

@end
