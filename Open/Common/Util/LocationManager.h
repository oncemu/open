//
//  LocationManager.h
//  Open
//
//  Created by mfp on 17/9/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class LocationInfo;

typedef void(^localSuccess)(LocationInfo *info);
typedef void(^localFailure)(NSString *errorMsg);

@interface LocationManager : NSObject

@property(nonatomic, copy) localSuccess localSuccessBlock;

@property(nonatomic, copy) localFailure localFailureBlock;

@property(nonatomic, strong) CLLocation *location;

//开始定位
- (void)startLocation;

//通过location解析位置信息
- (void)parseLocation:(CLLocation *)location
            searchKey:(NSString *)key
              success:(void (^)(NSArray *data))success
              failure:(void (^)(NSString *msg))failure;

@end

@interface LocationInfo : NSObject

@property(nonatomic, strong) CLLocation *location;
@property(nonatomic, copy) NSString *countryCode;//国家码
@property(nonatomic, copy) NSString *country;//国家
@property(nonatomic, copy) NSString *province;//省
@property(nonatomic, copy) NSString *city;//市
@property(nonatomic, copy) NSString *subLocality;//区
@property(nonatomic, copy) NSString *thoroughfare;//街道
@property(nonatomic, copy) NSString *subThoroughfare;//子街道
@property(nonatomic, copy) NSString *name;//具体位置名称
@property(nonatomic, copy) NSString *nowCond;//当前位置天气


@end
