//
//  ViewFactory.h
//
//  Created by mfp on 16/10/25.
//

#import <Foundation/Foundation.h>
#import "TTTAttributedLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ViewFactory : NSObject

+ (UILabel *)getLabel:(CGRect)frame font:(UIFont*)font textColor:(UIColor*__nullable)textColor text:(NSString*__nullable)text;

+ (UIButton *)getButton:(CGRect)frame selectImage:(UIImage *__nullable)sImage norImage:(UIImage *__nullable)nImage target:(nullable id)target action:(nonnull SEL)action;

+ (UIImageView *)getImageView:(CGRect)frame image:(UIImage *__nullable)image;

+ (UITextField *)getTextfield:(CGRect)frame font:(UIFont*)font textColor:(UIColor *)textColor;

+ (TTTAttributedLabel *)getAttributedLabel:(CGRect)frame font:(UIFont*)font textColor:(UIColor*__nullable)textColor text:(NSString*__nullable)text;

+ (UIButton *)getButton:(CGRect)frame font:(UIFont*)font selectTitle:(NSString *__nullable)sTitle norTitle:(NSString *__nullable)nTitle sColor:(UIColor *)sColor norColor:(UIColor *)norColor target:(nullable id)target action:(nonnull SEL)action;
@end
NS_ASSUME_NONNULL_END
