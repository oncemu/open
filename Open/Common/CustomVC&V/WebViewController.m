//
//  YQWebViewController.m
//  inWatch
//
//  Created by chenwang on 14-10-1.
//  Copyright (c) 2014年 com.inWatch. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()


@property (nonatomic, strong) UIImageView *failView;
@property (nonatomic, copy) NSString *path;
@property (nonatomic, copy) NSString *htmlString;

@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithURLPath:(NSString *)path showBackButton:(BOOL)isShowBack {
    self = [super initWithShowBackButton:isShowBack];
    if (self) {
        _path = path;
        if (![_path hasPrefix:@"http://"]) {
            _path = [NSString stringWithFormat:@"http://%@", _path];
        }
    }
    return self;
}

- (id)initWithHtmlString:(NSString *)string showBackButton:(BOOL)isShowBack {
    self = [super initWithShowBackButton:isShowBack];
    if (self) {
        _htmlString = string;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];

//自定义返回页面
//    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 64)];
//    backView.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:backView];
//    UIImageView *backImage = [[UIImageView alloc]initWithFrame:CGRectMake(15, (64-26)/2, 26, 26)];
//    backImage.image = [UIImage imageNamed:@"EXIT@2x"];
//    [backView addSubview:backImage];
//    UIView *tapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, 64)];
//    tapView.userInteractionEnabled = YES;
//    [tapView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goBack)]];
//    [backView addSubview:tapView];
//自定义返回页面

    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    _webView.autoresizesSubviews = YES;//自动调整大小
    _webView.scalesPageToFit =YES;
    _webView.delegate = self;
    [self.view addSubview:_webView];
    
    [self loadContent];
}

- (void)loadContent {
    if (self.path) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_path]];
        [_webView loadRequest:request];
    }else {
        [_webView loadHTMLString:self.htmlString baseURL:nil];
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
}

- (void)webViewDidFinishLoad:(UIWebView*)webView {
    if (self.isResizeFont) {
        NSString* stre = [NSString stringWithFormat:@"%d",180];
        stre = [stre stringByAppendingFormat:@"%@",@"%"];;
        NSString* str = [NSString stringWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%@'",stre];
        [self.webView stringByEvaluatingJavaScriptFromString:str];
    }
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reloadContent:)];
    tapGes.numberOfTapsRequired = 1;
    tapGes.numberOfTouchesRequired = 1;
    _failView.userInteractionEnabled = YES;
    [_failView addGestureRecognizer:tapGes];
    
}

- (void)reloadContent:(UITapGestureRecognizer *)tapGes {
    [tapGes.view removeFromSuperview];
    [self loadContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goBack {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (BOOL)prefersStatusBarHidden {
//    return NO;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
