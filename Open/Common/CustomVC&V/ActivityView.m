//
//  QXActivityView.m
//  ShowNail
//
//  Created by chenwang on 14-5-30.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import "ActivityView.h"

#define BUTTON_VIEW_SIDE 70.f
#define BUTTON_VIEW_FONT_SIZE 11.f

#define kButtionViewWidth  70
#define kButtionViewHeight 70

#pragma mark - ButtonView

@implementation ButtonView

- (id)initWithText:(NSString *)text image:(UIImage *)image handler:(ButtonViewHandler)handler
{
    self = [super init];
    if (self) {
//        self.text = text;
//        self.image = image;
        if (handler) {
            self.handler = handler;
            
        }
//        [self setup];
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(kButtionViewWidth));
            make.height.equalTo(@(kButtionViewHeight));
        }];
        [self setupSubViewsWithLblText:text btnImage:image];
        
    }
    return self;
    
}

- (void)setupSubViewsWithLblText:(NSString *)text btnImage:(UIImage *)image{
    self.imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.imageButton setImage:image forState:UIControlStateNormal];
    [self.imageButton setImage:image forState:UIControlStateHighlighted];
    [self.imageButton addTarget:self action:@selector(handleImageBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.imageButton];
    [self.imageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.centerX.equalTo(self.mas_centerX);
        make.width.equalTo(@(50));
        make.height.equalTo(@(50));
    }];
    
    
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.text = text;
    self.textLabel.font = [UIFont systemFontOfSize:BUTTON_VIEW_FONT_SIZE];//
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    self.textLabel.textColor = [UIColor colorWithHex:0x333333];                          // color 0x333333
    [self addSubview:self.textLabel];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.top.equalTo(self.imageButton.mas_bottom).offset(5.0f);

    }];
}

- (void)setup
{
    self.textLabel = [[UILabel alloc]init];
    self.textLabel.text = self.text;
    self.textLabel.backgroundColor = [UIColor clearColor];
    self.textLabel.font = [UIFont systemFontOfSize:BUTTON_VIEW_FONT_SIZE];
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    self.textLabel.textColor=[UIColor colorWithHex:0x999999];

    self.imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.imageButton setImage:self.image forState:UIControlStateNormal];
    [self.imageButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.textLabel];
    [self addSubview:self.imageButton];
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.textLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.imageButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *constraint = nil;
    NSDictionary *views = @{@"textLabel": self.textLabel, @"imageButton": self.imageButton};
    NSArray *constraints = nil;
    
    //view的宽高为70
    constraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:BUTTON_VIEW_SIDE];
    [self addConstraint:constraint];
    constraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:BUTTON_VIEW_SIDE];
    [self addConstraint:constraint];
    
    //label紧贴view的左右
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[textLabel]|" options:0 metrics:nil views:views];
    [self addConstraints:constraints];
    
    //imageView距离view左右各10, imageView的宽为50
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[imageButton(50)]-10-|" options:0 metrics:nil views:views];
    [self addConstraints:constraints];
    
    //竖直方向imageView和textLabel在一条直线上, 并且挨着, imageView的高为50
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imageButton(50)][textLabel]|" options:NSLayoutFormatAlignAllCenterX metrics:nil views:views];
    [self addConstraints:constraints];
    
}

- (void)handleImageBtnAction:(UIButton *)sender{
    if (self.handler) {
        self.handler(self);
    }
    if (self.activityView) {
        [self.activityView hide];
    }
}

- (void)buttonClicked:(UIButton *)button
{
    if (self.handler) {
        self.handler(self);
        
    }
    
    if (self.activityView) {
        [self.activityView hide];
        
    }
    
}

@end

#define ICON_VIEW_HEIGHT_SPACE 8

#pragma mark - HYActivityView

@interface ActivityView ()

@property (nonatomic, copy) NSString *title;

//将要显示在该视图上
@property (nonatomic, weak) UIView *referView;

//内容窗口
@property (nonatomic, strong) UIView *contentView;

//透明的关闭按钮
@property (nonatomic, strong) UIButton *closeButton;

//按钮加载的view
@property (nonatomic, strong) UIView *iconView;

//button数组
@property (nonatomic, strong) NSMutableArray *buttonArray;

//行数
@property (nonatomic, assign) NSUInteger lines;

//目前正在生效的numberOfButtonPerLine
@property (nonatomic, assign) int workingNumberOfButtonPerLine;

//按钮间的间隔大小
@property (nonatomic, assign) CGFloat buttonSpace;

//消失的时候移除
@property (nonatomic, strong) NSLayoutConstraint *contentViewAndViewConstraint;

//iconView高度的constraint
@property (nonatomic, strong) NSLayoutConstraint *iconViewHeightConstraint;

//buttonView的constraints
@property (nonatomic, strong) NSMutableArray *buttonConstraintsArray;

@end

@implementation ActivityView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    
}
#define kBgAlphaColor [UIColor colorWithHex:0x000000 alpha:0.6f]

- (id)initWithTitle:(NSString *)title referView:(UIView *)referView
{
    self = [super init];
    if (self) {
        self.title = title;
        self.buttonArray = [[NSMutableArray alloc] init];
        self.numberOfButtonPerLine = 3;
        if (referView) {
            self.referView = referView;
        }
        //实际的颜色见show方法内
        self.backgroundColor = ClearColor;
//        [self setup];
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)]];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deviceRotate:) name:UIDeviceOrientationDidChangeNotification object:nil];
        
    }
    return self;
    
}



- (void)calculateButtonSpaceWithNumberOfButtonPerLine:(int)number
{
    self.buttonSpace = (self.referView.bounds.size.width - BUTTON_VIEW_SIDE * number) / (number + 1);
    
    if (self.buttonSpace < 0) {
        [self calculateButtonSpaceWithNumberOfButtonPerLine:4];
        
    } else {
        self.workingNumberOfButtonPerLine = number;
        
    }
    
}
#define kContentViewBgColor [UIColor colorWithHex:0xf5f5f5]
#define kCancleBtnHeight 50
#define kTitleLblHeight  52

#define kCancleBtnTitleColor [UIColor colorWithHex:0xffe601]

- (void)setupSubviewWithTitle:(NSString *)title{
    self.contentView = [[UIView alloc] init];
    self.contentView.backgroundColor = kContentViewBgColor;
    [self addSubview:self.contentView];
    
    UIView *superView = self;
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(0);
        make.top.equalTo(superView.mas_bottom).offset(0);
        make.right.equalTo(superView.mas_right).offset(0);
    }];
    
    //取消按钮
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancelButton.titleLabel.font = Font(16);
    self.cancelButton.backgroundColor = [UIColor whiteColor];
    [self.cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:kCancleBtnTitleColor forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:kCancleBtnTitleColor forState:UIControlStateHighlighted];
    [self.cancelButton addTarget:self action:@selector(handleCancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.cancelButton];
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).offset(0);
        make.left.equalTo(self.contentView.mas_left).offset(0);
        make.right.equalTo(self.contentView.mas_right).offset(0);
        make.height.equalTo(@(kCancleBtnHeight));
    }];
    //分割线，暂且放在了取消按钮的顶部好了
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor colorWithHex:0xdfdfdf];
    [self.cancelButton addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.cancelButton.mas_left);
        make.top.equalTo(self.cancelButton.mas_top);
        make.right.equalTo(self.cancelButton.mas_right);
        make.height.equalTo(@(0.5));
    }];
    
    //标题
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.textColor = [UIColor colorWithHex:0x333333];
    self.titleLabel.font = Font(16);
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.text = title;
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(0);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.right.equalTo(self.contentView.mas_right).offset(-20);
        make.height.equalTo(@(kTitleLblHeight));
    }];
    
    //button 列表
    self.iconView = [[UIView alloc] init];
    [self.contentView addSubview:self.iconView];
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(0);
        make.left.equalTo(self.contentView.mas_left).offset(0);
        make.bottom.equalTo(self.cancelButton.mas_top).offset(0);
        make.right.equalTo(self.contentView.mas_right).offset(0);
    }];
    
}

#define kItemBtnToLeft       27
#define kItemBtnToTop        0
#define kItemBtnToBottom     12
#define kItemBtnToItemBtn_V  12
- (void)setupIconsView{
    NSInteger allBtns = self.buttonArray.count;
    NSInteger row = ((allBtns % self.numberOfButtonPerLine) == 0) ? (allBtns / self.numberOfButtonPerLine) : (allBtns / self.numberOfButtonPerLine + 1);
    
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(kItemBtnToTop + kButtionViewHeight * row + kItemBtnToItemBtn_V * (row - 1) + kItemBtnToBottom));
    }];
    [self.contentView updateConstraints];
    float itemBtnToItemBtn_H = (self.referView.frame.size.width - kItemBtnToLeft * 2 - kButtionViewWidth * self.numberOfButtonPerLine) / (self.numberOfButtonPerLine - 1);
    for (NSInteger i = 0 ; i < allBtns ; i ++) {
        NSInteger x = i % self.numberOfButtonPerLine;
        NSInteger y = i / self.numberOfButtonPerLine;
        UIView *view = [self.buttonArray objectAtIndex:i];
        [self.iconView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.iconView.mas_top).offset(kItemBtnToTop + y * (kButtionViewHeight + kItemBtnToItemBtn_V));
            make.left.equalTo(self.iconView.mas_left).offset(kItemBtnToLeft + x * (kButtionViewWidth + itemBtnToItemBtn_H));
        }];
    }
    [self layoutIfNeeded];
}

- (void)setup
{
    self.buttonArray = [NSMutableArray array];
    self.buttonConstraintsArray = [NSMutableArray array];
    self.lines = 0;
    self.numberOfButtonPerLine = 4;
    self.useGesturer = YES;
    
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6f];
    
    self.contentView = [[UIView alloc]init];
    self.bgColor = ColorWithHex(0xf5f5f5);
    [self addSubview:self.contentView];
    
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _closeButton.backgroundColor = ClearColor;
    [self.closeButton addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.closeButton];
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    self.titleLabel.text = self.title;
    self.titleLabel.textColor=[UIColor colorWithHex:0x333333];
    [self.contentView addSubview:self.titleLabel];
    
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    self.cancelButton.titleLabel.font = Font(16);
    [self.cancelButton setTitleColor:[UIColor colorWithHex:0xffe601] forState:UIControlStateNormal];
    [self.cancelButton addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.cancelButton];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(0, -10, SCREEN_WIDTH, 0.5);
    lineView.backgroundColor = [UIColor colorWithHex:0xdfdfdf];
    [self.cancelButton addSubview:lineView];
    
    self.iconView = [[UIView alloc]init];
    [self.contentView addSubview:self.iconView];
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.closeButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.iconView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self setNeedsUpdateConstraints];
    
    //添加下滑关闭手势
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeHandler:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self addGestureRecognizer:swipe];
    
}
/*
- (void)updateConstraints
{
    [super updateConstraints];
    
    NSArray *constraints = nil;
    NSLayoutConstraint *constraint = nil;
    NSDictionary *views = @{@"contentView": self.contentView, @"closeButton": self.closeButton, @"titleLabel": self.titleLabel, @"cancelButton": self.cancelButton, @"iconView": self.iconView, @"view": self, @"referView": self.referView};
    
    //view跟referView的宽高相等
    constraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.referView attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
    [self.referView addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.referView attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
    [self.referView addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.referView attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    [self.referView addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.referView attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    [self.referView addConstraint:constraint];
    
    //closeButton跟view的左右挨着
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[closeButton]|" options:0 metrics:nil views:views];
    [self addConstraints:constraints];
    
    //contentView跟view的左右挨着
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contentView]|" options:0 metrics:nil views:views];
    [self addConstraints:constraints];
    
    //垂直方向closeButton挨着contentView
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[closeButton(==view@999)][contentView]" options:0 metrics:nil views:views];
    [self addConstraints:constraints];
    
    //titleLabel跟contentView左右挨着
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[titleLabel]|" options:0 metrics:nil views:views];
    [self.contentView addConstraints:constraints];
    
    //cancelButton跟contentView左右挨着
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cancelButton]|" options:0 metrics:nil views:views];
    [self.contentView addConstraints:constraints];
    
    //iconView跟contentView左右挨着
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[iconView]|" options:0 metrics:nil views:views];
    [self.contentView addConstraints:constraints];
    
    //iconView的高度
    if (self.iconViewHeightConstraint) {
        [self.iconView removeConstraint:self.iconViewHeightConstraint];
        
    }
    self.iconViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.iconView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:self.lines * BUTTON_VIEW_SIDE + (self.lines + 1) * ICON_VIEW_HEIGHT_SPACE];
    [self.iconView addConstraint:self.iconViewHeightConstraint];
    
    //垂直方向titleLabel挨着iconView挨着cancelButton
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[titleLabel(==30)]-[iconView]-[cancelButton(==30)]-8-|" options:0 metrics:nil views:views];
    if ([self.title isEqualToString:@""]) {
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(5)-[iconView]-[cancelButton(==30)]-8-|" options:0 metrics:nil views:views];
    }
    [self.contentView addConstraints:constraints];
}
*/
- (void)prepareForShow
{
    //计算行数
    NSUInteger count = [self.buttonArray count];
    self.lines = count / self.workingNumberOfButtonPerLine;
    if (count % self.workingNumberOfButtonPerLine != 0) {
        self.lines++;
        
    }
    
    for (NSUInteger i = 0; i < [self.buttonArray count]; i++) {
        ButtonView *buttonView = [self.buttonArray objectAtIndex:i];
        [self.iconView addSubview:buttonView];
        
        NSUInteger y = i / self.workingNumberOfButtonPerLine;
        NSUInteger x = i % self.workingNumberOfButtonPerLine;
        
        //排列buttonView的位置
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:buttonView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.iconView attribute:NSLayoutAttributeTop multiplier:1 constant:(y + 1) * ICON_VIEW_HEIGHT_SPACE + y * BUTTON_VIEW_SIDE];
        [self.iconView addConstraint:constraint];
        [self.buttonConstraintsArray addObject:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:buttonView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.iconView attribute:NSLayoutAttributeLeading multiplier:1 constant:(x + 1) * self.buttonSpace + x * BUTTON_VIEW_SIDE];
        [self.iconView addConstraint:constraint];
        [self.buttonConstraintsArray addObject:constraint];
        
    }
    
    [self layoutIfNeeded];
    
    
}

- (void)show
{
    [self showWithAnimation:YES];
    /*
    if (self.isShowing) {
        return;
        
    }
    [self.referView addSubview:self];
    [self setNeedsUpdateConstraints];
    self.alpha = 0;
    
    [self prepareForShow];
    
    self.contentViewAndViewConstraint = [NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    [self addConstraint:self.contentViewAndViewConstraint];
    
    [UIView animateWithDuration:0.25f animations:^{
        self.alpha = 1;
        [self layoutIfNeeded];
        self.show = YES;
        
    }];
    */
}

- (void)showWithAnimation:(BOOL)animated{
    if (self.isShowing) {
        return;
    }
    [self.referView addSubview:self];
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.referView.mas_top);
        make.left.equalTo(self.referView.mas_left);
        make.bottom.equalTo(self.referView.mas_bottom);
        make.right.equalTo(self.referView.mas_right);
    }];
    
    [self setupSubviewWithTitle:self.title];
    [self setupIconsView];
    if (animated) {
        [UIView animateWithDuration:0.3 animations:^{
            self.backgroundColor = kBgAlphaColor;
            [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.mas_left).offset(0);
                make.bottom.equalTo(self.mas_bottom).offset(0);
                make.right.equalTo(self.mas_right).offset(0);
            }];
            [self layoutIfNeeded];
        }];
    }
    self.show = YES;

}

- (void)hide
{
    [self hideWithAnimation:YES];
    /*
    if (!self.isShowing) {
        return;
        
    }
    
    [UIView animateWithDuration:0.25f animations:^{
        self.alpha = 0;
        [self removeConstraint:self.contentViewAndViewConstraint];
        self.contentViewAndViewConstraint = nil;
        [self layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        self.show = NO;
        [self removeFromSuperview];
        
    }];
    */
}

- (void)hideWithAnimation:(BOOL)animated{
    self.show = NO;
    if (animated) {
        [UIView animateWithDuration:0.3 animations:^{
            self.backgroundColor = [UIColor clearColor];
            [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.mas_left).offset(0);
                make.top.equalTo(self.mas_bottom).offset(0);
                make.right.equalTo(self.mas_right).offset(0);
            }];
            [self layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }
}


- (void)deviceRotate:(NSNotification *)notification
{
    /*
    [self.iconView removeConstraints:self.buttonConstraintsArray];
    [self.buttonConstraintsArray removeAllObjects];
    
    [self calculateButtonSpaceWithNumberOfButtonPerLine:self.numberOfButtonPerLine];
    [self prepareForShow];
    
    [self.iconView removeConstraint:self.iconViewHeightConstraint];
    self.iconViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.iconView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:self.lines * BUTTON_VIEW_SIDE + (self.lines + 1) * ICON_VIEW_HEIGHT_SPACE];
    [self.iconView addConstraint:self.iconViewHeightConstraint];
     */
    
}

//- (void)setNumberOfButtonPerLine:(int)numberOfButtonPerLine
//{
//    _numberOfButtonPerLine = numberOfButtonPerLine;
//    [self calculateButtonSpaceWithNumberOfButtonPerLine:numberOfButtonPerLine];
//    
//}

- (void)setBgColor:(UIColor *)bgColor
{
    _bgColor = bgColor;
    self.contentView.backgroundColor = bgColor;
    
}

- (void)addButtonView:(ButtonView *)buttonView
{
    [self.buttonArray addObject:buttonView];
    buttonView.activityView = self;
    
}

- (void)closeButtonClicked:(UIButton *)button
{
//    [self hide];
    [self hideWithAnimation:YES];
}

- (void)swipeHandler:(UISwipeGestureRecognizer *)swipe
{
    if (self.useGesturer) {
        [self hide];
    }
}
- (void)handleCancelBtnAction:(id)sender{
    [self hideWithAnimation:YES];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */



@end
