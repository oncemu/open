//
//  CustomLabelTextView.m
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CustomLabelTextView.h"

@implementation CustomLabelTextView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _lblName = [[UILabel alloc]init];
    _lblName.textColor = [UIColor colorWithHex:0x666666];
    _lblName.font = [UIFont systemFontOfSize:15.0f];
    [self addSubview:_lblName];
    [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(15);
        make.top.mas_equalTo(self.mas_top).offset(35/2);
        make.size.mas_equalTo(CGSizeMake(80, 15));
    }];

    _textField = [[CustomTextFiled alloc]init];
    _textField.textColor = [UIColor colorWithHex:0x666666];
    _textField.font = [UIFont systemFontOfSize:15.0f];
    [self addSubview:_textField];
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.lblName.mas_right).offset(5);
        make.top.mas_equalTo(self.mas_top).offset(25/2);
        make.right.mas_equalTo(self.mas_right).offset(-30);
        make.height.mas_equalTo(25);
    }];
}

- (void)setTitle:(NSString *)title {
    _lblName.text = title;
}

- (void)setPlaceholder:(NSString *)placeholder {
    _textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHex:0xafafaf]}];
}


@end
