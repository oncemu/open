//
//  ViewFactory.m
//
//  Created by mfp on 16/10/25.
//

#import "ViewFactory.h"

@implementation ViewFactory

+ (UILabel *)getLabel:(CGRect)frame font:(UIFont*)font textColor:(UIColor*__nullable)textColor text:(NSString*__nullable)text {
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.font = font;
    label.backgroundColor = WhiteColor;
    label.text = text;
    label.textColor = textColor;
    return label;
}

+ (UIButton *)getButton:(CGRect)frame selectImage:(UIImage *__nullable)sImage norImage:(UIImage *__nullable)nImage target:(nullable id)target action:(nonnull SEL)action {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.adjustsImageWhenHighlighted = NO;
    button.frame = frame;
    button.backgroundColor = ClearColor;
    [button setImage:sImage forState:UIControlStateSelected];
    [button setImage:nImage forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return button;
}

+ (UIImageView *)getImageView:(CGRect)frame image:(UIImage *__nullable)image {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.layer.masksToBounds = YES;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    if (image) {
        imageView.image = image;
    }
    return imageView;
}

+ (UITextField *)getTextfield:(CGRect)frame font:(UIFont*)font textColor:(UIColor *)textColor {
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.backgroundColor = WhiteColor;
    textField.font = font;
    textField.textColor = textColor;
    return textField;
}

+ (TTTAttributedLabel *)getAttributedLabel:(CGRect)frame font:(UIFont*)font textColor:(UIColor*__nullable)textColor text:(NSString*__nullable)text {
    TTTAttributedLabel *label = [[TTTAttributedLabel alloc] initWithFrame:frame];
    label.font = font;
    label.textColor = textColor;
    label.text = text;
    return label;
}

+ (UIButton *)getButton:(CGRect)frame font:(UIFont*)font selectTitle:(NSString *__nullable)sTitle norTitle:(NSString *__nullable)nTitle sColor:(UIColor *)sColor norColor:(UIColor *)norColor target:(nullable id)target action:(nonnull SEL)action; {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    button.titleLabel.font = font;
    button.backgroundColor = ClearColor;
    if (sTitle) {
        [button setTitle:sTitle forState:UIControlStateSelected];
    }
    if (nTitle) {
        [button setTitle:nTitle forState:UIControlStateNormal];
    }
    if (sColor) {
        [button setTitleColor:sColor forState:UIControlStateSelected];
    }
    if (norColor) {
        [button setTitleColor:norColor forState:UIControlStateNormal];
    }
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return button;
}

@end
