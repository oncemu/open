//
//  UIBarButtonItem+QXBarbutton.h
//  ShownailSeller
//
//  Created by mfp on 16/10/27.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (QXBarbutton)

+ (UIBarButtonItem *)itemWithImageName:(NSString *)imageName target:(id)target action:(SEL)action;

@end
