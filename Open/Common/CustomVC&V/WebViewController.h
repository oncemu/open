//
//  YQWebViewController.h
//  inWatch
//
//  Created by chenwang on 14-10-1.
//  Copyright (c) 2014年 com.inWatch. All rights reserved.
//

#import "BaseVC.h"

@interface WebViewController : BaseVC<UIWebViewDelegate>
@property(nonatomic,assign) BOOL isResizeFont;
@property (nonatomic, strong) UIWebView *webView;

- (id)initWithURLPath:(NSString *)path showBackButton:(BOOL)isShowBack;

- (id)initWithHtmlString:(NSString *)string showBackButton:(BOOL)isShowBack;


@end
