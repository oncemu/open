//
//  SegmentBar.m
//  Open
//
//  Created by mfp on 17/7/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "SegmentBar.h"

#define KMinMargin   32
#define KOutSpreadW  5 //下划线多出来的长度
#define kIndicatorH   2

@interface SegmentBar ()

/** 内容承载视图 */
@property (nonatomic, weak) UIScrollView *contentView;
/** 添加的按钮数据 */
@property (nonatomic, strong) NSMutableArray *itemBtns;
/** 指示器 */
@property (nonatomic, weak) UIImageView *indicatorView;

@end

@implementation SegmentBar
{
    // 记录最后一次点击的按钮
    UIButton *_lastBtn;
}

+ (instancetype)segmentBarWithFrame:(CGRect)frame{
    SegmentBar *segmentBar = [[SegmentBar alloc]initWithFrame:frame];
    return segmentBar;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = ClearColor;
        self.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return self;
}

-  (void)setSelectIndex:(NSInteger)selectIndex{
    
    if (self.items.count == 0 || selectIndex < 0 || selectIndex > self.itemBtns.count- 1) {
        return;
    }
    
    _selectIndex = selectIndex;
    UIButton *btn = self.itemBtns[selectIndex];
    [self btnClick:btn];
}

- (void)setItems:(NSArray<NSString *> *)items{
    _items = items;
    // 删除之前添加过多的组件
    [self.itemBtns makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.itemBtns = nil;
    
    // 根据所有的选项数据源 创建Button 添加到内容视图
    for (NSString *item in items) {
        UIButton *btn = [[UIButton alloc]init];
        btn.tag = self.itemBtns.count;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchDown];
        [btn setTitleColor:ColorWithHex(0xb3b3b3) forState:UIControlStateNormal];
        [btn setTitleColor:ColorWithHex(0x333333) forState:UIControlStateSelected];
        btn.titleLabel.font = Font(17);
        [btn setTitle:item forState:UIControlStateNormal];
        [self.contentView addSubview:btn];
        [self.itemBtns addObject:btn];
    }
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

#pragma mark - private
- (void)btnClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(segmentBar:didSelectIndex:fromIndex:)]) {
        [self.delegate segmentBar:self didSelectIndex:sender.tag fromIndex:_lastBtn.tag];
    }
    _selectIndex = sender.tag;
    _lastBtn.selected = NO;
    sender.selected = YES;
    _lastBtn.titleLabel.font = Font(17);
    _lastBtn = sender;
    sender.titleLabel.font = Font(19);
    [UIView animateWithDuration:0.1 animations:^{
        self.indicatorView.frameSizeWidth = sender.frameSizeWidth + KOutSpreadW * 2;
        self.indicatorView.center = Point(sender.center.x, self.indicatorView.center.y);
    }];
    
    // 滚动到Btn的位置
    CGFloat scrollX = sender.center.x - self.contentView.frameSizeWidth * 0.5;
    
    // 考虑临界的位置
    if (scrollX < 0) {
        scrollX = 0;
    }
    if (scrollX > self.contentView.contentSize.width - self.contentView.frameSizeWidth) {
        scrollX = self.contentView.contentSize.width - self.contentView.frameSizeWidth;
    }
    [self.contentView setContentOffset:CGPointMake(scrollX, 0) animated:YES];
}

#pragma mark - layout
- (void)layoutSubviews{
    [super layoutSubviews];
    self.contentView.frame = self.bounds;
    
    CGFloat totalBtnWidth = 0;
    for (UIButton *btn in self.itemBtns) {
        [btn sizeToFit];
        btn.frameSizeWidth += 10;
        totalBtnWidth += btn.frameSizeWidth;
    }
    
    CGFloat caculateMargin = (self.frameSizeWidth - totalBtnWidth) / (self.items.count + 1);
    if (caculateMargin < KMinMargin) {
        caculateMargin = KMinMargin;
    }
    
    
    CGFloat lastX = caculateMargin;
    for (UIButton *btn in self.itemBtns) {
        [btn sizeToFit];
        btn.frameOriginY = 0;
        btn.frameOriginX = lastX;
        btn.frameSizeWidth += 10;
        lastX += btn.frameSizeWidth + caculateMargin;
    }
    
    self.contentView.contentSize = CGSizeMake(lastX, 0);
    
    if (self.itemBtns.count == 0) {
        return;
    }
    
    UIButton *btn = self.itemBtns[self.selectIndex];
    self.indicatorView.frameSizeWidth = btn.frameSizeWidth + KOutSpreadW * 2;
    self.indicatorView.center = Point(btn.center.x, self.indicatorView.center.y);
    self.indicatorView.frameSizeHeight = kIndicatorH;
    self.indicatorView.frameOriginY = self.frameSizeHeight - kIndicatorH;
    
}

- (CGSize)intrinsicContentSize {
    return UILayoutFittingExpandedSize;
}

#pragma mark - lazy-init


- (NSMutableArray<UIButton *> *)itemBtns {
    if (!_itemBtns) {
        _itemBtns = [NSMutableArray array];
    }
    return _itemBtns;
}

- (UIImageView *)indicatorView {
    if (!_indicatorView) {
        CGFloat indicatorH = kIndicatorH;
        UIImageView *indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frameSizeHeight - indicatorH, 0, indicatorH)];
        indicatorView.image = getImage(@"home_segline");
        [self.contentView addSubview:indicatorView];
        _indicatorView = indicatorView;
    }
    return _indicatorView;
}

- (UIScrollView *)contentView {
    if (!_contentView) {
        UIScrollView *scrollView = [[UIScrollView alloc] init];
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.scrollsToTop = NO;
        [self addSubview:scrollView];
        _contentView = scrollView;
    }
    return _contentView;
}

- (void)setFrame:(CGRect)frame {
    CGRect rect = CM(76, 0, SCREEN_WIDTH - 76 - 60, 33);
    super.frame = rect;
}
@end
