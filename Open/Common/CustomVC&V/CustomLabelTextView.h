//
//  CustomLabelTextView.h
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextFiled.h"

@interface CustomLabelTextView : UIView

@property (nonatomic, strong) UILabel *lblName;
@property (nonatomic, strong) CustomTextFiled *textField;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *placeholder;
@end
