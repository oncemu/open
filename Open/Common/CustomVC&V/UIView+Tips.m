//
//  UIView+Tips.m
//  ShowNail
//
//  Created by god、long on 15-1-30.
//  Copyright (c) 2015年 jianghaibo. All rights reserved.
//

#import "UIView+Tips.h"
#import "StringUtil.h"

@implementation UIView (Tips)

- (void)showErrorTips:(NSString *)tips
{
    UIView *backView = [[UIView alloc]initWithFrame:CM(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    backView.backgroundColor = ClearColor;
    backView.alpha = 0;
    [[self mainWindow] addSubview:backView];

    
    CGFloat tipsLabelH = [StringUtil getSizeWith:tips font:[UIFont systemFontOfSize:14.0f] size:Size(SCREEN_WIDTH - 116, MAXFLOAT)].height;
    UILabel *tipsLabel = [[UILabel alloc]initWithFrame:CM(58, (SCREEN_HEIGHT - tipsLabelH)/2, SCREEN_WIDTH - 116, tipsLabelH + 30)];
    tipsLabel.text = tips;
    tipsLabel.numberOfLines = 0;
    tipsLabel.textAlignment = NSTextAlignmentCenter;
    tipsLabel.font = [UIFont systemFontOfSize:14.0f];
    tipsLabel.textColor = WhiteColor;
    tipsLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7f];
    [backView addSubview:tipsLabel];

    
    
    [UIView animateWithDuration:0.3f animations:^{
        backView.alpha = 1;
    }];
    
    [UIView animateWithDuration:0.3f delay:1.5f options:UIViewAnimationOptionCurveEaseOut animations:^{
        backView.alpha = 0;
    } completion:^(BOOL finished) {
        [backView removeFromSuperview];
    }];
}

- (UIWindow *)mainWindow
{
    UIApplication *app = [UIApplication sharedApplication];
    if ([app.delegate respondsToSelector:@selector(window)])
    {
        return [app.delegate window];
    }
    else
    {
        return [app keyWindow];
    }
}



@end
