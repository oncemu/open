//
//  PicToolBar.m
//  Open
//
//  Created by mfp on 17/7/9.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PicToolBar.h"

static CGFloat image_title_sapce = 7;//图片与数字间距
static CGFloat item_space = 15; //元素之间间距

@interface PicToolBar()
{
    CGFloat origin_x;
}
@property(nonatomic, strong) NSMutableArray *itemList;

@end

@implementation PicToolBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = ClearColor;
        self.itemList = [NSMutableArray array];
        self.textColor = ColorWithHex(0x999999);
        self.textFont = Font(13);
        self.barAlignment = ToolBarAlignmentRight;
    }
    return self;
}

- (void)addItem:(UIImage *)image handle:(tapAction)handle {
    UIButton *btn = [ViewFactory getButton:CM(0, 0, 60, self.frameSizeHeight) selectImage:image norImage:image target:self action:@selector(buttonClicked:)];
    [btn setTitleColor:self.textColor forState:UIControlStateNormal];
    btn.titleLabel.font = self.textFont;
    [self.itemList addObject:btn];
    self.handle = handle;
    [self addSubview:btn];
    [self layoutButtons];
}

- (void)setCount:(NSInteger)count atIndex:(NSInteger)index {
    if (index < self.itemList.count) {
        UIButton *btn = self.itemList[index];
        NSString *countStr = [NSString stringWithFormat:@"%ld",count];
        [btn setTitle:countStr forState:UIControlStateNormal];
    }
    [self layoutButtons];
}

- (void)buttonClicked:(UIButton *)sender {
    if (self.handle) {
        NSInteger index = [self.itemList indexOfObject:sender];
        self.handle(sender,index);
    }
}

- (void)layoutButtons {
    origin_x = 0;
    for (NSInteger i = 0; i < self.itemList.count; i++) {
        UIButton *btn = self.itemList[i];
        CGFloat titleLen = [StringUtil getLabelLength:btn.currentTitle font:self.textFont height:self.frameSizeHeight];
        CGFloat imageLen = btn.imageView.frameSizeWidth;
        CGFloat btnWidth = titleLen + imageLen + image_title_sapce;
        if (titleLen == 0) {
            btnWidth -= image_title_sapce;
        }else{
            btn.imageEdgeInsets = UIEdgeInsetsMake(0, -image_title_sapce/2.0f, 0, image_title_sapce/2.0f);
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, image_title_sapce/2.0f, 0, -image_title_sapce/2.0f);
        }
        btn.frame = CM(origin_x, 0, btnWidth, self.frameSizeHeight);
        origin_x += (btnWidth + item_space);
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect rect = self.frame;
    self.frameSizeWidth = origin_x - item_space;
    if (self.barAlignment == ToolBarAlignmentRight) {
        CGFloat x = self.frameOriginX;
        self.frameOriginX = x - (self.frameSizeWidth - rect.size.width);
    }
}

- (void)clearData {
    for (UIButton *btn in self.itemList) {
        [btn setTitle:@"" forState:UIControlStateNormal];
    }
}

@end
