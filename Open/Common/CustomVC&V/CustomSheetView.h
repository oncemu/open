//
//  CustomSheetView.h
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CustomSheetBlock)(NSInteger index,NSString *titles);


@interface CustomSheetView : UIView

@property (nonatomic, copy) CustomSheetBlock block;

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
                       buttons:(NSArray *)buttons;

- (void)show;
- (void)dismiss;

@end
