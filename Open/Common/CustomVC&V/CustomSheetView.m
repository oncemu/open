//
//  CustomSheetView.m
//  Open
//
//  Created by onceMu on 2017/7/27.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CustomSheetView.h"

#define kSheetButtonTag   10000

@interface CustomSheetView ()

@property (nonatomic, strong) NSMutableArray *buttons;
@property (nonatomic, strong) NSString *titles;
@property (nonatomic, strong) UIView *blackView;
@property (nonatomic, strong) UIView *contentView;

@end

@implementation CustomSheetView

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
                      buttons:(NSArray *)buttons {
    self = [super initWithFrame:frame];
    if (self) {
        _titles = title;
        _buttons = [NSMutableArray arrayWithCapacity:1];
        [_buttons addObject:_titles];
        [_buttons addObjectsFromArray:buttons];
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    _blackView = [[UIView alloc]initWithFrame:self.bounds];
    _blackView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    _blackView.hidden = NO;
    [self addSubview:_blackView];

    
    NSInteger count = self.buttons.count;
    CGFloat height = count *40 +5;

    _contentView = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-height, SCREEN_WIDTH, height)];
    _contentView.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    [self addSubview:_contentView];

    CGFloat buttonHeight = 40;
    for (NSInteger i = 0; i<self.buttons.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (i == self.buttons.count - 1) {
            btn.frame = CGRectMake(0, i*buttonHeight+5, SCREEN_WIDTH, buttonHeight);
        }else {
            btn.frame = CGRectMake(0, i*buttonHeight, SCREEN_WIDTH, buttonHeight);
        }
        btn.tag = kSheetButtonTag + i;
        btn.backgroundColor = [UIColor colorWithHex:0xffffff];
        if (i == 0) {
            btn.titleLabel.font = [UIFont systemFontOfSize:13.0f];
            [btn setTitleColor:[UIColor colorWithHex:0x9f9f9f] forState:UIControlStateNormal];
        }else {
            btn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
            [btn setTitleColor:[UIColor colorWithHex:0x333333] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(onButtonViewClick:) forControlEvents:UIControlEventTouchUpInside];
        }
        if (i != self.buttons.count-1 || i != self.buttons.count -2) {
            UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, buttonHeight-0.5, SCREEN_WIDTH, 0.5)];
            line.backgroundColor = [UIColor colorWithHex:0xeeeeee];
            [btn addSubview:line];
        }
        [btn setTitle:self.buttons[i] forState:UIControlStateNormal];
        [_contentView addSubview:btn];
    }
}

- (void)onButtonViewClick:(UIButton *)sender {
    NSInteger tag = sender.tag - kSheetButtonTag;
    NSString *title = sender.titleLabel.text;
    if (![title isEqualToString:@"取消"]) {
        if (self.block) {
            self.block(tag, title);
        }
    }
    [self dismiss];
}

- (void)show {
    [[self mainWindow] addSubview:self];
}

- (void)dismiss {
    self.blackView.hidden = YES;
    [UIView animateWithDuration:0.3 animations:^{
        self.frameOriginY = SCREEN_HEIGHT;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (UIWindow *)mainWindow {
    UIApplication *app = [UIApplication sharedApplication];
    if ([app.delegate respondsToSelector:@selector(window)]) {
        return [app.delegate window];
    }
    else {
        return [app keyWindow];
    }
}

@end

