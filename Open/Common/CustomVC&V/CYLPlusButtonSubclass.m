

#import "CYLPlusButtonSubclass.h"
#import "CYLTabBarController.h"
#import "PublishPopView.h"
#import "PublishPictureVC.h"
#import "PublishTraveNotesVC.h"
#import "TakePhotoVC.h"
#import "PhotoAlbumVC.h"

@interface CYLPlusButtonSubclass ()<UIActionSheetDelegate> {
    CGFloat _buttonImageHeight;
}

@end

@implementation CYLPlusButtonSubclass

#pragma mark -
#pragma mark - Life Cycle

+ (void)load {
    //请在 `-application:didFinishLaunchingWithOptions:` 中进行注册，否则iOS10系统下存在Crash风险。
    //[super registerPlusButton];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.adjustsImageWhenHighlighted = NO;
    }
    return self;
}

+ (id)plusButton {
    CYLPlusButtonSubclass *button = [CYLPlusButtonSubclass buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    UIImage *buttonImage = [UIImage imageNamed:@"mall-selected"];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CM(0, 0, 70, kTabBarHeight);
    [button addTarget:button action:@selector(clickPublish) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

- (void)clickPublish {
    CYLTabBarController *tabBarController = [self cyl_tabBarController];
    __weak UIViewController *weakVC = tabBarController.selectedViewController;
    PublishPopView *view = [[PublishPopView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    view.publishBlock = ^(PublishType type) {
        switch (type) {
            case PublishTypePicture:
            {
                PhotoAlbumVC *vc = [[PhotoAlbumVC alloc] initWithShowBackButton:YES];
                vc.type = PublishTypePicture;
                vc.maxCount = 1;
                CustomNavigationController *nav = [[CustomNavigationController alloc]initWithRootViewController:vc];
                [weakVC presentViewController:nav animated:YES completion:nil];
            }
                break;
            case PublishTypeTravelNotes:
            {
                PhotoAlbumVC *vc = [[PhotoAlbumVC alloc] initWithShowBackButton:YES];
                vc.type = PublishTypeTravelNotes;
                vc.maxCount = 9;
                CustomNavigationController *nav = [[CustomNavigationController alloc]initWithRootViewController:vc];
                [weakVC presentViewController:nav animated:YES completion:nil];
            }
                break;
            default:
                break;
        }
    };

    [view show];
}

@end
