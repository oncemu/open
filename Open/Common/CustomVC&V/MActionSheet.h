//
//  MActionSheet.h
//  Open
//
//  Created by mfp on 17/9/12.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ActionSheetDelegate;

@interface MActionSheet : UIView
/**
 *  遮罩层背景颜色, 默认为blackColor
 */
@property (strong, nonatomic) UIColor *maskBackgroundColor;
/**
 *  遮罩层的alpha值，默认为0.5
 */
@property (nonatomic) CGFloat maskAlpha;

/**
 *  Default: [UIFont systemFontOfSize:20]
 */
@property (strong, nonatomic) UIFont *titleFont;
/**
 *  default: blackColor
 */
@property (strong, nonatomic) UIColor *titleColor;
/**
 *  default: whiteColor
 */
@property (strong, nonatomic) UIColor *titleBackgroundColor;

/**
 *  分割线颜色, default: lightGrayColor
 */
@property (strong, nonatomic) UIColor *lineColor;
/**
 *  按钮高度 默认为49
 */
@property (assign,nonatomic) CGFloat buttonHeight;
/**
 *  标题的高度 默认为49
 */
@property (assign,nonatomic) CGFloat titleHeight;

+ (instancetype)actionSheetWithTitle:(NSString *)title
                        buttonTitles:(NSArray *)buttonTitles
                   cancelButtonTitle:(NSString *)cancelButtonTitle
                            delegate:(id<ActionSheetDelegate>)delegate;

- (instancetype)initWithTitle:(NSString *)title
                 buttonTitles:(NSArray *)buttonTitles
            cancelButtonTitle:(NSString *)cancelButtonTitle
                     delegate:(id<ActionSheetDelegate>)delegate;

- (void)show;

@end
@protocol ActionSheetDelegate <NSObject>

@optional
- (void)actionSheet:(MActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;

- (UIFont *)actionSheet:(MActionSheet *)actionSheet buttonTextFontAtIndex:(NSInteger)bottonIndex;
- (UIColor *)actionSheet:(MActionSheet *)actionSheet buttonTextColorAtIndex:(NSInteger)bottonIndex;
- (UIColor *)actionSheet:(MActionSheet *)actionSheet buttonBackgroundColorAtIndex:(NSInteger)bottonIndex;

@end
