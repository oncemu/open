//
//  SegmentBar.h
//  Open
//
//  Created by mfp on 17/7/8.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SegmentBar;

@protocol SegmentBarDelegate <NSObject>

- (void)segmentBar:(SegmentBar *)segmentBar didSelectIndex: (NSInteger)toIndex fromIndex: (NSInteger)fromIndex;

@end


@interface SegmentBar : UIView

+ (instancetype)segmentBarWithFrame: (CGRect)frame;
/**代理*/
@property (nonatomic,weak) id<SegmentBarDelegate> delegate;
/**数据源*/
@property (nonatomic, strong)NSArray<NSString *> *items;
/**当前选中的索引，双向设置*/
@property (nonatomic,assign) NSInteger selectIndex;


@end
