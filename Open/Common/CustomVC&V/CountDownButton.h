//
//  QXCountDownButton.h
//  ShownailSeller
//
//  Created by onceMu on 2016/12/1.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountDownButton : UIButton

@property(nonatomic, assign) NSInteger timeout;
@property(nonatomic, copy) NSString *endTitle;
@property(nonatomic, assign) BOOL start;

@end
