//
//  UIBarButtonItem+QXBarbutton.m
//  ShownailSeller
//
//  Created by mfp on 16/10/27.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import "UIBarButtonItem+QXBarbutton.h"

@implementation UIBarButtonItem (QXBarbutton)

+ (UIBarButtonItem *)itemWithImageName:(NSString *)imageName target:(id)target action:(SEL)action
{
    UIButton *button = [[UIButton alloc] init];
    [button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];

    button.frameSize = button.currentBackgroundImage.size;
    button.adjustsImageWhenHighlighted = NO;
    
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

@end
