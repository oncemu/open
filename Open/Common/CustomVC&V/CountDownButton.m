//
//  QXCountDownButton.m
//  ShownailSeller
//
//  Created by onceMu on 2016/12/1.
//  Copyright © 2016年 shownail. All rights reserved.
//

#import "CountDownButton.h"

@implementation CountDownButton

-(void)startCountDown {
    if (_timeout <= 0) {
        _timeout = 120;
    }
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(_timeout <= 0){ //倒计时结束，关闭
            self.userInteractionEnabled=YES;
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [self setTitle:_endTitle forState:UIControlStateNormal];
                [self setTitleColor:[UIColor colorWithHex:0x737378] forState:UIControlStateNormal];
            });
        }else{
            self.userInteractionEnabled=NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [self setTitle:[NSString stringWithFormat:@"%ld秒",(long)_timeout] forState:UIControlStateNormal];
                [self setTitleColor:[UIColor colorWithHex:0xff2673] forState:UIControlStateNormal];
                _timeout--;
            });
        }
    });
    dispatch_resume(_timer);
}

-(void)setStart:(BOOL)start {
    _start = start;
    if (start) {
        [self startCountDown];
    }else{
        self.userInteractionEnabled = YES;
        [self setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.titleLabel.textColor = [UIColor colorWithHex:0x737378];
    }
}

-(void)setTimeout:(NSInteger)timeout {
    if (timeout) {
        _timeout = timeout;
    }else{
        _timeout = 120;
    }
}

@end
