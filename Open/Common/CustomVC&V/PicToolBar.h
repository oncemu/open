//
//  PicToolBar.h
//  Open
//
//  Created by mfp on 17/7/9.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 喜欢数，关注数，收藏数等组成的工具栏
 frame设定之后，根据对齐方式排版

 1、左对齐 则frame.origin.x不变，长度适配
 2、右对齐 通过改变frame.origin.x适配长度，结束点不变
 ***** 必须初始化frame的起始坐标origin.x 不能初始化为CGRectZero ！！！
 */

typedef NS_ENUM(NSUInteger, ToolBarAlignment)
{
    ToolBarAlignmentLeft = 0,
    ToolBarAlignmentRight
};

typedef void (^tapAction)(UIButton *btn, NSInteger index);

@interface PicToolBar : UIView

@property(nonatomic, copy)   tapAction handle;
@property(nonatomic, strong) UIColor *textColor;
@property(nonatomic, strong) UIFont  *textFont;
@property(nonatomic, assign) ToolBarAlignment barAlignment;

//添加元素
- (void)addItem:(UIImage *)image handle:(tapAction)handle;

//根据位置元素赋值，位置不存在元素则失效  赋值顺序从左到右
- (void)setCount:(NSInteger)count atIndex:(NSInteger)index;

- (void)clearData;
@end
