//
//  NotifyConstant.h
//
//  Created by jiang on 12-4-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

//监听网络状态(当网络不可达时）
#define kNetworkNotReachableNotfiicationName    @"NetworkNotReachableNotfiicationName"
#define kNetworkViaWWANNotfiicationName         @"NetworkViaWWANNotfiicationName"
#define kNetworkViaWIFINotfiicationName         @"NetworkViaWIFINotfiicationName"
#define kNetworkChangeNotificationName          @"NetworkChangeNotificationName"

//user
#define kUserLoginSuccessNotificationName                   @"UserLoginSuccessNotificationName"
#define kUserRegSuccessNotificationName                     @"UserRegSuccessNotificationName"
#define kUserUpdateSuccessNotificationName                  @"UserUpdateSuccessNotificationName"
#define kUserLogOutSuccessNotificationName                  @"UserLogOutSuccessNotificationName"
#define kUserInfoRefreshSuccessNotificationName             @"UserInfoRefreshSuccessNotificationName"

//加载keyword完成
#define kLoadKeywordSuccessNotificationName                 @"LoadKeywordSuccessNotificationName"

//冷启动完成
#define kLaunchCompleteNotificationName                     @"LaunchCompleteNotificationName"

//引导完成
#define kTeachStepFinishNotificationName                    @"TeachStepFinishNotificationName"

//清除缓存完成
#define kClearCacheSucessNotificationName                   @"ClearCacheSucessNotificationName"

//播放new keyword
#define kPlayNewKeywordNotificationName                     @"PlayNewKeywordNotificationName"

//点击图片播放new keyword
#define kPlayNewKeywordFromImageViewNotificationName        @"PlayNewKeywordFromImageViewNotificationName"

//弹出购买按钮
#define kChoosePurchaseActionViewNotificationName           @"ChoosePurchaseActionViewNotificationName"

#define kGetUserAgreementSuccessNotificationName            @"GetUserAgreementSuccessNotificationName"

#define kShareCouponSuccessNotificationName @"ShareCouponSuccessNotificationName"
//后台播放模式
#define kBackgroundModePauseNotificationName @"BackgroundModePauseNotificationName"
#define kBackgroundModePlayNotificationName @"BackgroundModePlayNotificationName"
#define kBackgroundModePreviousNotificationName @"BackgroundModePreviousNotificationName"
#define kBackgroundModeNextNotificationName @"BackgroundModeNextNotificationName"

#define kPublishAlbumSuccess @"publishAlbumSuccess"

//
static NSString * const kUserFansIdentifier = @"UserFansIdentifier";
static NSString * const kAlbumMakeSuccess = @"kAlbumMakeSuccess";

