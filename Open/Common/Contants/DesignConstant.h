//
//  DesignConstant.h
//  ShownailSeller
//
//  Created by onceMu on 15/11/18.
//  Copyright © 2015年 shownail. All rights reserved.
//

//默认英文字体
#define kCommonEnFontName              @"AmericanTypewriter"
#define kCommonBoldFontName              @"AmericanTypewriter-bold"

//字体
#define kSmallFont  [UIFont systemFontOfSize:12.0f]
#define kLargeFont  [UIFont systemFontOfSize:16.0f]
#define kMiddleFont [UIFont systemFontOfSize:13.0f]
#define kNormalFont [UIFont systemFontOfSize:13.0f]



#define kStatusbarColor                 [UIColor colorWithHex:0xfd9745]
/**
 *  主要字体颜色   深色
 *
 *  @return 字体颜色－深色
 */
#define DeepFontColor                    [UIColor colorWithHex:0x131516]
/**
 *  主要字体颜色   中色
 *
 *  @return 字体颜色－中色
 */
#define MiddleFontColor                  [UIColor colorWithHex:0x767b7b]
/**
 *  主要字体颜色   浅色
 *
 *  @return 字体颜色－浅色
 */
#define LightFontColor                   [UIColor colorWithHex:0xd0d7d7]

/**
 *  主要视图背景色  白色
 *
 *  @return 视图背景色－白色
 */
#define MainBackGroundColor              WhiteColor

/**
 *  cell背景色
 *
 *
 *  @return cell背景色
 */
#define CellBackGroundColr                      [UIColor colorWithHex:0xf6f6f6]
/**
 *  主要视图背景色  深色
 *
 *  @return 视图背景色－深色
 */
#define DeepBackGroundColor                    [UIColor colorWithHex:0xfcfafc]
/**
 *  主要的蓝色
 *
 *  @return 视图背景色－浅色
 */
#define LightBackGroundColor                   [UIColor colorWithHex:0x34b5ff]

/**
 *  主要的红色
 *
 *  @return 视图背景色－浅色
 */
#define MainRedColor                   [UIColor colorWithHex:0xff5f5f]


/**
 *  分割线背景色
 *
 *  @return 分割线背景色
 */
#define LineBackGroundColor                     [UIColor colorWithHex:0xdbdbdb]

/**
 *  最大字体大小
 *
 *  @return 返回最大字体大小 18.0f
 */
#define MAXFontSize                            [UIFont systemFontOfSize:18.0f]
/**
 *  最小字体大小
 *
 *  @return 返回最小字体大小 10.0f
 */
#define MinFontSize                            [UIFont systemFontOfSize:10.0f]

/**
 *  6以下设备默认间距
 *
 *  @return 10
 */
#define NormalSpacing                          10.0f

/**
 *  底部tabbar高度
 *
 *  @return 44.0f
 */
#define kTabBarHeight                          49.0f

/**
 *  通用的header高度
 *
 *
 *  @return 40.0f
 */
#define kHeaderBarHeight                       40.0f

#define BlueColor                       [UIColor blueColor]
#define BlackColor                      [UIColor blackColor]
#define WhiteColor                      [UIColor whiteColor]
#define GrayColor                       [UIColor grayColor]
#define YellowColor                     [UIColor yellowColor]
#define ClearColor                      [UIColor clearColor]
#define RedColor                        [UIColor redColor]
#define GreenColor                      [UIColor greenColor]
#define MagentaColor                    [UIColor magentaColor]
#define OrangeColor                     [UIColor orangeColor]
#define PurpleColor                     [UIColor purpleColor]
#define BrownColor                      [UIColor brownColor]
#define ClearColor                      [UIColor clearColor]
#define CyanColor                       [UIColor cyanColor]
//天空蓝
#define SkyBlueColor                    [UIColor colorWithRed:0/255.0f green:178/255.0f blue:238/255.0f alpha:1.0]
//麒麟黄
#define GoldenrodColor                  [UIColor colorWithRed:215/255.0f green:170/255.0f blue:51/255.0f alpha:1.0]
//海滩沙
#define SandColor                       [UIColor colorWithRed:222/255.0f green:182/255.0f blue:151/255.0f alpha:1.0]
//橙色
#define BurntOrangeColor                [UIColor colorWithRed:184/255.0f green:102/255.0f blue:37/255.0f alpha:1.0]
//绿色
#define YellowGreenColor                 [UIColor colorWithRed:192/255.0f green:242/255.0f blue:39/255.0f alpha:1.0]

#define ColorWithHex(S)                   [UIColor colorWithHex:(S)]

#define grayLineColor                       [UIColor colorWithRed:214/255.0f green:214/255.0f blue:214/255.0f alpha:1.0]



