//
//  ApiConstants.h
//
//  Created by jiang on 12-4-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  线上环境
 */
//static NSString * const URL_HOST = @"http://101.200.188.60/";


/**
 *  测试服务器地址
 */
static NSString * const URL_HOST = @"http://120.76.120.96:8084/";

//版本信息（每次新增版本都需要修改）
#define API_VERSION         @"1.1.0"


#pragma mark --用户注册、登录相关逻辑
/* 验证码相关 */
#define kCaptcha           @"captcha"

/* 登录 */
#define kSignIn            @"sign_in"

/* 退出 */
#define kLoiginOut         @"sign_out"

/* 注册 */
#define kSignUp            @"sign_up"


/* 用户信息 */
#define kUserInfo          @"user/profile"

//发布图片
#define kPost              @"posts"

//获取用户的点赞列表
#define kUserPrasieList   @"user/favorite_albums"
//获取用户发布的列表
#define kUserPublishList  @"user/albums"
//获取用户美图1
#define kUserPhotoListForSale   @"user/shelf_photo_albums"
//获取用户美图2
#define kUserPhotoList  @"user/photo_albums"
//更改用户背景图
#define kUserBackGround @"user/background?"

