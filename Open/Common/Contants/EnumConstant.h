//
//  EnumConstant.h
//  Open
//
//  Created by mfp on 17/9/7.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#ifndef EnumConstant_h
#define EnumConstant_h

typedef NS_ENUM(NSUInteger , PublishType) {
    PublishTypePicture = 0,//发布美图
    PublishTypeTravelNotes ,//发布游记
    PublishTypeNormal //普通换头像和背景
};


#endif /* EnumConstant_h */
