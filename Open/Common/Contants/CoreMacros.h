//
//  CoreMacros.h
//  Core
//
//  Created by libo on 12-1-10.
//  Copyright (c) 2012年 mac. All rights reserved.
//

///////////////////////////////////////////////////////////////////////////////////////////////////
// Safe releases
#pragma mark - common functions
#define RELEASE_SAFELY(__POINTER) { [__POINTER release]; __POINTER = nil; }
#define INVALIDATE_TIMER(__TIMER) { [__TIMER invalidate]; __TIMER = nil; }
#define INVALIDATE_CF_TIMER(__TIMER) { if (nil != (__TIMER)) {[__TIMER invalidate]; __TIMER = nil; } }
// Release a CoreFoundation object safely.
#define RELEASE_CF_SAFELY(__REF) { if (nil != (__REF)) { CFRelease(__REF); __REF = nil; } }

#define NavigationBar_HEIGHT 44
#define StatusBar_HEIGHT     [UIApplication sharedApplication].statusBarFrame.size.height

#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define SAFE_RELEASE(x) [x release];x=nil
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define CurrentSystemVersion ([[UIDevice currentDevice] systemVersion])
#define CurrentLanguage ([[NSLocale preferredLanguages] objectAtIndex:0])
#define CurrentDisplayName ([[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"])

#define BACKGROUND_COLOR [UIColor colorWithRed:242.0/255.0 green:236.0/255.0 blue:231.0/255.0 alpha:1.0]


//use dlog to print while in debug model
#ifdef DEBUG
# define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
# define DLog(...)
#endif


#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define ios7 ([CurrentSystemVersion floatValue] == 7.0)
#define OSVersionIsAtLeastiOS7 (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
#define OSVersionIsAtLeastiOS8 ([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)

#if TARGET_OS_IPHONE
//iPhone Device
#endif

#if TARGET_IPHONE_SIMULATOR
//iPhone Simulator
#endif


//ARC
#if __has_feature(objc_arc)
//compiling with ARC
#else
// compiling without ARC
#endif


//G－C－D
#define BACK(block) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
#define MAIN(block) dispatch_async(dispatch_get_main_queue(),block)

#define USER_DEFAULT [NSUserDefaults standardUserDefaults]
#define ImageNamed(_pointer) [UIImage imageNamed:[UIUtil imageName:_pointer]]
#define getImage(name) [UIImage imageNamed:name]
#pragma mark - degrees/radian functions
#define degreesToRadian(x) (M_PI * (x) / 180.0)
#define radianToDegrees(radian) (radian*180.0)/(M_PI)

#define ITTDEBUG
#define ITTLOGLEVEL_INFO 10
#define ITTLOGLEVEL_WARNING 3
#define ITTLOGLEVEL_ERROR 1

#ifndef ITTMAXLOGLEVEL

#ifdef DEBUG
#define ITTMAXLOGLEVEL ITTLOGLEVEL_INFO
#else
#define ITTMAXLOGLEVEL ITTLOGLEVEL_ERROR
#endif

#endif

// The general purpose logger. This ignores logging levels.
#ifdef ITTDEBUG
#define ITTDPRINT(xx, ...) NSLog(@"%s(%d): " xx, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define ITTDPRINT(xx, ...) ((void)0)
#endif

// Prints the current method's name.
#define ITTDPRINTMETHODNAME() ITTDPRINT(@"%s", __PRETTY_FUNCTION__)

// Log-level based logging macros.
#if ITTLOGLEVEL_ERROR <= ITTMAXLOGLEVEL
#define ITTDERROR(xx, ...) ITTDPRINT(xx, ##__VA_ARGS__)
#else
#define ITTDERROR(xx, ...) ((void)0)
#endif

#if ITTLOGLEVEL_WARNING <= ITTMAXLOGLEVEL
#define ITTDWARNING(xx, ...) ITTDPRINT(xx, ##__VA_ARGS__)
#else
#define ITTDWARNING(xx, ...) ((void)0)
#endif

#if ITTLOGLEVEL_INFO <= ITTMAXLOGLEVEL
#define ITTDINFO(xx, ...) ITTDPRINT(xx, ##__VA_ARGS__)
#else
#define ITTDINFO(xx, ...) ((void)0)
#endif

#ifdef ITTDEBUG
#define ITTDCONDITIONLOG(condition, xx, ...) { if ((condition)) { \
ITTDPRINT(xx, ##__VA_ARGS__); \
} \
} ((void)0)
#else
#define ITTDCONDITIONLOG(condition, xx, ...) ((void)0)
#endif

#define ITTAssert(condition, ...) \
do { \
if (!(condition)) { \
[[NSAssertionHandler currentHandler] \
handleFailureInFunction:[NSString stringWithUTF8String:__PRETTY_FUNCTION__] \
file:[NSString stringWithUTF8String:__FILE__] \
lineNumber:__LINE__ \
description:__VA_ARGS__]; \
} \
} while(0)



#define WIDTH [[UIScreen mainScreen] bounds].size.width
#define HEIGHT [[UIScreen mainScreen] bounds].size.height


#define VIEWWITHTAG(_OBJECT, _TAG) [_OBJECT viewWithTag : _TAG]


#define MyLocal(x, ...) NSLocalizedString(x, nil)

//这样就不需要填默认的参数了. 对于需要反复使用的方法比较方便。
//
//比如说MyLocal(@"0094") 代替了NSLocalizedString(@"0094", nil)


#define LOADIMAGE(file,ext) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:file ofType:ext]]

//读取本地图片的 和imageNamed一样，但是性能比后者要强很多，两个参数，前面一个是 文件名，后面一个是类型
//
//例如 imageView.image = LOADIMAGE(@"文件名",@"png");
//
//
//在资源不吃紧的情况下,这种反而是个累赘,因为没有缓存.
//
//imagenamed只是说要缓存,而且无法手动清空而已.缓存是好是坏,只能自己判定.


#define ALERTMSG(TITLE,STRMSG) [[UIAlertView alloc] initWithTitle:TITLE message:STRMSG delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil]
#define BARBUTTON(TITLE, SELECTOR) 	[[[UIBarButtonItem alloc] initWithTitle:TITLE style:UIBarButtonItemStylePlain target:self action:SELECTOR] autorelease]
#define SYSBARBUTTON(ITEM, SELECTOR) [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:ITEM target:self action:SELECTOR] autorelease]

//通用宏定义
#define WS(weakSelf)  __weak __typeof(self)weakSelf = self;

#define Run                             NSLog(@"%s",__FUNCTION__)
#define Wfenge                          NSLog(@"----------------------------------")
#define WIs(X)                          NSLog(@"%@", X)
#define WIsFloat(X)                     NSLog(@"%f", X)
#define WIsNumber(X)                    NSLog(@"%d", X)
#define WIsResult(result)               NSLog(@"%@",result?@"YES":@"NO");
#define WIsFrame(view)                  NSLog(@"%@",NSStringFromCGRect(view.frame));

#define CM(X,Y,W,H)                     CGRectMake(X, Y, W, H)
#define Size(W, H)                      CGSizeMake((W), (H))
#define Point(X, Y)                     CGPointMake((X), (Y))


#define BlueColor                       [UIColor blueColor]
#define BlackColor                      [UIColor blackColor]
#define WhiteColor                      [UIColor whiteColor]
#define GrayColor                       [UIColor grayColor]
#define YellowColor                     [UIColor yellowColor]
#define ClearColor                      [UIColor clearColor]
#define RedColor                        [UIColor redColor]
#define GreenColor                      [UIColor greenColor]
#define BlueColor                       [UIColor blueColor]
#define MagentaColor                    [UIColor magentaColor]
#define OrangeColor                     [UIColor orangeColor]
#define PurpleColor                     [UIColor purpleColor]
#define BrownColor                      [UIColor brownColor]
#define ClearColor                      [UIColor clearColor]
#define CyanColor                       [UIColor cyanColor]
//天空蓝
#define SkyBlueColor                    [UIColor colorWithRed:0/255.0f green:178/255.0f blue:238/255.0f alpha:1.0]
//麒麟黄
#define GoldenrodColor                  [UIColor colorWithRed:215/255.0f green:170/255.0f blue:51/255.0f alpha:1.0]
//海滩沙
#define SandColor                       [UIColor colorWithRed:222/255.0f green:182/255.0f blue:151/255.0f alpha:1.0]
//橙色
#define BurntOrangeColor                [UIColor colorWithRed:184/255.0f green:102/255.0f blue:37/255.0f alpha:1.0]
//绿色
#define YellowGreenColor                 [UIColor colorWithRed:192/255.0f green:242/255.0f blue:39/255.0f alpha:1.0]

#define ColorWithHex(S)                   [UIColor colorWithHex:(S)]

#define grayLineColor                       [UIColor colorWithRed:214/255.0f green:214/255.0f blue:214/255.0f alpha:1.0]
#define  LemonYellow        [UIColor colorWithHex:(0xffb200)]

#define  LightLemonYellow   [UIColor colorWithHex:(0xffe672)]
#define  BtnLemonYellow   [UIColor colorWithHex:(0xfdde32)]

#define SCALE(x)     (CGFloat)((WIDTH/375.0f) * x)

/**
 *  Description
 */

//设置字体大小
#define Font(S)                                 ([UIFont systemFontOfSize:(S)])
#define FontWithName(N, S)                      ([UIFont fontWithName:(N) size:(S)])
#define RandomNumber(A, B)                      (arc4random() % ((B) - (A) + 1) + (A))
#define BoldFont(S)                             ([UIFont boldSystemFontOfSize:S])

//在iPhone6尺寸上的效果图，根据屏幕宽度获取适配的高度
#define HEIGHT_FIT(h)        (SCREEN_WIDTH * (h)) / 375


