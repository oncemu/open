//
//  NSDictionary+Additions.h
//  Core
//
//  Created by Once on 14-8-8.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Additions)

- (NSDictionary *)dictionaryByReplacingNullsWithStrings;

@end
