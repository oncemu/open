//
//  NSStringAdditions.h
//  Leho
//
//  Created by Sun Yu on 12-2-8.
//  Copyright (c) 2012年 leho.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(NSStringEx)

- (NSString*) urlEncode:(NSStringEncoding)stringEncoding;
- (NSString*) urlDecode:(NSStringEncoding)stringEncoding;

/**
 *  MD5加密
 *
 *  @return MD5值
 */
- (NSString*) MD5;

/**
 *  SHA1加密
 *
 *  @return SHA1值
 */
- (NSString*) SHA1;

-(BOOL) isPureInt;


/**
 *  是否包含emoji表情
 *
 *  @return 是否包含emoji表情
 */
- (BOOL)hasEmoji;

+ (NSString*)componentsJoinedByDictionary:(NSDictionary *)dic
								seperator:(NSString *)seperator;

+ (NSString *)queryStringFromQueryDictionary:(NSDictionary *)query withURLEncode:(BOOL)doURLEncode;


/**
 *  字节长度
 *
 *  @return 字节长度
 */
- (unsigned long)convertToInt;
- (NSString*)cutByConvertInt:(unsigned long)strlength;

/**
 *  获取NSString路径文件的MIMEType
 *
 *  @return 指定文件的MIMEType
 */
-(NSString *)MIMEType;

+ (NSString *)formatNilString:(NSString *)string;

@end
