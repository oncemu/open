//
//  NSDictionary+Additions.m
//  Core
//
//  Created by Once on 14-8-8.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)

- (NSDictionary *)dictionaryByReplacingNullsWithStrings
{
    const NSMutableDictionary *replaced = [self mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    for(NSString *key in self)
    {
        const id object = [self objectForKeyedSubscript:key];
        if (object == nul) {
            [replaced setObject:blank forKey:key];
        }
    }
    return [replaced copy];
}

@end
