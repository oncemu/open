//
//  QXCustomImageView.m
//  ShowNail
//
//  Created by jianghaibo on 14-6-8.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import "CustomImageView.h"
#import "UIImageView+WebCache.h"

@implementation CustomImageView


- (id)init{
    self = [super init];
    if (self) {
        [self setContentScaleFactor:[[UIScreen mainScreen] scale]];
        self.contentMode =  UIViewContentModeScaleAspectFit;
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//        self.clipsToBounds  = YES;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentScaleFactor:[[UIScreen mainScreen] scale]];
        self.contentMode =  UIViewContentModeScaleAspectFit;
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//        self.clipsToBounds  = YES;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
