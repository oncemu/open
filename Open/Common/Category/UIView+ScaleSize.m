//
//  UIView+ScaleSize.m
//  3minpassion
//
//  Created by onceMu on 2017/5/7.
//  Copyright © 2017年 ZhiXing. All rights reserved.
//

#import "UIView+ScaleSize.h"

@implementation UIView (ScaleSize)

+ (CGSize)getCurrentScale {
    //处理缩放比例
    //    CGFloat imageScale = self.scale / 100.0;

    CGSize baseSize = CGSizeMake(750.0 / 2.0, 1334.0 / 2.0); //切图原始大小
    CGSize size = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
    ;
    //计算绽放比例
    CGFloat widthScale = (size.width) / baseSize.width;
    CGFloat heightScale = (size.height) / baseSize.height;

    return CGSizeMake(widthScale, heightScale);
    
}

@end
