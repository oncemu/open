//
//  UIImageView.m
//  lehoMagIphone
//
//  Created by 李 博 on 12-8-8.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "UIImageView+Extensions.h"

@implementation UIImageView (Extensions)

//图片逐渐显示
-(void)setImageGradually:(UIImage*)image
{
    self.image = image;
    [UIView animateWithDuration:0.3f animations:^{
        self.alpha = 0;
        self.alpha = 1;
    }];
}

@end
