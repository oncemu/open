//
//  UIImageAdditions.h
//  Leho
//
//  Created by Sun Yu on 3/10/12.
//  Copyright (c) 2012 leho.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage(addition)

-(id)scaleAndRotateImage:(NSInteger)size withOritation:(NSInteger)oritation;

-(id)scaleImageWith:(NSInteger)size ;
-(CGFloat)ImageHeightwithWidth:(NSInteger)size;



+ (UIImage *)resetSize:(UIImage *)image;

//拉伸
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

//Combine two UIImages－合成
+ (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2;

//splice two UIImages－拼接
+ (UIImage *)spliceImage:(UIImage *)image1 toImage:(UIImage *)image2 rect1:(CGRect)rect1 rect2:(CGRect)rect2;

//Create a UIImage from a part of another UIImage－截取
+ (UIImage *)imageFromImage:(UIImage *)image inRect:(CGRect)rect;

//截屏
+ (UIImage *)screenShotsImage:(UIView *)aView;

//Save UIImage to Photo Album－保存到相册
//This is just a one-liner:
//
//UIImageWriteToSavedPhotosAlbum(image, self, @selector(imageSavedToPhotosAlbum: didFinishSavingWithError: contextInfo:), context);
//And to know if the save was successful:
+ (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;

+ (UIImage*)imageFromView:(UIView*)view;
+ (UIImage*)imageFromView:(UIView*)view scaledToSize:(CGSize)newSize;
+ (void)beginImageContextWithSize:(CGSize)size;

@end
