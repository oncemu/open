//
//  UIImage+fixorientation.h
//  lehoMagPad
//
//  Created by Sun Yu on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//图片方向修正
#import <UIKit/UIKit.h>

@interface UIImage (fixorientation)
//若image的orientation不为up，返回一个方向旋转为up的image
- (UIImage *)fixOrientation:(UIImage *)aImage;

/** 按给定的方向旋转图片 */
- (UIImage*)rotate:(UIImageOrientation)orient;

/** 垂直翻转 */
- (UIImage *)flipVertical;

/** 水平翻转 */
- (UIImage *)flipHorizontal;

@end
