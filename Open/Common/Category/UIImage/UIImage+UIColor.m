//
//  UIImage+UIColor.m
//  Core
//
//  Created by Once on 14-4-29.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import "UIImage+UIColor.h"

@implementation UIImage (UIColor)

+ (UIImage *)createImageWithColor:(UIColor *)color
{
    return [self createImageWithColor:color rect:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f)];
}

+ (UIImage *)createImageWithColor:(UIColor *)color rect:(CGRect)rect {
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage*theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}


@end
