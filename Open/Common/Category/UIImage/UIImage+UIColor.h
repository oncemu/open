//
//  UIImage+UIColor.h
//  Core
//
//  Created by Once on 14-4-29.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIColor)

+ (UIImage *)createImageWithColor:(UIColor *)color;

+ (UIImage *)createImageWithColor:(UIColor *)color rect:(CGRect)rect;

@end
