//
//  UIImage-Extensions.h
//  The9AdPlatform
//
//  Created by zhang xiaodong on 11-5-31.
//  Copyright 2011 the9. All rights reserved.
//
//图片缩放+图像旋转＋其它
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (CS_Extensions)
- (UIImage*)imageAtRect:(CGRect)rect;
//缩放比例大小图像－小于图片比例进行截图
- (UIImage*)imageByScalingProportionallyToMinimumSize:(CGSize)targetSize;
//比例大小进行缩放的图像 如：宽或者高为0时，自动跟根据宽或者高成比例缩放
- (UIImage*)imageByScalingProportionallyToSize:(CGSize)targetSize;
//缩放大小的图像-不按比例
- (UIImage*)imageByScalingToSize:(CGSize)targetSize;
//弧度图像旋转
- (UIImage*)imageRotatedByRadians:(CGFloat)radians;
//图像旋转
- (UIImage*)imageRotatedByDegrees:(CGFloat)degrees;
//裁剪图片使图片填满整个imageview
- (UIImage *)clippingPicture:(CGSize)targetSize;
@end;
