//
//  UIImageView.h
//  lehoMagIphone
//
//  Created by 李 博 on 12-8-8.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Extensions)
//图片逐渐显示
-(void)setImageGradually:(UIImage*)image;
@end
