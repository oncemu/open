//
//  UIButton+countDown.m
//  TSYSDK
//
//  Created by Heinoc on 16/6/27.
//  Copyright © 2016年 Heinoc. All rights reserved.
//

#import "UIButton+countDown.h"
#import <objc/runtime.h>


static const void *beforeCountDownTitle = &beforeCountDownTitle;
static const void *beforeCountDownBGColor = &beforeCountDownBGColor;
static const void *timeOut = &timeOut;

@implementation UIButton (countDown)

@dynamic beforeCountDownTitle;
@dynamic beforeCountDownBGColor;
@dynamic timeOut;

- (void)startWithTime:(NSInteger)timeLine title:(NSString *)title countDownTitle:(NSString *)subTitle mainColor:(UIColor *)mColor countColor:(UIColor *)color {
    
    self.beforeCountDownTitle = title;
    self.beforeCountDownBGColor = mColor;
    
    //倒计时时间
//    __block NSInteger timeOut = timeLine;
    self.timeOut = timeLine;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    //每秒执行一次
    dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_timer, ^{
        
        //倒计时结束，关闭
        if (self.timeOut <= 0) {
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                self.backgroundColor = mColor;
                [self setTitle:title forState:UIControlStateNormal];
                self.userInteractionEnabled = YES;
            });
            
        } else {
            int allTime = (int)timeLine + 1;
            int seconds = self.timeOut % allTime;
            NSString *timeStr = [NSString stringWithFormat:@"%0.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.backgroundColor = color;
                [self setTitle:[NSString stringWithFormat:@"%@%@",timeStr,subTitle] forState:UIControlStateNormal];
                self.userInteractionEnabled = NO;
            });
            self.timeOut--;
        }
    });
    dispatch_resume(_timer);
}

-(void)cancelCountDownTimer{
//    if (self.timer == nil || dispatch_source_testcancel(self.timer)) {
//        return;
//    }
    self.timeOut = 0;
    
}


#pragma mark - category中添加自定义属性的getter/setter
- (NSString *)beforeCountDownTitle {
    return objc_getAssociatedObject(self, beforeCountDownTitle);
}

- (void)setBeforeCountDownTitle:(NSString *)input {
    objc_setAssociatedObject(self, beforeCountDownTitle, input, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)beforeCountDownBGColor {
    return objc_getAssociatedObject(self, beforeCountDownBGColor);
}

- (void)setBeforeCountDownBGColor:(NSString *)input {
    objc_setAssociatedObject(self, beforeCountDownBGColor, input, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSInteger)timeOut {
    return [objc_getAssociatedObject(self, timeOut) intValue];
}

- (void)setTimeOut:(NSInteger)input {
    NSNumber *number= [[NSNumber alloc] initWithInteger:input];
    objc_setAssociatedObject(self, timeOut, number, OBJC_ASSOCIATION_COPY);
}



@end
