//
//  UIView+Border.h
//  taoshouyouiphone
//
//  Created by Heinoc on 16/7/26.
//  Copyright © 2016年 Heinoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Border)

-(void)addBottomBorderWithColor: (UIColor*) color andWidth:(CGFloat) borderWidth;
-(void)addLeftBorderWithColor: (UIColor*) color andWidth:(CGFloat) borderWidth;
-(void)addRightBorderWithColor: (UIColor*) color andWidth:(CGFloat) borderWidth;
-(void)addTopBorderWithColor: (UIColor*) color andWidth:(CGFloat) borderWidth;

@end
