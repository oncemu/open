//
//  UIDeviceHardware.h
//
#define kIphone4String @"iPhone 4"
#define kIphone4sString @"iPhone 4S"
#define kIphone6String @"iPhone 6"
#define kIphone6plusString @"iPhone 6 Plus"

#import <Foundation/Foundation.h>

@interface UIDeviceHardware : NSObject
    + (NSString *) platform;
    + (NSString *) platformString;
@end
