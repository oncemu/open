//
//  UIView+ScaleSize.h
//  3minpassion
//
//  Created by onceMu on 2017/5/7.
//  Copyright © 2017年 ZhiXing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ScaleSize)
+ (void)getCurrentScale;

@end
