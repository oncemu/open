//
//  NSStringAdditions.m
//  Leho
//
//  Created by Sun Yu on 12-2-8.
//  Copyright (c) 2012年 leho.com. All rights reserved.
//

#import "NSStringAdditions.h"
#import <CommonCrypto/CommonDigest.h>
#import <MobileCoreServices/MobileCoreServices.h>

static NSInteger strCompare(id str1, id str2, void *context){
	return [((NSString*)str1) compare:str2 options:NSLiteralSearch];
}

@implementation NSString(NSStringEx)

- (NSString*) urlEncode:(NSStringEncoding)stringEncoding
{
	
	NSArray *escapeChars = [NSArray arrayWithObjects:@";", @"/", @"?", @":",
							@"@", @"&", @"=", @"+", @"$", @",", @"!",
                            @"'", @"(", @")", @"*", @"-", @"~", @"_", nil];
	
	NSArray *replaceChars = [NSArray arrayWithObjects:@"%3B", @"%2F", @"%3F", @"%3A",
							 @"%40", @"%26", @"%3D", @"%2B", @"%24", @"%2C", @"%21",
                             @"%27", @"%28", @"%29", @"%2A", @"%2D", @"%7E", @"%5F", nil];
	
	int len = (int)[escapeChars count];
	
//	NSString *tempStr = [self stringByAddingPercentEscapesUsingEncoding:stringEncoding];
    NSString *tempStr = [self stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
	
	if (tempStr == nil) {
		return nil;
	}
	
	NSMutableString *temp = [tempStr mutableCopy];
	
	int i;
	for (i = 0; i < len; i++) {
		
		[temp replaceOccurrencesOfString:[escapeChars objectAtIndex:i]
							  withString:[replaceChars objectAtIndex:i]
								 options:NSLiteralSearch
								   range:NSMakeRange(0, [temp length])];
	}
	
	NSString *outStr = [NSString stringWithString: temp];
	
	return outStr;
}

- (NSString*) urlDecode:(NSStringEncoding)stringEncoding
{
    
	NSArray *escapeChars = [NSArray arrayWithObjects:@";", @"/", @"?", @":",
							@"@", @"&", @"=", @"+", @"$", @",", @"!",
                            @"'", @"(", @")", @"*", @"-", @"~", @"_", nil];
	
	NSArray *replaceChars = [NSArray arrayWithObjects:@"%3B", @"%2F", @"%3F", @"%3A",
							 @"%40", @"%26", @"%3D", @"%2B", @"%24", @"%2C", @"%21",
                             @"%27", @"%28", @"%29", @"%2A", @"%2D", @"%7E", @"%5F", nil];
	
	int len = (int)[escapeChars count];
	
	NSMutableString *temp = [self mutableCopy];
	
	int i;
	for (i = 0; i < len; i++) {
		
		[temp replaceOccurrencesOfString:[replaceChars objectAtIndex:i]
							  withString:[escapeChars objectAtIndex:i]
								 options:NSLiteralSearch
								   range:NSMakeRange(0, [temp length])];
	}
	NSString *outStr = [NSString stringWithString: temp];
    
//	return [outStr stringByReplacingPercentEscapesUsingEncoding:stringEncoding];
    return [outStr stringByRemovingPercentEncoding];
}

- (NSString*) MD5
{
	const char *cStr = [self UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
    if(cStr)
    {
        CC_MD5( cStr, (CC_LONG)strlen(cStr), result );
        return [[NSString stringWithFormat:
                 @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                 result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],
                 result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]
                 ] lowercaseString];
    }
    else {
        return nil;
    }
}


- (NSString*) SHA1
{
    const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:self.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (CC_LONG)data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

- (BOOL)hasEmoji
{
    __block BOOL emoji = NO;
    [self enumerateSubstringsInRange:NSMakeRange(0, self.length)
                             options:NSStringEnumerationByComposedCharacterSequences
                          usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                              const unichar hs = [substring characterAtIndex: 0];
                              
                              if (0xd800 <= hs && hs <= 0xdbff) {
                                  const unichar ls = [substring characterAtIndex: 1];
                                  const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                                  if (0x1d000 <= uc && uc <= 0x1f77f) {
                                      emoji = YES;
                                      *stop = YES;
                                  }
                              } else {
                                  if (0x2100 <= hs && hs <= 0x27ff) {
                                      emoji = YES;
                                      *stop = YES;
                                  }
                                  else if (hs == 0x2b05 || hs == 0x2b06 || hs == 0x2b07 ||
                                           hs == 0x2b55 || hs == 0x2b50 || hs == 0x2b1b || hs == 0x2b1c ||
                                           hs == 0x00a9 || hs == 0x00ae ||
                                           hs == 0x2934 || hs == 0x2935 ||
                                           hs == 0x2049 || hs == 0x203c || hs == 0x2002 || hs == 0x2003 || hs == 0x2005 ||
                                           hs == 0x3030 || hs == 0x303d ||
                                           hs == 0x3297 || hs == 0x3299 ||
                                           hs == 0x231a || hs == 0x231b || hs == 0x23f0 || hs == 0x23f3 ||
                                           (hs >= 0x23e9 && hs <= 0x23ec) ||
                                           hs == 0x24c2 ||
                                           (hs >= 0x25fb && hs <= 0x25fe) ||
                                           hs == 0x25aa || hs == 0x25ab || hs == 0x25b6 || hs == 0x25c0) {
                                      emoji = YES;
                                      *stop = YES;
                                  }
                                  else if ((hs >= 0x0030 && hs <= 0x0039) || hs == 0x0023) { //1234567890#
                                      if (substring.length > 1) { //1⃣2⃣3⃣...
                                          const unichar ls = [substring characterAtIndex:1];
                                          if (ls == 0x20E3) { // ⃣
                                              emoji = YES;
                                              *stop = YES;
                                          }
                                      }
                                  }
                              }
                          }];
    
    return emoji;
}

+ (NSString*)componentsJoinedByDictionary:(NSDictionary *)dic
								seperator:(NSString *)seperator {
	if (!dic || dic.count == 0) {
		return nil;
	}
	NSArray *allkeys = [dic allKeys];
	NSMutableString *ms = [NSMutableString string];
	for (NSString *key in allkeys) {
		[ms appendFormat:@"%@%@=%@", seperator, key, [dic objectForKey:key]];
	}
	return [ms substringFromIndex:1];
}

+ (NSString *)queryStringFromQueryDictionary:(NSDictionary *)query withURLEncode:(BOOL)doURLEncode{
	if (!query) {
		return nil;
	}
	
	NSMutableString *buffer = [[NSMutableString alloc] initWithCapacity:0];
	// 将查询字典参数拼接成字符串,URL Encode
	for (id key in query) {
		NSString* value = [NSString stringWithFormat:@"%@",[query objectForKey:key]];
		if (doURLEncode) {
			value = [value urlEncode:NSUTF8StringEncoding];
		}
		[buffer appendString:[NSString stringWithFormat:@"&%@=%@", key, value]];
	}
	
	NSString* ret = [buffer substringFromIndex:1]; // 去掉第一个'&'
    
	return ret;
}

- (NSMutableDictionary*)emotionMapStr{
    NSArray *emotionarr = [NSArray arrayWithObjects:
                           @"[哈哈]",//1
                           @"[可怜]",//2
                           @"[偷笑]",//3
                           @"[问号]",//4
                           @"[可爱]",//5
                           @"[开心]",//6
                           @"[抓狂]",//7
                           @"[流泪]",//8
                           @"[汗]",//9
                           //		"[购物]",//10-
                           //		"[打折]",//11-
                           //		"[组团]",//12-
                           //		"[长草]",//13-
                           //		"[免费]",//14-
                           //		"[秒杀]",//15-
                           @"[臭美]",//16
                           @"[爽]",//17
                           //		"[快递]",//18-
                           @"[钱包]",//19
                           @"[生日快乐]",//20
                           @"[喜欢]",//21
                           @"[吃惊2]",//22
                           @"[得瑟]",//23
                           @"[酷]",//24
                           @"[思考]",//25
                           @"[衰]",//26
                           @"[划算]",//27
                           @"[相机]",//28
                           @"[嘻嘻]",//29
                           @"[显摆]",//30
                           @"[调皮]",//31
                           @"[挖鼻屎]",//32
                           @"[怒]",//33
                           @"[委屈]",//34
                           @"[调戏]",//35
                           @"[红包]",//36
                           @"[单身]"//37,
                           ,nil];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:20];
    for(int i = 0; i <  [emotionarr count]; i++){
        NSString * estr = [emotionarr objectAtIndex:i];
        [dict setObject:[NSString stringWithFormat:@"emotion%d",i] forKey:estr];
    }
    return dict;
}

/**
 * 判断NSString 是否为数字
 */
- (BOOL)isPureInt{
    NSScanner* scan = [NSScanner scannerWithString:self];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

- (unsigned long)convertToInt
{
    unsigned long strlength = 0;
    char* p = (char*)[self cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i=0 ; i<[self lengthOfBytesUsingEncoding:NSUnicodeStringEncoding] ;i++) {
        if (*p) {
            p++;
            strlength++;
        }
        else {
            p++;
        }
        
    }
    return strlength;
}

- (NSString*)cutByConvertInt:(unsigned long)strlength
{
    NSString *subString = strlength < [self length] ? [self substringFromIndex:strlength] : [self copy];
    
    while ([subString convertToInt] > strlength) {
        subString = [self substringToIndex:[subString length] - 1];
    }
    return subString;
}

-(NSString *)MIMEType
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:self]) {
        return nil;
    }
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge  CFStringRef)[self pathExtension], NULL);
    CFStringRef MIMEType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
    if (!MIMEType) {
        return @"application/octet-stream";
    }
    return (__bridge NSString *)MIMEType;
}

+ (NSString *)formatNilString:(NSString *)string {
    if (string == nil) {
        return @"";
    }
    if ([string isEqualToString:@"<null>"] || [string isEqualToString:@"[null]"]) {
        return @"";
    }
    if ([string isEqual:[NSNull null]]) {
        return @"";
    }
    if (![string isKindOfClass:[NSString class]]) {
        return @"";
    }
    return string;
}

@end
