//
//  DLNetworkModel.m
//  lehoMagPad
//
//  Created by jiang on 12-5-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "NetworkModel.h"
#import "GlobalDataModel.h"
#import "NotifyConstant.h"

@interface NetworkModel()
@property(nonatomic, retain) NSTimer *nsTimer; 
@property(nonatomic, retain) Reachability* internetReach;
@end

@implementation NetworkModel
@synthesize networkStatus;
@synthesize nsTimer;
@synthesize internetReach;

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    NetworkStatus oldStatus = networkStatus;
    networkStatus = [internetReach currentReachabilityStatus];

    switch (networkStatus)
    {
        case NotReachable:
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkNotReachableNotfiicationName object:nil];
            break;
        }  
        case ReachableViaWWAN:
        {
            if (oldStatus != ReachableViaWWAN) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkViaWWANNotfiicationName object:nil];
            }
            break;
        }
        case ReachableViaWiFi:
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkViaWIFINotfiicationName object:nil];
            break;
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkChangeNotificationName object:nil];
}

-(id) init {
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
        self.internetReach = [Reachability reachabilityForInternetConnection];
        [internetReach startNotifier];
        [self reachabilityChanged:nil];
    }
    return  self;
}

@end

