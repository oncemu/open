//
//  TSYBaseNetService.h
//  taoshouyouiphone
//
//  Created by Once on 14-4-17.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

//#import "AFHTTPRequestOperationManager.h"
#import "AFNetworking.h"
#import "AFURLSessionManager.h"
#import "AFHTTPSessionManager.h"
#import "AFURLRequestSerialization.h"
#import "GlobalDataModel.h"
#import "ApiConstants.h"
#import <YYModel.h>

/**
 *  网络基类里面有判断，只有当网络返回值中code 为1的时候次才是正确的网络请求、其它的都会放到failure block中
 */
typedef NS_ENUM(NSUInteger, TSYBaseNetRefreshType) {
    TSYBaseNetRefreshTypeDefault,
    TSYBaseNetRefreshTypeImage,
    TSYBaseNetRefreshTypePay
};

typedef NS_ENUM(NSInteger , RequestMethod) {
    RequestMethodGet = 0,
    RequestMethodPost,
    RequestMethodHead,
    RequestMethodPut,
    RequestMethodDelete,
    RequestMethodPatch
};

typedef NS_ENUM(NSInteger , RequestSerializerType) {
    RequestSerializerTypeHTTP = 0,
    RequestSerializerTypeJSON,
};


//成功block回调
typedef void(^responseSuccess)(NSString *requestInfo, id responseObject);
//失败block回调
typedef void(^responseFailure)(NSError *error,id responseObject);
//通用参数key值
static NSString const *baseRequestCodeKey = @"verifyCode";

static NSString * const kBaseErrorMessage = @"error_msg";


@interface BaseNetService : AFHTTPSessionManager

/* 全局数据 */
@property(nonatomic, strong) GlobalDataModel *gdm;
/* 请求参数 */
@property(nonatomic, strong) NSMutableDictionary *queryDictionary;
/* error描述 */
@property(nonatomic, strong) NSError *error;
/* error code */
@property(nonatomic, copy) NSString *code;
/* response data */
@property(nonatomic, strong) id responseObject;
/* 是否需要缓存，默认打开 */
@property(nonatomic, assign) BOOL isNeedCache;

@property (nonatomic, retain) dispatch_queue_t requestTaskQueue;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;



///**
// *  基础接口
// *
// *  @param success 成功回调
// *  @param failure 失败回调
// */
//- (void)sendToSeriviceWithSuccess:(responseSuccess)success
//                          failure:(responseFailure)failure;


/* 请求的URL */
- (NSString *)requestURLString;

/* Http请求的方法  */
- (RequestMethod)requestMethod;

/* 请求的SerializerType */
- (RequestSerializerType)requestSerializerType;

/* 在HTTP报头添加的自定义参数 */
- (NSDictionary *)requestHeaderFieldValueDictionary;

/* 请求的连接超时时间，默认为120秒 */
- (NSTimeInterval)requestTimeoutInterval;

/* 当前请求是否在执行 */
- (BOOL)isExecuting;

/* 取消当前请求 */
- (void)cancel;

/* 构造AFHTTPRequestSerializer */
- (AFHTTPRequestSerializer *)bulidCustomSerializer;

/* 无网络从缓存取数据*/
- (void)getResponseObjectFromCacheWithSuccess:(responseSuccess)success
                                      failure:(responseFailure)failure;

/* 请求成功后的处理方法 */
- (void)responseSuccessHandle:(id)responseObject
                      success:(responseSuccess)success
                      failure:(responseFailure)failure;


/**
 *  URLDecoder
 *
 *  @param str str description
 *
 *  @return return URLDecodedString
 */
-(NSString *)URLDecodedString:(NSString *)str;


/**
 *  GET网络请求(block返回)
 *
 *  @param URLString  接口地址（不包含URL_HOST）
 *  @param parameters 请求参数
 *  @param success        success block返回
 *  @param failure        failure block返回
 */
- (void)requestGETwithUrl:(NSString *)URLString
               parameters:(NSMutableDictionary*)parameters
                  success:(responseSuccess)success
                  failure:(responseFailure)failure;

/**
 *  POST网络请求(block返回)
 *
 *  @param URLString  接口地址（不包含URL_HOST）
 *  @param parameters 请求参数
 *  @param success        success block返回
 *  @param failure        failure block返回
 */
- (void)requestPOSTwithUrl:(NSString *)URLString
                parameters:(NSMutableDictionary*)parameters
                   success:(responseSuccess)success
                   failure:(responseFailure)failure;


/**
 DELETE网络请求(block返回)

 @param URLString 接口地址
 @param parameters 参数
 @param success 成功返回
 @param failure 失败返回
 */
//- (void)requestDELETEwithUrl:(NSString *)URLString
//               parameters:(NSMutableDictionary*)parameters
//                  success:(responseSuccess)success
//                  failure:(responseFailure)failure;


/**
 *  POST网络请求(block返回)
 *
 *  @param URLString  接口地址（不包含URL_HOST）
 *  @param parameters json参数
 *  @param success        success block返回
 *  @param failure        failure block返回
 */
- (void)requestPOSTJSONWithUrl:(NSString *)URLString
                parameters:(NSMutableDictionary *)parameters
                   success:(responseSuccess)success
                   failure:(responseFailure)failure;



/**
 上传文件block回调

 @param URLString  上传url地址
 @param images 上传二进制流
 @param parameters 参数
 @param filePath   文件地址
 @param success    成功
 @param failed     失败
 */
- (void)requestUpLoadFileWithURLString:(NSString *)URLString
                            images:(NSArray *)images
                            parameters:(NSMutableDictionary *)parameters
                              filePath:(NSString *)filePath
                               success:(responseSuccess)success
                                failed:(responseFailure)failed;

/**
 *  获取NSArray的加密校验串
 *
 *  @param array 根据接口verifyCode的参数顺序初始化的NSArray
 *
 *  @return 返回verifyCode
 */
-(NSString*) getVerifyCodeByArray:(NSArray*)array;

//上传图片
- (void)uploadImage:(NSArray *)images
             params:(NSDictionary *)params
                url:(NSString *)url
           progress:(void (^)(NSProgress *progress))progress
            success:(responseSuccess)success
            failure:(responseFailure)failure;

@end
