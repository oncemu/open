//
//  TSYBaseNetService.m
//  taoshouyouiphone
//
//  Created by Once on 14-4-17.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import "BaseNetService.h"
#import <sys/sysctl.h>
#import "ApiConstants.h"
#import "EGOCache.h"
#import "OpenUDID.h"
#import "NSStringAdditions.h"
#import <objc/runtime.h>
#import "UIDeviceUtil.h"
#import "YYModel.h"

#define ACCESS_KEY  @"df^has83$47kj!gha31jk_dg46f%aj"
#define REUQEST_METHOD  @"requestMethod"
#define REUQEST_API_URL  @"requestApiURL"

@implementation BaseNetService


- (instancetype)init {
    return [self initWithBaseURL:[NSURL URLWithString:URL_HOST]];
}


- (instancetype)initWithBaseURL:(NSURL *)url {
    return [self initWithBaseURL:url sessionConfiguration:nil];
}

- (instancetype)initWithSessionConfiguration:(NSURLSessionConfiguration *)configuration {
    return [self initWithBaseURL:[NSURL URLWithString:URL_HOST] sessionConfiguration:configuration];
}

- (instancetype)initWithBaseURL:(NSURL *)url
           sessionConfiguration:(NSURLSessionConfiguration *)configuration {
    
    self = [super initWithBaseURL:(NSURL *)url sessionConfiguration:(NSURLSessionConfiguration *)configuration];
    
    self.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    
    if (self) {
        _queryDictionary = [[NSMutableDictionary alloc]initWithCapacity:1];
        _gdm = [GlobalDataModel sharedInstance];
        _isNeedCache = YES;
    }
    
    self.requestTaskQueue = dispatch_queue_create("netservice.queue", NULL);
    
    return self;
}

#pragma mark  请求自定义相关方法



- (BOOL)isExecuting {
    return [self isExecuting];
}

- (void)cancel {
    [self cancel];
}

#pragma mark 通用配置方法

/* Http请求的方法 */
- (RequestMethod)requestMethod {
    return RequestMethodPost;
}

/* 请求的SerializerType */
- (RequestSerializerType)requestSerializerType {
    return RequestSerializerTypeJSON;
}

/* 请求的连接超时时间，默认为120秒 */
- (NSTimeInterval)requestTimeoutInterval {
    return 120;
}

/* 构造缓存的key */
- (NSString *)buildCacheKey {
    NSMutableString *cacheKey = [[NSMutableString alloc] init];
    [cacheKey appendString:[self requestURLString]];
    if ([self.queryDictionary count] > 0) {
        [cacheKey appendString:[self buildGetRquestURL]];
    }
    return [cacheKey MD5];
}

#pragma mark 构造请求url

/**
 *  构造请求url
 *
 *  @return 域名+请求方法名
 */
- (NSString *)requestURLString {
    NSString *requestMethod = [NSString stringWithFormat:@"%@",[self.queryDictionary objectForKey:REUQEST_METHOD]];
    //    NSString *api_version = [[TSYApiParser sharedInstance]getApiVersionWithAppName:requestMethod];
    // DLog(@"%@",api_version);
    //    [self.queryDictionary setObject:api_version forKey:@"api_ver"];
    NSMutableString *urlString = [[NSMutableString alloc] initWithCapacity:1];
    [urlString appendFormat:@"%@",[self.queryDictionary objectForKey:REUQEST_API_URL]];
    [urlString appendString:requestMethod];
    return urlString;
}

/**
 *  构造get方式的请求url
 *
 *  @return 将所有请求参数按key=value的方式构造成一个字符串
 */
- (NSString *)buildGetRquestURL {
    NSMutableString *urlString = [[NSMutableString alloc] init];
    for (NSString *key in self.queryDictionary.allKeys) {
        [urlString appendString:[NSString stringWithFormat:@"%@=%@&",key,[self.queryDictionary objectForKey:key]]];
    }
    return urlString;
}

#pragma mark 创建头部参数

// 把所有参数签名的MD5值
- (NSString *)buildSignture {
    NSEnumerator *enumerator = [self.queryDictionary keyEnumerator];
    NSMutableString *signture = [NSMutableString stringWithCapacity:1];
    NSString *theKey;
    [signture appendString:ACCESS_KEY];
    while (theKey = [enumerator nextObject]) {
        [signture appendString:theKey];
    }
    [signture appendString:ACCESS_KEY];
    return [signture MD5];
}

// 在HTTP报头添加的自定义参数
- (NSDictionary *)requestHeaderFieldValueDictionary {
    //用户cookie
    NSMutableString *session = [NSMutableString stringWithCapacity:1];
    if (self.gdm.isLogin) {
        [session appendFormat:@"Token %@",self.gdm.accessToken];
    }
    
    //userAgent
    NSString *appVersion = [[[NSBundle mainBundle]infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSMutableString *userAgent = [NSMutableString stringWithCapacity:1];
    [userAgent appendString:@"jiabiaoyun"];
    [userAgent appendString:@" "];
    [userAgent appendString:appVersion];
    NSString *name = [UIDeviceUtil hardwareDescription];
    [userAgent appendString:@";"];
    [userAgent appendString:@"iOS"];
    [userAgent appendString:@"("];
    [userAgent appendString:name];
    [userAgent appendString:@" "];
    CGFloat version = [[[UIDevice currentDevice] systemVersion] floatValue];
    [userAgent appendFormat:@"%f",version];
    [userAgent appendString:@")"];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
//                          [self buildSignture],@"signature",
                          session,@"Authorization",
//                          userAgent,@"User-Agent",
                          nil];
    return dict;
}

#pragma mark 自定义请求方法

/* 构造AFHTTPRequestSerializer */
- (AFHTTPRequestSerializer *)bulidCustomSerializer {
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    [serializer setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    serializer.timeoutInterval = [self requestTimeoutInterval];
    
    // 构造头参数
    NSDictionary *headerParamers = [self requestHeaderFieldValueDictionary];
    for (NSString *key in headerParamers.allKeys) {
        [serializer setValue:[headerParamers objectForKey:key] forHTTPHeaderField:key];
    }
    return serializer;
}

/* 无网络从缓存取数据*/
- (void)getResponseObjectFromCacheWithSuccess:(responseSuccess)success
                                      failure:(responseFailure)failure {
    NSString *requestMethod = [self.queryDictionary objectForKeyedSubscript:REUQEST_METHOD];
    NSString *cacheKey = [self buildCacheKey];
    
    id responseObject = [[EGOCache globalCache] objectForKey:cacheKey];
    if (responseObject && success) {
        success(requestMethod, responseObject);
    }else{
        failure(nil, responseObject);
    }
    [self.queryDictionary removeAllObjects];
}


/* 请求成功后的处理方法 */
- (void)responseSuccessHandle:(id)responseObject
                      success:(responseSuccess)success
                      failure:(responseFailure)failure {
    NSString *requestMethod = [self.queryDictionary objectForKeyedSubscript:REUQEST_METHOD];
    //判断是否添加缓存
    if (self.isNeedCache) {
        [[EGOCache globalCache] setObject:responseObject forKey:[self buildCacheKey]];
    }
    [self.queryDictionary removeAllObjects];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if ([responseObject isKindOfClass:[NSDictionary class]] && [(NSDictionary *)responseObject objectForKey:@"statusCode"]) {
        NSString *code = [(NSDictionary *)responseObject objectForKey:@"statusCode"];
        if ([code isKindOfClass:[NSNumber class]]) {
            code = code.description;
        }
        if ([code isEqualToString:@"1"]) {
            if (success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    success(requestMethod, responseObject);
                });
            }
        }else{
            if (failure) {
                NSString *errorInfo = [(NSDictionary *)responseObject objectForKey:@"message"];
                //            NSMutableString *list = [NSMutableString stringWithFormat:@"%@%@",requestMethod,errorInfo];
                NSError *error = [NSError errorWithDomain:errorInfo code:[code integerValue] userInfo:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    failure(error, responseObject);
                });
            }
        }
    }else {
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                success(requestMethod, responseObject);
            });
        }
    }
}


/**
 *  URLDecoder
 *
 *  @param str str
 *
 *  @return return value description
 */
- (NSString *)URLDecodedString:(NSString *)str{
//    NSString *decodedString=(__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (__bridge CFStringRef)str, CFSTR(""), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    NSString *decodedString = (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapes(NULL, (__bridge CFStringRef)str, CFSTR(""));
    return decodedString;
}


- (void)requestGETwithUrl:(NSString *)URLString parameters:(NSMutableDictionary *)parameters success:(responseSuccess)success failure:(responseFailure)failure{
    
    dispatch_async(self.requestTaskQueue, ^{
        
        NSMutableDictionary *finalParameters = [NSMutableDictionary dictionary];
        if (parameters != nil) {
            [finalParameters addEntriesFromDictionary:parameters];
        }
        [self addHttpHeader];
        //添加公参
        [self addCommonParamsTo:finalParameters];
        
        NSString *url = self.baseURL != nil ? [NSString stringWithFormat:@"%@%@", self.baseURL.description, URLString] : URLString;
        
        DLog(@"getUrl:%@\nparams:%@", url, finalParameters);
        WS(weakSelf);
        [self GET:url parameters:finalParameters progress:^(NSProgress * _Nonnull uploadProgress) {
            
        }  success:^(NSURLSessionDataTask *task, id responseObject) {
            [weakSelf responseSuccessHandle:responseObject success:success failure:failure];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            DLog(@"\n***** 网络请求失败 *****\nerror=%@", error);
//            NSData *object = (NSData *)[error.userInfo objectForKey:AFNetworkingOperationFailingURLResponseDataErrorKey];
//            //            NSString *errMsg = [[NSString alloc] initWithData:object encoding:NSUTF8StringEncoding];
//            NSMutableDictionary *parames = [NSJSONSerialization JSONObjectWithData:object options:NSJSONReadingMutableLeaves error:nil];
//            if ([parames objectForKey:@"detail"]) {
//                [parames setObject:[parames objectForKey:@"detail"] forKey:kBaseErrorMessage];
//            }
            NSMutableDictionary *parames = [self parserError:error];
            if (failure) {
                failure(error,parames);
            }
            
        }];
        
    });
    
}

- (void)requestPOSTwithUrl:(NSString *)URLString parameters:(NSMutableDictionary *)parameters success:(responseSuccess)success failure:(responseFailure)failure{
    dispatch_async(self.requestTaskQueue, ^{
        NSMutableDictionary *finalParameters = [NSMutableDictionary dictionary];
        if (parameters != nil) {
            [finalParameters addEntriesFromDictionary:parameters];
        }
        [self addHttpHeader];
        [self addCommonParamsTo:finalParameters]; //添加公参

        NSString *url = self.baseURL != nil ? [NSString stringWithFormat:@"%@%@", self.baseURL.description, URLString] : URLString;
        DLog(@"url = %@\nparams = %@", url, finalParameters);
        WS(weakSelf);
        [self POST:url parameters:finalParameters progress:^(NSProgress * _Nonnull uploadProgress) {
            
        }  success:^(NSURLSessionDataTask *task, id responseObject) {
            [weakSelf responseSuccessHandle:responseObject success:success failure:failure];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            DLog(@"%@",[error.userInfo objectForKey:AFNetworkingOperationFailingURLResponseDataErrorKey]);
            NSMutableDictionary *parames = [self parserError:error];
            if (failure) {
                failure(error,parames);
            }
        }];
    });
}

- (void)requestPOSTJSONWithUrl:(NSString *)URLString parameters:(NSMutableDictionary  *)parameters success:(responseSuccess)success failure:(responseFailure)failure{
    
    dispatch_async(self.requestTaskQueue, ^{
//        [self addHttpHeader];
        
        NSString *finalJsonString = [parameters yy_modelToJSONString];
        
        NSString *url = self.baseURL != nil ? [NSString stringWithFormat:@"%@%@", self.baseURL.description, URLString] : URLString;
        DLog(@"\n***** 网络请求开始 *****\nrequestURL=%@\nparams:%@\n\n", url, finalJsonString);
        DLog(@"%@",self.requestSerializer.HTTPRequestHeaders);
        
        
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializerWithWritingOptions:0] requestWithMethod:@"POST" URLString:url parameters:nil error:nil];

        
        req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
        [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [req setHTTPBody:[finalJsonString dataUsingEncoding:NSUTF8StringEncoding]];
        [req setAllHTTPHeaderFields:[self requestHeaderFieldValueDictionary]];
        
        [[self dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            
            if (!error) {
                NSLog(@"Reply JSON: %@", responseObject);
                [self responseSuccessHandle:responseObject success:success failure:failure];
            } else {
                NSLog(@"Error: %@, %@, %@", error, response, responseObject);
                NSDictionary *parames = [self parserError:error];
                DLog(@"%@",parames);
                if (failure) {
                    failure(error,[self parserError:error]);
                }
            }
        }] resume];
        
    });
    
}



- (void)requestUpLoadFileWithURLString:(NSString *)URLString
                            images:(NSArray *)images
                            parameters:(NSMutableDictionary *)parameters
                              filePath:(NSString *)filePath
                               success:(responseSuccess)success
                                failed:(responseFailure)failed {
    dispatch_async(self.requestTaskQueue, ^{

        NSMutableDictionary *finalParameters = [[NSMutableDictionary alloc] init];
        if (parameters) {
            [finalParameters addEntriesFromDictionary:parameters];
        }
        [self addHttpHeader];
        //添加公参
        [self addCommonParamsTo:finalParameters];

        NSString *url = self.baseURL != nil ? [NSString stringWithFormat:@"%@%@", self.baseURL.description, URLString] : URLString;
        DLog(@"\n***** 开始上传图片 *****\nrequestURL=%@\nparams:%@\n\n", url, finalParameters);
        [self POST:url parameters:finalParameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            for (NSInteger i=0; i<images.count; i++) {
                UIImage * image = images[i];
                NSDate *date = [NSDate date];
                NSDateFormatter *formormat = [[NSDateFormatter alloc]init];
                [formormat setDateFormat:@"HHmmss"];
                NSString *dateString = [formormat stringFromDate:date];
                NSString *fileName = [NSString  stringWithFormat:@"%@.jpg",dateString];
                NSData *imageData = UIImageJPEGRepresentation(image, 0.3);
                DLog(@"图片大小 = %lu",imageData.length/1024);
                [formData  appendPartWithFileData:imageData name:@"background" fileName:fileName mimeType:@"image/jpg/png/jpeg"];
            }
        } progress:^(NSProgress * _Nonnull uploadProgress) {

        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self responseSuccessHandle:responseObject success:success failure:failed];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSMutableDictionary *parames = [self parserError:error];
            if (failed) {
                failed(error,parames);
            }
        }];
    });
}

- (NSMutableDictionary *)parserError:(NSError *)error {
    NSMutableDictionary *parames = nil;
    if ([error.userInfo objectForKey:AFNetworkingOperationFailingURLResponseDataErrorKey]) {
        id object = [error.userInfo objectForKey:AFNetworkingOperationFailingURLResponseDataErrorKey];
        if ([object isKindOfClass:[NSData class]]) {
            parames = [NSJSONSerialization JSONObjectWithData:object options:NSJSONReadingAllowFragments error:nil];
        }else if ([object isKindOfClass:[NSError class]]) {
            [self parserError:object];
        }else {
            [parames setObject:@"网络请求失败" forKey:kBaseErrorMessage];
        }
    }
    if ([error.userInfo objectForKey:@"NSUnderlyingError"]) {
        id object = [error.userInfo objectForKey:@"NSUnderlyingError"];
        if ([object isKindOfClass:[NSData class]]) {
            parames = [NSJSONSerialization JSONObjectWithData:object options:NSJSONReadingMutableLeaves error:nil];
        }else if ([object isKindOfClass:[NSError class]]) {
            NSError *err = (NSError *)object;
            DLog(@"%@",err.domain);
            if ([err.userInfo objectForKey:AFNetworkingOperationFailingURLResponseDataErrorKey]) {
                DLog(@"%@",[err.userInfo objectForKey:AFNetworkingOperationFailingURLResponseDataErrorKey]);
                id xxx = [err.userInfo objectForKey:AFNetworkingOperationFailingURLResponseDataErrorKey];
                if ([xxx isKindOfClass:[NSData class]]) {
                    NSString *dataString = [[NSString alloc] initWithData:xxx encoding:NSUTF8StringEncoding];
                    DLog(@"%@",dataString);
                    [parames setObject:dataString forKey:kBaseErrorMessage];
                }
            }
        }else {
            [parames setObject:@"网络请求失败" forKey:kBaseErrorMessage];
        }
    }
    return parames;
}


/**
 设置请求参数头
 */
- (void)addHttpHeader {
    NSDictionary *headerParamers = [self requestHeaderFieldValueDictionary];
    for (NSString *key in headerParamers.allKeys) {
        [self.requestSerializer setValue:[headerParamers objectForKey:key] forHTTPHeaderField:key];
    }
}

/**
 *  为网络请求params添加公参
 *
 *  @param params 网络请求params
 */
- (void)addCommonParamsTo:(NSMutableDictionary*)params {
    [params setObject:self.gdm.accessToken == nil ? @"" : self.gdm.accessToken forKey:@"accessToken"]; //用户token
    //这是假数据token，需要改掉
//    [params setObject:@"2d59f4daaf4ad490c2c1f4822ed1ee026adfd857" forKey:@"token"];
    [params setObject:@"1.0" forKey:@"vc"]; //版本号
//    [params setObject:self.gdm.channel == nil ? @"" : self.gdm.channel forKey:@"channel"]; //渠道
    [params setObject:[UIDeviceUtil hardwareString] forKey:@"pla"]; //手机型号
}

- (NSString*)getVerifyCodeByArray:(NSArray*)array {
    NSMutableString *buff = [[NSMutableString alloc] initWithCapacity:32];
    
    for (NSString *str in array) {
        [buff appendString:str];
    }
    
    NSString *result = [buff stringByReplacingOccurrencesOfString:@" " withString:@""];;
    
    DLog(@"加密前：%@", result);

    return [[result MD5] SHA1];
}

//上传图片
- (void)uploadImage:(NSArray *)images
             params:(NSDictionary *)params
                url:(NSString *)url
           progress:(void (^)(NSProgress *progress))progress
            success:(responseSuccess)success
            failure:(responseFailure)failure {
    dispatch_async(self.requestTaskQueue, ^{
        
        NSMutableDictionary *finalParameters = [[NSMutableDictionary alloc] init];
        if (params) {
            [finalParameters addEntriesFromDictionary:params];
        }
        [self addHttpHeader];
        //添加公参
        [self addCommonParamsTo:finalParameters];
        
        NSString *urlStr = self.baseURL != nil ? [NSString stringWithFormat:@"%@%@", self.baseURL.description, url] : url;
        DLog(@"url : %@\nparams:%@", url, finalParameters);
        [self POST:urlStr parameters:finalParameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            for (NSInteger i=0; i<images.count; i++) {
                UIImage * image = images[i];
                NSDate *date = [NSDate date];
                NSDateFormatter *formormat = [[NSDateFormatter alloc]init];
                [formormat setDateFormat:@"HHmmss"];
                NSString *dateString = [formormat stringFromDate:date];
                NSString *fileName = [NSString  stringWithFormat:@"%@.jpg",dateString];
                NSData *imageData = UIImageJPEGRepresentation(image, 0.6);
                DLog(@"图片大小 = %lu",imageData.length/1024);
                [formData  appendPartWithFileData:imageData name:@"photos" fileName:fileName mimeType:@"image/jpg/png/jpeg"];
            }
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            DLog(@"%@",progress);
            if (progress) {
                progress(uploadProgress);
            }
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self responseSuccessHandle:responseObject success:success failure:failure];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSMutableDictionary *parames = [self parserError:error];
            if (failure) {
                failure(error,parames);
            }
        }];
    });
}

@end
