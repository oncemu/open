//
//  DLNetworkModel.h
//  lehoMagPad
//
//  Created by jiang on 12-5-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface NetworkModel : NSObject {
    NetworkStatus networkStatus;
}
@property(nonatomic, assign) NetworkStatus networkStatus;
@end
