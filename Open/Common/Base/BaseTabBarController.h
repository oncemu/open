//
//  QXBaseTabBarController.h
//  ShownailSeller
//
//  Created by onceMu on 15/11/6.
//  Copyright © 2015年 shownail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CYLTabBarController.h"


@interface BaseTabBarController : NSObject

/** 根视图控制器 */
@property (nonatomic, strong) CYLTabBarController *tabBarController;

@end
