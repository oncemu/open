//
//  PicBuyView.h
//  Open
//
//  Created by mfp on 17/7/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"

@interface PicBuyView : UIView

@end

@interface PayToolBar : UIView

@property(nonatomic, strong) id object;

@end

@interface PicBuyHeader : UIView
@property(nonatomic, strong) SDCycleScrollView *cycleScrollView;
@property(nonatomic, strong) NSMutableArray *listArray;

@end

//产品细节
@interface ProductPicCell : UITableViewCell

@end

//购买详细
@interface ProductInfoCell : UITableViewCell

@end
