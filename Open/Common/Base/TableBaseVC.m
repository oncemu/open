//
//  TableBaseVC.m
//  taoshouyouiphone
//
//  Created by jianghaibo on 14-4-20.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import "TableBaseVC.h"
#import "CoreMacros.h"
#import "UIView+Helpers.h"


@interface TableBaseVC ()

@end

@implementation TableBaseVC

#pragma mark - Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _dataList = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

- (void)dealloc {

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (id)initWithShowBackButton:(BOOL)isShowBack showHeaderRefresh:(BOOL)isShowHeader showFooterRefresh:(BOOL)isShowFooter {
    self =[super initWithShowBackButton:isShowBack];
    if (self) {
        _dataList = [[NSMutableArray alloc]initWithCapacity:0];
        _isShowHeader = isShowHeader;
        _isShowFooter = isShowFooter;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionFooterHeight = 0;
    self.tableView.sectionHeaderHeight = 0;
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.tableView];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }

    if (_isShowHeader) {
        self.header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        self.tableView.mj_header = self.header;
        self.header.lastUpdatedTimeLabel.hidden = YES;
//        [self.header setTitle:@"加载数据" forState:MJRefreshStateIdle];
        [self startRefresh];
    }
    if (_isShowFooter) {
        self.footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
//        self.footer.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark --LoadData
- (void)startRefresh {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.header beginRefreshing];
    });
}

- (void)loadData {
    
}

- (void)loadMoreData {
    
}

- (void)loadDataFinishWithHeader:(BOOL)isHeaderRefresh withData:(NSArray *)data {
    if (_header) {
        [_header endRefreshing];
    }
    if (_footer) {
        [_footer endRefreshing];
        _footer.hidden = (data.count < 10);
    }
    if (isHeaderRefresh) {
        if (data.count != 0) {
            self.tableView.mj_footer = self.footer;
        }
        self.noDataButton.hidden = (data.count > 0);
    }else{
        if ([data count]==0) {
            [_footer endRefreshingWithNoMoreData];
        }
    }
    [self.tableView reloadData];
}

- (void)reloadData {
    [self.header beginRefreshing];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}


@end
