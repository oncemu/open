//
//  BaseLandscapeVC.m
//  Open
//
//  Created by mfp on 17/10/14.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "BaseLandscapeVC.h"

@interface BaseLandscapeVC ()

@end

@implementation BaseLandscapeVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self switchToLandscape];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    [self switchToLandscape];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//支持的方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeRight;
}

//是否可以旋转
- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return  UIInterfaceOrientationLandscapeRight;
}

//点击按钮旋转到横屏
- (void)switchToLandscape {
    [[UIDevice currentDevice] setValue:@(UIDeviceOrientationUnknown) forKey:@"orientation"];
    [[UIDevice currentDevice] setValue:@(UIDeviceOrientationLandscapeRight) forKey:@"orientation"];
}

@end
