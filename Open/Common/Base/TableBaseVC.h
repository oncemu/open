//
//  TableBaseVC.h
//  taoshouyouiphone
//
//  Created by jianghaibo on 14-4-20.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//  Table 基类，提供数据加载的方法

#import "BaseVC.h"
#import <MJRefresh.h>

@interface TableBaseVC : BaseVC<UITableViewDataSource,UITableViewDelegate>

// 自定义tableview
@property (nonatomic,strong) UITableView *tableView;
// 数据列表
@property (nonatomic,strong) NSMutableArray *dataList;
// header
@property (nonatomic,strong) MJRefreshNormalHeader *header;
//footer
@property (nonatomic,strong) MJRefreshAutoNormalFooter *footer;
//是否显示header
@property (nonatomic,assign) BOOL isShowHeader;
//是否显示footer
@property (nonatomic,assign) BOOL isShowFooter;


/**
 *  调用此方法，集成上拉下拉加载功能
 *
 *  @param isShowBack   是否显示返回按钮
 *  @param isShowHeader 是否集成下拉刷新
 *  @param isShowFooter 是否集成上拉加载
 *
 *  @return id object
 */
- (instancetype)initWithShowBackButton:(BOOL)isShowBack showHeaderRefresh:(BOOL)isShowHeader showFooterRefresh:(BOOL)isShowFooter;
/**
 *  下拉刷新数据
 */
- (void)loadData;
/**
 *  上拉加载数据
 */
- (void)loadMoreData;
/**
 *  下拉刷新动画
 */
- (void)startRefresh;

- (void)loadDataFinishWithHeader:(BOOL)isHeaderRefresh withData:(NSArray *)data;
@end
