//
//  BaseVC.m
//  taoshouyouiphone
//
//  Created by jianghaibo on 14-4-20.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//

#import "BaseVC.h"
#import "SRAlertView.h"
#import "CodeLoginVC.h"
#import "HomeNetService.h"

@interface BaseVC ()
@property(nonatomic, strong) HomeNetService *homeService;
@end

@implementation BaseVC

- (id)init {
    if (self) {
        if (!_gdm) {
            _gdm = [GlobalDataModel sharedInstance];
        }
        _isFirstLoadData = YES;
        _appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if (!_gdm) {
            _gdm = [GlobalDataModel sharedInstance];
        }
        _isFirstLoadData = YES;
        _appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!_gdm) {
        _gdm = [GlobalDataModel sharedInstance];
    }
    _appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    self.view.autoresizingMask = UIViewAutoresizingNone;
    self.view.backgroundColor = [UIColor colorWithHex:0xeeeeee];
    // 设置导航栏样式
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithHex:0xf8f8f8]];
    // 设置UINavigationBar为半透明
    [self.navigationController.navigationBar setTranslucent:YES];
    // 设置状态栏的字体大小和颜色
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          kStatusbarColor, NSForegroundColorAttributeName,
                                                          [UIFont fontWithName:kCommonEnFontName size:18.0f], NSFontAttributeName,
                                                          nil]];
    
    //自定义返回上级手势
    self.navigationController.fd_interactivePopDisabled = YES;
    if (self.isShowBackButton) {
        //自定义返回按钮
        /* 此方法有bug，需要单独自定义，然后单独写返回手势
        UIImage *backButtonImage = [[UIImage imageNamed:@"back"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 30, 0, 0)];
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        //将返回按钮的文字position设置不在屏幕上显示
        [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(NSIntegerMin, NSIntegerMin) forBarMetrics:UIBarMetricsDefault];
         */


        //当从a controller push 到 b controller 的时候，系统遵循下面的规则
        //1.a 自定义了BackBarButtonItem ，则b会显示a 的返回按钮
        //2.b 自定义了LeftBarButtonItem ，则b会显示a 的返回按钮
        //3.a 没有定义BackBarButtonItem，b没有定义LeftBarButtonItem，则b会显示系统自带的返回按钮
        UIBarButtonItem *item = [self getBarButtonItemWithImage:[UIImage imageNamed:@"base_back"] title:nil action:@selector(goBack)];
        self.navigationItem.leftBarButtonItem = item;
        item.tintColor = [UIColor colorWithHex:0xf3df01];
    }else{
        self.navigationItem.hidesBackButton = YES;
    }
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSucceed) name:kUserLoginSuccessNotificationName object:nil];
}

#pragma mark - 点击空白区域自动收起键盘
- (void)fingerTapped:(UITapGestureRecognizer *)gestureRecognizer{
    [self.view endEditing:YES];//点击空白区域自动收起键盘
}

#pragma mark public method
- (id)initWithShowBackButton:(BOOL)isShowBackButton {
    _isShowBackButton = isShowBackButton;
    self =[self initWithNibName:nil bundle:nil];
    if (self) {
    }
    return self;
}

- (void)setBarButtonItemWithImage:(UIImage *)image title:(NSString *)title  isLeft:(BOOL)isLeft {//设置导航两侧按钮，isLeft:是左侧还是右侧
    UIBarButtonItem *barButtonItem = nil;
    if (isLeft) {
        barButtonItem = [self getBarButtonItemWithImage:image title:title action:@selector(leftButtonItemAction)];
        self.navigationItem.leftBarButtonItem = barButtonItem;
    }else {
        barButtonItem = [self getBarButtonItemWithImage:image title:title action:@selector(rightButtonItemAction)];
        self.navigationItem.rightBarButtonItem = barButtonItem;
    }
}

- (UIBarButtonItem *)getBarButtonItemWithImage:(UIImage *)image title:(NSString *)title action:(nullable SEL)action {
    UIBarButtonItem *barButtonItem = nil;
    if (image) {
        barButtonItem = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:action];
    }
    if (title && title.length > 0) {
        barButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self action:action];
        barButtonItem.tintColor = ColorWithHex(0x666666);
    }
    return barButtonItem;
}

- (void)leftButtonItemAction {}
- (void)rightButtonItemAction {}

- (void)registAutoFoldUpKeyBoard {
    //点击空白区域自动收起键盘
    self.view.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fingerTapped:)];
    [self.view addGestureRecognizer:singleTap];
}

- (void)pushVC:(UIViewController *)viewController
      animated:(BOOL)animated {
    viewController.hidesBottomBarWhenPushed = YES;
    if (self.parentVC) {
        [self.parentVC.navigationController pushViewController:viewController animated:animated];
    }else{
        [self.navigationController pushViewController:viewController animated:animated];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [segue.destinationViewController setHidesBottomBarWhenPushed:YES];
}

#pragma mark 状态栏适配方法
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

#pragma mark - Public

/**
 *  网络判断方法
 *  @return YES为有网状态
 */
- (BOOL)checkNetWork {
    NetworkStatus networkStatus = self.gdm.networkModel.networkStatus;
    if (networkStatus == NotReachable) {
        return NO;
    }
    return YES;
}

//show 是否弹出登录框
- (BOOL)checkLogin:(BOOL)show {
    if (show && !self.gdm.isLogin) {
        [self showLoginVC];
    }
    return self.gdm.isLogin;
}

//登录成功方法，只在子类需要的地方实现
- (void)loginSucceed {}

- (void)goBack {
    if (self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)reloadData {}
#pragma mark 跳转到登录界面
-(void)showLoginViewControllerWithCompletion {
    CodeLoginVC *login = [[CodeLoginVC alloc]initWithShowBackButton:NO];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:login];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)startQQWithQQNumber:(NSString*)qq{
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"mqq://im/chat?chat_type=wpa&uin=%@&version=1&src_type=web", qq]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    [self.view addSubview:webView];
}

- (void)showLoginVC {
    [self showLoginViewControllerWithCompletion];
}

- (void)alertLoginAndRegVC {

}

- (void)showNoNetWorkStauts {
   
}

- (UIButton *)noDataButton {
    if (!_noDataButton) {
        _noDataButton = [ViewFactory getButton:CM(50, 120, SCREEN_WIDTH - 100, SCREEN_WIDTH - 100) font:Font(16) selectTitle:@"没有数据" norTitle:@"没有数据" sColor:ColorWithHex(0x999999) norColor:ColorWithHex(0x999999) target:self action:@selector(clickNoData)];
        [self.view addSubview:_noDataButton];
    }
    return _noDataButton;
}

- (void)clickNoData {
    self.noDataButton.hidden = YES;
    if (self.noDataAction) {
        self.noDataAction();
    }
}

- (void)followUser:(NSInteger)userid like:(BOOL)like success:(void(^)())success failed:(void(^)())failed {
    [self.homeService usersLikes:userid like:like success:^(NSString *requestInfo, id responseObject) {
        success();
    } failure:^(NSError *error, id responseObject) {
        failed();
    }];
}

- (HomeNetService *)homeService {
    if (!_homeService) {
        _homeService = [[HomeNetService alloc] init];
    }
    return _homeService;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

//是否可以旋转
- (BOOL)shouldAutorotate
{
    return YES;
}
//支持的方向
-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return  UIInterfaceOrientationPortrait;
}
@end
