//
//  QXCollectionBaseVC.h
//  ShownailSeller
//
//  Created by onceMu on 15/11/6.
//  Copyright © 2015年 shownail. All rights reserved.
//

#import "BaseVC.h"
#import <MJRefresh/MJRefresh.h>

@interface CollectionBaseVC : BaseVC<UICollectionViewDataSource,UICollectionViewDelegate>

// 自定义
@property (nonatomic, strong) UICollectionView *collectionView;
// 数据列表
@property (nonatomic, strong) NSMutableArray *dataList;
/** 刷新组件 */
@property (nonatomic, strong) MJRefreshNormalHeader *header;
/** 刷新组件 */
@property (nonatomic, strong) MJRefreshAutoStateFooter *footer;
/** 空白提示 本地化对应的key */
@property (nonatomic, copy) NSString *emptyText;

/**
 *  调用此方法，集成上拉下拉加载功能
 *
 *  @param isShowBack   是否显示返回按钮
 *  @param isShowHeader 是否集成下拉刷新
 *  @param isShowFooter 是否集成上拉加载
 *
 *  @return id object
 */
- (id)initWithShowBackButton:(BOOL)isShowBack showHeaderRefresh:(BOOL)isShowHeader showFooterRefresh:(BOOL)isShowFooter;

/**
 *  下拉加载开始刷新
 */
- (void)startRefresh;
/**
 *  开始加载数据
 */
- (void)loadData;
/**
 *  上拉加载更多数据
 */
- (void)loadMoreData;

/**
 *  改变刷新组件状态
 */
- (void)changeRefreshStatus:(BOOL)isHeader
                       data:(NSArray *)data;

@end
