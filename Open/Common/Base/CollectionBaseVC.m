//
//  QXCollectionBaseVC.m
//  ShownailSeller
//
//  Created by onceMu on 15/11/6.
//  Copyright © 2015年 shownail. All rights reserved.
//

#import "CollectionBaseVC.h"
#import <UIScrollView+EmptyDataSet.h>


@interface CollectionBaseVC ()<DZNEmptyDataSetDelegate,DZNEmptyDataSetSource>

/** 是否显示header */
@property (nonatomic, assign) BOOL isShowHeader;
/** 是否显示footer */
@property (nonatomic, assign) BOOL isShowFooter;

@end

@implementation CollectionBaseVC

#pragma mark - Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _dataList = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

- (void)dealloc {
    
}

- (id)initWithShowBackButton:(BOOL)isShowBack showHeaderRefresh:(BOOL)isShowHeader showFooterRefresh:(BOOL)isShowFooter {
    self =[super initWithShowBackButton:isShowBack];
    if (self) {
        _isShowHeader = isShowHeader;
        _isShowFooter = isShowFooter;
        _dataList = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UICollectionViewLayout *layout = [[UICollectionViewLayout alloc]init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.collectionView];
    if (_isShowHeader) {
        self.header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        self.collectionView.mj_header = self.header;
        [self startRefresh];
    }
    if (_isShowFooter) {
        self.footer = [MJRefreshAutoStateFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
        self.collectionView.mj_footer = self.footer;
        self.footer.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark --Publick Method
- (void)loadData {
    
}

- (void)loadMoreData {
    if (self.footer) {
        [self.footer beginRefreshing];
    }
}

- (void)startRefresh {
    if (self.header) {
        [self.header beginRefreshing];
    }
}

- (void)changeRefreshStatus:(BOOL)isHeader
                       data:(NSArray *)data {
    [self.collectionView reloadData];
    if (isHeader) {
        if (self.header) {
            [self.header endRefreshing];
        }
        if (self.footer) {
            self.noDataButton.hidden = !(data.count == 0);
            self.footer.hidden = (data.count < 10);
        }
    }else{
        if (self.footer) {
            [self.footer endRefreshing];
            self.footer.hidden = (data.count < 10);
            if (data.count < 10) {
                [self.footer endRefreshingWithNoMoreData];
            }
        }
    }
}

- (void)reloadData {
    [self.header beginRefreshing];
}

#pragma mark --UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}




@end
