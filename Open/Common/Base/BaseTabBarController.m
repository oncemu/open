//
//  QXBaseTabBarController.m
//  ShownailSeller
//
//  Created by onceMu on 15/11/6.
//  Copyright © 2015年 shownail. All rights reserved.
//

#import "BaseTabBarController.h"
#import "HomeVC.h"
#import "MallVC.h"
#import "UserCenterVC.h"
#import "TravelVC.h"
#import "PhotoVC.h"
#import "CYLPlusButtonSubclass.h"


@interface BaseTabBarController ()

@end

@implementation BaseTabBarController

/**
 *  lazy load
 *
 *  @return CYLTabBarController
 */
- (CYLTabBarController *)tabBarController
{
    if (_tabBarController == nil) {
        HomeVC *homeVC = [[HomeVC alloc] initWithShowBackButton:NO];
        CustomNavigationController *homeNav = [[CustomNavigationController alloc]initWithRootViewController:homeVC];
        
        PhotoVC *photoVC = [[PhotoVC alloc] initWithShowBackButton:NO];
        CustomNavigationController *photoNav = [[CustomNavigationController alloc]initWithRootViewController:photoVC];
        
        MallVC *mallVC = [[MallVC alloc] initWithShowBackButton:NO];
        CustomNavigationController *mallNav = [[CustomNavigationController alloc]initWithRootViewController:mallVC];
        
//        TravelVC *travelVC = [[TravelVC alloc]initWithShowBackButton:NO];
//        CustomNavigationController *travelNav = [[CustomNavigationController alloc]initWithRootViewController:travelVC];
        
        UserCenterVC *userVC = [[UserCenterVC alloc]initWithShowBackButton:NO];
        RTRootNavigationController *userNav = [[RTRootNavigationController alloc]initWithRootViewController:userVC];
        
        UIEdgeInsets imageInsets = UIEdgeInsetsZero;//UIEdgeInsetsMake(4.5, 0, -4.5, 0);
        UIOffset titlePositionAdjustment = UIOffsetZero;//UIOffsetMake(0, MAXFLOAT);
        
        CYLTabBarController *tabBarController = [CYLTabBarController tabBarControllerWithViewControllers:@[homeNav,photoNav,mallNav,userNav] tabBarItemsAttributes:[self tababbarItems] imageInsets:imageInsets titlePositionAdjustment:titlePositionAdjustment];
        
//        CYLTabBarController *tabBarController = [[CYLTabBarController alloc]init];
//        [self setTabbarItems:tabBarController];
//        [tabBarController setViewControllers:@[homeNav,photoNav,travelNav,userNav]];
        [[self class] customizeTabBarAppearance];
        _tabBarController = tabBarController;
    }
    return _tabBarController;
}

- (NSArray *)tababbarItems {
    NSDictionary *home = @{
                           CYLTabBarItemTitle : @"",
                           CYLTabBarItemImage : @"home-unselected",
                           CYLTabBarItemSelectedImage : @"home-selected"
                           };
    NSDictionary *photo = @{
                               CYLTabBarItemTitle:
                                   @"",
                               CYLTabBarItemImage:@"photo-unselected",
                               CYLTabBarItemSelectedImage:@"photo-selected"
                               };
//    NSDictionary *mall = @{
//                              CYLTabBarItemTitle : @"",
//                              CYLTabBarItemImage : @"mall-selected",
//                              CYLTabBarItemSelectedImage : @"mall-selected"
//                              };
    NSDictionary *travel = @{
                             CYLTabBarItemTitle : @"",
                             CYLTabBarItemImage : @"album-unselected",
                             CYLTabBarItemSelectedImage : @"album-selected"
                             };
    NSDictionary *userCenter = @{
                           CYLTabBarItemTitle : @"",
                           CYLTabBarItemImage : @"user-unselected",
                           CYLTabBarItemSelectedImage : @"user-selected"
                           };
    return @[home,photo,travel,userCenter];
}


+ (void)customizeTabBarAppearance {
    
    //去除 TabBar 自带的顶部阴影
//    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
//    [[UITabBar appearance]setBackgroundColor:[UIColor clearColor]];
    // set the text color for unselected state
    // 普通状态下的文字属性
    NSMutableDictionary *normalAttrs = [NSMutableDictionary dictionary];
    normalAttrs[NSForegroundColorAttributeName] = [UIColor colorWithHex:0x131516];
    
    // set the text color for selected state
    // 选中状态下的文字属性
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSForegroundColorAttributeName] = [UIColor colorWithHex:0x34b5ff];
    
    // set the text Attributes
    // 设置文字属性
    UITabBarItem *tabBar = [UITabBarItem appearance];
    [tabBar setTitleTextAttributes:normalAttrs forState:UIControlStateNormal];
    [tabBar setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
//
//    // Set the dark color to selected tab (the dimmed background)
//    // TabBarItem选中后的背景颜色
//    [[UITabBar appearance] setSelectionIndicatorImage:[self imageFromColor:[UIColor colorWithRed:26/255.0 green:163/255.0 blue:133/255.0 alpha:1] forSize:CGSizeMake([UIScreen mainScreen].bounds.size.width/5.0f, 49) withCornerRadius:0]];
//    
//    // set the bar background color
//    // 设置背景图片
//    // UITabBar *tabBarAppearance = [UITabBar appearance];
//    // [tabBarAppearance setBackgroundImage:[UIImage imageNamed:@"tabbar_background_ios7"]];
}

@end
