//
//  PicBuyView.m
//  Open
//
//  Created by mfp on 17/7/17.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "PicBuyView.h"

@implementation PicBuyView


@end


@interface PayToolBar ()

@property(nonatomic, strong) UIImageView *dotView;
@property(nonatomic, strong) UILabel *nameLabel;
@property(nonatomic, strong) UILabel *priceLabel;
@property(nonatomic, strong) UIButton *payBtn;

@end

@implementation PayToolBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = ColorWithHex(0xeeeeee);
        self.dotView = [ViewFactory getImageView:CGRectZero image:getImage(@"")];
        self.dotView.backgroundColor = LemonYellow;
        self.nameLabel = [ViewFactory getLabel:CGRectZero font:Font(14) textColor:ColorWithHex(0x666666) text:nil];
        self.nameLabel.backgroundColor = ColorWithHex(0xeeeeee);
        self.priceLabel = [ViewFactory getLabel:CGRectZero font:Font(10) textColor:ColorWithHex(0x666666) text:nil];
        self.priceLabel.backgroundColor = ColorWithHex(0xeeeeee);
        self.payBtn = [ViewFactory getButton:CGRectZero font:Font(15) selectTitle:@"结算" norTitle:@"结算" sColor:ColorWithHex(0x666666) norColor:ColorWithHex(0x666666) target:self action:@selector(pay)];
        self.payBtn.backgroundColor = ColorWithHex(0xffe401);
        [self addSubview:self.dotView];
        [self addSubview:self.nameLabel];
        [self addSubview:self.priceLabel];
        [self addSubview:self.payBtn];
    }
    return self;
}

- (void)setObject:(id)object {
    if (_object != object) {
        _object = object;
    }
    self.nameLabel.text = @"冲印相片";
    self.priceLabel.text = @"¥223.00";
}

- (void)pay {
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.dotView.frame = CM(17, (self.frameSizeHeight - 10)/2.0f, 10, 10);
    self.nameLabel.frame = CM(CGRectGetMaxX(self.dotView.frame) + 17, self.frameSizeHeight/2.0f - 15, self.frameSizeWidth - 80 - 40, 15);
    self.priceLabel.frame = CM(self.nameLabel.frameOriginX, CGRectGetMaxY(self.nameLabel.frame) + 4, self.nameLabel.frameSizeWidth, 11);
    self.payBtn.frame = CM(self.frameSizeWidth - 70, 0, 70, self.frameSizeHeight);
}

@end

@implementation PicBuyHeader

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CM(0, 0, frame.size.width, frame.size.height) imageNamesGroup:@[]];
        self.cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
        [self addSubview:self.cycleScrollView];
    }
    return self;
}

- (void)setListArray:(NSMutableArray *)listArray {
    _listArray = listArray;
    self.cycleScrollView.imageURLStringsGroup = listArray;
}

@end


@interface ProductPicCell ()

@property(nonatomic, strong) UILabel *headLabel;
@property(nonatomic, strong) UICollectionView *Collection;

@end

@implementation ProductPicCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

@end

@interface ProductInfoCell ()

@property(nonatomic, strong) UILabel *headLabel;

@end

@implementation ProductInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

@end

