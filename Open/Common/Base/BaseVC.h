//
//  BaseVC.h
//  taoshouyouiphone
//
//  Created by jianghaibo on 14-4-20.
//  Copyright (c) 2014年 jianghaibo. All rights reserved.
//  基类（处理包括网络提示，数据加载方法，友盟统计，用户登录相关方法

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "GlobalDataModel.h"
#import "UIColor+Hex.h"
#import <KVNProgress/KVNProgress.h>
#import "BaseNetService.h"
#import "NSStringAdditions.h"
#import "UIView+Helpers.h"
#import "UINavigationController+FDFullscreenPopGesture.h"


@interface BaseVC : UIViewController

@property (nonatomic, retain) AppDelegate *appDelegate;

/** 上级页面 */
@property (nonatomic,assign) BaseVC *parentVC;
/** 全局参数 */
@property (nonatomic,strong) GlobalDataModel *gdm;
/** 是否第一次加载数据 */
@property (nonatomic,assign) BOOL isFirstLoadData;

@property (nonatomic,assign) BOOL isShowBackButton;

@property(nonatomic, strong) UIButton *noDataButton;
@property(nonatomic, copy) void (^noDataAction)();



/**
 *  自定义初始化方法
 *  @param isShowBackButton 是否显示返回按钮
 *  @return object
 */
- (id)initWithShowBackButton:(BOOL)isShowBackButton;

/**
 *  注册点击空白区域自动收起键盘
 */
-(void)registAutoFoldUpKeyBoard;

/**
 *  返回方法
 */
- (void)goBack;

/**
 *  跳转方法
 *
 *  @param viewController 跳转controller
 *  @param animated       是否需要动画
 */
- (void)pushVC:(UIViewController *)viewController
      animated:(BOOL)animated;

/**
 *  调用登录界面
 *
 *  
 */
-(void)showLoginViewControllerWithCompletion;


/**
 *  呼起QQ聊天
 *
 *  @param qq qq号码
 */
-(void)startQQWithQQNumber:(NSString*)qq;

/**
 *  判断用户是否有网络
 *
 *  @return return value description
 */
- (BOOL)checkNetWork;

- (BOOL)checkLogin:(BOOL)show;//show 是否弹出登录框

- (void)loginSucceed;//登录成功方法，只在子类需要的地方实现 省的到处写通知

- (void)alertLoginAndRegVC;

//显示没有网络提示
- (void)showNoNetWorkStauts;

- (void)setBarButtonItemWithImage:(UIImage *)image title:(NSString *)title  isLeft:(BOOL)isLeft;//设置导航两侧按钮，isLeft:是左侧还是右侧
- (void)leftButtonItemAction;
- (void)rightButtonItemAction;

- (void)reloadData;

//关注某人
- (void)followUser:(NSInteger)userid like:(BOOL)like success:(void(^)())success failed:(void(^)())failed;
@end
