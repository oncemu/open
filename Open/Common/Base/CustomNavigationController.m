//
//  CustomNavigationController.m
//  Open
//
//  Created by mfp on 17/7/14.
//  Copyright © 2017年 彭彬. All rights reserved.
//

#import "CustomNavigationController.h"

@interface CustomNavigationController ()

@end

@implementation CustomNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotate{
    return [self.topViewController shouldAutorotate];

}
//支持的方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return  self.topViewController.preferredInterfaceOrientationForPresentation;
}


@end
